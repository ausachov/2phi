
void plotMeasurements(char* Type="B2chic2X")
{
    gROOT->Reset();
    gStyle->SetOptStat(001);
    gROOT->ProcessLine(".x lhcbStyle.C");
    
    
    switch (Type)
    {
        case "B2chic1X":
        {
            //    CLEO2 0.00435±0.00029±0.00040
            //    BELLE 0.00363±0.00022±0.00034
            //    BABAR 0.00367±0.00035±0.00044
            //    PDG 0.00386±0.00027
            Double_t pdgAv = 0.00386;
            Double_t eRpdgAv = 0.00027;
            
            int nB2Chic1Experiments = 4;
            Double_t B2Chic1 [20]        = {0.00435,0.00363,0.00367,pdgAv,    0,0,0,0,0,0};
            Double_t errStatB2Chic1 [20] = {0.00029,0.00022,0.00035,eRpdgAv,  0,0,0,0,0,0};
            Double_t errSysB2Chic1 [20]  = {0.00040,0.00034,0.00044,0,        0,0,0,0,0,0};
            
            
            
            //    L3  0.019 ±0.007 ±0.001
            //    DELPHI  0.0113+0.0058 −0.0050 ±0.0004
            //    OUR = (2.76 ± 0.59 ± 0.23 ± 0.89) × 10−3,
            
            //    PDG  0.014+-0.004
            //    we average with PDG

            
            Double_t ourChic1 = 0.00276;
            Double_t ourErrStatChic1 = 0.00059;
            Double_t ourErrSysChic1 = TMath::Sqrt(0.00089*0.00089+0.00023*0.00023);
            
            Double_t pdgAv = 0.014;
            Double_t eRpdgAv = 0.004;
            
            int nbHad2Chic1Experiments = 3;
            Double_t bHad2Chic1 [10] = {0.019,0.0113,ourChic1};
            Double_t errStatbHad2Chic1 [10] = {0.007,0.0054,ourErrStatChic1};
            Double_t errSysbHad2Chic1  [10] = {0.0001,0.0004,ourErrSysChic1};
            
            
            for(int i=0;i<nbHad2Chic1Experiments;i++)
            {
                B2Chic1[nB2Chic1Experiments+i]        = bHad2Chic1[i];
                errStatB2Chic1[nB2Chic1Experiments+i] = errStatbHad2Chic1[i];
                errSysB2Chic1[nB2Chic1Experiments+i]  = errSysbHad2Chic1[i];
                
                
               // cout<<bHad2Chic1[i]<<"  "<<errStatbHad2Chic1[i]<<"  "<<errSysbHad2Chic1[i]<<endl;
            }
            
            int points = nB2Chic1Experiments+nbHad2Chic1Experiments;
            
            B2Chic1[points]        = pdgAv;
            errStatB2Chic1[points] = eRpdgAv;
            errSysB2Chic1[points]  = 0;
            
            
            
            Double_t w1 = (1/eRpdgAv)**2;
            Double_t w2 = 1/(ourErrSysChic1**2+ourErrStatChic1**2);
            Double_t x1 = pdgAv;
            Double_t x2 = ourChic1;
            
            Double_t Average = (x1*w1+x2*w2)/(w1+w2);
            Double_t ErrorAverage = TMath::Sqrt(1/(w1+w2));
            
            
            B2Chic1[points+1]        = Average;
            errStatB2Chic1[points+1] = ErrorAverage;
            errSysB2Chic1[points+1]  = 0;
            
            int nTotalPoins = nbHad2Chic1Experiments+nB2Chic1Experiments+1+1;

            break;
        }
        case "B2chic2X":
        {
            //    CLEO2 6.9±3.5±0.3
            //    BELLE 18.0+2.3−2.8±2.6
            //    BABAR 21.0±4.5±3.1
            //    PDG 14 ±4 OUR AVERAGE
            //    OUR = (1.15 ± 0.20 ± 0.07 ± 0.36)*0.001,
            
            
            Double_t pdgAv = 0.0014;
            Double_t eRpdgAv = 0.0004;
            
            
            Double_t ourChic2 = 0.00115;
            Double_t ourErrStatChic2 =  0.00020;
            Double_t ourErrSysChic2 =  TMath::Sqrt(0.00007*0.00007+0.00036*0.00036);
            
            int nB2Chic1Experiments = 5;
            Double_t B2Chic1 [10] = {0.00069,0.0018,0.0021,pdgAv,ourChic2};
            Double_t errStatB2Chic1 [10] = {0.00035,0.00025,0.00045,eRpdgAv,ourErrStatChic2};
            Double_t errSysB2Chic1 [10] = {0.00003,0.00026,0.00031,0,ourErrSysChic2};
            
            int nTotalPoins = nB2Chic1Experiments;
            
            
            break;
        }
            
        default:
            break;
    }
    
    
    
    
    Double_t errTotB2Chic1 [20];
    Double_t errTotbHad2Chic1 [20];
    
    
    
    //nTotalPoins=7;
    
    for(int i=0;i<nTotalPoins;i++)
        errTotB2Chic1[i] = TMath::Sqrt(errStatB2Chic1[i]*errStatB2Chic1[i]+errSysB2Chic1[i]*errSysB2Chic1[i]);

    
    
    Double_t YB2Chic1 [20] = {15,14,13,12,9,8,7,4,3,2};
    Double_t errYB2Chic1 [20] = {0,0,0,0,0,0,0,0,0,0};


    
    TGraphErrors* graphB2Chic1 = new TGraphErrors(nTotalPoins,B2Chic1,YB2Chic1,errTotB2Chic1,errYB2Chic1);
    TGraphErrors* graphB2Chic1Stat = new TGraphErrors(nTotalPoins,B2Chic1,YB2Chic1,errStatB2Chic1,errYB2Chic1);
    
    graphB2Chic1->SetLineColor(kRed);
    graphB2Chic1->SetMarkerSize(0.7);
    graphB2Chic1Stat->SetMarkerSize(0.7);
    

    
    graphB2Chic1->GetYaxis()->SetNdivisions(0);
    graphB2Chic1->GetXaxis()->SetLimits(0,0.0265);

    
    Double_t minX = graphB2Chic1->GetXaxis()->GetXmin();
    Double_t maxX = graphB2Chic1->GetXaxis()->GetXmax();
    
    TLine* Horiz1 = new TLine(minX,10.5,maxX,10.5);
    Horiz1->SetLineColor(kBlack);
    Horiz1->SetLineStyle(kDashed);

//    TLine* Horiz2 = new TLine(minX,5.5,maxX,5.5);
//    Horiz2->SetLineColor(kBlack);
//    Horiz2->SetLineStyle(kDashed);
    
    
    graphB2Chic1->Draw("AP");
    graphB2Chic1Stat->Draw("P");
    Horiz1->Draw("same");
    //Horiz2->Draw("same");
}


void plotTogether()
{
    TCanvas* canv = new TCanvas("canv","canv",800,800);
    canv->Divide(1,2);
    canv->cd(1);
    plotMeasurements("B2chic1X");
    canv->cd(2);
    plotMeasurements("B2chic2X");
}
