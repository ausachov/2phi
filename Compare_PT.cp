#include <TMath.h>




void BranchDrawSplot()
{
  gROOT->Reset();
  //gStyle->SetOptStat(kFALSE);
  
  Double_t range0=2800;
  Double_t range1=4000;
  Int_t nch=120;
  
  TChain * DecayTree=new TChain("DecayTree");
  DecayTree->Add("DoubleSplotAll.root");
  const Int_t NEn=DecayTree->GetEntries();
  Double_t Branch_Value, Branch_Value1, jpsi_pt;
  DecayTree->SetBranchAddress("Jpsi_PT",&jpsi_pt);
    
    
    Double_t Jpsi_m_scaled,NEtac_sw,diPhiWeight,Jpsi_PZ,NBgr_sw;
    
DecayTree->SetBranchAddress("Jpsi_m_scaled",&Jpsi_m_scaled);
DecayTree->SetBranchAddress("NEta_sw",&NEtac_sw);
DecayTree->SetBranchAddress("NBgr_sw",&NBgr_sw);
DecayTree->SetBranchAddress("diphiweight",&diPhiWeight);


  TH1D * m_phiphi_hist = new TH1D("Data","Data" , nch, range0, range1);

  for (Int_t i=0; i<NEn; i++) 
  {
	  DecayTree->GetEntry(i);
      m_phiphi_hist->Fill(Jpsi_m_scaled,NEtac_sw);
      //m_phiphi_hist->Fill(Jpsi_m_scaled,NBgr_sw);
      
  }
    
  
    
  TCanvas *canv1 = new TCanvas("canv1","B_MM",5,85,800,600);
//   m_phiphi_hist->SetAxisRange(5,50);
  m_phiphi_hist->SetXTitle("B_MM");
  m_phiphi_hist->DrawCopy();
  

}
