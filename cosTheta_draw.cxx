#include <TMath.h>
#include <Math.h>


void cosTheta_draw()
{
    gROOT->Reset();
    gStyle->SetOptStat(001);
    
    Double_t Cos1, Cos2;
    Double_t PE1, PE2, jPE1;
    Double_t PX1, PX2, jPX1;
    Double_t PY1, PY2, jPY1;
    Double_t PZ1, PZ2, jPZ1;
    
    Double_t range0=-1;
    Double_t range1=1;
    Int_t nch=20;
    Double_t binWidth = (range1-range0)/nch;
    
    
    
    Double_t BMass;


    
    TChain * DecayTreeMC=new TChain("DecayTree");
    
    DecayTreeMC->Add("../MC/CAL_0/Etac/AllMC_Reduced.root");
    //DecayTreeMC->Add("../MC/GenSpectr/Chic0/res.root");

    Double_t Mrange0=2890;
    Double_t Mrange1=3100;
    Double_t bin_width=10.;
    

    
    Int_t Mnch=int((Mrange1-Mrange0)/bin_width);

    
    const Int_t NEnMC=DecayTreeMC->GetEntries();
    
    //DecayTreeMC->SetBranchAddress("Jpsi_m_scaled",&BMass);
    
    DecayTreeMC->SetBranchAddress("Phi1_CosTheta",&Cos1);
    DecayTreeMC->SetBranchAddress("Phi2_CosTheta",&Cos2);
    
    
    DecayTreeMC->SetBranchAddress("Phi1_PE",&PE1);
    DecayTreeMC->SetBranchAddress("Phi2_PE",&PE2);
    
    DecayTreeMC->SetBranchAddress("Phi1_PX",&PX1);
    DecayTreeMC->SetBranchAddress("Phi2_PX",&PX2);
    
    DecayTreeMC->SetBranchAddress("Phi1_PY",&PY1);
    DecayTreeMC->SetBranchAddress("Phi2_PY",&PY2);
    
    DecayTreeMC->SetBranchAddress("Phi1_PZ",&PZ1);
    DecayTreeMC->SetBranchAddress("Phi2_PZ",&PZ2);
    
    DecayTreeMC->SetBranchAddress("Jpsi_PE",&jPE1);
    DecayTreeMC->SetBranchAddress("Jpsi_PX",&jPX1);
    DecayTreeMC->SetBranchAddress("Jpsi_PY",&jPY1);
    DecayTreeMC->SetBranchAddress("Jpsi_PZ",&jPZ1);

    TH1D * mcPHSP_hist = new TH1D("mcPHSP_hist","mcPHSP_hist" , nch, range0, range1);
    TH1D * test_hist = new TH1D("test_hist","test_hist" , nch, range0, range1);

    
    for (Int_t i=0; i<NEnMC; i++)
    {
        DecayTreeMC->GetEntry(i);
        mcPHSP_hist->Fill(Cos1);
        mcPHSP_hist->Fill(Cos2);
        
        TLorentzVector mother;
        mother.SetPxPyPzE(jPX1,jPY1,jPZ1,jPE1);
        TLorentzVector daughter1;
        daughter1.SetPxPyPzE(PX1,PY1,PZ1,PE1);
        TLorentzVector daughter2;
        daughter2.SetPxPyPzE(PX2,PY2,PZ2,PE2);
        
        TVector3 boost = mother.BoostVector();
        
        //mother2.Boost(-boost);
        daughter1.Boost(-boost);
        TVector3 boostedDaughter = daughter1.BoostVector();
        Double_t cosT = TMath::Cos(boostedDaughter.Angle(boost));
        test_hist->Fill(cosT);
 
        daughter2.Boost(-boost);
        boostedDaughter = daughter2.BoostVector();
        cosT = TMath::Cos(boostedDaughter.Angle(boost));
        test_hist->Fill(cosT);
        
        cout<<cosT<<"  "<<Cos2<<endl;
        
        

    }
    

    cout<<"test_hist Integral = "<<test_hist->Integral()/2<<endl;
    test_hist->Scale(1./NEnMC);
    test_hist->SetLineColor(kBlue);
    test_hist->SetLineWidth(2);
    test_hist->Draw();
    cout<<"test_hist Integral = "<<test_hist->Integral()<<endl;
    
    
    cout<<"mcPHSP_hist Integral = "<<mcPHSP_hist->Integral()/2<<endl;
    mcPHSP_hist->Scale(1./NEnMC);
    mcPHSP_hist->SetLineColor(kRed);
    mcPHSP_hist->SetLineWidth(2);
    //mcPHSP_hist->Draw("same");
    cout<<"mcPHSP_hist Integral = "<<mcPHSP_hist->Integral()<<endl;
    
    
    
}
