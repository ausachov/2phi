// $Id: $
using namespace RooFit;


//0 - unbinnned interpolation
//1 - binned
void MakeCut(int Type=0)
{
    
  Double_t minPT=0., minCos1 = -1, minCos2 = -1;
  Double_t maxPT=30000., maxCos1 = 1, maxCos2 = 1;
  Int_t nBinsPT=20, nBinsCos=7;
    
  TChain *chain = new TChain("DecayTree");

  chain->Add("Data_RunII/All2phi.root");

//    chain->Add("Data_07_15/2phi2011.root");
//    chain->Add("Data_07_15/2phi2012.root");
 
    
    
//  TProof::Open("");

    RooRealVar varPT("varPT","varPT",0,30000);
    RooRealVar varCos1("Cos1","Cos1",-1,1);
    RooRealVar varCos2("Cos2","Cos2",-1,1);
  
    TFile *newfile = new TFile("2phi_afterCut.root","recreate");
    TTree *newtree = chain->CopyTree("");
 
    Double_t weight,phi1_cos,phi2_cos;
    Double_t jpsi_pt;

    
    chain->SetBranchAddress("Phi1_CosTheta",&phi1_cos);
    chain->SetBranchAddress("Phi2_CosTheta",&phi2_cos);
    chain->SetBranchAddress("Jpsi_PT",&jpsi_pt);
    
      newtree->Print();
      newfile->Write();
      
      delete chain;
      delete newfile;
}

