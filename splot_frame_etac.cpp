

using namespace RooFit;
using namespace RooStats;


//0 - etac
//1 - chic0
//2 - chic1
//3 - chic2
//4 - etac2

void splot_frame_etac(int nCharmonia)
{
    gROOT->Reset();
    gROOT->SetStyle("Plain");
    gROOT->ProcessLine(".L RooRelBreitWigner.cxx+");
    TProof::Open("");
    
    Float_t etacMass =	2983.6;
    Float_t chi0Mass =	3414.75;
    Float_t chi1Mass =	3510.66;
    Float_t hcMass   = 	3525.38;
    Float_t chi2Mass =	3556.2;
    Float_t etac2Mass =	3639.4;
    Float_t x3872Mass = 	3871.69;
    Float_t x3915Mass = 	3918.4;
    Float_t x3927Mass = 	3927.2;
    
    Float_t jpsi_mass =3096.97;
    
    Float_t etacMassError =	0.7;
    Float_t chi0MassError =	0.31;
    Float_t chi1MassError =	0.07;
    Float_t hcMassError   = 0.11;
    Float_t chi2MassError =	0.09;
    Float_t etac2MassError =1.3;
    Float_t x3872MassError = 0.17;
    Float_t x3915MassError = 1.9;
    Float_t x3927MassError = 2.6;
    
    Float_t etacGamma =	32.2;
    Float_t chi0Gamma =	10.5;
    Float_t chi1Gamma =	0.84;
    Float_t hcGamma   = 	0.7;
    Float_t chi2Gamma =	1.93;
    Float_t etac2Gamma =	11.3;
    
    Float_t x3915Gamma = 	20;
    Float_t x3927Gamma = 	24;
    
    Float_t etacGammaError =	0.9;
    Float_t chi0GammaError =	0.6;
    Float_t chi1GammaError =	0.04;
    Float_t hcGammaError   = 	0.4;
    Float_t chi2GammaError =	0.11;
    Float_t etac2GammaError =	3.1;
    Float_t x3915GammaError = 	5;
    Float_t x3927GammaError = 	6;
    
    Float_t EtacSigmaN_MC = 6.311;
    
    Float_t SigmaPar = 0.212;
    
    Float_t etacMassBase = 2982.93;
    Float_t chi0MassBase = 3412.62;
    Float_t chi1MassBase = 3508.23;
    Float_t chi2MassBase = 3557.37;
    Float_t etac2MassBase = 3653.86;
    Float_t etacGammaBase = 31.80;
	

    switch(nCharmonia)
    {
        case 0:
            Float_t minMass = 2800;
            Float_t maxMass = 3200;
            Float_t binWidthETA = 0.25;
            Float_t binWidthPT = 2000.;
            Float_t binWidthPhiCos = 0.1;
            Float_t binWidthnTracks = 50;
            break;
        default:
            Float_t minMass = 3300;
            Float_t maxMass = 3800;
            Float_t binWidthETA = 0.75;
            Float_t binWidthPT = 5000.;
            Float_t binWidthPhiCos = 0.2;
            Float_t binWidthnTracks = 150;
            break;
    }
	Float_t binWidth = 10.;
	Int_t binN = int((maxMass-minMass)/binWidth);
    
    
    Float_t minPT = 0;
    Float_t maxPT = 25000;
    Float_t minETA = 1.5;
    Float_t maxETA = 5;
    Float_t minPhiCos = 0;
    Float_t maxPhiCos = 1;
    Float_t minnTracks = 0;
    Float_t maxnTracks = 600;
    
    Int_t binNPT = int((maxPT-minPT)/binWidthPT);
    RooRealVar Jpsi_PT("Jpsi_PT","Jpsi_PT",minPT,maxPT);
    
    Int_t binNETA = int((maxETA-minETA)/binWidthETA);
    RooRealVar Jpsi_ETA("Jpsi_ETA", "Jpsi_ETA", minETA, maxETA);
    
    Int_t binNnTracks = int((maxnTracks-minnTracks)/binWidthnTracks);
    RooRealVar nTracks("nTracks", "nTracks", minnTracks, maxnTracks);


    Int_t binNPhiCos = int((maxPhiCos-minPhiCos)/binWidthPhiCos);
    RooRealVar Jpsi_Phi1_CosThetaABS_Mix("Phi1_CosThetaABS_Mix", "Phi1_CosThetaABS_Mix", minPhiCos, maxPhiCos);
    
    RooRealVar Jpsi_Y("Jpsi_Y", "Jpsi_Y", 2, 4.5, "MeV");
    
    
    
    
    
    
	RooRealVar Jpsi_m_scaled("Jpsi_m_scaled", "Jpsi_m_scaled", minMass, maxMass, "MeV");
	Jpsi_m_scaled.setBins(10000);
	
	Float_t Centre = (minMass+maxMass)/2;

	RooArgList listComp, listNorm;

	RooRealVar FourKMass("FourKMass", "FourKMass", 4*493.677);
	RooRealVar var2PhiMass("var2PhiMass", "var2PhiMass", 2038.9);
	RooRealVar varSigmaFactor("varSigmaFactor","varSigmaFactor",2.16);
	RooRealVar FitMin("FitMin","FitMin",minMass);
	RooRealVar varNarrowFraction("varNarrowFraction","varNarrowFraction",0.87);
	RooRealVar ASig("ASig", "ASig", 0.206);
    RooRealVar radius("radius", "radius", 1.5);
    
  
	RooRealVar massa("massa", "massa", 1019.46, "MeV");
	RooRealVar massb("massb", "massb", 1019.46, "MeV");
	RooRealVar ConvCentre("ConvCentre","ConvCentre",0) ;	


    RooRealVar varEtacMass("M(#eta_{c})", "varEtacMass", 2984.00, 2975, 2989);
    RooRealVar varEtacGamma("varEtacGamma", "varEtacGamma", 31.82);
	RooFormulaVar varEtacSigmaN("varEtacSigmaN", "varEtacSigmaN", "sqrt(@1-@2)*(@0)", RooArgList(ASig, varEtacMass, FourKMass));
	RooFormulaVar varEtacSigmaW("varEtacSigmaW", "varEtacSigmaW", "@0*@1", RooArgList(varSigmaFactor, varEtacSigmaN));
	RooRealVar varEtacNumber("NEta", "varEtacNumber", 6800, 1e1, 1e4);

    
	RooRelBreitWigner pdfEtacBWrel = RooRelBreitWigner("pdfEtacBWrel", "pdfEtacBWrel", Jpsi_m_scaled, varEtacMass, varEtacGamma,
			RooConst(0), radius, massa, massb);
	RooGaussian pdfEtacGaussN("pdfEtacGaussN","pdfEtacGaussN",Jpsi_m_scaled,ConvCentre,varEtacSigmaN);
	RooGaussian pdfEtacGaussW("pdfEtacGaussW","pdfEtacGaussW",Jpsi_m_scaled,ConvCentre,varEtacSigmaN);
	RooFFTConvPdf pdfEtacN("pdfEtacN","pdfEtacN",Jpsi_m_scaled,pdfEtacBWrel,pdfEtacGaussN) ;
	RooFFTConvPdf pdfEtacW("pdfEtacW","pdfEtacW",Jpsi_m_scaled,pdfEtacBWrel,pdfEtacGaussW) ;
	RooAddPdf pdfEtac("pdfEtac","pdfEtac",RooArgList(pdfEtacN,pdfEtacW),varNarrowFraction);

    
    
    
    


    RooRealVar varChi0Mass("M(#chi_{c0})", "varChi0Mass", chi0Mass, chi0Mass-5.00, chi0Mass+5.00);
    RooRealVar varChi1Mass("M(#chi_{c1})", "varChi1Mass", chi1Mass-5.00, chi1Mass+5.00);
    RooRealVar varChi2Mass("M(#chi_{c2})", "varChi2Mass", chi2Mass-5.00, chi2Mass+5.00);
    RooRealVar varEtac2Mass("M(#eta_{c}(2S))", "varEtac2Mass", etac2Mass, etac2Mass-15.00, etac2Mass+5.00);
    
    RooRealVar varChi0Gamma("varChi0Gamma", "varChi0Gamma", chi0Gamma);
    RooRealVar varChi1Gamma("varChi1Gamma", "varChi1Gamma", chi1Gamma);
    RooRealVar varChi2Gamma("varChi2Gamma", "varChi2Gamma", chi2Gamma);
    RooRealVar varEtac2Gamma("#Gamma(#eta_{c}(2S))", "varEtac2Gamma", etac2Gamma);

    RooFormulaVar varChi0SigmaN("varChi0SigmaN", "varChi0SigmaN", "sqrt(@1-@2)*(@0)", RooArgList(ASig, varChi0Mass, FourKMass));
    RooFormulaVar varChi1SigmaN("varChi1SigmaN", "varChi1SigmaN", "sqrt(@1-@2)*(@0)", RooArgList(ASig, varChi1Mass, FourKMass));
    RooFormulaVar varChi2SigmaN("varChi2SigmaN", "varChi2SigmaN", "sqrt(@1-@2)*(@0)", RooArgList(ASig, varChi2Mass, FourKMass));
    RooFormulaVar varEtac2SigmaN("varEtac2SigmaN", "varEtac2SigmaN", "sqrt(@1-@2)*(@0)", RooArgList(ASig, varEtac2Mass, FourKMass));
    
    RooFormulaVar varEtacSigmaW("varEtacSigmaW", "varEtacSigmaW", "@0*@1", RooArgList(varSigmaFactor, varEtacSigmaN));
    RooFormulaVar varChi0SigmaW("varChi0SigmaW", "varChi0SigmaW", "@0*@1", RooArgList(varSigmaFactor, varChi0SigmaN));
    RooFormulaVar varChi1SigmaW("varChi1SigmaW", "varChi1SigmaW", "@0*@1", RooArgList(varSigmaFactor, varChi1SigmaN));
    RooFormulaVar varChi2SigmaW("varChi2SigmaW", "varChi2SigmaW", "@0*@1", RooArgList(varSigmaFactor, varChi2SigmaN));
    RooFormulaVar varEtac2SigmaW("varEtac2SigmaW", "varEtac2SigmaW", "@0*@1", RooArgList(varSigmaFactor, varEtac2SigmaN));
    
    RooRealVar varChi0Number("NChi0", "varChi0Number", 1000,0,1e5);
    RooRealVar varChi1Number("NChi1", "varChi1Number", 1000,0,1e5);
    RooRealVar varChi2Number("NChi2", "varChi2Number",1000,0,1e5);
    RooRealVar varEtac2Number("NEta2", "varEtac2Number", 400,0,1e5);

    
    RooRelBreitWigner pdfChi0BWrel = RooRelBreitWigner("pdfChi0BWrel", "pdfChi0BWrel", Jpsi_m_scaled, varChi0Mass, varChi0Gamma,
                                                       RooConst(0), radius, massa, massb);
    RooGaussian pdfChi0GaussN("pdfChi0GaussN","pdfChi0GaussN",Jpsi_m_scaled,ConvCentre,varChi0SigmaN);
    RooGaussian pdfChi0GaussW("pdfChi0GaussW","pdfChi0GaussW",Jpsi_m_scaled,ConvCentre,varChi0SigmaN);
    RooFFTConvPdf pdfChi0N("pdfChi0N","pdfChi0N",Jpsi_m_scaled,pdfChi0BWrel,pdfChi0GaussN) ;
    RooFFTConvPdf pdfChi0W("pdfChi0W","pdfChi0W",Jpsi_m_scaled,pdfChi0BWrel,pdfChi0GaussW) ;
    RooAddPdf pdfChi0("pdfChi0","pdfChi0",RooArgList(pdfChi0N,pdfChi0W),varNarrowFraction);
    
    
    RooRelBreitWigner pdfChi1BWrel = RooRelBreitWigner("pdfChi1BWrel", "pdfChi1BWrel", Jpsi_m_scaled, varChi1Mass, varChi1Gamma,
                                                       RooConst(1), radius, massa, massb);
    RooGaussian pdfChi1GaussN("pdfChi1GaussN","pdfChi1GaussN",Jpsi_m_scaled,ConvCentre,varChi1SigmaN);
    RooGaussian pdfChi1GaussW("pdfChi1GaussW","pdfChi1GaussW",Jpsi_m_scaled,ConvCentre,varChi1SigmaN);
    RooFFTConvPdf pdfChi1N("pdfChi1N","pdfChi1N",Jpsi_m_scaled,pdfChi1BWrel,pdfChi1GaussN) ;
    RooFFTConvPdf pdfChi1W("pdfChi1W","pdfChi1W",Jpsi_m_scaled,pdfChi1BWrel,pdfChi1GaussW) ;
    RooAddPdf pdfChi1("pdfChi1","pdfChi1",RooArgList(pdfChi1N,pdfChi1W),varNarrowFraction);
    
    
    RooRelBreitWigner pdfChi2BWrel = RooRelBreitWigner("pdfChi2BWrel", "pdfChi2BWrel", Jpsi_m_scaled, varChi2Mass, varChi2Gamma,
                                                       RooConst(2), radius, massa, massb);
    RooGaussian pdfChi2GaussN("pdfChi2GaussN","pdfChi2GaussN",Jpsi_m_scaled,ConvCentre,varChi2SigmaN);
    RooGaussian pdfChi2GaussW("pdfChi2GaussW","pdfChi2GaussW",Jpsi_m_scaled,ConvCentre,varChi2SigmaN);
    RooFFTConvPdf pdfChi2N("pdfChi2N","pdfChi2N",Jpsi_m_scaled,pdfChi2BWrel,pdfChi2GaussN) ;
    RooFFTConvPdf pdfChi2W("pdfChi2W","pdfChi2W",Jpsi_m_scaled,pdfChi2BWrel,pdfChi2GaussW) ;
    RooAddPdf pdfChi2("pdfChi2","pdfChi2",RooArgList(pdfChi2N,pdfChi2W),varNarrowFraction);	
    
    
    RooRelBreitWigner pdfEtac2BWrel = RooRelBreitWigner("pdfEtac2BWrel", "pdfEtac2BWrel", Jpsi_m_scaled, varEtac2Mass, varEtac2Gamma,
                                                        RooConst(0), radius, massa, massb);
    RooGaussian pdfEtac2GaussN("pdfEtac2GaussN","pdfEtac2GaussN",Jpsi_m_scaled,ConvCentre,varEtac2SigmaN);
    RooGaussian pdfEtac2GaussW("pdfEtac2GaussW","pdfEtac2GaussW",Jpsi_m_scaled,ConvCentre,varEtac2SigmaW);
    RooFFTConvPdf pdfEtac2N("pdfEtac2N","pdfEtac2N",Jpsi_m_scaled,pdfEtac2BWrel,pdfEtac2GaussN) ;
    // 	pdfEtac2N.setShift(0,Centre);
    RooFFTConvPdf pdfEtac2W("pdfEtac2W","pdfEtac2W",Jpsi_m_scaled,pdfEtac2BWrel,pdfEtac2GaussW) ;
    // 	pdfEtac2W.setShift(0,Centre);
    RooAddPdf pdfEtac2("pdfEtac2","pdfEtac2",RooArgList(pdfEtac2N,pdfEtac2W),varNarrowFraction);	
    
	
	RooRealVar varBgrNumber("NBgr", "number of background events", 6.5e4, 1e1, 1e5);
	RooRealVar varB0("varB0", "varB0", -1e-3, -1e-1, 1e-1);
	RooRealVar varB1("varB1", "varB1", 5e-7, -1, 1);

    RooGenericPdf pdfBgr("pdfBgr", "pdfBgr", "sqrt(@0-@1)*(1+(@0-@2)*@3)",    //O2 without exp
                         RooArgSet(Jpsi_m_scaled, var2PhiMass, FitMin, varB0));
//	RooGenericPdf pdfBgr("pdfBgr", "pdfBgr", "sqrt(@0-@1)*(1+(@0-@2)*@3+(@0-@2)*(@0-@2)*@4)",    //O2 without exp
//			RooArgSet(Jpsi_m_scaled, var2PhiMass, FitMin, varB0, varB1));


	listComp.add(pdfBgr);
	listNorm.add(varBgrNumber);

    
    
    switch(nCharmonia)
    {
        case 0:
            listComp.add(pdfEtac);
            listNorm.add(varEtacNumber);
            break;
        default:
            listComp.add(pdfChi0);
            listComp.add(pdfChi1);
            listComp.add(pdfChi2);
            listComp.add(pdfEtac2);
            
            listNorm.add(varChi0Number);
            listNorm.add(varChi1Number);
            listNorm.add(varChi2Number);
            listNorm.add(varEtac2Number);
            break;
    }

	RooAddPdf pdfModel("pdfModel", "pdfModel", listComp, listNorm);

    RooRealVar diPhiWeight("diPhiWeight","diPhiWeight",-20,20);
    RooRealVar diphiweight("diphiweight","diphiweight",-20,20);
    
    switch(nCharmonia)
    {
        case 0:
            TFile* file = new TFile("splotAddEtac.root");
            break;
        default:
            TFile* file = new TFile("splotAddChic.root");
            break;
    }
    TTree* tree = (TTree*)file->Get("DecayTree");

    RooDataSet* DataSet = new RooDataSet("DataSetFr", "DataSetFr", tree, RooArgList(Jpsi_m_scaled,Jpsi_PT,Jpsi_ETA,Jpsi_Phi1_CosThetaABS_Mix,nTracks,diPhiWeight,diphiweight),"","diPhiWeight");

    RooFitResult* results = pdfModel->fitTo(*DataSet/*, ExternalConstraints(GMConstraint)*//*, Minos(true)*/, Save(true), PrintLevel(0), NumCPU(4));
    results = pdfModel->fitTo(*DataSet, Save(true), PrintLevel(0), NumCPU(4),SumW2Error(kTRUE));
    //results = pdfModel->fitTo(*DataSet, Save(true), Minos(true),PrintLevel(0), NumCPU(4),SumW2Error(kTRUE),Extended(true));
    
    
    RooPlot* frame = Jpsi_m_scaled.frame(Title("Jpsi_m_scaled"));
    DataSet->plotOn(frame, Binning(binN, minMass, maxMass),DataError(RooAbsData::SumW2));
    pdfModel.plotOn(frame);
    TCanvas* canvA = new TCanvas("canvA", "canvA", 1000, 600);
    frame->Draw();
    
    ASig.setConstant(kTRUE);
    varB0.setConstant(kTRUE);
    
    switch(nCharmonia)
    {
        case 0:
            varEtacMass.setConstant(kTRUE);
            break;
        default:
            varChi0Mass.setConstant(kTRUE);
            varChi1Mass.setConstant(kTRUE);
            varChi2Mass.setConstant(kTRUE);
            varEtac2Mass.setConstant(kTRUE);
            break;
    }
    
    
    
    switch(nCharmonia)
    {
        case 0:
            SPlot* sDataEtac = new SPlot("sDataEtac", "sDataEtac", *DataSet, &pdfModel, RooArgList(varEtacNumber,varBgrNumber));
            RooDataSet* dsetNewEtac = new RooDataSet(DataSet->GetName(),DataSet->GetTitle(),DataSet,*DataSet->get(),0,"NEta_sw");
            TFile* fileMCEtac = new TFile("../MC/CAL_0/Etac/AllMC_Reduced.root");
            RooPlot* framePTEtac = Jpsi_PT.frame(Title("Etac : PT"));
            RooPlot* frameETAEtac = Jpsi_ETA.frame(Title("Etac : ETA"));
            RooPlot* framePhiCosEtac = Jpsi_Phi1_CosThetaABS_Mix.frame(Title("Etac : Abs(Phi CosTheta)"));
            RooPlot* framenTracksEtac = nTracks.frame(Title("Etac : nTracks"));
            Double_t DataScaleFactorEtac = 1/varEtacNumber.getVal();
            
            
            TTree* treeMCEtac = (TTree*)fileMCEtac->Get("DecayTree");
            RooDataSet* MCDataSetEtac = new RooDataSet("MCDataSetEtac", "MCDataSetEtac", treeMCEtac, RooArgList(Jpsi_m_scaled,Jpsi_PT,Jpsi_ETA,Jpsi_Phi1_CosThetaABS_Mix,nTracks),"","");
            Double_t numMCEtac = MCDataSetEtac->numEntries();
            
            dsetNewEtac->plotOn(framePTEtac, Binning(binNPT, minPT, maxPT),LineColor(kRed),MarkerSize(0.2),MarkerColor(kRed),Rescale(DataScaleFactorEtac));
            MCDataSetEtac->plotOn(framePTEtac, Binning(binNPT, minPT, maxPT),LineColor(kBlue),MarkerSize(0.2),MarkerColor(kBlue),Rescale(1./numMCEtac));
            
            dsetNewEtac->plotOn(frameETAEtac, Binning(binNETA, minETA, maxETA),LineColor(kRed),MarkerSize(0.2),MarkerColor(kRed),Rescale(DataScaleFactorEtac));
            MCDataSetEtac->plotOn(frameETAEtac, Binning(binNETA, minETA, maxETA),LineColor(kBlue),MarkerSize(0.2),MarkerColor(kBlue),Rescale(1./numMCEtac));
            
            dsetNewEtac->plotOn(framePhiCosEtac, Binning(binNPhiCos, minPhiCos, maxPhiCos),LineColor(kRed),MarkerSize(0.2),MarkerColor(kRed),Rescale(DataScaleFactorEtac));
            MCDataSetEtac->plotOn(framePhiCosEtac, Binning(binNPhiCos, minPhiCos, maxPhiCos),LineColor(kBlue),MarkerSize(0.2),MarkerColor(kBlue),Rescale(1./numMCEtac));
            
            dsetNewEtac->plotOn(framenTracksEtac, Binning(binNnTracks, minnTracks, maxnTracks),LineColor(kRed),MarkerSize(0.2),MarkerColor(kRed),Rescale(DataScaleFactorEtac));
            MCDataSetEtac->plotOn(framenTracksEtac, Binning(binNnTracks, minnTracks, maxnTracks),LineColor(kBlue),MarkerSize(0.2),MarkerColor(kBlue),Rescale(1./numMCEtac));
            
            
            TCanvas* canvSplotEtac = new TCanvas("canvSplotEtac", "canvSplotEtac", 1000, 600);
            canvSplotEtac->Divide(4,1);
            canvSplotEtac->cd(1);
            framePTEtac->GetYaxis()->SetRangeUser(0,0.5);
            framePTEtac->Draw();
            
            canvSplotEtac->cd(2);
            framePhiCosEtac->GetYaxis()->SetRangeUser(0,0.5);
            framePhiCosEtac->Draw();
            
            canvSplotEtac->cd(3);
            frameETAEtac->GetYaxis()->SetRangeUser(0,0.5);
            frameETAEtac->Draw();
            
            canvSplotEtac->cd(4);
            framenTracksEtac->GetYaxis()->SetRangeUser(0,0.5);
            framenTracksEtac->Draw();
            
            break;
        case 1:
            SPlot* sDataChic0 = new SPlot("sDataChic0", "sDataChic0", *DataSet, &pdfModel, RooArgList(varChi0Number,varChi1Number,varChi2Number,varEtac2Number,varBgrNumber));
            RooDataSet* dsetNewChic0 = new RooDataSet(DataSet->GetName(),DataSet->GetTitle(),DataSet,*DataSet->get(),0,"NChi0_sw");
            TFile* fileMCChic0 = new TFile("../MC/CAL_0/Chic0/AllMC_Reduced.root");
            RooPlot* framePTChic0 = Jpsi_PT.frame(Title("Chic0 : PT"));
            RooPlot* frameETAChic0 = Jpsi_ETA.frame(Title("Chic0 : ETA"));
            RooPlot* framePhiCosChic0 = Jpsi_Phi1_CosThetaABS_Mix.frame(Title("Chic0 : Abs(Phi CosTheta)"));
            RooPlot* framenTracksChic0 = nTracks.frame(Title("Chic0 : nTracks"));
            Double_t DataScaleFactorChic0 = 1/varChi0Number.getVal();

            
            TTree* treeMCChic0 = (TTree*)fileMCChic0->Get("DecayTree");
            RooDataSet* MCDataSetChic0 = new RooDataSet("MCDataSetChic0", "MCDataSetChic0", treeMCChic0, RooArgList(Jpsi_m_scaled,Jpsi_PT,Jpsi_ETA,Jpsi_Phi1_CosThetaABS_Mix,nTracks),"","");
            Double_t numMCChic0 = MCDataSetChic0->numEntries();
            
            dsetNewChic0->plotOn(framePTChic0, Binning(binNPT, minPT, maxPT),LineColor(kRed),MarkerSize(0.2),MarkerColor(kRed),Rescale(DataScaleFactorChic0));
            MCDataSetChic0->plotOn(framePTChic0, Binning(binNPT, minPT, maxPT),LineColor(kBlue),MarkerSize(0.2),MarkerColor(kBlue),Rescale(1./numMCChic0));
            
            dsetNewChic0->plotOn(frameETAChic0, Binning(binNETA, minETA, maxETA),LineColor(kRed),MarkerSize(0.2),MarkerColor(kRed),Rescale(DataScaleFactorChic0));
            MCDataSetChic0->plotOn(frameETAChic0, Binning(binNETA, minETA, maxETA),LineColor(kBlue),MarkerSize(0.2),MarkerColor(kBlue),Rescale(1./numMCChic0));
            
            dsetNewChic0->plotOn(framePhiCosChic0, Binning(binNPhiCos, minPhiCos, maxPhiCos),LineColor(kRed),MarkerSize(0.2),MarkerColor(kRed),Rescale(DataScaleFactorChic0));
            MCDataSetChic0->plotOn(framePhiCosChic0, Binning(binNPhiCos, minPhiCos, maxPhiCos),LineColor(kBlue),MarkerSize(0.2),MarkerColor(kBlue),Rescale(1./numMCChic0));
            
            dsetNewChic0->plotOn(framenTracksChic0, Binning(binNnTracks, minnTracks, maxnTracks),LineColor(kRed),MarkerSize(0.2),MarkerColor(kRed),Rescale(DataScaleFactorChic0));
            MCDataSetChic0->plotOn(framenTracksChic0, Binning(binNnTracks, minnTracks, maxnTracks),LineColor(kBlue),MarkerSize(0.2),MarkerColor(kBlue),Rescale(1./numMCChic0));
            

            
            TCanvas* canvSplotChic0 = new TCanvas("canvSplotChic0", "canvSplotChic0", 1000, 600);
            canvSplotChic0->Divide(4,1);
            canvSplotChic0->cd(1);
            framePTChic0->GetYaxis()->SetRangeUser(0,1.);
            framePTChic0->Draw();
            
            canvSplotChic0->cd(2);
            framePhiCosChic0->GetYaxis()->SetRangeUser(0,1.);
            framePhiCosChic0->Draw();
            
            canvSplotChic0->cd(3);
            frameETAChic0->GetYaxis()->SetRangeUser(0,1.);
            frameETAChic0->Draw();

            canvSplotChic0->cd(4);
            framenTracksChic0->GetYaxis()->SetRangeUser(0,1.);
            framenTracksChic0->Draw();
            
            SPlot* sDataChic1 = new SPlot("sDataChic1", "sDataChic1", *DataSet, &pdfModel, RooArgList(varChi0Number,varChi1Number,varChi2Number,varEtac2Number,varBgrNumber));
            RooDataSet* dsetNewChic1 = new RooDataSet(DataSet->GetName(),DataSet->GetTitle(),DataSet,*DataSet->get(),0,"NChi1_sw");
            TFile* fileMCChic1 = new TFile("../MC/CAL_0/Chic1/AllMC_Reduced.root");
            RooPlot* framePTChic1 = Jpsi_PT.frame(Title("Chic1 : PT"));
            RooPlot* frameETAChic1 = Jpsi_ETA.frame(Title("Chic1 : ETA"));
            RooPlot* framePhiCosChic1 = Jpsi_Phi1_CosThetaABS_Mix.frame(Title("Chic1 : Abs(Phi CosTheta)"));
            RooPlot* framenTracksChic1 = nTracks.frame(Title("Chic1 : nTracks"));
            Double_t DataScaleFactorChic1 = 1/varChi1Number.getVal();
            
            TTree* treeMCChic1 = (TTree*)fileMCChic1->Get("DecayTree");
            RooDataSet* MCDataSetChic1 = new RooDataSet("MCDataSetChic1", "MCDataSetChic1", treeMCChic1, RooArgList(Jpsi_m_scaled,Jpsi_PT,Jpsi_ETA,Jpsi_Phi1_CosThetaABS_Mix,nTracks),"","");
            Double_t numMCChic1 = MCDataSetChic1->numEntries();
            
            dsetNewChic1->plotOn(framePTChic1, Binning(binNPT, minPT, maxPT),LineColor(kRed),MarkerSize(0.2),MarkerColor(kRed),Rescale(DataScaleFactorChic1));
            MCDataSetChic1->plotOn(framePTChic1, Binning(binNPT, minPT, maxPT),LineColor(kBlue),MarkerSize(0.2),MarkerColor(kBlue),Rescale(1./numMCChic1));
            
            dsetNewChic1->plotOn(frameETAChic1, Binning(binNETA, minETA, maxETA),LineColor(kRed),MarkerSize(0.2),MarkerColor(kRed),Rescale(DataScaleFactorChic1));
            MCDataSetChic1->plotOn(frameETAChic1, Binning(binNETA, minETA, maxETA),LineColor(kBlue),MarkerSize(0.2),MarkerColor(kBlue),Rescale(1./numMCChic1));
            
            dsetNewChic1->plotOn(framePhiCosChic1, Binning(binNPhiCos, minPhiCos, maxPhiCos),LineColor(kRed),MarkerSize(0.2),MarkerColor(kRed),Rescale(DataScaleFactorChic1));
            MCDataSetChic1->plotOn(framePhiCosChic1, Binning(binNPhiCos, minPhiCos, maxPhiCos),LineColor(kBlue),MarkerSize(0.2),MarkerColor(kBlue),Rescale(1./numMCChic1));
            
            dsetNewChic1->plotOn(framenTracksChic1, Binning(binNnTracks, minnTracks, maxnTracks),LineColor(kRed),MarkerSize(0.2),MarkerColor(kRed),Rescale(DataScaleFactorChic1));
            MCDataSetChic1->plotOn(framenTracksChic1, Binning(binNnTracks, minnTracks, maxnTracks),LineColor(kBlue),MarkerSize(0.2),MarkerColor(kBlue),Rescale(1./numMCChic1));
            
            
            
        
            TCanvas* canvSplotChic1 = new TCanvas("canvSplotChic1", "canvSplotChic1", 1000, 600);
            canvSplotChic1->Divide(4,1);
            canvSplotChic1->cd(1);
            framePTChic1->GetYaxis()->SetRangeUser(0,1.);
            framePTChic1->Draw();
            
            canvSplotChic1->cd(2);
            framePhiCosChic1->GetYaxis()->SetRangeUser(0,1.);
            framePhiCosChic1->Draw();
            
            canvSplotChic1->cd(3);
            frameETAChic1->GetYaxis()->SetRangeUser(0,1.);
            frameETAChic1->Draw();
            
            canvSplotChic1->cd(4);
            framenTracksChic1->GetYaxis()->SetRangeUser(0,1.);
            framenTracksChic1->Draw();

            SPlot* sDataChic2 = new SPlot("sDataChic2", "sDataChic2", *DataSet, &pdfModel, RooArgList(varChi0Number,varChi1Number,varChi2Number,varEtac2Number,varBgrNumber));
            RooDataSet* dsetNewChic2 = new RooDataSet(DataSet->GetName(),DataSet->GetTitle(),DataSet,*DataSet->get(),0,"NChi2_sw");
            TFile* fileMCChic2 = new TFile("../MC/CAL_0/Chic2/AllMC_Reduced.root");
            RooPlot* framePTChic2 = Jpsi_PT.frame(Title("Chic2 : PT"));
            RooPlot* frameETAChic2 = Jpsi_ETA.frame(Title("Chic2 : ETA"));
            RooPlot* framePhiCosChic2 = Jpsi_Phi1_CosThetaABS_Mix.frame(Title("Chic2 : Abs(Phi CosTheta)"));
            RooPlot* framenTracksChic2 = nTracks.frame(Title("Chic2 : nTracks"));
            Double_t DataScaleFactorChic2 = 1/varChi2Number.getVal();
            
            TTree* treeMCChic2 = (TTree*)fileMCChic2->Get("DecayTree");
            RooDataSet* MCDataSetChic2 = new RooDataSet("MCDataSetChic2", "MCDataSetChic2", treeMCChic2, RooArgList(Jpsi_m_scaled,Jpsi_PT,Jpsi_ETA,Jpsi_Phi1_CosThetaABS_Mix,nTracks),"","");
            Double_t numMCChic2 = MCDataSetChic2->numEntries();
            
            dsetNewChic2->plotOn(framePTChic2, Binning(binNPT, minPT, maxPT),LineColor(kRed),MarkerSize(0.2),MarkerColor(kRed),Rescale(DataScaleFactorChic2));
            MCDataSetChic2->plotOn(framePTChic2, Binning(binNPT, minPT, maxPT),LineColor(kBlue),MarkerSize(0.2),MarkerColor(kBlue),Rescale(1./numMCChic2));
            
            dsetNewChic2->plotOn(frameETAChic2, Binning(binNETA, minETA, maxETA),LineColor(kRed),MarkerSize(0.2),MarkerColor(kRed),Rescale(DataScaleFactorChic2));
            MCDataSetChic2->plotOn(frameETAChic2, Binning(binNETA, minETA, maxETA),LineColor(kBlue),MarkerSize(0.2),MarkerColor(kBlue),Rescale(1./numMCChic2));
            
            dsetNewChic2->plotOn(framePhiCosChic2, Binning(binNPhiCos, minPhiCos, maxPhiCos),LineColor(kRed),MarkerSize(0.2),MarkerColor(kRed),Rescale(DataScaleFactorChic2));
            MCDataSetChic2->plotOn(framePhiCosChic2, Binning(binNPhiCos, minPhiCos, maxPhiCos),LineColor(kBlue),MarkerSize(0.2),MarkerColor(kBlue),Rescale(1./numMCChic2));
            
            dsetNewChic2->plotOn(framenTracksChic2, Binning(binNnTracks, minnTracks, maxnTracks),LineColor(kRed),MarkerSize(0.2),MarkerColor(kRed),Rescale(DataScaleFactorChic2));
            MCDataSetChic2->plotOn(framenTracksChic2, Binning(binNnTracks, minnTracks, maxnTracks),LineColor(kBlue),MarkerSize(0.2),MarkerColor(kBlue),Rescale(1./numMCChic2));
            

            
            TCanvas* canvSplotChic2 = new TCanvas("canvSplotChic2", "canvSplotChic2", 1000, 600);
            canvSplotChic2->Divide(4,1);
            canvSplotChic2->cd(1);
            framePTChic2->GetYaxis()->SetRangeUser(0,1.);
            framePTChic2->Draw();
            
            canvSplotChic2->cd(2);
            framePhiCosChic2->GetYaxis()->SetRangeUser(0,1.);
            framePhiCosChic2->Draw();
            
            canvSplotChic2->cd(3);
            frameETAChic2->GetYaxis()->SetRangeUser(0,1.);
            frameETAChic2->Draw();
            
            canvSplotChic2->cd(4);
            framenTracksChic2->GetYaxis()->SetRangeUser(0,1.);
            framenTracksChic2->Draw();

            SPlot* sDataEtac2 = new SPlot("sDataEtac2", "sDataEtac2", *DataSet, &pdfModel, RooArgList(varChi0Number,varChi1Number,varChi2Number,varEtac2Number,varBgrNumber));
            RooDataSet* dsetNewEtac2 = new RooDataSet(DataSet->GetName(),DataSet->GetTitle(),DataSet,*DataSet->get(),0,"NEta2_sw");
            TFile* fileMCEtac2 = new TFile("../MC/CAL_0/Etac2/AllMC_Reduced.root");
            RooPlot* framePTEtac2 = Jpsi_PT.frame(Title("Etac2 : PT"));
            RooPlot* frameETAEtac2 = Jpsi_ETA.frame(Title("Etac2 : ETA"));
            RooPlot* framePhiCosEtac2 = Jpsi_Phi1_CosThetaABS_Mix.frame(Title("Etac2 : Abs(Phi CosTheta)"));
            RooPlot* framenTracksEtac2 = nTracks.frame(Title("Etac2 : nTracks"));
            Double_t DataScaleFactorEtac2 = 1/varEtac2Number.getVal();
            
            
            TTree* treeMCEtac2 = (TTree*)fileMCEtac2->Get("DecayTree");
            RooDataSet* MCDataSetEtac2 = new RooDataSet("MCDataSetEtac2", "MCDataSetEtac2", treeMCEtac2, RooArgList(Jpsi_m_scaled,Jpsi_PT,Jpsi_ETA,Jpsi_Phi1_CosThetaABS_Mix,nTracks),"","");
            Double_t numMCEtac2 = MCDataSetEtac2->numEntries();
            
            dsetNewEtac2->plotOn(framePTEtac2, Binning(binNPT, minPT, maxPT),LineColor(kRed),MarkerSize(0.2),MarkerColor(kRed),Rescale(DataScaleFactorEtac2));
            MCDataSetEtac2->plotOn(framePTEtac2, Binning(binNPT, minPT, maxPT),LineColor(kBlue),MarkerSize(0.2),MarkerColor(kBlue),Rescale(1./numMCEtac2));
            
            dsetNewEtac2->plotOn(frameETAEtac2, Binning(binNETA, minETA, maxETA),LineColor(kRed),MarkerSize(0.2),MarkerColor(kRed),Rescale(DataScaleFactorEtac2));
            MCDataSetEtac2->plotOn(frameETAEtac2, Binning(binNETA, minETA, maxETA),LineColor(kBlue),MarkerSize(0.2),MarkerColor(kBlue),Rescale(1./numMCEtac2));
            
            dsetNewEtac2->plotOn(framePhiCosEtac2, Binning(binNPhiCos, minPhiCos, maxPhiCos),LineColor(kRed),MarkerSize(0.2),MarkerColor(kRed),Rescale(DataScaleFactorEtac2));
            MCDataSetEtac2->plotOn(framePhiCosEtac2, Binning(binNPhiCos, minPhiCos, maxPhiCos),LineColor(kBlue),MarkerSize(0.2),MarkerColor(kBlue),Rescale(1./numMCEtac2));
            
            dsetNewEtac2->plotOn(framenTracksEtac2, Binning(binNnTracks, minnTracks, maxnTracks),LineColor(kRed),MarkerSize(0.2),MarkerColor(kRed),Rescale(DataScaleFactorEtac2));
            MCDataSetEtac2->plotOn(framenTracksEtac2, Binning(binNnTracks, minnTracks, maxnTracks),LineColor(kBlue),MarkerSize(0.2),MarkerColor(kBlue),Rescale(1./numMCEtac2));
            

            TCanvas* canvSplotEtac2 = new TCanvas("canvSplotEtac2", "canvSplotEtac2", 1000, 600);
            canvSplotEtac2->Divide(4,1);
            canvSplotEtac2->cd(1);
            framePTEtac2->GetYaxis()->SetRangeUser(0,1.);
            framePTEtac2->Draw();
            
            canvSplotEtac2->cd(2);
            framePhiCosEtac2->GetYaxis()->SetRangeUser(0,1.);
            framePhiCosEtac2->Draw();
            
            canvSplotEtac2->cd(3);
            frameETAEtac2->GetYaxis()->SetRangeUser(0,1.);
            frameETAEtac2->Draw();
            
            canvSplotEtac2->cd(4);
            framenTracksEtac2->GetYaxis()->SetRangeUser(0,1.);
            framenTracksEtac2->Draw();
            break;

    }
    

}


