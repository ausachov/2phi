

using namespace RooFit;

//0 - Base
//1 - Fix M(Chic) to PDG
//2 - MC Sigma For Etac
//3 - Parabolic BG
//4 - f0 in 2D
//5 - add X's to fit and hc
//6 - MC Sigma in 2D
//7 - splot
//8 - R=0.5
//9 - R=3.0
//10 GammaEtac - dGamma
//11 GammaEtac + dGamma

//12- 970, 70
//13- 1010, 70
//14- 990, 40
//15- 990, 100

//16- Exp in 2D


void fit_etac2(char* pdfLabel, int nbin)
{

  
	Float_t SigmaPar = 0.224;
	Float_t minMass = 3300;
	Float_t maxMass = 3700;  
  
  
	int Type=0;
	
	Float_t etacMass =	2983.6; 
	Float_t chi0Mass =	3414.75; 
	Float_t chi1Mass =	3510.66; 
	Float_t hcMass   = 	3525.38; 
	Float_t chi2Mass =	3556.2; 
	Float_t etac2Mass =	3639.4; 
	Float_t x3872Mass = 	3871.69; 
	Float_t x3915Mass = 	3918.4; 
	Float_t x3927Mass = 	3927.2; 
	
	Float_t etacMassError =	0.7; 
	Float_t chi0MassError =	0.31; 
	Float_t chi1MassError =	0.07; 
	Float_t hcMassError   = 0.11; 
	Float_t chi2MassError =	0.09; 
	Float_t etac2MassError =1.3; 
	Float_t x3872MassError = 0.17; 
	Float_t x3915MassError = 1.9; 
	Float_t x3927MassError = 2.6; 
	
	Float_t etacGamma =	32.2; 
	Float_t chi0Gamma =	10.5; 
	Float_t chi1Gamma =	0.84; 
	Float_t hcGamma   = 	0.7; 
	Float_t chi2Gamma =	1.93; 
	Float_t etac2Gamma =	11.3;

	Float_t x3915Gamma = 	20; 
	Float_t x3927Gamma = 	24; 	
	
	Float_t etacGammaError =	0.9; 
	Float_t chi0GammaError =	0.6; 
	Float_t chi1GammaError =	0.04; 
	Float_t hcGammaError   = 	0.4; 
	Float_t chi2GammaError =	0.11; 
	Float_t etac2GammaError =	3.1; 
	Float_t x3915GammaError = 	5; 
	Float_t x3927GammaError = 	6; 
	
	Float_t EtacSigmaN_MC = 6.311;	
	
//	gROOT->Reset();
// 	gROOT->SetStyle("Plain");
// 	TProof::Open("");
	Float_t binWidth = 10.;
	Int_t binN = int((maxMass-minMass)/binWidth);
	RooRealVar Jpsi_m_scaled("Jpsi_m_scaled", "Jpsi_m_scaled", minMass, maxMass, "MeV");
    	RooRealVar varNDiPhi_sw("varNDiPhi_sw", "varNDiPhi_sw", -5, 5);
	Jpsi_m_scaled.setBins(10000);
	
	Float_t Centre = (minMass+maxMass)/2;

// 	histogramms:
	switch(Type)
	{
	  case 4:
	    TFile* file = new TFile("diPhiPureAll.root");
	    break;
	  case 7:
	    TFile* file = new TFile("splotAll.root");
	    break;
	  case 6:
	    TFile* file = new TFile("diPhiPureAll_MC.root");
	    break;
	  case 16:
	    TFile* file = new TFile("diPhiPureAllExp.root");
	    break;
	  default:
	    TFile* file = new TFile("diPhiPureAll.root");
	    break;
	}


	
	
if(Type==7)
{
        TTree* chain;
	file->GetObject("DecayTree", chain);
        RooDataSet* DataSet = new RooDataSet("dset", "dset", chain, RooArgList(Jpsi_m_scaled, varNDiPhi_sw), "", "varNDiPhi_sw");
}
else
{
	TH1D *histJpsiDiPhi  = (TH1D*) file->Get("histJpsiDiPhi");
	RooDataHist* DataSet = new RooDataHist("DataSet", "DataSet", Jpsi_m_scaled, histJpsiDiPhi);
}


	RooArgList listComp, listNorm;
	RooArgSet showParams;

	
	
	RooRealVar FourKMass("FourKMass", "FourKMass", 4*493.677);
	RooRealVar varSigmaFactor("varSigmaFactor","varSigmaFactor",2.16);
	RooRealVar FitMin("FitMin","FitMin",minMass);
	
	RooRealVar varNarrowFraction("varNarrowFraction","varNarrowFraction",0.87);
//  	RooRealVar ASig("ASig", "ASig", SigmaPar, 1e-1, 1);
	RooRealVar ASig("ASig", "ASig", SigmaPar);

if(Type==8)
  RooRealVar radius("radius", "radius", 0.5);
else
  if(Type==9)
    RooRealVar radius("radius", "radius", 3.0);
  else
	RooRealVar radius("radius", "radius", 1.5);
  
	RooRealVar massa("massa", "massa", 1019.46, "MeV");
	RooRealVar massb("massb", "massb", 1019.46, "MeV");
	RooRealVar ConvCentre("ConvCentre","ConvCentre",0) ;	
	
	showParams.add(ASig);
	showParams.add(varNarrowFraction);

  
	RooRealVar varChi0Number("N(#chi_{c0})", "varChi0Number", 0, 1e4);

	
	RooRealVar varChi0Mass("M(#chi_{c0})", "varChi0Mass", chi0Mass);
	RooRealVar varChi0Gamma("varChi0Gamma", "varChi0Gamma", chi0Gamma);
	RooFormulaVar varChi0SigmaN("varChi0SigmaN", "varChi0SigmaN", "sqrt(@1-@2)*(@0)", RooArgList(ASig, varChi0Mass, FourKMass));
	RooFormulaVar varChi0SigmaW("varChi0SigmaW", "varChi0SigmaW", "@0*@1", RooArgList(varSigmaFactor, varChi0SigmaN));

	RooRelBreitWigner pdfChi0BWrel = RooRelBreitWigner("pdfChi0BWrel", "pdfChi0BWrel", Jpsi_m_scaled, varChi0Mass, varChi0Gamma,
			RooConst(0), radius, massa, massb);


	RooGaussian pdfChi0GaussN("pdfChi0GaussN","pdfChi0GaussN",Jpsi_m_scaled,ConvCentre,varChi0SigmaN);
	RooGaussian pdfChi0GaussW("pdfChi0GaussW","pdfChi0GaussW",Jpsi_m_scaled,ConvCentre,varChi0SigmaN);
	
	RooFFTConvPdf pdfChi0N("pdfChi0N","pdfChi0N",Jpsi_m_scaled,pdfChi0BWrel,pdfChi0GaussN) ;
	RooFFTConvPdf pdfChi0W("pdfChi0W","pdfChi0W",Jpsi_m_scaled,pdfChi0BWrel,pdfChi0GaussW) ;
	
	RooAddPdf pdfChi0("pdfChi0","pdfChi0",RooArgList(pdfChi0N,pdfChi0W),varNarrowFraction);	
	listComp.add(pdfChi0);
	listNorm.add(varChi0Number);
	showParams.add(varChi0Number);

	

	RooRealVar varChi1Number("N(#chi_{c1})", "varChi1Number", 0, 1e4);
	RooRealVar varChi1Mass("M(#chi_{c1})", "varChi1Mass", chi1Mass);

	RooRealVar varChi1Gamma("varChi1Gamma", "varChi1Gamma", chi1Gamma);
	RooFormulaVar varChi1SigmaN("varChi1SigmaN", "varChi1SigmaN", "sqrt(@1-@2)*(@0)", RooArgList(ASig, varChi1Mass, FourKMass));
	RooFormulaVar varChi1SigmaW("varChi1SigmaW", "varChi1SigmaW", "@0*@1", RooArgList(varSigmaFactor, varChi1SigmaN));
	
	
	RooRelBreitWigner pdfChi1BWrel = RooRelBreitWigner("pdfChi1BWrel", "pdfChi1BWrel", Jpsi_m_scaled, varChi1Mass, varChi1Gamma,
	RooConst(1), radius, massa, massb);

	
	RooGaussian pdfChi1GaussN("pdfChi1GaussN","pdfChi1GaussN",Jpsi_m_scaled,ConvCentre,varChi1SigmaN);
	RooGaussian pdfChi1GaussW("pdfChi1GaussW","pdfChi1GaussW",Jpsi_m_scaled,ConvCentre,varChi1SigmaN);
	
	RooFFTConvPdf pdfChi1N("pdfChi1N","pdfChi1N",Jpsi_m_scaled,pdfChi1BWrel,pdfChi1GaussN) ;
	RooFFTConvPdf pdfChi1W("pdfChi1W","pdfChi1W",Jpsi_m_scaled,pdfChi1BWrel,pdfChi1GaussW) ;
	
	RooAddPdf pdfChi1("pdfChi1","pdfChi1",RooArgList(pdfChi1N,pdfChi1W),varNarrowFraction);	
	listComp.add(pdfChi1);
	listNorm.add(varChi1Number);
	showParams.add(varChi1Number);
	



	RooRealVar varChi2Number("N(#chi_{c2})", "varChi2Number", 0,1e4);
	RooRealVar varChi2Mass("M(#chi_{c2})", "varChi2Mass", chi2Mass);

	RooRealVar varChi2Gamma("varChi2Gamma", "varChi2Gamma", chi2Gamma);
	RooFormulaVar varChi2SigmaN("varChi2SigmaN", "varChi2SigmaN", "sqrt(@1-@2)*(@0)", RooArgList(ASig, varChi2Mass, FourKMass));
	RooFormulaVar varChi2SigmaW("varChi2SigmaW", "varChi2SigmaW", "@0*@1", RooArgList(varSigmaFactor, varChi2SigmaN));

	
	RooRelBreitWigner pdfChi2BWrel = RooRelBreitWigner("pdfChi2BWrel", "pdfChi2BWrel", Jpsi_m_scaled, varChi2Mass, varChi2Gamma,
			RooConst(2), radius, massa, massb);

	RooGaussian pdfChi2GaussN("pdfChi2GaussN","pdfChi2GaussN",Jpsi_m_scaled,ConvCentre,varChi2SigmaN);
	RooGaussian pdfChi2GaussW("pdfChi2GaussW","pdfChi2GaussW",Jpsi_m_scaled,ConvCentre,varChi2SigmaN);
	
	RooFFTConvPdf pdfChi2N("pdfChi2N","pdfChi2N",Jpsi_m_scaled,pdfChi2BWrel,pdfChi2GaussN) ;
	RooFFTConvPdf pdfChi2W("pdfChi2W","pdfChi2W",Jpsi_m_scaled,pdfChi2BWrel,pdfChi2GaussW) ;
	
	RooAddPdf pdfChi2("pdfChi2","pdfChi2",RooArgList(pdfChi2N,pdfChi2W),varNarrowFraction);	
	listComp.add(pdfChi2);
	listNorm.add(varChi2Number);
	showParams.add(varChi2Number);	
	
	RooRealVar varEtac2Number("N(#eta_{c}(2S))", "varEtac2Number",300,0,2000);
//	RooRealVar varEtac2MassPDG("m(#eta_{c}(2S))", "varEtac2MassPDG", 3638.9, 3638.9-10, 3638.9+5);
if(Type==10)
  RooRealVar varEtac2Gamma("#Gamma(#eta_{c}(2S))", "varEtac2Gamma", etac2Gamma-etac2GammaError);
else
    if(Type==11)  
	RooRealVar varEtac2Gamma("#Gamma(#eta_{c}(2S))", "varEtac2Gamma", etac2Gamma+etac2GammaError);
    else
	RooRealVar varEtac2Gamma("#Gamma(#eta_{c}(2S))", "varEtac2Gamma", etac2Gamma);
	RooRealVar varEtac2Mass("M(#eta_{c}(2S))", "varEtac2Mass", etac2Mass);
 
	RooGaussian GammaConstraint("GammaConstraint","GammaConstraint",varEtac2Gamma,RooConst(etac2Gamma),RooConst(etac2GammaError)) ;
// 	RooGaussian MassConstraint("MassConstraint","MassConstraint", varEtac2Mass, RooConst(3639.4), RooConst(1.3)) ;
	RooGaussian MassConstraint("MassConstraint","MassConstraint", varEtac2Mass, RooConst(etac2Mass), RooConst(etac2MassError)) ;
	RooProdPdf GMConstraint("GMConstraint", "GMConstraint", RooArgSet(GammaConstraint, MassConstraint));
	
	RooFormulaVar varEtac2SigmaN("varEtac2SigmaN", "varEtac2SigmaN", "sqrt(@1-@2)*(@0)", RooArgList(ASig, varEtac2Mass, FourKMass));
	RooFormulaVar varEtac2SigmaW("varEtac2SigmaW", "varEtac2SigmaW", "@0*@1", RooArgList(varSigmaFactor, varEtac2SigmaN));
	RooRelBreitWigner pdfEtac2BWrel = RooRelBreitWigner("pdfEtac2BWrel", "pdfEtac2BWrel", Jpsi_m_scaled, varEtac2Mass, varEtac2Gamma,
			RooConst(0), radius, massa, massb);

	RooGaussian pdfEtac2GaussN("pdfEtac2GaussN","pdfEtac2GaussN",Jpsi_m_scaled,ConvCentre,varEtac2SigmaN);
	RooGaussian pdfEtac2GaussW("pdfEtac2GaussW","pdfEtac2GaussW",Jpsi_m_scaled,ConvCentre,varEtac2SigmaN);
	
	RooFFTConvPdf pdfEtac2N("pdfEtac2N","pdfEtac2N",Jpsi_m_scaled,pdfEtac2BWrel,pdfEtac2GaussN) ;
	RooFFTConvPdf pdfEtac2W("pdfEtac2W","pdfEtac2W",Jpsi_m_scaled,pdfEtac2BWrel,pdfEtac2GaussW) ;
	
	RooAddPdf pdfEtac2("pdfEtac2","pdfEtac2",RooArgList(pdfEtac2N,pdfEtac2W),varNarrowFraction);	
	listComp.add(pdfEtac2);
	listNorm.add(varEtac2Number);
	showParams.add(varEtac2Gamma);


	RooRealVar varBgrNumber("N_{bgr}", "number of background events", 6.5e4, 0, 1e5);
	char label[200];
	RooRealVar varE0("varE0", "varE0", -3e-3, -1e-1, 0);
	RooRealVar varA0("varA0", "varA0", 1e-3,-1e-1, +1e-1);
	RooRealVar varA1("varA1", "varA1", 5e-6,-1e-3, +1e-3);
	
	RooRealVar varB0("varB0", "varB0", -1e-3, -1e-1, 1e-1);
	RooRealVar varB1("varB1", "varB1", 5e-7, -1, 1);
	

	
	RooRealVar var2PhiMass("var2PhiMass", "var2PhiMass", 2038.9);


if(Type==3)
	RooGenericPdf pdfBgr("pdfBgr", "pdfBgr", "sqrt(@0-@1)*(1+(@0-@2)*@3+(@0-@2)*(@0-@2)*@4)",    //O2 without exp
			RooArgSet(Jpsi_m_scaled, var2PhiMass, FitMin, varB0, varB1));
else 
 	RooGenericPdf pdfBgr("pdfBgr", "pdfBgr", "sqrt(@0-@1)*exp(@0*@3)",
    		        RooArgSet(Jpsi_m_scaled, var2PhiMass, FitMin, varE0));	
/* 	RooGenericPdf pdfBgr("pdfBgr", "pdfBgr", "sqrt(@0-@1)*exp(@0*@3)*(1+(@0-@2)*@4)",
    		        RooArgSet(Jpsi_m_scaled, var2PhiMass, FitMin, varE0, varA0));*/	
	
//  	RooGenericPdf pdfBgr("pdfBgr", "pdfBgr", "sqrt(@0-@1)*exp(@0*@2)*(1+(@0-@1)*@3+(@0-@1)*(@0-@1)*@3)",
//     		        RooArgSet(Jpsi_m_scaled, var2PhiMass, varE0, varA0, varA1/*, varD0*/));
	
	
	
	
	
	
//     	RooGenericPdf pdfBgr("pdfBgr", "pdfBgr", "exp(@0*@1)*(1+(@0-@3)*@2+(@0-@3)*(@0-@3)*@4)*sqrt(@0-@3)",
//     			RooArgSet(Jpsi_m_scaled, varC0, varA0, var2PhiMass, varD0));
// 	RooGenericPdf pdfBgr("pdfBgr", "pdfBgr", "exp(@0*@1)*(1+(@0-@3)*@2+(@0-@3)*(@0-@3)*@4+(@0-@3)*(@0-@3)*(@0-@3)*@5)*sqrt(@0-@3)", //exp with O
// 			RooArgSet(Jpsi_m_scaled, varC0, varA0, var2PhiMass, varD0, varF0));

	listComp.add(pdfBgr);
	listNorm.add(varBgrNumber);

	RooAddPdf pdfModel("pdfModel", "pdfModel", listComp, listNorm);
	

	RooFitResult* results = pdfModel.fitTo(*DataSet/*, ExternalConstraints(GMConstraint)*//*, Minos(true)*/, Save(true), PrintLevel(0), NumCPU(4));
	

if(Type==7)
  	results = pdfModel.fitTo(*DataSet, Save(true), PrintLevel(0), NumCPU(4));
else
	results = pdfModel.chi2FitTo(*DataSet,Extended(varBgrNumber.getVal()), Minos(true), PrintLevel(0));

	results = pdfModel.chi2FitTo(*DataSet,Extended(varBgrNumber.getVal()), Minos(true), PrintLevel(0));

	
	RooPlot* frame = Jpsi_m_scaled.frame(Title("Jpsi_m_scaled"));
	DataSet->plotOn(frame, Binning(binN, minMass, maxMass));

	pdfModel.plotOn(frame);
	pdfModel.paramOn(frame, Layout(0.65, 0.95, 0.95), Parameters(showParams), ShowConstants(true), Format("NEALU",AutoPrecision(1)));
	frame->getAttText()->SetTextSize(0.03) ;

	TCanvas* canvA = new TCanvas("canvA", "canvA", 1000, 600);
	frame->Draw();
	
	char pdfName[100];
	sprintf(pdfName,"%s.pdf",pdfLabel);
	
	canvA->SaveAs(pdfName);
	delete canvA; 
	delete DataSet;
	delete file;
	
	
	
	char txtName[100];
	sprintf(txtName,"%s.txt",pdfLabel);
	
	
	FILE * fout;
	if(nbin==0)
	  fout = fopen ("params.txt", "w");
	else
	  fout = fopen ("params.txt", "a");
		
	fprintf(fout,"%i %f %f %f\n",
		nbin,
		varEtac2Number.getVal(),
		-varEtac2Number.getErrorLo(),varEtac2Number.getErrorHi());
	fclose(fout);
	


}


void SysFramework()
{
  for(int i=0;i<17;i++)
    fit_chic(i);
}