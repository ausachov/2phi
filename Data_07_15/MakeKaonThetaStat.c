void MakeKaonThetaStat()
{
    TChain *chain = new TChain("DecayTree");
    chain->Add("Reduced_MagDown2011.root");
    chain->Add("Reduced_MagUp2011.root");
    chain->Add("Reduced_MagDown2012.root");
    chain->Add("Reduced_MagUp2012.root");
    
    TFile *newfile = new TFile("All2phiKaonCosTheta.root","recreate");
    TTree *newtree = new TTree("DecayTree","DecayTree");
    
    Double_t Phi1_m_scaled_Mix;
    Double_t Phi2_m_scaled_Mix;
    Double_t Phi1_m_scaled;
    Double_t Phi2_m_scaled;
    Double_t Jpsi_m_scaled, Jpsi_m_scaled_new;
    Double_t Kaon1_CosTheta,Kaon2_CosTheta,Kaon3_CosTheta,Kaon4_CosTheta;
    Double_t Kaon_CosTheta;
    
    
    
    chain->SetBranchAddress("Phi1_m_scaled",&Phi1_m_scaled);
    chain->SetBranchAddress("Phi2_m_scaled",&Phi2_m_scaled);
    chain->SetBranchAddress("Jpsi_m_scaled",&Jpsi_m_scaled);
    chain->SetBranchAddress("Kaon1_CosTheta",&Kaon1_CosTheta);
    chain->SetBranchAddress("Kaon2_CosTheta",&Kaon2_CosTheta);
    chain->SetBranchAddress("Kaon3_CosTheta",&Kaon3_CosTheta);
    chain->SetBranchAddress("Kaon4_CosTheta",&Kaon4_CosTheta);
    
    
    TBranch *Phi1_m_scaled_Mix_Branch = newtree->Branch("Phi1_m_scaled_Mix", &Phi1_m_scaled_Mix, "Phi1_m_scaled_Mix/D");
    TBranch *Phi2_m_scaled_Mix_Branch = newtree->Branch("Phi2_m_scaled_Mix", &Phi2_m_scaled_Mix, "Phi2_m_scaled_Mix/D");
    TBranch *Kaon_CosTheta_Branch = newtree->Branch("Kaon_CosTheta", &Kaon_CosTheta, "Kaon_CosTheta/D");
    TBranch *Jpsi_m_scaled_Branch = newtree->Branch("Jpsi_m_scaled", &Jpsi_m_scaled_new, "Jpsi_m_scaled/D");
    

    
    Long64_t NEntries = chain->GetEntries();
    
    for (Long64_t i = 0; i < NEntries; i++)
    {
        chain->GetEntry(i);
        Jpsi_m_scaled_new = Jpsi_m_scaled;
        switch (i%2)
        {
            case 0:
                Phi1_m_scaled_Mix = Phi1_m_scaled;
                Phi2_m_scaled_Mix = Phi2_m_scaled;
                Kaon_CosTheta = TMath::Abs(Kaon1_CosTheta);
                newtree->Fill();
                Kaon_CosTheta = TMath::Abs(Kaon2_CosTheta);
                newtree->Fill();
                Kaon_CosTheta = TMath::Abs(Kaon3_CosTheta);
                newtree->Fill();
                Kaon_CosTheta = TMath::Abs(Kaon4_CosTheta);
                newtree->Fill();
                break;
            case 1:
                Phi1_m_scaled_Mix = Phi2_m_scaled;
                Phi2_m_scaled_Mix = Phi1_m_scaled;
                Kaon_CosTheta = TMath::Abs(Kaon1_CosTheta);
                newtree->Fill();
                Kaon_CosTheta = TMath::Abs(Kaon2_CosTheta);
                newtree->Fill();
                Kaon_CosTheta = TMath::Abs(Kaon3_CosTheta);
                newtree->Fill();
                Kaon_CosTheta = TMath::Abs(Kaon4_CosTheta);
                newtree->Fill();
                break;
        }
    }
    
    newtree->Print();
    newfile->Write();
    newfile->Close();
    
    delete chain;
    delete newfile;
}
