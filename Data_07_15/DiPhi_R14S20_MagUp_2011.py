DaVinciVersion = 'v33r4'
myJobName = 'DiPhi_R14S20_MagUp'
myApplication = DaVinci()
myApplication.version = DaVinciVersion
myApplication.user_release_area = '/afs/cern.ch/user/j/jhe/cmtuser'
]
myApplication.optsfile = ['DaVinci_DiPhi_R14S20_2011.py' ]

data = BKQuery( path ="/LHCb/Collision11/Beam3500GeV-VeloClosed-MagUp/Real Data/Reco14/Stripping20r1/90000000/CHARM.MDST",dqflag=['OK']).getDataset()

mySplitter = SplitByFiles( filesPerJob = 20, maxFiles = -1, ignoremissing = True )

myBackend = Dirac()
j = Job (
    name         = myJobName,
    application  = myApplication,
    splitter     = mySplitter,
    outputfiles  = [ SandboxFile('Tuple.root'),
                     SandboxFile('DVHistos.root')
                     ],
    inputdata    = data,
    backend      = myBackend,
    do_auto_resubmit = True
    )
j.submit(keep_going=True, keep_on_fail=True)

#myInputsandbox = []
#myOutputsandbox = ['DVHistos.root', 'summary.xml', 'Tuple.root']

#j = Job (
#    name         = myJobName,
#    application  = myApplication,
#    splitter     = mySplitter,
#    inputsandbox = myInputsandbox,
#    outputsandbox= myOutputsandbox,
#    inputdata    = data,
##    outputdata   = myOutputData,
#    backend      = myBackend,
#    do_auto_resubmit = True
#    )
