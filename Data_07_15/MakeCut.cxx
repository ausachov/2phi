// $Id: $
{
  TChain *chain = new TChain("DecayTree");
  chain->Add("All2phi.root");
//    chain->Add("Data_ok_cuts/All2phi.root");
  
  
  TCut Cut("Jpsi_m_scaled>2923&&Jpsi_m_scaled<3053");

  TFile *newfile = new TFile("All2phiEtac.root","recreate");
  TTree *newtree = chain->CopyTree(Cut);
 
  newtree->Print();
  newfile->Write();
  
  delete chain;
  delete newfile;
  
}

