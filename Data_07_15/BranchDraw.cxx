#include <TMath.h>




void BranchDraw()
{
    gROOT->Reset();
    //gStyle->SetOptStat(kFALSE);
    
    Double_t range0=0;
    Double_t range1=3e9;
    Int_t nch=10000000;
    
    TChain * DecayTree=new TChain("DecayTree");
    //DecayTree->Add("All2phi.root");
    DecayTree->Add("All2phi.root");
    

    
    const Int_t NEn=DecayTree->GetEntries();
//    Double_t Branch_Value, Branch_Value1, jpsi_pt;
    ULong64_t Branch_Value;
    UInt_t Branch_Value1;
    DecayTree->SetBranchAddress("eventNumber",&Branch_Value);
    DecayTree->SetBranchAddress("runNumber",&Branch_Value1);
    //   DecayTree->SetBranchAddress("Phi1_P",&Phi1_P);
    //   DecayTree->SetBranchAddress("Phi2_P",&Phi2_P);
    
    TH1D * m_phiphi_hist = new TH1D("Data","Data" , nch, range0, range1);
    
    DecayTree->GetEntry(0);
    int nRun = Branch_Value1;
    
    int iBin = 0;
    int nMultiple = 0;
    char sel [200] = "";
    
    for (Int_t i=0; i<NEn; i++)
    {
        DecayTree->GetEntry(i);
        
        
        sprintf(sel,"eventNumber==%llu && runNumber==%i",Branch_Value,Branch_Value1);
        cout<<sel<<endl;
        
        if(DecayTree->GetEntries(sel)>1)
            nMultiple++;
        
//        if(Branch_Value1==nRun)
//        {
//        
//            if(Branch_Value>range0 && Branch_Value<range1)
//            {
//                if(Branch_Value1==nRun)
//                {
//                    iBin = m_phiphi_hist->FindBin(Branch_Value);
//                    if(m_phiphi_hist->GetBinContent(iBin)>0.5)nMultiple++;
//                }
//                else
//                {   m_phiphi_hist->Clear();
//                    nRun = Branch_Value1;
//                }
//                m_phiphi_hist->Fill(Branch_Value);
//            }
//        }
//        else
//        {
//            nRun = Branch_Value1;
//            m_phiphi_hist->Clear();
//        }
        
    }

    
    cout<<nRun<<endl;
    cout<<"nMultiple = "<<nMultiple<<"/"<<NEn<<endl;
    TCanvas *canv1 = new TCanvas("canv1","B_MM",5,85,800,600);
    //   m_phiphi_hist->SetAxisRange(5,50);
    m_phiphi_hist->SetXTitle("B_MM");
    m_phiphi_hist->DrawCopy();
    
}
