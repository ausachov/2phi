void bs_EventsNumbers()
{
    TChain* chain = new TChain("DecayTree");
    chain->Add("2phi_afterCut.root");
    
    TTree *newtree = chain->CopyTree("Jpsi_m_scaled>5325 && Jpsi_m_scaled<5405");
    
    ULong64_t Branch_Value;
    UInt_t RunNumber;
    Double_t mScaled;
    newtree->SetBranchAddress("eventNumber",&Branch_Value);
    newtree->SetBranchAddress("runNumber",&RunNumber);
    newtree->SetBranchAddress("Jpsi_m_scaled",&mScaled);
    
    FILE *fp = fopen("Bs_EventsNumbers.txt", "w");
    
    for(Long64_t i=0; i<newtree->GetEntries(); i++)
    {
        newtree->GetEntry(i);
        fprintf(fp,"%3.0f   %3.0f   %3.1f\n",RunNumber,Branch_Value,mScaled);
    }
    fclose(fp);
}