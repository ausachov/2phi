void plot_MC_Data_comp()
{
    TChain *chain = new TChain("DecayTree");
    chain->Add("2phi_afterCut.root");

    const char* etacCut = "Jpsi_m_scaled>2890 && Jpsi_m_scaled<3100";
    const char* chic0Cut = "Jpsi_m_scaled>3380 && Jpsi_m_scaled<3420";
    const char* chic1Cut = "Jpsi_m_scaled>3490 && Jpsi_m_scaled<3535";
    const char* chic2Cut = "Jpsi_m_scaled>3535 && Jpsi_m_scaled<3585";
    const char* etac2Cut = "Jpsi_m_scaled>3600 && Jpsi_m_scaled<3685";
    
    
    TTree *etac_tree = chain->CopyTree(etacCut);
    TTree *chic0_tree = chain->CopyTree(chic0Cut);
    TTree *chic1_tree = chain->CopyTree(chic1Cut);
    TTree *chic2_tree = chain->CopyTree(chic2Cut);
    TTree *etac2_tree = chain->CopyTree(etac2Cut);
    
    
    
    TChain *etacMC = new TChain("DecayTree");
    etacMC->Add("../MC/CAL_0/Etac/AllMC_Reduced.root");
    TChain *chic0MC = new TChain("DecayTree");
    chic0MC->Add("../MC/CAL_0/Chic1/AllMC_Reduced.root");
    TChain *chic1MC = new TChain("DecayTree");
    chic1MC->Add("../MC/CAL_0/Chic1/AllMC_Reduced.root");
    TChain *chic2MC = new TChain("DecayTree");
    chic2MC->Add("../MC/CAL_0/Chic2/AllMC_Reduced.root");
    TChain *etac2MC = new TChain("DecayTree");
    etac2MC->Add("../MC/CAL_0/Etac2/AllMC_Reduced.root");

    
    
    const char* br_name = "nTracks";
    float min=0.;
    float max=400.;
    int nBins=5;
    
    Int_t etac_val,chic0_val,chic1_val,chic2_val,etac2_val;
    Int_t etac_val_MC,chic0_val_MC,chic1_val_MC,chic2_val_MC,etac2_val_MC;
    
    etac_tree->SetBranchAddress(br_name,&etac_val);
    chic0_tree->SetBranchAddress(br_name,&chic0_val);
    chic1_tree->SetBranchAddress(br_name,&chic1_val);
    chic2_tree->SetBranchAddress(br_name,&chic2_val);
    etac2_tree->SetBranchAddress(br_name,&etac2_val);
    
    etacMC->SetBranchAddress(br_name,&etac_val_MC);
    chic0MC->SetBranchAddress(br_name,&chic0_val_MC);
    chic1MC->SetBranchAddress(br_name,&chic1_val_MC);
    chic2MC->SetBranchAddress(br_name,&chic2_val_MC);
    etac2MC->SetBranchAddress(br_name,&etac2_val_MC);

    
    TH1F* etac_hist = new TH1F("etac","etac",nBins,min,max);
    TH1F* chic0_hist = new TH1F("chic0","chic0",nBins,min,max);
    TH1F* chic1_hist = new TH1F("chic1","chic1",nBins,min,max);
    TH1F* chic2_hist = new TH1F("chic2","chic2",nBins,min,max);
    TH1F* etac2_hist = new TH1F("etac2","etac2",nBins,min,max);
    
    TH1F* etac_hist_MC = new TH1F("etac_MC","etac_MC",nBins,min,max);
    TH1F* chic0_hist_MC = new TH1F("chic0_MC","chic0_MC",nBins,min,max);
    TH1F* chic1_hist_MC = new TH1F("chic1_MC","chic1_MC",nBins,min,max);
    TH1F* chic2_hist_MC = new TH1F("chic2_MC","chic2_MC",nBins,min,max);
    TH1F* etac2_hist_MC = new TH1F("etac2_MC","etac2_MC",nBins,min,max);
    
    for(int i=0;i<etac_tree->GetEntries();i++)
    {
        etac_tree->GetEntry(i);
        etac_hist->Fill(etac_val);
    }
    etac_hist->SetBinError(nBins+1,etac_hist->GetBinError(nBins)/etac_hist->Integral());
    etac_hist->Scale(1/etac_hist->Integral());
    
    
    for(int i=0;i<etac2_tree->GetEntries();i++)
    {
        etac2_tree->GetEntry(i);
        etac2_hist->Fill(etac2_val);
    }
    etac2_hist->SetBinError(nBins+1,etac2_hist->GetBinError(nBins)/etac2_hist->Integral());
    etac2_hist->Scale(1/etac2_hist->Integral());
    
    
    for(int i=0;i<chic0_tree->GetEntries();i++)
    {
        chic0_tree->GetEntry(i);
        chic0_hist->Fill(chic0_val);
    }
    chic0_hist->SetBinError(nBins+1,chic0_hist->GetBinError(nBins)/chic0_hist->Integral());
    chic0_hist->Scale(1/chic0_hist->Integral());
 
    
    for(int i=0;i<chic1_tree->GetEntries();i++)
    {
        chic1_tree->GetEntry(i);
        chic1_hist->Fill(chic1_val);
    }
    chic1_hist->SetBinError(nBins+1,chic1_hist->GetBinError(nBins)/chic1_hist->Integral());
    chic1_hist->Scale(1/chic1_hist->Integral());
    
    
    for(int i=0;i<chic2_tree->GetEntries();i++)
    {
        chic2_tree->GetEntry(i);
        chic2_hist->Fill(chic2_val);
    }
    chic2_hist->SetBinError(nBins+1,chic2_hist->GetBinError(nBins)/chic2_hist->Integral());
    chic2_hist->Scale(1/chic2_hist->Integral());
    
    

    
    
    
    
    for(int i=0;i<etacMC->GetEntries();i++)
    {
        etacMC->GetEntry(i);
        etac_hist_MC->Fill(etac_val_MC);
    }
    etac_hist_MC->SetBinError(nBins+1,etac_hist_MC->GetBinError(nBins)/etac_hist_MC->Integral());
    etac_hist_MC->Scale(1/etac_hist_MC->Integral());
    
    for(int i=0;i<etac2MC->GetEntries();i++)
    {
        etac2MC->GetEntry(i);
        etac2_hist_MC->Fill(etac2_val_MC);
    }
    etac2_hist_MC->SetBinError(nBins+1,etac2_hist_MC->GetBinError(nBins)/etac2_hist_MC->Integral());
    etac2_hist_MC->Scale(1/etac2_hist_MC->Integral());
    
    for(int i=0;i<chic0MC->GetEntries();i++)
    {
        chic0MC->GetEntry(i);
        chic0_hist_MC->Fill(chic0_val_MC);
    }
    chic0_hist_MC->SetBinError(nBins+1,chic0_hist_MC->GetBinError(nBins)/chic0_hist_MC->Integral());
    chic0_hist_MC->Scale(1/chic0_hist_MC->Integral());
    
    for(int i=0;i<chic1MC->GetEntries();i++)
    {
        chic1MC->GetEntry(i);
        chic1_hist_MC->Fill(chic1_val_MC);
    }
    chic1_hist_MC->SetBinError(nBins+1,chic1_hist_MC->GetBinError(nBins)/chic1_hist_MC->Integral());
    chic1_hist_MC->Scale(1/chic1_hist_MC->Integral());
    
    for(int i=0;i<chic2MC->GetEntries();i++)
    {
        chic2MC->GetEntry(i);
        chic2_hist_MC->Fill(chic2_val_MC);
    }
    chic2_hist_MC->SetBinError(nBins+1,chic2_hist_MC->GetBinError(nBins)/chic2_hist_MC->Integral());
    chic2_hist_MC->Scale(1/chic2_hist_MC->Integral());
    
    
    TH1F* chic0_to_etac = new TH1F("chic0_to_etac","chic0_to_etac",nBins,min,max);
    TH1F* chic0_to_etac_MC = new TH1F("chic0_to_etac_MC","chic0_to_etac_MC",nBins,min,max);
    
    TH1F* chic1_to_etac = new TH1F("chic1_to_etac","chic1_to_etac",nBins,min,max);
    TH1F* chic1_to_etac_MC = new TH1F("chic1_to_etac_MC","chic1_to_etac_MC",nBins,min,max);
    
    TH1F* chic2_to_etac = new TH1F("chic2_to_etac","chic2_to_etac",nBins,min,max);
    TH1F* chic2_to_etac_MC = new TH1F("chic2_to_etac_MC","chic2_to_etac_MC",nBins,min,max);
    
    TH1F* etac2_to_etac = new TH1F("etac2_to_etac","etac2_to_etac",nBins,min,max);
    TH1F* etac2_to_etac_MC = new TH1F("etac2_to_etac_MC","etac2_to_etac_MC",nBins,min,max);
    
    float eps;
    
    for(int i=0;i<nBins;i++)
    {
        if(etac_hist->GetBinContent(i+1)!=0 && chic0_hist->GetBinContent(i+1)!=0)
        {
            chic0_to_etac->SetBinContent(i+1,chic0_hist->GetBinContent(i+1)/etac_hist->GetBinContent(i+1));
            eps = TMath::Sqrt(chic0_hist->GetBinError(i+1)/chic0_hist->GetBinContent(i+1)*chic0_hist->GetBinError(i+1)/chic0_hist->GetBinContent(i+1)
                             +
                              etac_hist->GetBinError(i+1)/etac_hist->GetBinContent(i+1)*etac_hist->GetBinError(i+1)/etac_hist->GetBinContent(i+1));
            chic0_to_etac->SetBinError(i+1,eps*chic0_to_etac->GetBinContent(i+1));
            
        }
        if(etac_hist_MC->GetBinContent(i+1)!=0 && chic0_hist_MC->GetBinContent(i+1)!=0)
        {
            chic0_to_etac_MC->SetBinContent(i+1,chic0_hist_MC->GetBinContent(i+1)/etac_hist_MC->GetBinContent(i+1));
            eps = TMath::Sqrt(chic0_hist_MC->GetBinError(i+1)/chic0_hist_MC->GetBinContent(i+1)*chic0_hist_MC->GetBinError(i+1)/chic0_hist_MC->GetBinContent(i+1)
                              +
                              etac_hist_MC->GetBinError(i+1)/etac_hist_MC->GetBinContent(i+1)*etac_hist_MC->GetBinError(i+1)/etac_hist_MC->GetBinContent(i+1));
            chic0_to_etac_MC->SetBinError(i+1,eps*chic0_to_etac_MC->GetBinContent(i+1));
        }
        
        
        
        
        
        
        
        if(etac_hist->GetBinContent(i+1)!=0 && chic1_hist->GetBinContent(i+1)!=0)
        {
            chic1_to_etac->SetBinContent(i+1,chic1_hist->GetBinContent(i+1)/etac_hist->GetBinContent(i+1));
            eps = TMath::Sqrt(chic1_hist->GetBinError(i+1)/chic1_hist->GetBinContent(i+1)*chic1_hist->GetBinError(i+1)/chic1_hist->GetBinContent(i+1)
                              +
                              etac_hist->GetBinError(i+1)/etac_hist->GetBinContent(i+1)*etac_hist->GetBinError(i+1)/etac_hist->GetBinContent(i+1));
            chic1_to_etac->SetBinError(i+1,eps*chic1_to_etac->GetBinContent(i+1));
            
        }
        if(etac_hist_MC->GetBinContent(i+1)!=0 && chic1_hist_MC->GetBinContent(i+1)!=0)
        {
            chic1_to_etac_MC->SetBinContent(i+1,chic1_hist_MC->GetBinContent(i+1)/etac_hist_MC->GetBinContent(i+1));
            eps = TMath::Sqrt(chic1_hist_MC->GetBinError(i+1)/chic1_hist_MC->GetBinContent(i+1)*chic1_hist_MC->GetBinError(i+1)/chic1_hist_MC->GetBinContent(i+1)
                              +
                              etac_hist_MC->GetBinError(i+1)/etac_hist_MC->GetBinContent(i+1)*etac_hist_MC->GetBinError(i+1)/etac_hist_MC->GetBinContent(i+1));
            chic1_to_etac_MC->SetBinError(i+1,eps*chic1_to_etac_MC->GetBinContent(i+1));
        }
        
        

        if(etac_hist->GetBinContent(i+1)!=0 && chic2_hist->GetBinContent(i+1)!=0)
        {
            chic2_to_etac->SetBinContent(i+1,chic2_hist->GetBinContent(i+1)/etac_hist->GetBinContent(i+1));
            eps = TMath::Sqrt(chic2_hist->GetBinError(i+1)/chic2_hist->GetBinContent(i+1)*chic2_hist->GetBinError(i+1)/chic2_hist->GetBinContent(i+1)
                              +
                              etac_hist->GetBinError(i+1)/etac_hist->GetBinContent(i+1)*etac_hist->GetBinError(i+1)/etac_hist->GetBinContent(i+1));
            chic2_to_etac->SetBinError(i+1,eps*chic2_to_etac->GetBinContent(i+1));
            
        }
        if(etac_hist_MC->GetBinContent(i+1)!=0 && chic2_hist_MC->GetBinContent(i+1)!=0)
        {
            chic2_to_etac_MC->SetBinContent(i+1,chic2_hist_MC->GetBinContent(i+1)/etac_hist_MC->GetBinContent(i+1));
            eps = TMath::Sqrt(chic2_hist_MC->GetBinError(i+1)/chic2_hist_MC->GetBinContent(i+1)*chic2_hist_MC->GetBinError(i+1)/chic2_hist_MC->GetBinContent(i+1)
                              +
                              etac_hist_MC->GetBinError(i+1)/etac_hist_MC->GetBinContent(i+1)*etac_hist_MC->GetBinError(i+1)/etac_hist_MC->GetBinContent(i+1));
            chic2_to_etac_MC->SetBinError(i+1,eps*chic2_to_etac_MC->GetBinContent(i+1));
        }
        
        
        
        if(etac_hist->GetBinContent(i+1)!=0 && etac2_hist->GetBinContent(i+1)!=0)
        {
            etac2_to_etac->SetBinContent(i+1,etac2_hist->GetBinContent(i+1)/etac_hist->GetBinContent(i+1));
            eps = TMath::Sqrt(etac2_hist->GetBinError(i+1)/etac2_hist->GetBinContent(i+1)*etac2_hist->GetBinError(i+1)/etac2_hist->GetBinContent(i+1)
                              +
                              etac_hist->GetBinError(i+1)/etac_hist->GetBinContent(i+1)*etac_hist->GetBinError(i+1)/etac_hist->GetBinContent(i+1));
            etac2_to_etac->SetBinError(i+1,eps*etac2_to_etac->GetBinContent(i+1));
            
        }
        if(etac_hist_MC->GetBinContent(i+1)!=0 && etac2_hist_MC->GetBinContent(i+1)!=0)
        {
            etac2_to_etac_MC->SetBinContent(i+1,etac2_hist_MC->GetBinContent(i+1)/etac_hist_MC->GetBinContent(i+1));
            eps = TMath::Sqrt(etac2_hist_MC->GetBinError(i+1)/etac2_hist_MC->GetBinContent(i+1)*etac2_hist_MC->GetBinError(i+1)/etac2_hist_MC->GetBinContent(i+1)
                              +
                              etac_hist_MC->GetBinError(i+1)/etac_hist_MC->GetBinContent(i+1)*etac_hist_MC->GetBinError(i+1)/etac_hist_MC->GetBinContent(i+1));
            etac2_to_etac_MC->SetBinError(i+1,eps*etac2_to_etac_MC->GetBinContent(i+1));
        }
    }
    
//    etac_hist->Draw("E");
//    etac_hist_MC->SetLineColor(kRed);
//    etac_hist_MC->Draw("Esame");
    
    gStyle->SetOptStat(000);

    TCanvas* c0 = new TCanvas("c0","c0",800, 700) ;
    chic0_to_etac->Draw("E");
    chic0_to_etac->SetXTitle(br_name);
    chic0_to_etac_MC->SetLineColor(kRed);
    chic0_to_etac_MC->Draw("Esame");
    
    TLegend *legend=new TLegend(0.7,0.75,0.90,0.90);
    legend->AddEntry(chic0_to_etac,"Data");
    legend->AddEntry(chic0_to_etac_MC,"MC");
    legend->Draw();
   
    
    TCanvas* c1 = new TCanvas("c1","c1",800, 700) ;
    chic1_to_etac->Draw("E");
    chic1_to_etac->SetXTitle(br_name);
    chic1_to_etac_MC->SetLineColor(kRed);
    chic1_to_etac_MC->Draw("Esame");
    legend->Draw();
    
    
    TCanvas* c2 = new TCanvas("c2","c2",800, 700) ;
    chic2_to_etac->Draw("E");
    chic2_to_etac->SetXTitle(br_name);
    chic2_to_etac_MC->SetLineColor(kRed);
    chic2_to_etac_MC->Draw("Esame");
    legend->Draw();
    
    
    TCanvas* c3 = new TCanvas("c3","c3",800, 700) ;
    etac2_to_etac->Draw("E");
    etac2_to_etac->SetXTitle(br_name);
    etac2_to_etac_MC->SetLineColor(kRed);
    etac2_to_etac_MC->Draw("Esame");
    legend->Draw();
    
}