void MakeAllStat()
{
  TChain *chain = new TChain("DecayTree");
  chain->Add("Reduced_MagDown2011.root");
  chain->Add("Reduced_MagUp2011.root");
  chain->Add("Reduced_MagDown2012.root");
  chain->Add("Reduced_MagUp2012.root");
  
  TFile *newfile = new TFile("All2phi.root","recreate");
  TTree *newtree = chain->CopyTree("");
  
  Double_t Phi1_m_scaled_Mix, Phi1_CosThetaABS_Mix;
  Double_t Phi2_m_scaled_Mix, Phi2_CosThetaABS_Mix;
  Double_t Phi1_m_scaled, Phi1_P, Phi1_PX, Phi1_PY, Phi1_PZ, Phi1_CosTheta;
  Double_t Phi2_m_scaled, Phi2_P, Phi2_PX, Phi2_PY, Phi2_PZ, Phi2_CosTheta;
  
  Double_t Jpsi_m_scaled, Jpsi_m_scaled_plus, Jpsi_m_scaled_minus, Jpsi_P;
  
  Double_t alpha = 0.03*0.01;
  Double_t phiMass = 1019.46;

   
  chain->SetBranchAddress("Phi1_m_scaled",&Phi1_m_scaled);   chain->SetBranchAddress("Phi2_m_scaled",&Phi2_m_scaled);
  chain->SetBranchAddress("Phi1_CosTheta",&Phi1_CosTheta);   chain->SetBranchAddress("Phi2_CosTheta",&Phi2_CosTheta);
    
  chain->SetBranchAddress("Jpsi_m_scaled",&Jpsi_m_scaled);
  chain->SetBranchAddress("Jpsi_P",&Jpsi_P);
  
  chain->SetBranchAddress("Phi1_P",&Phi1_P);   	 chain->SetBranchAddress("Phi2_P",&Phi2_P);
  chain->SetBranchAddress("Phi1_PX",&Phi1_PX);   chain->SetBranchAddress("Phi2_PX",&Phi2_PX);
  chain->SetBranchAddress("Phi1_PY",&Phi1_PY);   chain->SetBranchAddress("Phi2_PY",&Phi2_PY);
  chain->SetBranchAddress("Phi1_PZ",&Phi1_PZ);   chain->SetBranchAddress("Phi2_PZ",&Phi2_PZ);
  
   
  TBranch *Phi1_m_scaled_Mix_Branch = newtree->Branch("Phi1_m_scaled_Mix", &Phi1_m_scaled_Mix, "Phi1_m_scaled_Mix/D");
  TBranch *Phi2_m_scaled_Mix_Branch = newtree->Branch("Phi2_m_scaled_Mix", &Phi2_m_scaled_Mix, "Phi2_m_scaled_Mix/D");
    
  TBranch *Phi1_CosThetaABS_Mix_Branch = newtree->Branch("Phi1_CosThetaABS_Mix", &Phi1_CosThetaABS_Mix, "Phi1_CosThetaABS_Mix/D");
  TBranch *Phi2_CosThetaABS_Mix_Branch = newtree->Branch("Phi2_CosThetaABS_Mix", &Phi2_CosThetaABS_Mix, "Phi2_CosThetaABS_Mix/D");

  Long64_t NEntries = newtree->GetEntries();

  for (Long64_t i = 0; i < NEntries; i++)
  {
     chain->GetEntry(i);
     if(i%2 == 0)
     {
       Phi1_m_scaled_Mix = Phi1_m_scaled;
       Phi2_m_scaled_Mix = Phi2_m_scaled;
         
       Phi1_CosThetaABS_Mix = TMath::Abs(Phi1_CosTheta);
       Phi2_CosThetaABS_Mix = TMath::Abs(Phi2_CosTheta);
     }
     else
     {
       Phi1_m_scaled_Mix = Phi2_m_scaled;
       Phi2_m_scaled_Mix = Phi1_m_scaled;
         
         Phi1_CosThetaABS_Mix = TMath::Abs(Phi2_CosTheta);
         Phi2_CosThetaABS_Mix = TMath::Abs(Phi1_CosTheta);
     }
     
     Phi1_m_scaled_Mix_Branch->Fill();
     Phi2_m_scaled_Mix_Branch->Fill();
      
     Phi1_CosThetaABS_Mix_Branch->Fill();
     Phi2_CosThetaABS_Mix_Branch->Fill();
  }
  
  newtree->Print();
  newfile->Write();
  
  delete chain;
  delete newfile;
}
