/*
 * bs.cpp
 *
 *  Created on: May 31, 2013
 *      Author: maksym
 */
using namespace RooFit;

void bs(){
	gROOT->Reset();
	gROOT->SetStyle("Plain");
	TProof::Open("");

	Float_t BsMass = 5366.77; // ±0.24 meV

	Float_t minMass = 5220;
	Float_t maxMass = 5500;
	Float_t binWidth = 10.;	
	Int_t binN = int((maxMass-minMass)/binWidth);
	RooRealVar varMass("M(#phi#phi)", "M(#phi#phi)", minMass, maxMass, "MeV");
	
	
	
	


// //	unbinned data set:
// 	TChain * DecayTree=new TChain("DecayTree");
// 	DecayTree->Add("Data_phi12/AllStat_mixPhi.root");
// //	chain->Add("Reduced_MagUp2012.root");
// //	chain->Add("Reduced_MagDown2012.root");
	
	TFile* file = new TFile("bsPureAll.root");
	TH1D *histJpsiDiPhi  = (TH1D*) file->Get("histJpsiDiPhi");
	RooDataHist* dset = new RooDataHist("dset", "dset", varMass, histJpsiDiPhi);
	//RooDataSet* dset = new RooDataSet("dset", "dset", DecayTree, varMass);

	RooPlot* frame = varMass.frame(Title("Bs candidates"));

    RooRealVar varBsNumber("N(B_{s})", "varBsNumber", 2560, 0, 1e6);
    
    RooRealVar varBsMass("M(Bs)", "M(Bs)", 5375, 5375-20, 5375+20);
    
    
    RooRealVar varSigmaFactor("varSigmaFactor","varSigmaFactor",2.2);
    RooRealVar varNarrowFraction("varNarrowFraction","varNarrowFraction",0.87);
    
    RooRealVar varBsSigmaN("varBsSigmaN", "varBsSigmaN", 13,5,40);
    RooFormulaVar varBsSigmaW("varBsSigmaW", "varBsSigmaW", "@0*@1", RooArgList(varSigmaFactor, varBsSigmaN));
    RooGaussian pdfBsN("pdfBsN", "pdfBsN", varMass, varBsMass, varBsSigmaN);
    RooGaussian pdfBsW("pdfBsW", "pdfBsW", varMass, varBsMass, varBsSigmaW);
    RooAddPdf pdfBs("pdfBs","pdfBs",RooArgList(pdfBsN,pdfBsW),varNarrowFraction);
	

//	RooRealVar vardMBdBs("M(Bd)", "M(Bd)", BsMass-5279.58);
//	RooFormulaVar varBdMass("M(Bd)", "M(Bd)", "@0-@1", RooArgList(varBsMass, vardMBdBs));
	RooRealVar varBdMass("M(Bd)", "M(Bd)",5279.58);
	RooRealVar BdBsRatio("BdBsRatio", "BdBsRatio",0.01,0,0.5);
	RooFormulaVar varBdNumber("N(B_{d})", "varBdNumber", "@0*@1",RooArgList(varBsNumber, BdBsRatio));
                            
    
    RooGaussian pdfBdN("pdfBdN", "pdfBdN", varMass, varBdMass, varBsSigmaN);
    RooGaussian pdfBdW("pdfBdW", "pdfBdW", varMass, varBdMass, varBsSigmaW);
    RooAddPdf pdfBd("pdfBd","pdfBd",RooArgList(pdfBdN,pdfBdW),varNarrowFraction);



	
	
	RooRealVar varBgrNBs("varBgrNBs", "varBgrNBs", 1000,0, 3e3);
	RooRealVar varC4("varC4", "varC4", -1,1);
	RooExponential pdfBsBgr("pdfBsBgr", "pdfBsBgr", varMass, varC4);
//	RooGenericPdf pdfBsBgr("pdfBsBgr", "pdfBsBgr","1+(@0-5200)*@1", RooArgSet(varMass, varC4));

	RooAddPdf pdfModelBs("pdfModelBs", "pdfModelBs", RooArgList(pdfBs, pdfBd, pdfBsBgr), 
							RooArgList(varBsNumber, varBdNumber, varBgrNBs));
	
	BdBsRatio.setConstant(kTRUE);
	BdBsRatio.setVal(0.022);
	
	RooFitResult* results = pdfModelBs.chi2FitTo(*dset, Strategy(2), Save(true), Minos(true), PrintLevel(0), NumCPU(4));
    results = pdfModelBs.chi2FitTo(*dset, Strategy(2), Save(true), Minos(true), PrintLevel(0), NumCPU(4));
	
	Float_t optNLL = results->minNll();
	RooPlot* frame3 =varMass.frame();

	dset->plotOn(frame3, Binning(binN, minMass, maxMass));
	pdfModelBs.plotOn(frame3);
	RooArgSet showParams;
	showParams.add(varBsNumber);
	showParams.add(varBsSigmaN);
	showParams.add(varBsMass);
	showParams.add(BdBsRatio);
	pdfModelBs.paramOn(frame3, Layout(0.65, 0.95, 0.95), Parameters(showParams), ShowConstants(true));

	TCanvas* canvB = new TCanvas("canvB", "canvB", 1000, 600);
//	canvB->Divide(1, 2);
//	canvB->cd(1);
	frame3->Draw();
	
	showParams.writeToFile("params.txt");

	Float_t x[100], y[100];
	int NPoints = 40;
	
	for(int i=0;i<NPoints;i++)
	{
 	  x[i]=0.002*i;
	  BdBsRatio.setVal(x[i]);
	  results = pdfModelBs.chi2FitTo(*dset, Strategy(2), Save(true), PrintLevel(0), NumCPU(4));
	  y[i] = results->minNll() - optNLL;
	}
	TGraph* NllGr = new TGraph(NPoints, x, y);
	TCanvas* canvGr = new TCanvas("canvGr", "canvGr", 900, 700);
	NllGr->SetLineColor(2);
	NllGr->SetLineWidth(2);
	NllGr->Draw();
	
	TLine* horiz = new TLine(x[0], 4, x[NPoints-1],4);
	horiz->SetLineColor(1);
	horiz->SetLineWidth(2);
	horiz->Draw("same");
 	
 	for(int i=0;i<NPoints;i++)cout<<x[i]<<"   "<<y[i]<<endl;
	
	
}



