// $Id: $
{
  TChain *chain = new TChain("DecayTree");

  chain->Add("All2phi.root");
  
  
  TCut Cut("Jpsi_m_scaled>2700&&Jpsi_m_scaled<3700");

  TFile *newfile = new TFile("All2phi_afterCut.root","recreate");
  TTree *newtree = chain->CopyTree(Cut);
 
  newtree->Print();
  newfile->Write();
  
  delete chain;
  delete newfile;
  
}

