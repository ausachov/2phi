#include <TMath.h>




void main()
{
  gROOT->Reset();
   gStyle->SetOptStat(kFALSE);
 
   TFile* file = new TFile("Data+MC_Res/gr.root");
   TGraphErrors* VarGr = (TGraphErrors*)file->Get("Graph");
   VarGr->GetXaxis()->SetRangeUser(-0.1,0.5);
   VarGr->Draw("AP");
}
