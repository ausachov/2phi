
using namespace RooFit;


void diPhiPure(float cut){
//	gROOT->Reset();
//	gROOT->SetStyle("Plain");
	gStyle->SetOptStat(000);
	TProof::Open("");

 
	Float_t minMassJpsi = 2800;
	Float_t maxMassJpsi = 3150;

	Float_t binWidthJpsi = 10.;
	Int_t binNJpsi = int((maxMassJpsi-minMassJpsi)/binWidthJpsi);
	Float_t minMassPhi = 1009;
	Float_t maxMassPhi = 1031;
	Float_t binWidthPhi = 1.;
	Int_t binNPhi = int((maxMassPhi-minMassPhi)/binWidthPhi);
	
	
	Float_t PhiMass = 1019.46;
	
 	RooRealVar Jpsi_m_scaled("Jpsi_m_scaled", "Jpsi_m_scaled", minMassJpsi, maxMassJpsi, "MeV");
	RooRealVar Phi1_MM_Mix("Phi1_MM_Mix", "Phi1_MM_Mix", minMassPhi, maxMassPhi, "MeV");
	RooRealVar Phi2_MM_Mix("Phi2_MM_Mix", "Phi2_MM_Mix", minMassPhi, maxMassPhi, "MeV");
	RooRealVar Kaon1_ProbNNk("Kaon1_ProbNNk", "Kaon1_ProbNNk", 0,1);
	RooRealVar Kaon2_ProbNNk("Kaon2_ProbNNk", "Kaon2_ProbNNk", 0,1);
	RooRealVar Kaon3_ProbNNk("Kaon3_ProbNNk", "Kaon3_ProbNNk", 0,1);
	RooRealVar Kaon4_ProbNNk("Kaon4_ProbNNk", "Kaon4_ProbNNk", 0,1);

	TH1F* histJpsiDiPhi = new TH1F("histJpsiDiPhi", "histJpsiDiPhi", binNJpsi, minMassJpsi, maxMassJpsi);
	TH1F* histJpsiDiK = new TH1F("histJpsiDiK", "histJpsiDiK", binNJpsi, minMassJpsi, maxMassJpsi);
	TH1F* histJpsiPhiK = new TH1F("histJpsiPhiK", "histJpsiPhiK", binNJpsi, minMassJpsi, maxMassJpsi);
	TH1F* histSigma = new TH1F("histSigma", "histSigma", binNJpsi, minMassJpsi-4*493.677, maxMassJpsi-4*493.677);

	TChain* chain = new TChain("DecayTree");

	
	char IDlabel[200];
	sprintf(IDlabel,"Kaon1_ProbNNk>%f && Kaon2_ProbNNk>%f && Kaon3_ProbNNk>%f && Kaon4_ProbNNk>%f",cut,cut,cut,cut);	
	
	chain->Add("Data/All2phi_afterCut.root");
// 	TCut Cut("Kaon1_ProbNNk>0.1");
// 	TTree* redChain = chain->CopyTree(Cut);
	
	RooDataSet* dsetFull = new RooDataSet("dsetFull", "dsetFull", chain, 
		    RooArgSet(Jpsi_m_scaled, Phi1_MM_Mix, Phi2_MM_Mix, Kaon1_ProbNNk, Kaon2_ProbNNk, Kaon3_ProbNNk, Kaon4_ProbNNk),
					      IDlabel);

	RooRealVar varPhiMass("varPhiMass", "varPhiMass", 1019.46, 1019.46-1, 1019.46+1);
//	RooRealVar varPhiMass("varPhiMass", "varPhiMass", 1019.46);

	  RooRealVar varSigma("varSigma", "varSigma", 1.15);

	RooRealVar varPhiGamma("varPhiGamma", "varPhiGamma", 4.26);
	RooRealVar var2KMass("var2KMass", "var2KMass", 493.67*2);
	

	

	RooGenericPdf pdfPhi1("pdfPhi1", "pdfPhi1", "sqrt(@0-@1)*TMath::Voigt(@0-@2,@3,@4)",
			      RooArgList(Phi1_MM_Mix, var2KMass, varPhiMass,varSigma,varPhiGamma));	
	RooGenericPdf pdfPhi2("pdfPhi2", "pdfPhi2", "sqrt(@0-@1)*TMath::Voigt(@0-@2,@3,@4)",
			      RooArgList(Phi2_MM_Mix, var2KMass, varPhiMass,varSigma,varPhiGamma));
	

// 	  RooRealVar varA1("varA1", "varA1", 0,-0.1,10);
// 	  RooRealVar varA2("varA2", "varA2", 0,-0.1,10);
	
	  RooRealVar varA1("varA1", "varA1", 0);
	  RooRealVar varA2("varA2", "varA2", 0);

	  RooGenericPdf pdfRoot1A1("pdfRoot1A1", "pdfRoot1A1", "sqrt(@0-@1)*(1+(@0-@1)*@2)", RooArgList(Phi1_MM_Mix, var2KMass,varA1));
	  RooGenericPdf pdfRoot1A2("pdfRoot1A2", "pdfRoot1A2", "sqrt(@0-@1)*(1+(@0-@1)*@2)", RooArgList(Phi1_MM_Mix, var2KMass,varA2));
	  RooGenericPdf pdfRoot2A1("pdfRoot2A1", "pdfRoot2A1", "sqrt(@0-@1)*(1+(@0-@1)*@2)", RooArgList(Phi2_MM_Mix, var2KMass,varA1));
	  RooGenericPdf pdfRoot2A2("pdfRoot2A2", "pdfRoot2A2", "sqrt(@0-@1)*(1+(@0-@1)*@2)", RooArgList(Phi2_MM_Mix, var2KMass,varA2));
  
	
	
	RooProdPdf pdfSS("pdfSS", "pdfSS", RooArgSet(pdfPhi1, pdfPhi2));
	RooProdPdf pdfSB("pdfSB", "pdfSB", RooArgSet(pdfPhi1, pdfRoot2A1));
	RooProdPdf pdfBS("pdfBS", "pdfBS", RooArgSet(pdfRoot1A1, pdfPhi2));
	RooProdPdf pdfBB("pdfBB", "pdfBB", RooArgSet(pdfRoot1A2, pdfRoot2A2));
	
	
	
	RooRealVar varNDiPhi("varNDiPhi", "varNDiPhi", 0, 1e7);	
	RooRealVar varNPhiK("varNPhiK", "varNPhiK", 0, 1e7);
	RooRealVar varNDiK("varNDiK", "varNDiK", 0, 1e7);
	
	

	
	RooAddPdf pdfModel1("pdfModel1", "pdfModel1",  RooArgList(pdfPhi1,pdfPhi1,pdfRoot1A1,pdfRoot1A2), 
							RooArgList(varNDiPhi,varNPhiK,varNPhiK,varNDiK));
	RooAddPdf pdfModel2("pdfModel2", "pdfModel2",  RooArgList(pdfPhi2,pdfRoot2A1,pdfPhi2,pdfRoot2A2), 
							RooArgList(varNDiPhi,varNPhiK,varNPhiK,varNDiK));
	


		
	

	
	RooAddPdf pdfModel("pdfModel", "pdfModel",  RooArgList(pdfSS,pdfSB,pdfBS,pdfBB), RooArgList(varNDiPhi,varNPhiK,varNPhiK,varNDiK));


	char label[200];
	Float_t massJpsiLo, massJpsiHi;

	for (Int_t i=0; i<binNJpsi; i++){
		massJpsiLo = minMassJpsi + i*binWidthJpsi;
		massJpsiHi = minMassJpsi + (i+1)*binWidthJpsi;


// 		  varA1.setConstant(kFALSE);
// 		  varA2.setConstant(kFALSE);
		
		varPhiMass.setConstant(kFALSE);
		varNDiK.setConstant(kFALSE);

		
		sprintf(label, "Jpsi_m_scaled>%i&&Jpsi_m_scaled<%i", massJpsiLo, massJpsiHi);
		RooDataSet* dset = dsetFull->reduce(Cut(label), Name("dset"), Title("dset"));
		
		varNDiPhi.setVal(dset->numEntries()/1.5);
		varNDiK.setVal(dset->numEntries()/3);
		varNPhiK.setVal(dset->numEntries()/4);

		varNDiPhi.setMax(Double_t(dset->numEntries()));	
		varNDiK.setMax(Double_t(dset->numEntries()));	
		varNPhiK.setMax(Double_t(dset->numEntries()));	
		
		
		RooFitResult* res = pdfModel.fitTo(*dset, Save(true), PrintLevel(0));
		res = pdfModel.fitTo(*dset, Save(true), PrintLevel(0));
		
		Double_t EDM = res->edm();
		
		if(EDM>2e-3)
		{
		  varPhiMass.setConstant(kTRUE);
		  varPhiMass.setVal(PhiMass);
		  res = pdfModel.fitTo(*dset, Save(true), PrintLevel(0));
		  res = pdfModel.fitTo(*dset, Save(true), PrintLevel(0));
		  EDM = res->edm();  
		}		
		

		
		if(EDM>2e-3)
		{
		    if(TMath::Abs(varA2.getVal(0))<(varA2.getError()/2))
		    {
		      varA2.setConstant(kTRUE);
		      varA2.setVal(0);
		      res = pdfModel.fitTo(*dset, Save(true), PrintLevel(0));
		      EDM = res->edm();
		    }
				    
	    
		    if(TMath::Abs(varA1.getVal(0))<(varA1.getError()/2))
		    {
		      varA1.setConstant(kTRUE);
		      varA1.setVal(0);
		      res = pdfModel.fitTo(*dset, Save(true), PrintLevel(0));
		      EDM = res->edm();
		    }
		    
		    if(EDM>2e-3)
		    {
		      varPhiMass.setConstant(kTRUE);
		      varPhiMass.setVal(PhiMass);
		      res = pdfModel.fitTo(*dset, Save(true), PrintLevel(0));
		      EDM = res->edm();  
		    }
		    
		    if(EDM>2e-3)
		    {
		      varNDiK.setConstant(kTRUE);
		      varNDiK.setVal(0);
		      varA2.setConstant(kTRUE);
		      varA2.setVal(0);
		      res = pdfModel.fitTo(*dset, Save(true), PrintLevel(0));
		      EDM = res->edm();  
		    }
		
// 		    if(EDM>2e-3)
// 		    {
// 		      varNPhiK.setVal(0);
// 		      res = pdfModel.fitTo(*dset, Save(true), PrintLevel(0));
// 		      EDM = res->edm(); 
// 		    }
		     
		}
		
		

		res = pdfModel.fitTo(*dset, Save(true), PrintLevel(0));
		EDM = res->edm(); 
		
		if(TMath::Abs(varNDiK.getVal())<(varNDiK.getError()/2.))
		{
		  varNDiK.setConstant(kTRUE);
		  res = pdfModel.fitTo(*dset, Save(true), PrintLevel(0));
		}		
		
		histJpsiDiPhi->SetBinContent(i+1, varNDiPhi.getVal());
		histJpsiDiPhi->SetBinError(i+1, varNDiPhi.getError());

		histJpsiDiK->SetBinContent(i+1, varNDiK.getVal());
		histJpsiDiK->SetBinError(i+1, varNDiK.getError());

		histJpsiPhiK->SetBinContent(i+1, varNPhiK.getVal());
		histJpsiPhiK->SetBinError(i+1, varNPhiK.getError());
		
		histSigma->SetBinContent(i+1, varSigma.getVal());
		histSigma->SetBinError(i+1, varSigma.getError());
		

		if(EDM>2e-3)return;
		


		delete dset;
		delete res;
	}

	
	




	TFile file("diPhiPure.root","recreate");
	  

	    
	TCanvas* canvA = new TCanvas("canvA", "canvA", 800, 700);
	canvA->Divide(1, 3);
	canvA->cd(1);
	histJpsiDiPhi->Draw();
	canvA->cd(2);
	histJpsiPhiK->Draw();
	canvA->cd(3);
	histJpsiDiK->Draw();		  
	    
	    
	histJpsiDiPhi->Write();
	histJpsiDiK->Write();
	histJpsiPhiK->Write();
	histSigma->Write();
	file.Close();
	
	delete chain;
	delete canvA;
	delete dsetFull;
	delete histJpsiDiK;
	delete histJpsiDiPhi;
	delete histJpsiPhiK;
	
}





Double_t fit(char* label, double signal){
// 	gROOT->Reset();
	gROOT->SetStyle("Plain");
	TProof::Open("");

	Float_t etacMass =	2983.6; 
	Float_t chi0Mass =	3414.75; 
	Float_t chi1Mass =	3510.66; 
	Float_t hcMass   = 	3525.38; 
	Float_t chi2Mass =	3556.2; 
	Float_t etac2Mass =	3639.4; 
	Float_t x3872Mass = 	3871.69; 
	Float_t x3915Mass = 	3918.4; 
	Float_t x3927Mass = 	3927.2; 
	
	Float_t etacMassError =	0.7; 
	Float_t chi0MassError =	0.31; 
	Float_t chi1MassError =	0.07; 
	Float_t hcMassError   = 0.11; 
	Float_t chi2MassError =	0.09; 
	Float_t etac2MassError =1.3; 
	Float_t x3872MassError = 0.17; 
	Float_t x3915MassError = 1.9; 
	Float_t x3927MassError = 2.6; 
	
	Float_t etacGamma =	32.2; 
	Float_t chi0Gamma =	10.5; 
	Float_t chi1Gamma =	0.84; 
	Float_t hcGamma   = 	0.7; 
	Float_t chi2Gamma =	1.93; 
	Float_t etac2Gamma =	11.3;

	Float_t x3915Gamma = 	20; 
	Float_t x3927Gamma = 	24; 	
	
	Float_t etacGammaError =	0.9; 
	Float_t chi0GammaError =	0.6; 
	Float_t chi1GammaError =	0.04; 
	Float_t hcGammaError   = 	0.4; 
	Float_t chi2GammaError =	0.11; 
	Float_t etac2GammaError =	3.1; 
	Float_t x3915GammaError = 	5; 
	Float_t x3927GammaError = 	6; 
	
	Float_t EtacSigmaN_MC = 6.311;



	Float_t SigmaPar = 0.224;
	
	Float_t minMass = 2800;
	Float_t maxMass = 3150;
	Float_t binWidth = 10.;
	Int_t binN = int((maxMass-minMass)/binWidth);
	RooRealVar Jpsi_m_scaled("Jpsi_m_scaled", "Jpsi_m_scaled", minMass, maxMass, "MeV");
    	RooRealVar varNDiPhi_sw("varNDiPhi_sw", "varNDiPhi_sw", -5, 5);
	Jpsi_m_scaled.setBins(10000);
	
	Float_t Centre = (minMass+maxMass)/2;


	TFile* file = new TFile("diPhiPure.root");


	
	

	TH1D *histJpsiDiPhi  = (TH1D*) file->Get("histJpsiDiPhi");
	RooDataHist* DataSet = new RooDataHist("DataSet", "DataSet", Jpsi_m_scaled, histJpsiDiPhi);


	RooArgList listComp, listNorm, listComp2;
	RooArgSet showParams;

	
	
	RooRealVar FourKMass("FourKMass", "FourKMass", 4*493.677);
	RooRealVar varSigmaFactor("varSigmaFactor","varSigmaFactor",2.16);
	RooRealVar FitMin("FitMin","FitMin",minMass);
	
	RooRealVar varNarrowFraction("varNarrowFraction","varNarrowFraction",0.87);
//  	RooRealVar ASig("ASig", "ASig", SigmaPar, 1e-1, 1);
	RooRealVar ASig("ASig", "ASig", SigmaPar,0.1,0.3);


	RooRealVar radius("radius", "radius", 1.5);
  
	RooRealVar massa("massa", "massa", 1019.46, "MeV");
	RooRealVar massb("massb", "massb", 1019.46, "MeV");
	RooRealVar ConvCentre("ConvCentre","ConvCentre",0) ;	
	
	showParams.add(ASig);
	showParams.add(varNarrowFraction);

//========== fitting model components:
	RooRealVar varEtacNumber("N(#eta_{c}(1S))", "varEtacNumber", 6800, 1e3, 1e5);
	RooRealVar varEtacMass("M(#eta_{c})", "varEtacMass", 2984, 2975, 2989);
	RooRealVar varEtacGamma("varEtacGamma", "varEtacGamma", 32.2);

	RooFormulaVar varEtacSigmaN("varEtacSigmaN", "varEtacSigmaN", "sqrt(@1-@2)*(@0)", RooArgList(ASig, varEtacMass, FourKMass));

	RooFormulaVar varEtacSigmaW("varEtacSigmaW", "varEtacSigmaW", "@0*@1", RooArgList(varSigmaFactor, varEtacSigmaN));

	RooRelBreitWigner pdfEtacBWrel = RooRelBreitWigner("pdfEtacBWrel", "pdfEtacBWrel", Jpsi_m_scaled, varEtacMass, varEtacGamma,
			RooConst(0), radius, massa, massb);

	RooGaussian pdfEtacGaussN("pdfEtacGaussN","pdfEtacGaussN",Jpsi_m_scaled,ConvCentre,varEtacSigmaN);
	RooGaussian pdfEtacGaussW("pdfEtacGaussW","pdfEtacGaussW",Jpsi_m_scaled,ConvCentre,varEtacSigmaN);
	
	RooFFTConvPdf pdfEtacN("pdfEtacN","pdfEtacN",Jpsi_m_scaled,pdfEtacBWrel,pdfEtacGaussN) ;
	RooFFTConvPdf pdfEtacW("pdfEtacW","pdfEtacW",Jpsi_m_scaled,pdfEtacBWrel,pdfEtacGaussW) ;
	
	RooAddPdf pdfEtac("pdfEtac","pdfEtac",RooArgList(pdfEtacN,pdfEtacW),varNarrowFraction);	

	listComp.add(pdfEtac);
	listComp2.add(pdfEtac);
	listNorm.add(varEtacNumber);
	showParams.add(varEtacNumber);
	showParams.add(varEtacMass);
	showParams.add(varEtacGamma);

	


	RooRealVar varBgrNumber("N_{bgr}", "number of background events", 6.5e4, 1e4, 1e5);

	RooRealVar varE0("varE0", "varE0", -3e-3, -1e-1, 0);
	RooRealVar varA0("varA0", "varA0", 1e-3,-1e-1, +1e-1);
	RooRealVar varA1("varA1", "varA1", 5e-6,-1e-3, +1e-3);
	
	RooRealVar varB0("varB0", "varB0", -1e-3, -1e-1, 1e-1);
	RooRealVar varB1("varB1", "varB1", 5e-7, -1, 1);
	

	
	RooRealVar var2PhiMass("var2PhiMass", "var2PhiMass", 2038.9);



 	RooGenericPdf pdfBgr("pdfBgr", "pdfBgr", "sqrt(@0-@1)*exp(@0*@3)",
    		        RooArgSet(Jpsi_m_scaled, var2PhiMass, FitMin, varE0));	


	listComp.add(pdfBgr);
	listComp2.add(pdfBgr);
	listNorm.add(varBgrNumber);

	RooAddPdf pdfModel("pdfModel", "pdfModel", listComp, listNorm);
	RooAddPdf pdfModel2("pdfModel2", "pdfModel2", listComp2, listNorm);
	
	RooFitResult* results = pdfModel.fitTo(*DataSet/*, ExternalConstraints(GMConstraint)*//*, Minos(true)*/, Save(true), PrintLevel(0), NumCPU(4));
	results = pdfModel.chi2FitTo(*DataSet,Extended(varEtacNumber.getVal()+varBgrNumber.getVal()), PrintLevel(0));


	
	RooPlot* frame = Jpsi_m_scaled.frame(Title("Jpsi_m_scaled"));
	DataSet->plotOn(frame, Binning(binN, minMass, maxMass));

	pdfModel.plotOn(frame);
	pdfModel.paramOn(frame, Layout(0.65, 0.95, 0.95), Parameters(showParams), ShowConstants(true), Format("NEALU",AutoPrecision(1)));
	frame->getAttText()->SetTextSize(0.03) ;
  
	


	char filename [200];
	sprintf(filename,"%s_prior.pdf",label);
	
	TCanvas* canvA = new TCanvas("canvA", "canvA", 1000, 600);
	frame->Draw();
	canvA->SaveAs(filename);
	

	
	
	varEtacNumber.setVal(signal);
	varEtacNumber.setConstant(kTRUE);
	
	RooDataSet *dataRes = pdfModel.generate(Jpsi_m_scaled);
	
	varEtacNumber.setConstant(kFALSE);
	results = pdfModel.fitTo(*dataRes, PrintLevel(0));
	
	
	RooPlot* frame2 = Jpsi_m_scaled.frame(Title("Jpsi_m_scaled"));
	dataRes->plotOn(frame2,Binning(binN, minMass, maxMass));
	pdfModel.plotOn(frame2);
	pdfModel.paramOn(frame2, Layout(0.65, 0.95, 0.95), Parameters(showParams), ShowConstants(true), Format("NEALU",AutoPrecision(1)));

	
	sprintf(filename,"%s.pdf",label);
	
	TCanvas* canvB = new TCanvas("canvB", "canvB", 1000, 600);
	frame2->Draw();
	canvB->SaveAs(filename);

	file->Close();
	delete DataSet;
	delete canvA;
	delete file;
	
	return varEtacNumber.getVal()/varEtacNumber.getError();
}


void Optimize()
{
    gROOT->Reset(); 
    
    Int_t NPoints = 20;
    Double_t min, max, step;
    min=0;
    max=0.4;
    step = (max-min)/double(NPoints);
    
    Double_t x[100], y[100],ex[100], ey[100];
    for(Int_t i=0; i<NPoints;i++)
    {
        float cut = min+step*i;

              
 	char label[200];
 	sprintf(label, "Kaon1_ProbNNk>%3.3f && Kaon2_ProbNNk>%3.3f && Kaon3_ProbNNk>%3.3f && Kaon4_ProbNNk>%3.3f",cut,cut,cut,cut);
	
	TCut Cut(label);
	
	TChain *MCChain = new TChain("DecayTree");
 	MCChain->Add("Data/AllMC_Reduced.root");
	TTree *MCTree = MCChain->CopyTree(Cut);
	Int_t Signal = MCTree->GetEntries();
 	delete MCChain;


	
	char pureCommand [100];
// 	gROOT->ProcessLine(".L diPhiPure.cpp");
// 	sprintf(pureCommand, "diPhiPure(%f)",cut);
	diPhiPure(cut);


	
	
	double signal = Signal*6610./6368.;
	Double_t Efficiency = fit(label,signal);
	
	x[i]=cut;
	ex[i]=0;
	y[i]=Efficiency;
	ey[i]=1;
    }
    
    TGraphErrors* VarGr = new TGraphErrors(NPoints, x, y, ex ,ey);
    VarGr->SetMarkerStyle(8);
    VarGr->SetMarkerColor(1);
    VarGr->Draw("AP");
    VarGr->SaveAs("gr.root");
    
    for(int i=0;i<NPoints;i++)cout<<x[i]<<"  "<<y[i]<<endl;
}
