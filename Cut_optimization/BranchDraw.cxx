#include <TMath.h>




void main()
{
  gROOT->Reset();
   gStyle->SetOptStat(kFALSE);
  
  Double_t range0=0;
  Double_t range1=200;
  Int_t nch=50;
  
  TChain * DecayTree=new TChain("DecayTree");
  DecayTree->Add("Data/All2phi.root");
  const Int_t NEn=DecayTree->GetEntries();
  Double_t Branch_Value;
  DecayTree->SetBranchAddress("Jpsi_FDCHI2_OWNPV",&Branch_Value);

  TH1D * m_phiphi_hist = new TH1D("3#phi candidates","3#phi candidates" , nch, range0, range1);
  for (Int_t i=0; i<NEn; i++) 
  {
	  DecayTree->GetEntry(i);
	  m_phiphi_hist->Fill(Branch_Value);
  }
  
  TCanvas *canv1 = new TCanvas("canv1","Jpsi_FDCHI2_OWNPV",5,85,800,600);
//   m_phiphi_hist->SetAxisRange(5,50);
  m_phiphi_hist->SetXTitle("Jpsi_FDCHI2_OWNPV");
  m_phiphi_hist->DrawCopy();
  
//   TCanvas *canv2 = new TCanvas("canv2","Jpsi_FDCHI2_OWNPV",5,85,1200,800);
//   m_phiphi_hist->SetAxisRange(6000,8000);
//   m_phiphi_hist->DrawCopy();
//   
//   TCanvas *canv3 = new TCanvas("canv3","Jpsi_FDCHI2_OWNPV",5,85,1200,800);
//   m_phiphi_hist->SetAxisRange(8000,10000);
//   m_phiphi_hist->DrawCopy();
//   
//   TCanvas *canv4 = new TCanvas("canv4","Jpsi_FDCHI2_OWNPV",5,85,1200,800);
//   m_phiphi_hist->SetMaximum(50);
//   m_phiphi_hist->SetAxisRange(10000,12000);
//   m_phiphi_hist->DrawCopy();
}
