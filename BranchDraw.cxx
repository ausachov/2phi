#include <TMath.h>




void BranchDraw()
{
  gROOT->Reset();
  //gStyle->SetOptStat(kFALSE);
  
  Double_t range0=-1.5;
  Double_t range1=15;
  Int_t nch=200;
  
  TChain * DecayTree=new TChain("DecayTree");
  DecayTree->Add("2phi_afterCut.root");
  const Int_t NEn=DecayTree->GetEntries();
  Double_t Branch_Value, Branch_Value1, jpsi_pt;
  DecayTree->SetBranchAddress("Jpsi_PT",&jpsi_pt);
    
    
  Double_t Jpsi_ENDVERTEX_Z,Jpsi_OWNPV_Z,Jpsi_MM,Jpsi_PZ;
    
DecayTree->SetBranchAddress("Jpsi_ENDVERTEX_Z",&Jpsi_ENDVERTEX_Z);
DecayTree->SetBranchAddress("Jpsi_OWNPV_Z",&Jpsi_OWNPV_Z);
DecayTree->SetBranchAddress("Jpsi_MM",&Jpsi_MM);
DecayTree->SetBranchAddress("Jpsi_PZ",&Jpsi_PZ);

  TH1D * m_phiphi_hist = new TH1D("Data","Data" , nch, range0, range1);

  for (Int_t i=0; i<NEn; i++) 
  {
	  DecayTree->GetEntry(i);
      if(Jpsi_MM>2950 && Jpsi_MM<3050)
        m_phiphi_hist->Fill((3.3)*(Jpsi_ENDVERTEX_Z-Jpsi_OWNPV_Z)*Jpsi_MM/Jpsi_PZ);
      
  }
    
  
    
  TCanvas *canv1 = new TCanvas("canv1","B_MM",5,85,800,600);
//   m_phiphi_hist->SetAxisRange(5,50);
  m_phiphi_hist->SetXTitle("B_MM");
  m_phiphi_hist->DrawCopy();
  
//   TCanvas *canv2 = new TCanvas("canv2","B_MM",5,85,1200,800);
//   m_phiphi_hist->SetAxisRange(6000,8000);
//   m_phiphi_hist->DrawCopy();
//   
//   TCanvas *canv3 = new TCanvas("canv3","B_MM",5,85,1200,800);
//   m_phiphi_hist->SetAxisRange(8000,10000);
//   m_phiphi_hist->DrawCopy();
//   
//   TCanvas *canv4 = new TCanvas("canv4","B_MM",5,85,1200,800);
//   m_phiphi_hist->SetMaximum(50);
//   m_phiphi_hist->SetAxisRange(10000,12000);
//   m_phiphi_hist->DrawCopy();
}
