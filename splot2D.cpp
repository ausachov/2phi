/*
 * splot2D.cpp
 *
 *  Created on: Jun 6, 2013
 *      Author: maksym
 */
using namespace RooFit;
using namespace RooStats;


//0 - Etac
//1 - Chic

void splot2D(int nCharmonia){
	int Type=1;
	
	gROOT->Reset();
	gROOT->SetStyle("Plain");
	gStyle->SetOptStat(000);
	TProof::Open("");

    
    switch(nCharmonia)
    {
        case 0:
            Float_t minMassJpsi = 2800;
            Float_t maxMassJpsi = 3950;
            break;
        case 1:
            Float_t minMassJpsi = 3300;
            Float_t maxMassJpsi = 3800;
            break;
    }
    
	Float_t binWidthJpsi = 10.;
	Int_t binNJpsi = int((maxMassJpsi-minMassJpsi)/binWidthJpsi);
	Float_t minMassPhi = 1009;
	Float_t maxMassPhi = 1031;
	Float_t binWidthPhi = 1.;
	Int_t binNPhi = int((maxMassPhi-minMassPhi)/binWidthPhi);
	
	
	Float_t PhiMass = 1019.46;
	RooRealVar Jpsi_m_scaled("Jpsi_m_scaled", "Jpsi_m_scaled", minMassJpsi, maxMassJpsi, "MeV");
	RooRealVar Jpsi_PT("Jpsi_PT", "Jpsi_PT", 0, 25e3, "MeV");
    RooRealVar Jpsi_ETA("Jpsi_ETA", "Jpsi_ETA", 1, 7);
    RooRealVar nTracks("nTracks", "nTracks", 0, 600);
    RooRealVar Jpsi_Phi1_CosThetaABS_Mix("Phi1_CosThetaABS_Mix", "Phi1_CosThetaABS_Mix", 0, 1);
	RooRealVar Jpsi_Y("Jpsi_Y", "Jpsi_Y", 2, 4.5, "MeV");
	RooRealVar Phi1_m_scaled_Mix("Phi1_m_scaled_Mix", "Phi1_m_scaled_Mix", minMassPhi, maxMassPhi, "MeV");
	RooRealVar Phi2_m_scaled_Mix("Phi2_m_scaled_Mix", "Phi2_m_scaled_Mix", minMassPhi, maxMassPhi, "MeV");
	TH1F* histJpsiDiPhi = new TH1F("histJpsiDiPhi", "histJpsiDiPhi", binNJpsi, minMassJpsi, maxMassJpsi);
	TH1F* histJpsiDiK = new TH1F("histJpsiDiK", "histJpsiDiK", binNJpsi, minMassJpsi, maxMassJpsi);
	TH1F* histJpsiPhiK = new TH1F("histJpsiPhiK", "histJpsiPhiK", binNJpsi, minMassJpsi, maxMassJpsi);

	TChain* chain = new TChain("DecayTree");
	chain->Add("2phi_afterCut.root");


	RooDataSet* dsetFull = new RooDataSet("dsetFull", "dsetFull", chain, RooArgSet(Jpsi_m_scaled, Phi1_m_scaled_Mix, Phi2_m_scaled_Mix, Jpsi_PT, Jpsi_ETA, Jpsi_Phi1_CosThetaABS_Mix,nTracks),"");


	RooRealVar varPhiMass("varPhiMass", "varPhiMass", 1019.46, 1019.46-1, 1019.46+1);
//	RooRealVar varPhiMass("varPhiMass", "varPhiMass", 1019.46);
	if(Type==0)
	  RooRealVar varSigma("varSigma", "varSigma", 1.20);
	if(Type==2)
	  RooRealVar varSigma("varSigma", "varSigma", 1.15);
	if(Type==1)
	  RooRealVar varSigma("varSigma", "varSigma", 1, 0.1, 4);
	RooRealVar varPhiGamma("varPhiGamma", "varPhiGamma", 4.26);
	RooRealVar var2KMass("var2KMass", "var2KMass", 493.67*2);
	

	RooRealVar varA1("varA1", "varA1", 0,0,1);
	RooRealVar varA2("varA2", "varA2", 0);
	

	RooGenericPdf pdfPhi1("pdfPhi1", "pdfPhi1", "sqrt(@0-@1)*TMath::Voigt(@0-@2,@3,@4)",
			      RooArgList(Phi1_m_scaled_Mix, var2KMass, varPhiMass,varSigma,varPhiGamma));	
	RooGenericPdf pdfPhi2("pdfPhi2", "pdfPhi2", "sqrt(@0-@1)*TMath::Voigt(@0-@2,@3,@4)",
			      RooArgList(Phi2_m_scaled_Mix, var2KMass, varPhiMass,varSigma,varPhiGamma));

	RooGenericPdf pdfRoot1A1("pdfRoot1A1", "pdfRoot1A1", "sqrt(@0-@1)*(1+(@0-@1)*@2)", RooArgList(Phi1_m_scaled_Mix, var2KMass,varA1));
	RooGenericPdf pdfRoot1A2("pdfRoot1A2", "pdfRoot1A2", "sqrt(@0-@1)*(1+(@0-@1)*@2)", RooArgList(Phi1_m_scaled_Mix, var2KMass,varA2));
	RooGenericPdf pdfRoot2A1("pdfRoot2A1", "pdfRoot2A1", "sqrt(@0-@1)*(1+(@0-@1)*@2)", RooArgList(Phi2_m_scaled_Mix, var2KMass,varA1));
	RooGenericPdf pdfRoot2A2("pdfRoot2A2", "pdfRoot2A2", "sqrt(@0-@1)*(1+(@0-@1)*@2)", RooArgList(Phi2_m_scaled_Mix, var2KMass,varA2));
	
	
	RooProdPdf pdfSS("pdfSS", "pdfSS", RooArgSet(pdfPhi1, pdfPhi2));
	RooProdPdf pdfSB("pdfSB", "pdfSB", RooArgSet(pdfPhi1, pdfRoot2A1));
	RooProdPdf pdfBS("pdfBS", "pdfBS", RooArgSet(pdfRoot1A1, pdfPhi2));
	RooProdPdf pdfBB("pdfBB", "pdfBB", RooArgSet(pdfRoot1A2, pdfRoot2A2));
	
	
	
	RooRealVar varNDiPhi("varNDiPhi", "varNDiPhi", 0, 1e7);	
	RooRealVar varNPhiK("varNPhiK", "varNPhiK", 0, 1e7);
	RooRealVar varNDiK("varNDiK", "varNDiK", 0, 1e7);
	
	

	
	RooAddPdf pdfModel1("pdfModel1", "pdfModel1",  RooArgList(pdfPhi1,pdfPhi1,pdfRoot1A1,pdfRoot1A2), 
							RooArgList(varNDiPhi,varNPhiK,varNPhiK,varNDiK));
	RooAddPdf pdfModel2("pdfModel2", "pdfModel2",  RooArgList(pdfPhi2,pdfRoot2A1,pdfPhi2,pdfRoot2A2), 
							RooArgList(varNDiPhi,varNPhiK,varNPhiK,varNDiK));

	
	RooAddPdf pdfModel("pdfModel", "pdfModel",  RooArgList(pdfSS,pdfSB,pdfBS,pdfBB), RooArgList(varNDiPhi,varNPhiK,varNPhiK,varNDiK));



	RooFitResult* res = pdfModel.fitTo(*dsetFull, Save(true), PrintLevel(0));
	varPhiMass.setConstant(kTRUE);
	varSigma.setConstant(kTRUE);
    varA1.setConstant(kTRUE);
	SPlot* sData1 = new SPlot("sData1", "sData1", *dsetFull, &pdfModel, RooArgList(varNDiPhi, varNPhiK, varNDiK));

//	RooArgSet* argSet = (RooArgSet*) dsetFull->get();
//	RooRealVar* Phi1S_sw = (RooRealVar*) argSet->find("Phi1S_sw");
//	RooRealVar* Phi2S_sw = (RooRealVar*) argSet->find("Phi2S_sw");
//	RooFormulaVar phi("phi", "phi", "@0*@1", RooArgList(*Phi1S_sw, *Phi2S_sw));
//	dsetFull->addColumn(phi);

	RooDataSet * dset = new RooDataSet(dsetFull->GetName(),
			dsetFull->GetTitle(),dsetFull,*dsetFull->get(),0,"varNDiPhi_sw") ;

	RooPlot* frame1 = Phi1_m_scaled_Mix.frame(Title("#phi_{1} mass"));
	dsetFull->plotOn(frame1, Binning(binNPhi, minMassPhi, maxMassPhi));
	pdfModel1.plotOn(frame1);
	pdfModel.plotOn(frame1, Components(pdfBS), LineStyle(kDashed));

	RooPlot* frame2 = Phi2_m_scaled_Mix.frame(Title("#phi_{2} mass"));
	dsetFull->plotOn(frame2, Binning(binNPhi, minMassPhi, maxMassPhi));
	pdfModel2.plotOn(frame2);
	pdfModel.plotOn(frame2, Components(pdfSB), LineStyle(kDashed));

	TCanvas* canvA = new TCanvas("canvA", "canvA", 1000, 400);
	canvA->Divide(3, 1);
	canvA->cd(1);
	TH2D* histViz = pdfModel.createHistogram("Phi1_m_scaled_Mix, Phi2_m_scaled_Mix");
	histViz->Draw("surf");
	canvA->cd(2);
	frame1->Draw();
	canvA->cd(3);
	frame2->Draw();
	canvA->SaveAs("diKMass.png");

	RooPlot* frame3 = Jpsi_m_scaled.frame(Title("#phi#phi mass"));
	dset->plotOn(frame3,Binning(binNJpsi, minMassJpsi, maxMassJpsi));
//	dsetFull->plotOn(frame3);
	TCanvas* canvB = new TCanvas("canvB", "canvB", 800, 400);
	frame3->Draw();
    
    
    RooPlot* frame4 = Phi1_m_scaled_Mix.frame(Title("#phi#phi mass"));
    dset->plotOn(frame4,DataError(RooAbsData::SumW2));
    //	dsetFull->plotOn(frame3);
    TCanvas* canvC = new TCanvas("canvC", "canvC", 800, 400);
    frame4->Draw();

    switch(nCharmonia)
    {
        case 0:
            TFile file("SplotEtac.root","recreate");
            break;
        case 1:
            TFile file("SplotChic.root","recreate");
            break;
    }

	TTree* tree = dset->tree();
	tree->SetName("DecayTree");
	tree->SetTitle("DecayTree");
	tree->Write();
	file.Close();
	
	
}

appendBranch(int nCharmonia)
{
    TChain *chain = new TChain("DecayTree");
    
    switch(nCharmonia)
    {
        case 0:
            chain->Add("SplotEtac.root");
            TFile *newfile = new TFile("SplotAddEtac.root","recreate");
            break;
        case 1:
            chain->Add("SplotChic.root");
            TFile *newfile = new TFile("SplotAddChic.root","recreate");
            break;
    }
    
    TTree *newtree = chain->CopyTree("");
    Double_t varNDiPhi_sw, varNDiK_sw,varNPhiK_sw,MassWeighted,Jpsi_m_scaled,diPhiWeight,diphiweight;

    
    chain->SetBranchAddress("varNDiPhi_sw",&varNDiPhi_sw);
    chain->SetBranchAddress("varNDiK_sw",&varNDiK_sw);
    chain->SetBranchAddress("varNPhiK_sw",&varNPhiK_sw);
    chain->SetBranchAddress("Jpsi_m_scaled",&Jpsi_m_scaled);
    TBranch *MassWeighted_Branch = newtree->Branch("MassWeighted", &MassWeighted, "MassWeighted/D");
    TBranch *diPhiWeight_Branch = newtree->Branch("diPhiWeight", &diPhiWeight, "diPhiWeight/D");
    TBranch *diphiweight_Branch = newtree->Branch("diphiweight", &diphiweight, "diphiweight/D");

    Long64_t NEntries = newtree->GetEntries();
    
    for (Long64_t i = 0; i < NEntries; i++)
    {
        chain->GetEntry(i);
        diPhiWeight =(varNDiPhi_sw);
        diphiweight =(varNDiPhi_sw);
        MassWeighted = varNDiPhi_sw*Jpsi_m_scaled;
        MassWeighted_Branch->Fill();
        diPhiWeight_Branch->Fill();
        diphiweight_Branch->Fill();
    }
    
    newtree->Print();
    newfile->Write();
    
    delete chain;
    delete newfile;

}



