#include <TMath.h>


void cosTheta_comp()
{
    gROOT->Reset();
    gStyle->SetOptStat(001);
    
    Double_t Cos1, Cos2, Cos3;
    
    Double_t range0=-1;
    Double_t range1=1;
    Int_t nch=20;
    Double_t binWidth = (range1-range0)/nch;
    
    
    
    Double_t BMass;


    
    TChain * DecayTreeMC=new TChain("MCDecayTreeTuple/MCDecayTree");
    
    //DecayTreeMC->Add("../MC/CAL_0/Etac2/AllMC_Reduced.root");
    DecayTreeMC->Add("../MC/GenSpectr/Chic0/res.root");

    Double_t Mrange0=2890;
    Double_t Mrange1=3100;
    Double_t bin_width=10.;
    

    
    Int_t Mnch=int((Mrange1-Mrange0)/bin_width);

    
    const Int_t NEnMC=DecayTreeMC->GetEntries();
    
    //DecayTreeMC->SetBranchAddress("Jpsi_m_scaled",&BMass);
    
    DecayTreeMC->SetBranchAddress("Phi1_TRUECosTheta",&Cos1);
    DecayTreeMC->SetBranchAddress("Phi2_TRUECosTheta",&Cos2);
    
    
    
    
    
    
    TH1D * mcPHSP_hist = new TH1D("mcPHSP_hist","mcPHSP_hist" , nch, range0, range1);
    TH1D * mc1_hist = new TH1D("","" , nch, range0, range1);
    TH1D * mc2_hist = new TH1D("","" , nch, range0, range1);
    
    
    TH1D * mcPHSP_BMasshist = new TH1D("mcPHSP_BMasshist","mcPHSP_BMasshist" , Mnch, Mrange0, Mrange1);
    TH1D * mc1_BMasshist = new TH1D("mc1_BMasshist","mc1_BMasshist" , Mnch, Mrange0, Mrange1);
    TH1D * mc2_BMasshist = new TH1D("mc2_BMasshist","mc2_BMasshist" , Mnch, Mrange0, Mrange1);
    
    for (Int_t i=0; i<NEnMC; i++)
    {
        DecayTreeMC->GetEntry(i);
        mcPHSP_hist->Fill(Cos1);
        mcPHSP_hist->Fill(Cos2);
        
        mc1_hist->Fill(Cos1,3*Cos1*Cos1/2);
        mc1_hist->Fill(Cos2,3*Cos2*Cos2/2);
        
        mc2_hist->Fill(Cos1,3*(1-Cos1*Cos1)/4);
        mc2_hist->Fill(Cos2,3*(1-Cos2*Cos2)/4);
        

    }
    
    
    
    

    
    cout<<"BLUE mc1_hist Integral/PHSP = "<<100*(2*mc1_hist->Integral()/mcPHSP_hist->Integral()-1)<<" %"<<endl;
    mc1_hist->Scale(2*1./mc1_hist->Integral());
    mc1_hist->SetLineColor(kBlue);
    mc1_hist->SetLineWidth(2);
    mc1_hist->DrawCopy();
    cout<<"mc1_hist Integral = "<<mc1_hist->Integral()<<endl;
    
    cout<<"GREEN mc2_hist Integral/PHSP = "<<100*(2*mc2_hist->Integral()/mcPHSP_hist->Integral()-1)<<" %"<<endl;
    mc2_hist->Scale(2*1./mc2_hist->Integral());
    mc2_hist->SetLineColor(kGreen+2);
    mc2_hist->SetLineWidth(2);
    mc2_hist->DrawCopy("same");
    cout<<"mc2_hist Integral = "<<mc2_hist->Integral()<<endl;
    
    cout<<"mcPHSP_hist Integral = "<<mcPHSP_hist->Integral()/2<<endl;
    mcPHSP_hist->Scale(1./NEnMC);
    mcPHSP_hist->SetLineColor(kRed);
    mcPHSP_hist->SetLineWidth(2);
    mcPHSP_hist->Draw("same");
    cout<<"mcPHSP_hist Integral = "<<mcPHSP_hist->Integral()<<endl;
    
    
    
}
