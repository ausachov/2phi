

using namespace RooFit;

//0 - Base
//1 - Fix M(Chic) to PDG
//2 - MC Sigma For Etac
//3 - Parabolic BG
//4 - f0 in 2D
//5 - add X's to fit and hc


void fit_ratio_to_etac(int Type){
	gROOT->Reset();
	gROOT->SetStyle("Plain");
	TProof::Open("");

	Float_t etacMass =	2983.6; 
	Float_t chi0Mass =	3414.75; 
	Float_t chi1Mass =	3510.66; 
	Float_t hcMass   = 	3525.38; 
	Float_t chi2Mass =	3556.2; 
	Float_t etac2Mass =	3639.4; 
	Float_t x3872Mass = 	3871.69; 
	Float_t x3915Mass = 	3918.4; 
	Float_t x3927Mass = 	3927.2; 
	
	Float_t etacMassError =	0.7; 
	Float_t chi0MassError =	0.31; 
	Float_t chi1MassError =	0.07; 
	Float_t hcMassError   = 0.11; 
	Float_t chi2MassError =	0.09; 
	Float_t etac2MassError =1.3; 
	Float_t x3872MassError = 0.17; 
	Float_t x3915MassError = 1.9; 
	Float_t x3927MassError = 2.6; 
	
	Float_t etacGamma =	32.2; 
	Float_t chi0Gamma =	10.5; 
	Float_t chi1Gamma =	0.84; 
	Float_t hcGamma   = 	0.7; 
	Float_t chi2Gamma =	1.93; 
	Float_t etac2Gamma =	11.3;

	Float_t x3915Gamma = 	20; 
	Float_t x3927Gamma = 	24; 	
	
	Float_t etacGammaError =	0.9; 
	Float_t chi0GammaError =	0.6; 
	Float_t chi1GammaError =	0.04; 
	Float_t hcGammaError   = 	0.4; 
	Float_t chi2GammaError =	0.11; 
	Float_t etac2GammaError =	3.1; 
	Float_t x3915GammaError = 	5; 
	Float_t x3927GammaError = 	6; 
	
	Float_t EtacSigmaN_MC = 6.311;



	Float_t SigmaPar = 0.232;
	
	Float_t minMass = 3300;
	Float_t maxMass = 3690;
	Float_t binWidth = 10.;
	Int_t binN = int((maxMass-minMass)/binWidth);
	RooRealVar varMass("M(#phi#phi)", "M(#phi#phi)", minMass, maxMass, "MeV");
	varMass.setBins(10000);



// 	histogramms:
if(Type==4)
  	TFile* file = new TFile("diPhiPureAll_f0.root");
else
	TFile* file = new TFile("diPhiPureAll.root");

	TH1D *histJpsiDiPhi  = (TH1D*) file->Get("histJpsiDiPhi");
	
	RooDataHist* DataSet = new RooDataHist("DataSet", "DataSet", varMass, histJpsiDiPhi);


	RooArgList listComp, listNorm;
	RooArgList NRlistComp, NRlistNorm;
	RooArgSet showParams;
	
	
	RooRealVar FourKMass("FourKMass", "FourKMass", 4*493.677);
	RooRealVar varSigmaFactor("varSigmaFactor","varSigmaFactor",2.16);
	RooRealVar FitMin("FitMin","FitMin",minMass);
	
	RooRealVar varNarrowFraction("varNarrowFraction","varNarrowFraction",0.87);
//  	RooRealVar ASig("ASig", "ASig", SigmaPar, 1e-1, 1);
	RooRealVar ASig("ASig", "ASig", SigmaPar,0.1,0.3);
	
	RooRealVar radius("radius", "radius", 1);
	RooRealVar massa("massa", "massa", 1019.46, "MeV");
	RooRealVar massb("massb", "massb", 1019.46, "MeV");
	RooRealVar ConvCentre("ConvCentre","ConvCentre",0) ;	
/*	
	showParams.add(ASig);
	showParams.add(varNarrowFraction);*/

//========== fitting model components:
	RooRealVar varEtacNumber("N(#eta_{c}(1S))", "varEtacNumber", 6800, 1e3, 1e4);
	RooRealVar varEtacMass("M(#eta_{c})", "varEtacMass", 2984, 2975, 2989);
	RooRealVar varEtacGamma("varEtacGamma", "varEtacGamma", 32, 10, 60);
if(Type==2) 	
	RooRealVar varEtacSigmaN("varEtacSigmaN", "varEtacSigmaN", EtacSigmaN_MC); 
else
	RooFormulaVar varEtacSigmaN("varEtacSigmaN", "varEtacSigmaN", "sqrt(@1-@2)*(@0)", RooArgList(ASig, varEtacMass, FourKMass));

	RooFormulaVar varEtacSigmaW("varEtacSigmaW", "varEtacSigmaW", "@0*@1", RooArgList(varSigmaFactor, varEtacSigmaN));

	RooRelBreitWigner pdfEtacBWrel = RooRelBreitWigner("pdfEtacBWrel", "pdfEtacBWrel", varMass, varEtacMass, varEtacGamma,
			RooConst(0), radius, massa, massb);

	RooGaussian pdfEtacGaussN("pdfEtacGaussN","pdfEtacGaussN",varMass,ConvCentre,varEtacSigmaN);
	RooGaussian pdfEtacGaussW("pdfEtacGaussW","pdfEtacGaussW",varMass,ConvCentre,varEtacSigmaN);
	
	RooFFTConvPdf pdfEtacN("pdfEtacN","pdfEtacN",varMass,pdfEtacBWrel,pdfEtacGaussN) ;
	RooFFTConvPdf pdfEtacW("pdfEtacW","pdfEtacW",varMass,pdfEtacBWrel,pdfEtacGaussW) ;
	
	RooAddPdf pdfEtac("pdfEtac","pdfEtac",RooArgList(pdfEtacN,pdfEtacW),varNarrowFraction);	

	listComp.add(pdfEtac);
	listNorm.add(varEtacNumber);
	showParams.add(varEtacNumber);

	
	RooVoigtian NRpdfEtacN("NRpdfEtacN", "NRpdfEtacN", varMass, varEtacMass,/*varEtacMassPDG,*/ varEtacGamma, varEtacSigmaN);
	RooVoigtian NRpdfEtacW("NRpdfEtacW", "NRpdfEtacW", varMass, varEtacMass,/*varEtacMassPDG,*/ varEtacGamma, varEtacSigmaW);
	RooAddPdf NRpdfEtac("NRpdfEtac","NRpdfEtac",RooArgList(NRpdfEtacN,NRpdfEtacW),varNarrowFraction);
	NRlistComp.add(NRpdfEtac);
	NRlistNorm.add(varEtacNumber);

	
	

	

	RooRealVar varChi0Number("N(#chi_{c0})", "varChi0Number", 0, 1e4);

if(Type==1)
	RooRealVar varChi0Mass("M(#chi_{c0})", "varChi0Mass", chi0Mass);
else
 	RooRealVar varChi0Mass("M(#chi_{c0})", "varChi0Mass", chi0Mass, chi0Mass-5, chi0Mass+5);


//        RooFormulaVar varChi0Mass("M(#chi_{c0})", "varChi0Mass", "@0+@1", RooArgList(varEtacMass, vardMEtacChi0));
	RooRealVar varChi0Gamma("varChi0Gamma", "varChi0Gamma", chi0Gamma);
	RooFormulaVar varChi0SigmaN("varChi0SigmaN", "varChi0SigmaN", "sqrt(@1-@2)*(@0)", RooArgList(ASig, varChi0Mass, FourKMass));
	RooFormulaVar varChi0SigmaW("varChi0SigmaW", "varChi0SigmaW", "@0*@1", RooArgList(varSigmaFactor, varChi0SigmaN));

	RooRelBreitWigner pdfChi0BWrel = RooRelBreitWigner("pdfChi0BWrel", "pdfChi0BWrel", varMass, varChi0Mass, varChi0Gamma,
			RooConst(0), radius, massa, massb);


	RooGaussian pdfChi0GaussN("pdfChi0GaussN","pdfChi0GaussN",varMass,ConvCentre,varChi0SigmaN);
	RooGaussian pdfChi0GaussW("pdfChi0GaussW","pdfChi0GaussW",varMass,ConvCentre,varChi0SigmaN);
	
	RooFFTConvPdf pdfChi0N("pdfChi0N","pdfChi0N",varMass,pdfChi0BWrel,pdfChi0GaussN) ;
	RooFFTConvPdf pdfChi0W("pdfChi0W","pdfChi0W",varMass,pdfChi0BWrel,pdfChi0GaussW) ;
	
	RooAddPdf pdfChi0("pdfChi0","pdfChi0",RooArgList(pdfChi0N,pdfChi0W),varNarrowFraction);	
	listComp.add(pdfChi0);
	listNorm.add(varChi0Number);
	showParams.add(varChi0Number);
	
	
	RooVoigtian NRpdfChi0N("NRpdfChi0N", "NRpdfChi0N", varMass, varChi0Mass,/*varChi0MassPDG,*/ varChi0Gamma, varChi0SigmaN);
	RooVoigtian NRpdfChi0W("NRpdfChi0W", "NRpdfChi0W", varMass, varChi0Mass,/*varChi0MassPDG,*/ varChi0Gamma, varChi0SigmaW);
	RooAddPdf NRpdfChi0("NRpdfChi0","NRpdfChi0",RooArgList(NRpdfChi0N,NRpdfChi0W),varNarrowFraction);
	NRlistComp.add(NRpdfChi0);
	NRlistNorm.add(varChi0Number);

	
	RooRealVar varChi1Number("N(#chi_{c1})", "varChi1Number", 0,1e4);
	RooRealVar varChi1MassPDG("varChi1MassPDG", "varChi1MassPDG", chi1Mass);
	//RooFormulaVar varChi1Mass("varChi1Mass", "varChi1Mass", "@0-(@1-@2)", RooArgList(varChi0Mass, varChi0MassPDG, varChi1MassPDG));
if(Type==1)
	RooRealVar varChi1Mass("M(#chi_{c1})", "varChi1Mass", chi1Mass);
else
        RooRealVar varChi1Mass("M(#chi_{c1})", "varChi1Mass", chi1Mass-5, chi1Mass+5);


	RooRealVar varChi1Gamma("varChi1Gamma", "varChi1Gamma", chi1Gamma);
	RooFormulaVar varChi1SigmaN("varChi1SigmaN", "varChi1SigmaN", "sqrt(@1-@2)*(@0)", RooArgList(ASig, varChi1Mass, FourKMass));
	RooFormulaVar varChi1SigmaW("varChi1SigmaW", "varChi1SigmaW", "@0*@1", RooArgList(varSigmaFactor, varChi1SigmaN));
	
	
	RooRelBreitWigner pdfChi1BWrel = RooRelBreitWigner("pdfChi1BWrel", "pdfChi1BWrel", varMass, varChi1Mass, varChi1Gamma,
	RooConst(1), radius, massa, massb);


	
	RooGaussian pdfChi1GaussN("pdfChi1GaussN","pdfChi1GaussN",varMass,ConvCentre,varChi1SigmaN);
	RooGaussian pdfChi1GaussW("pdfChi1GaussW","pdfChi1GaussW",varMass,ConvCentre,varChi1SigmaN);
	
	RooFFTConvPdf pdfChi1N("pdfChi1N","pdfChi1N",varMass,pdfChi1BWrel,pdfChi1GaussN) ;
	RooFFTConvPdf pdfChi1W("pdfChi1W","pdfChi1W",varMass,pdfChi1BWrel,pdfChi1GaussW) ;
	
	RooAddPdf pdfChi1("pdfChi1","pdfChi1",RooArgList(pdfChi1N,pdfChi1W),varNarrowFraction);	
	listComp.add(pdfChi1);
	listNorm.add(varChi1Number);
	showParams.add(varChi1Number);
	
	RooVoigtian NRpdfChi1N("NRpdfChi1N", "NRpdfChi1N", varMass, varChi1Mass,/*varChi1MassPDG,*/ varChi1Gamma, varChi1SigmaN);
	RooVoigtian NRpdfChi1W("NRpdfChi1W", "NRpdfChi1W", varMass, varChi1Mass,/*varChi1MassPDG,*/ varChi1Gamma, varChi1SigmaW);
	RooAddPdf NRpdfChi1("NRpdfChi1","NRpdfChi1",RooArgList(NRpdfChi1N,NRpdfChi1W),varNarrowFraction);
	NRlistComp.add(NRpdfChi1);
	NRlistNorm.add(varChi1Number);


	RooRealVar varChi2toEtacRatio("N(#chi_{c2})/N(#eta_{c})","varChi2toEtacRatio",0.090, 0 ,0.3);
	RooRealVar varChi2Number("N(#chi_{c2})", "varChi2Number", 0,1e4);
	
if(Type==1)
	RooRealVar varChi2Mass("M(#chi_{c2})", "varChi2Mass", chi2Mass);
else
        RooRealVar varChi2Mass("M(#chi_{c2})", "varChi2Mass", chi2Mass-5, chi2Mass+5);

	RooRealVar varChi2Gamma("varChi2Gamma", "varChi2Gamma", chi2Gamma);
	RooFormulaVar varChi2SigmaN("varChi2SigmaN", "varChi2SigmaN", "sqrt(@1-@2)*(@0)", RooArgList(ASig, varChi2Mass, FourKMass));
	RooFormulaVar varChi2SigmaW("varChi2SigmaW", "varChi2SigmaW", "@0*@1", RooArgList(varSigmaFactor, varChi2SigmaN));

	
	RooRelBreitWigner pdfChi2BWrel = RooRelBreitWigner("pdfChi2BWrel", "pdfChi2BWrel", varMass, varChi2Mass, varChi2Gamma,
			RooConst(2), radius, massa, massb);

	RooVoigtian NRpdfChi2N("NRpdfChi2N", "NRpdfChi2N", varMass, varChi2Mass,/*varChi2MassPDG,*/ varChi2Gamma, varChi2SigmaN);
	RooVoigtian NRpdfChi2W("NRpdfChi2W", "NRpdfChi2W", varMass, varChi2Mass,/*varChi2MassPDG,*/ varChi2Gamma, varChi2SigmaW);
	RooAddPdf NRpdfChi2("NRpdfChi2","NRpdfChi2",RooArgList(NRpdfChi2N,NRpdfChi2W),varNarrowFraction);
	NRlistComp.add(NRpdfChi2);
	NRlistNorm.add(varChi2Number);
	

	RooGaussian pdfChi2GaussN("pdfChi2GaussN","pdfChi2GaussN",varMass,ConvCentre,varChi2SigmaN);
	RooGaussian pdfChi2GaussW("pdfChi2GaussW","pdfChi2GaussW",varMass,ConvCentre,varChi2SigmaN);
	
	RooFFTConvPdf pdfChi2N("pdfChi2N","pdfChi2N",varMass,pdfChi2BWrel,pdfChi2GaussN) ;
	RooFFTConvPdf pdfChi2W("pdfChi2W","pdfChi2W",varMass,pdfChi2BWrel,pdfChi2GaussW) ;
	
	RooAddPdf pdfChi2("pdfChi2","pdfChi2",RooArgList(pdfChi2N,pdfChi2W),varNarrowFraction);	
	listComp.add(pdfChi2);
	listNorm.add(varChi2Number);
	showParams.add(varChi2Number);
	//showParams.add(varChi1Mass);
        //showParams.add(varChi0Mass);
	//showParams.add(varChi2Mass);

	RooRealVar varEtac2toEtac1Ratio("N(#eta_{c}(2S)/N(#eta_{c}(1S)","varEtac2toEtac1Ratio",0.059, 0,0.3);
	RooRealVar varEtac2Number("N(#eta_{c}(2S))", "varEtac2Number", 0,1e4);
//	RooRealVar varEtac2MassPDG("m(#eta_{c}(2S))", "varEtac2MassPDG", 3638.9, 3638.9-10, 3638.9+5);
	RooRealVar varEtac2Gamma("#Gamma(#eta_{c}(2S))", "varEtac2Gamma", 11.3, 0, 50);
	RooRealVar varEtac2Mass("M(#eta_{c}(2S))", "varEtac2Mass", etac2Mass, etac2Mass-15, etac2Mass+5);
 
	RooGaussian GammaConstraint("GammaConstraint","GammaConstraint",varEtac2Gamma,RooConst(etac2Gamma),RooConst(etac2GammaError)) ;
// 	RooGaussian MassConstraint("MassConstraint","MassConstraint", varEtac2Mass, RooConst(3639.4), RooConst(1.3)) ;
	RooGaussian MassConstraint("MassConstraint","MassConstraint", varEtac2Mass, RooConst(etac2Mass), RooConst(etac2MassError)) ;
	RooProdPdf GMConstraint("GMConstraint", "GMConstraint", RooArgSet(GammaConstraint, MassConstraint));
	
	RooFormulaVar varEtac2SigmaN("varEtac2SigmaN", "varEtac2SigmaN", "sqrt(@1-@2)*(@0)", RooArgList(ASig, varEtac2Mass, FourKMass));
	RooFormulaVar varEtac2SigmaW("varEtac2SigmaW", "varEtac2SigmaW", "@0*@1", RooArgList(varSigmaFactor, varEtac2SigmaN));
	RooRelBreitWigner pdfEtac2BWrel = RooRelBreitWigner("pdfEtac2BWrel", "pdfEtac2BWrel", varMass, varEtac2Mass, varEtac2Gamma,
			RooConst(0), radius, massa, massb);

	RooGaussian pdfEtac2GaussN("pdfEtac2GaussN","pdfEtac2GaussN",varMass,ConvCentre,varEtac2SigmaN);
	RooGaussian pdfEtac2GaussW("pdfEtac2GaussW","pdfEtac2GaussW",varMass,ConvCentre,varEtac2SigmaN);
	
	RooFFTConvPdf pdfEtac2N("pdfEtac2N","pdfEtac2N",varMass,pdfEtac2BWrel,pdfEtac2GaussN) ;
	RooFFTConvPdf pdfEtac2W("pdfEtac2W","pdfEtac2W",varMass,pdfEtac2BWrel,pdfEtac2GaussW) ;
	
	RooAddPdf pdfEtac2("pdfEtac2","pdfEtac2",RooArgList(pdfEtac2N,pdfEtac2W),varNarrowFraction);	
	listComp.add(pdfEtac2);
	listNorm.add(varEtac2Number);
	showParams.add(varEtac2Number);

	RooVoigtian NRpdfEtac2N("NRpdfEtac2N", "NRpdfEtac2N", varMass, varEtac2Mass,/*varEtac2MassPDG,*/ varEtac2Gamma, varEtac2SigmaN);
	RooVoigtian NRpdfEtac2W("NRpdfEtac2W", "NRpdfEtac2W", varMass, varEtac2Mass,/*varEtac2MassPDG,*/ varEtac2Gamma, varEtac2SigmaW);
	RooAddPdf NRpdfEtac2("NRpdfEtac2","NRpdfEtac2",RooArgList(NRpdfEtac2N,NRpdfEtac2W),varNarrowFraction);
	NRlistComp.add(NRpdfEtac2);
	NRlistNorm.add(varEtac2Number);
	
if(Type==5)
{
	RooRealVar AASig("AASig", "AASig", SigmaPar);


	RooRealVar varHctoChi0Ratio("N(#h_{c})/N(#chi_{c0})","varHctoChi0Ratio",0.005,0,0.2);
	RooFormulaVar varHcNumber("N(h_{c})", "varHcNumber", "@0*@1", RooArgList(varChi0Number,varHctoChi0Ratio));
	RooRealVar varHcMass("varHcMass", "varHcMass", hcMass);
	RooFormulaVar varHcSigmaN("varHcSigmaN", "varHcSigmaN", "sqrt(@1-@2)*(@0)", RooArgList(AASig, varHcMass, FourKMass));
	RooFormulaVar varHcSigmaW("varHcSigmaW", "varHcSigmaW", "@0*@1", RooArgList(varSigmaFactor, varHcSigmaN));
	RooGaussian pdfHcN("pdfHcN", "pdfHcN", varMass, varHcMass, varHcSigmaN);
	RooGaussian pdfHcW("pdfHcW", "pdfHcW", varMass, varHcMass, varHcSigmaW);
	RooAddPdf pdfHc("pdfHc","pdfHc",RooArgList(pdfHcN,pdfHcW),varNarrowFraction);
	listComp.add(pdfHc);
	listNorm.add(varHcNumber);
	showParams.add(varHctoChi0Ratio);
	

	RooRealVar varX3872toChi1Ratio("N(X(3872))/N(#chi_{c1})","varX3872toChi1Ratio",0.05,0,0.3);
	RooFormulaVar varX3872Number("N(X(3872))", "varX3872Number", "@0*@1", RooArgList(varChi1Number,varX3872toChi1Ratio));
	RooRealVar varX3872Mass("varX3872Mass", "varX3872Mass", x3872Mass);
	RooFormulaVar varX3872SigmaN("varX3872SigmaN", "varX3872SigmaN", "sqrt(@1-@2)*(@0)", RooArgList(AASig, varX3872Mass, FourKMass));
	RooFormulaVar varX3872SigmaW("varX3872SigmaW", "varX3872SigmaW", "@0*@1", RooArgList(varSigmaFactor, varX3872SigmaN));
	RooGaussian pdfX3872N("pdfX3872N", "pdfX3872N", varMass, varX3872Mass, varX3872SigmaN);
	RooGaussian pdfX3872W("pdfX3872W", "pdfX3872W", varMass, varX3872Mass, varX3872SigmaW);
	RooAddPdf pdfX3872("pdfX3872","pdfX3872",RooArgList(pdfX3872N,pdfX3872W),varNarrowFraction);
	listComp.add(pdfX3872);
	listNorm.add(varX3872Number);
	showParams.add(varX3872toChi1Ratio);

	
	
	
	RooRealVar varX3915toChi0Ratio("N(X(3915))/N(#chi_{c0})","varX3915toChi0Ratio",0.005,0,0.3);
	RooFormulaVar varX3915Number("N(X(3915))", "varX3915Number", "@0*@1", RooArgList(varChi0Number,varX3915toChi0Ratio));
	RooRealVar varX3915Mass("varX3915Mass", "varX3915Mass", x3915Mass);
	RooRealVar varX3915Gamma("var3915Gamma", "var3915Gamma", x3915Gamma);
	RooFormulaVar varX3915SigmaN("varX3915SigmaN", "varX3915SigmaN", "sqrt(@1-@2)*(@0)", RooArgList(AASig, varX3915Mass, FourKMass));
	RooFormulaVar varX3915SigmaW("varX3915SigmaW", "varX3915SigmaW", "@0*@1", RooArgList(varSigmaFactor, varX3915SigmaN));
	RooRelBreitWigner pdfX3915BWrel = RooRelBreitWigner("pdfX3915BWrel", "pdfX3915BWrel", varMass, varX3915Mass, varX3915Gamma,
			RooConst(0), radius, massa, massb);

	RooGaussian pdfX3915GaussN("pdfX3915GaussN","pdfX3915GaussN",varMass,ConvCentre,varX3915SigmaN);
	RooGaussian pdfX3915GaussW("pdfX3915GaussW","pdfX3915GaussW",varMass,ConvCentre,varX3915SigmaN);
	
	RooFFTConvPdf pdfX3915N("pdfX3915N","pdfX3915N",varMass,pdfX3915BWrel,pdfX3915GaussN) ;
	RooFFTConvPdf pdfX3915W("pdfX3915W","pdfX3915W",varMass,pdfX3915BWrel,pdfX3915GaussW) ;
	
	RooAddPdf pdfX3915("pdfX3915","pdfX3915",RooArgList(pdfX3915N,pdfX3915W),varNarrowFraction);
	listComp.add(pdfX3915);
	listNorm.add(varX3915Number);
	showParams.add(varX3915toChi0Ratio);
	
	
	RooRealVar varX3927toChi2Ratio("N(X(3927))/N(#chi_{c2})","varX3927toChi2Ratio",0.005,0,0.3);
	RooFormulaVar varX3927Number("N(X(3927))", "varX3927Number", "@0*@1", RooArgList(varChi2Number,varX3927toChi2Ratio));
	RooRealVar varX3927Mass("varX3927Mass", "varX3927Mass", x3927Mass);
	RooRealVar varX3927Gamma("var3927Gamma", "var3927Gamma", x3927Gamma);
	RooFormulaVar varX3927SigmaN("varX3927SigmaN", "varX3927SigmaN", "sqrt(@1-@2)*(@0)", RooArgList(AASig, varX3927Mass, FourKMass));
	RooFormulaVar varX3927SigmaW("varX3927SigmaW", "varX3927SigmaW", "@0*@1", RooArgList(varSigmaFactor, varX3927SigmaN));
	RooRelBreitWigner pdfX3927BWrel = RooRelBreitWigner("pdfX3927BWrel", "pdfX3927BWrel", varMass, varX3927Mass, varX3927Gamma,
			RooConst(2), radius, massa, massb);

	RooGaussian pdfX3927GaussN("pdfX3927GaussN","pdfX3927GaussN",varMass,ConvCentre,varX3927SigmaN);
	RooGaussian pdfX3927GaussW("pdfX3927GaussW","pdfX3927GaussW",varMass,ConvCentre,varX3927SigmaN);
	
	RooFFTConvPdf pdfX3927N("pdfX3927N","pdfX3927N",varMass,pdfX3927BWrel,pdfX3927GaussN) ;
	RooFFTConvPdf pdfX3927W("pdfX3927W","pdfX3927W",varMass,pdfX3927BWrel,pdfX3927GaussW) ;
	
	RooAddPdf pdfX3927("pdfX3927","pdfX3927",RooArgList(pdfX3927N,pdfX3927W),varNarrowFraction);
	listComp.add(pdfX3927);
	listNorm.add(varX3927Number);
	showParams.add(varX3927toChi2Ratio);

	
	//RooGaussian HcMCons("HcMCons","HcMCons",varHcMass,RooConst(3525.38),RooConst(0.11)) ;
// 	RooGaussian X3872MCons("X3872MCons","X3872MCons",varX3872Mass,RooConst(x3872Mass),RooConst(x3872MassE)) ;
// 	RooGaussian X3915MCons("X3915MCons","X3915MCons",varX3915Mass,RooConst(3918.4),RooConst(1.9)) ;
// 	RooGaussian X3927MCons("X3927MCons","X3927MCons",varX3927Mass,RooConst(3927.2),RooConst(2.6)) ;
	
// 	RooGaussian X3915GCons("X3915GCons","X3915GCons",varX3915Gamma,RooConst(x3915Gamma),RooConst(x3915GammaError)) ;
// 	RooGaussian X3927GCons("X3927GCons","X3927GCons",varX3927Gamma,RooConst(x3927Gamma),RooConst(x3927GammaError)) ;
// 	RooProdPdf XConstraint("XConstraint", "XConstraint", 
// 			       RooArgSet(X3915GCons,X3927GCons));
	
}

	RooRealVar varBgrNumber("N_{bgr}", "number of background events", 6.5e4, 1e4, 1e5);
	char label[200];
	RooRealVar varE0("varE0", "varE0", -3e-3, -1e-1, 0);
	RooRealVar varA0("varA0", "varA0", 1e-3,-1e-1, +1e-1);
	RooRealVar varA1("varA1", "varA1", 5e-6,-1e-3, +1e-3);
	
	RooRealVar varB0("varB0", "varB0", -1e-3, -1e-1, 1e-1);
	RooRealVar varB1("varB1", "varB1", 5e-7, -1, 1);
	

	
	RooRealVar var2PhiMass("var2PhiMass", "var2PhiMass", 2038.9);


if(Type==3)
	RooGenericPdf pdfBgr("pdfBgr", "pdfBgr", "sqrt(@0-@1)*(1+(@0-@2)*@3+(@0-@2)*(@0-@2)*@4)",    //O2 without exp
			RooArgSet(varMass, var2PhiMass, FitMin, varB0, varB1));
else
 	RooGenericPdf pdfBgr("pdfBgr", "pdfBgr", "sqrt(@0-@1)*exp(@0*@3)*(1+(@0-@2)*@4)",
    		        RooArgSet(varMass, var2PhiMass, FitMin, varE0, varA0));	
//  	RooGenericPdf pdfBgr("pdfBgr", "pdfBgr", "sqrt(@0-@1)*exp(@0*@3)*(1+(@0-@2)*@4+(@0-@2)*(@0-@2)*@5)",
//     		        RooArgSet(varMass, var2PhiMass, FitMin, varE0, varA0, varA1));	
	
//  	RooGenericPdf pdfBgr("pdfBgr", "pdfBgr", "sqrt(@0-@1)*exp(@0*@2)*(1+(@0-@1)*@3+(@0-@1)*(@0-@1)*@3)",
//     		        RooArgSet(varMass, var2PhiMass, varE0, varA0, varA1/*, varD0*/));
	
	

	listComp.add(pdfBgr);
	listNorm.add(varBgrNumber);
	
	NRlistComp.add(pdfBgr);
	NRlistNorm.add(varBgrNumber);

	RooAddPdf pdfModel("pdfModel", "pdfModel", listComp, listNorm);
	RooAddPdf NRpdfModel("pdfModel", "pdfModel", NRlistComp, NRlistNorm);

	
//	results = pdfModel.fitTo(*DataSet, ExternalConstraints(GMConstraint), Save(true), PrintLevel(0), NumCPU(4));
//	results = pdfModel.fitTo(*DataSet/*, ExternalConstraints(G1Constraint)*/, Strategy(2)/*, Minos(true)*/, Save(true), PrintLevel(0), NumCPU(4));

	
	RooPlot* frame = varMass.frame(Title("M(#phi#phi)"));
//	DataSet->plotOn(frame, Binning(binN, minMass, maxMass));

//	pdfModel.plotOn(frame,NormRange("fit_range"));
	pdfModel.plotOn(frame,LineWidth(2));
	NRpdfModel.plotOn(frame,LineColor(kRed),LineWidth(2));
	pdfModel.paramOn(frame, Layout(0.65, 0.95, 0.95), Parameters(showParams), ShowConstants(true), Format("NEALU",AutoPrecision(1)));
	frame->getAttText()->SetTextSize(0.03) ;



	TCanvas* canvA = new TCanvas("canvA", "canvA", 1000, 600);
	frame->Draw();
	
        showParams.writeToFile("params.txt");

}


