// $Id: $
{
  TChain *chain = new TChain("DecayTree");
  chain->Add("All2phi.root");
//    chain->Add("Data_ok_cuts/All2phi.root");
  
  
    TCut trigger0Cut("Jpsi_L0HadronDecision_TOS || Jpsi_L0Global_TIS");
    TCut trigger1Cut("Jpsi_Hlt1TrackMVADecision_TOS || Jpsi_Hlt1TwoTrackMVADecision_TOS");
    TCut trigger2Cut("Jpsi_Hlt2PhiIncPhiDecision_TOS || Jpsi_Hlt2Topo2BodyDecision_TOS || Jpsi_Hlt2Topo3BodyDecision_TOS || Jpsi_Hlt2Topo4BodyDecision_TOS");
    
    TCut incphicut("Jpsi_Hlt1IncPhiDecision_TOS&&(Jpsi_Hlt2CcDiHadronDiPhiDecision_TOS||Jpsi_Hlt2PhiIncPhiDecision_TOS)");

  TFile *newfile = new TFile("All2phi_runICuts.root","recreate");
  TTree *newtree = chain->CopyTree(trigger0Cut&&trigger1Cut&&trigger2Cut);
 
  newtree->Print();
  newfile->Write();
  
  delete chain;
  delete newfile;
  
}

