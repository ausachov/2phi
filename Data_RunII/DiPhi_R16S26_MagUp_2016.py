DaVinciVersion = 'v41r2'
myJobName = 'DiPhi_R16S26_MagUp_2016'
myApplication = DaVinci()
myApplication.version = DaVinciVersion
#myApplication.user_release_area = '/afs/cern.ch/user/j/jhe/cmtuser'
myApplication.platform='x86_64-slc6-gcc49-opt'
myApplication.optsfile = ['DaVinci_DiPhi_R16S26_2016.py','DB_R16S26.py']

data = BKQuery(path="/LHCb/Collision16/Beam6500GeV-VeloClosed-MagUp/Real Data/Reco16/Stripping26/90000000/CHARM.MDST",
               dqflag=['OK']).getDataset()

validData = LHCbDataset(files=['LFN:'+lfn for lfn, rep in data.getReplicas().iteritems() if len(rep)])

mySplitter = SplitByFiles( filesPerJob = 10, maxFiles = -1, ignoremissing = True, bulksubmit=False )

myBackend = Dirac()
j = Job (
         name         = myJobName,
         application  = myApplication,
         splitter     = mySplitter,
         outputfiles  = [ LocalFile('Tuple.root'),
                         LocalFile('DVHistos.root')
                         ],
         backend      = myBackend,
         inputdata    = validData,
         do_auto_resubmit = True,
         parallel_submit = True
         )
j.submit(keep_going=True, keep_on_fail=True)