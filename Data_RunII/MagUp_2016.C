// $Id: $
{
    TChain *chain = new TChain("Jpsi2DiPhiDetachedTuple/DecayTree");
    
    
    int nSj = 1025;
    char label [400];
    for(int i=0;i<nSj;i++)
    {
        sprintf(label,"/afs/cern.ch/work/a/ausachov/gangadir/workspace/ausachov/LocalXML/127/%i/output/Tuple.root",i);
        chain->Add(label);
    }
  
  TCut KaonIPCut("Kaon1_IPCHI2_OWNPV>4 && Kaon2_IPCHI2_OWNPV>4 && Kaon3_IPCHI2_OWNPV>4 && Kaon4_IPCHI2_OWNPV>4");
  TCut KaonTRCut("Kaon1_TRACK_CHI2NDOF<5 && Kaon2_TRACK_CHI2NDOF<5 && Kaon3_TRACK_CHI2NDOF<5 && Kaon4_TRACK_CHI2NDOF<5");
  TCut PIDCut("Kaon1_ProbNNk>0.1 && Kaon2_ProbNNk>0.1 && Kaon3_ProbNNk>0.1 && Kaon4_ProbNNk>0.1");
  TCut PhiMCut("abs(Phi1_MM-1020)<12 && abs(Phi2_MM-1020)<12");
  TCut PhiVerCut("Phi1_ENDVERTEX_CHI2<25 && Phi2_ENDVERTEX_CHI2<25");
  TCut JpsiCut("Jpsi_ENDVERTEX_CHI2<45 && Jpsi_MM>2700 && Jpsi_MM<6000 && Jpsi_FDCHI2_OWNPV>100");// >49");
  TCut PTCut("Kaon1_PT>500 && Kaon2_PT>500 && Kaon3_PT>500 && Kaon4_PT>500");
  TCut trigger0Cut("Jpsi_L0HadronDecision_TOS || Jpsi_L0Global_TIS");
  TCut trigger1Cut("Jpsi_Hlt1IncPhiDecision_TOS || Jpsi_Hlt1TrackMVADecision_TOS || Jpsi_Hlt1TwoTrackMVADecision_TOS");
  TCut trigger2Cut("Jpsi_Hlt2PhiIncPhiDecision_TOS || Jpsi_Hlt2Topo2BodyDecision_TOS || Jpsi_Hlt2Topo3BodyDecision_TOS || Jpsi_Hlt2Topo4BodyDecision_TOS || Jpsi_Hlt2CcDiHadronDiPhiDecision_TOS");
  
  TCut totCut = 
    KaonIPCut 
    && KaonTRCut
    && PIDCut 
    && PhiMCut
    && PhiVerCut
    && JpsiCut
    && PTCut
    && trigger0Cut
    && trigger1Cut
    && trigger2Cut;

  TFile *newfile = new TFile("Reduced_MagUp2016.root","recreate");
  TTree *newtree = chain->CopyTree(totCut);
  
//   newtree->Print();
  newfile->Write();
  
  delete chain;
  delete newfile;
  
}
