def fillTuple( tuple, branches, myTriggerList ):
    
    from Configurables import DecayTreeTuple, TupleToolTISTOS, TupleToolDecay, TupleToolRecoStats, TupleToolEventInfo, TupleToolDecay
    
    # for MicroDST
    tuple.RootInTES  = "/Event/Charm/"
    
    tuple.Branches = branches
    
    tuple.ToolList = [
                      "TupleToolAngles",
                      "TupleToolEventInfo",
                      "TupleToolGeometry",
                      "TupleToolKinematic",
                      "TupleToolPid",
                      "TupleToolPrimaries",
                      "TupleToolRecoStats",
                      "TupleToolTrackInfo"
                      ]
        
    tuple.addTool(TupleToolDecay, name = 'Jpsi')
    tuple.addTool(TupleToolDecay, name = 'B')
                      
    # TISTOS for Jpsi
    tuple.Jpsi.ToolList += [ "TupleToolTISTOS/TupleToolTISTOSForJpsi" ]
    tuple.Jpsi.addTool(TupleToolTISTOS("TupleToolTISTOSForJpsi") )
    tuple.Jpsi.TupleToolTISTOSForJpsi.Verbose=True
    tuple.Jpsi.TupleToolTISTOSForJpsi.TriggerList = myTriggerList
                      
    tuple.B.ToolList += [ "TupleToolTISTOS/TupleToolTISTOSForB" ]
    tuple.B.addTool(TupleToolTISTOS("TupleToolTISTOSForB") )
    tuple.B.TupleToolTISTOSForB.Verbose=True
    tuple.B.TupleToolTISTOSForB.TriggerList = myTriggerList
                      
    # RecoStats for filling SpdMult, etc
    tuple.addTool(TupleToolRecoStats, name="TupleToolRecoStats")
    tuple.TupleToolRecoStats.Verbose=True
                      
    #LoKi one
    from Configurables import LoKi__Hybrid__TupleTool
    LoKi_Jpsi=LoKi__Hybrid__TupleTool("LoKi_Jpsi")
    LoKi_Jpsi.Variables = {
            "LOKI_FDCHI2"          : "BPVVDCHI2",
            "LOKI_FDS"             : "BPVDLS",
            "LOKI_DIRA"            : "BPVDIRA",
            "LOKI_BPVCORRM"        : "BPVCORRM",
            "m_scaled" : "DTF_FUN ( M , False )" ,
            "m_pv" : "DTF_FUN ( M , True )" ,
            "c2dtf_1" : "DTF_CHI2NDOF( False )" ,
            "c2dtf_2" : "DTF_CHI2NDOF( True  )"
            }

    tuple.addTool(TupleToolDecay, name="Jpsi")
    tuple.Jpsi.ToolList+=["LoKi::Hybrid::TupleTool/LoKi_Jpsi"]
    tuple.Jpsi.addTool(LoKi_Jpsi)
    
    
    
    tuple.addTool(TupleToolDecay, name = 'Phi1')
    # TISTOS for Jpsi
    tuple.Phi1.ToolList+=[ "TupleToolTISTOS/TupleToolTISTOSForPhi1" ]
    tuple.Phi1.addTool(TupleToolTISTOS, name="TupleToolTISTOSForPhi1" )
    tuple.Phi1.TupleToolTISTOSForPhi1.Verbose=True
    tuple.Phi1.TupleToolTISTOSForPhi1.TriggerList = myTriggerList
    #LoKi one
    from Configurables import LoKi__Hybrid__TupleTool
    LoKi_Phi1=LoKi__Hybrid__TupleTool("LoKi_Phi1")
    LoKi_Phi1.Variables = {
        "LOKI_FDCHI2"          : "BPVVDCHI2",
        "LOKI_FDS"             : "BPVDLS",
        "LOKI_DIRA"            : "BPVDIRA",
        "LOKI_BPVCORRM"        : "BPVCORRM",
        "m_scaled" : "DTF_FUN ( M , False )",
        "m_pv" : "DTF_FUN ( M , True )",
        "c2dtf_1" : "DTF_CHI2NDOF( False )" ,
        "c2dtf_2" : "DTF_CHI2NDOF( True  )"
    }
    
    tuple.addTool(TupleToolDecay, name="Phi1")
    tuple.Phi1.ToolList+=["LoKi::Hybrid::TupleTool/LoKi_Phi1"]
    tuple.Phi1.addTool(LoKi_Phi1)
    
    
    
    
    
    tuple.addTool(TupleToolDecay, name = 'Phi2')
    # TISTOS for Jpsi
    tuple.Phi2.ToolList+=[ "TupleToolTISTOS/TupleToolTISTOSForPhi2" ]
    tuple.Phi2.addTool(TupleToolTISTOS, name="TupleToolTISTOSForPhi2" )
    tuple.Phi2.TupleToolTISTOSForPhi2.Verbose=True
    tuple.Phi2.TupleToolTISTOSForPhi2.TriggerList = myTriggerList
    #LoKi one
    from Configurables import LoKi__Hybrid__TupleTool
    LoKi_Phi2=LoKi__Hybrid__TupleTool("LoKi_Phi2")
    LoKi_Phi2.Variables = {
        "LOKI_FDCHI2"          : "BPVVDCHI2",
        "LOKI_FDS"             : "BPVDLS",
        "LOKI_DIRA"            : "BPVDIRA",
        "LOKI_BPVCORRM"        : "BPVCORRM",
        "m_scaled" : "DTF_FUN ( M , False )",
        "m_pv" : "DTF_FUN ( M , True )",
        "c2dtf_1" : "DTF_CHI2NDOF( False )" ,
        "c2dtf_2" : "DTF_CHI2NDOF( True  )"
    }

    tuple.addTool(TupleToolDecay, name="Phi2")
    tuple.Phi2.ToolList+=["LoKi::Hybrid::TupleTool/LoKi_Phi2"]
    tuple.Phi2.addTool(LoKi_Phi2)






    LoKi_All=LoKi__Hybrid__TupleTool("LoKi_All")
    LoKi_All.Variables = {
        "ETA"                  : "ETA",
        "Y"                    : "Y"
    }
    tuple.ToolList+=["LoKi::Hybrid::TupleTool/LoKi_All"]
    tuple.addTool(LoKi_All)

from os import environ
from GaudiKernel.SystemOfUnits import *
from Gaudi.Configuration import *
# DecayTreeTuple

from Configurables import DecayTreeTuple

myTriggerList = [
                 # L0
                 "L0ElectronDecision",
                 "L0PhotonDecision",
                 "L0HadronDecision",
                 # L0 Muon
                 "L0MuonDecision",
                 "L0MuonHighDecision",
                 "L0DiMuonDecision",
                 
                 
                 
                 # Hlt1 track
                 "Hlt1B2PhiPhi_LTUNBDecision",
                 "Hlt1TrackMuonDecision",
                 "Hlt1TrackPhotonDecision",
                 "Hlt1AllL0Decision",
                 
                 "Hlt1TrackMVADecision",
                 "Hlt1TwoTrackMVADecision",
                 "Hlt1B2HH_LTUNB_KPiDecision",
                 "Hlt1B2HH_LTUNB_KKDecision",
                 "Hlt1B2HH_LTUNB_PiPiDecision",
                 "Hlt1IncPhiDecision",
                 "Hlt1DiProtonDecision",
                 "Hlt1DiProtonLowMultDecision",
                 "Hlt1LowMultVeloCut_HadronsDecision",
                 "Hlt1LowMultPassThroughDecision",
                 
                 
                 # Hlt2 Topo
                 "Hlt2PhiIncPhiDecision",
                 "Hlt2PhiPromptPhi2EETurboDecision",
                 "Hlt2PhiBs2PhiPhiDecision",
                 "Hlt2B2HH_B2HHDecision",
                 "Hlt2CcDiHadronDiProtonDecision",
                 "Hlt2CcDiHadronDiProtonLowMultDecision",
                 "Hlt2CcDiHadronDiPhiDecision",
                 "Hlt2PhiIncPhiDecision",
                 "Hlt2PhiBs2PhiPhiDecision",
                 
                 "Hlt2Topo2BodyDecision",
                 "Hlt2Topo3BodyDecision",
                 "Hlt2Topo4BodyDecision"
                 ]

"""
    DiPhi
    """
Jpsi2DiPhiBranches={
    "Kaon1" :  "J/psi(1S) -> ( phi(1020) ->^K+ K-) ( phi(1020) -> K+ K-)"
    ,"Kaon2" :  "J/psi(1S) -> ( phi(1020) -> K+^K-) ( phi(1020) -> K+ K-)"
    ,"Kaon3" :  "J/psi(1S) -> ( phi(1020) -> K+ K-) ( phi(1020) ->^K+ K-)"
    ,"Kaon4" :  "J/psi(1S) -> ( phi(1020) -> K+ K-) ( phi(1020) -> K+^K-)"
    ,"Phi1"  :  "J/psi(1S) -> ^(phi(1020) -> K+ K-) ( phi(1020) -> K+ K-)"
    ,"Phi2"  :  "J/psi(1S) -> ( phi(1020) -> K+ K-) ^(phi(1020) -> K+ K-)"
    ,"Jpsi"  :  "^(J/psi(1S) -> ( phi(1020) -> K+ K-) ( phi(1020) -> K+ K-))"
    }

Jpsi2DiPhiTuple = DecayTreeTuple("Jpsi2DiPhiDetachedTuple")
Jpsi2DiPhiTuple.Decay = "J/psi(1S) -> ^(phi(1020) -> ^K+ ^K-) ^(phi(1020) -> ^K+ ^K-)"
Jpsi2DiPhiTuple.Inputs = [ "Phys/Ccbar2PhiPhiDetachedLine/Particles" ]
fillTuple( Jpsi2DiPhiTuple, Jpsi2DiPhiBranches, myTriggerList )






from Configurables import EventNodeKiller
eventNodeKiller = EventNodeKiller('DAQkiller')
eventNodeKiller.Nodes = ['/Event/DAQ',
                         '/Event/pRec']

"""
    Event-level filters
    """
#from PhysConf.Filters import LoKi_Filters
#Jpsi2ppFilters = LoKi_Filters (
#    STRIP_Code = """
#    HLT_PASS('StrippingCcbar2PhiPhiLineDecision')
#    | HLT_PASS('StrippingCcbar2PhiPhiDetachedLineDecision')
#    | HLT_PASS('StrippingCcbar2PhiKKDetachedLineDecision')
#    | HLT_PASS('StrippingCcbar2PhiBs2JpsiPhiLineDecision')
#    | HLT_PASS('StrippingCcbar2PhiBs2TriPhiLineDecision')
#    | HLT_PASS('StrippingCcbar2PhiB2JpsiKLineDecision')
#    | HLT_PASS('StrippingCcbar2PhiPiPiDetachedLineDecision')
#    """
#    )
from PhysConf.Filters import LoKi_Filters
Jpsi2ppFilters = LoKi_Filters (
                               STRIP_Code = """
                                   HLT_PASS('StrippingCcbar2PhiPhiDetachedLineDecision')
                                   """
                               )

"""
    Scale momentum
"""
from Configurables import TrackScaleState
StateScale = TrackScaleState("StateScale", RootInTES = '/Event/Charm')
year = "2016"

from Configurables import DaVinci
DaVinci().EventPreFilters = Jpsi2ppFilters.filters ('Jpsi2ppFilters')
DaVinci().EvtMax = -1                          # Number of events
DaVinci().SkipEvents = 0                       # Events to skip
DaVinci().PrintFreq = 1000
DaVinci().DataType = year
DaVinci().Simulation    = False
DaVinci().HistogramFile = "DVHistos.root"      # Histogram file
DaVinci().TupleFile = "Tuple.root"             # Ntuple
DaVinci().InputType='MDST'
DaVinci().UserAlgorithms = [ eventNodeKiller,
                            StateScale, 
                            Jpsi2DiPhiTuple
                            ]        # The algorithms

dv = DaVinci ( RootInTES  = '/Event/Charm' )

# Get Luminosity
DaVinci().Lumi = True

# database
from Configurables import CondDB
CondDB( LatestGlobalTagByDataType = year )

# database
# DaVinci().DDDBtag   = "dddb-20120831"
# DaVinci().CondDBtag = "cond-20121108"

from Configurables import MessageSvc
MessageSvc().setWarning = [ 'RFileCnv' ]
