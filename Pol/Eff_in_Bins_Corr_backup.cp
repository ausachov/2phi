//VarType
// 0 - PT
// 1 - CosTheta
// 2 - nTracks
// 3 - Eta

void ProcessData(int DataType=1, int VarType = 0)
{
    gROOT->Reset();
    switch(VarType)
    {
        case 0:
            switch(DataType)
            {
                case 1:
                    Float_t BinsEdges[10]={0,7500,11000,40000};
                    int NPoints = 3;
                    break;
                case 0:
                    Float_t BinsEdges[10]={0,40000};
                    int NPoints = 1;
                    break;
            }
            break;
        case 1:
            switch(DataType)
            {
                case 1:
                    Float_t BinsEdges[10]={0.,0.3,0.6,1.};
                    int NPoints = 3;
                    break;
                case 0:
                    Float_t BinsEdges[10]={0,1};
                    int NPoints = 1;
                    break;
            }
            break;
        case 2:
            switch(DataType)
            {
                case 1:
                    Int_t BinsEdges[10]={0,150,250,530};
                    int NPoints = 3;
                    break;
                case 0:
                    Int_t BinsEdges[10]={0,530};
                    int NPoints = 1;
                    break;
            }
            break;
        case 3:
            switch(DataType)
            {
                case 1:
                    Double_t BinsEdges[10]={2,3,3.5,4.5};
                    int NPoints = 3;
                    break;
                case 0:
                    Double_t BinsEdges[10]={-10,10};
                    int NPoints = 1;
                    break;
            }
            break;
        case 4:
            switch(DataType)
            {
                case 1:
                    Float_t BinsEdges[10]={0.,0.3,0.6,1.};
                    int NPoints = 3;
                    break;
                case 0:
                    Float_t BinsEdges[10]={0,1};
                    int NPoints = 1;
                    break;
            }
            break;
    }
    
    TChain *chain = new TChain("DecayTree");
    if(VarType==4) chain->Add("../Data_07_15/All2phiAloneKaonCosTheta.root");
    else           chain->Add("../Data_07_15/All2phi.root");

    char filename[100], label[100];
    
    gROOT->ProcessLine(".L ../diPhiPure.cp");
    gROOT->ProcessLine(".L ../diPhi_fit_framework.cpp");
    gROOT->ProcessLine(".L ../RooRelBreitWigner.cxx+");
    
    
    char varName [100];
    switch(VarType)
    {
        case 0:
            sprintf(varName,"Jpsi_PT");
            break;
        case 1:
            sprintf(varName,"Phi1_CosThetaABS_Mix");
            break;
        case 2:
            sprintf(varName,"nTracks");
            break;
        case 3:
            sprintf(varName,"Jpsi_ETA");
            break;
        case 4:
            sprintf(varName,"Kaon_CosTheta");
            break;
    }

    for(int ii=1; ii<=NPoints; ii++)
    {

        sprintf(label, "%s>=%f&&%s<=%f", varName, BinsEdges[ii-1], varName, BinsEdges[ii]);
        
        TCut CutPT(label);
        sprintf(filename,"%s.root",label);
        
        TFile *newfile = new TFile(filename,"recreate");
        TTree *newtree = chain->CopyTree(CutPT);
        
        newtree->Print();
        newfile->Write();
        newfile->Close();
        
        char commandPure[100];
        sprintf(commandPure, "diPhiPure(6,0,\"%s\")",filename);
        gROOT->ProcessLine(commandPure);
        
        char commandFit[100];
        sprintf(commandFit, "PTFramework(3,0,%i)",ii);
        gROOT->ProcessLine(commandFit);
        delete newfile;
    }
    delete chain;
}

//resType
// 1 - relative b->chic*BR(phiphi)
// 0 - relative b->chic


//DataType
// 0 - 1 bin
// 1 - 3 bins


void plotRelStat_in_Bins(int resType=1, int DataType = 12, int VarType)
{
    switch(VarType)
    {
        case 0:
            switch(DataType)
            {
                case 1:
                    Double_t BinsEdges[10]={0,7500,11000,40000};
                    int NPoints = 3;
                    break;
                case 0:
                    Double_t BinsEdges[10]={0,40000};
                    int NPoints = 1;
                    break;
            }
            break;
        case 1:
            switch(DataType)
            {
                case 1:
                    Double_t BinsEdges[10]={0.,0.3,0.6,1.};
                    int NPoints = 3;
                    break;
                case 0:
                    Double_t BinsEdges[10]={0,1};
                    int NPoints = 1;
                    break;
            }
            break;
        case 2:
            switch(DataType)
            {
                case 1:
                    Double_t BinsEdges[10]={0,150,250,530};
                    int NPoints = 3;
                    break;
                case 0:
                    Double_t BinsEdges[10]={0,530};
                    int NPoints = 1;
                    break;
            }
            break;
        case 3:
            switch(DataType)
            {
                case 1:
                    Double_t BinsEdges[10]={2,3,3.5,4.5};
                    int NPoints = 3;
                    break;
                case 0:
                    Double_t BinsEdges[10]={-10,10};
                    int NPoints = 1;
                    break;
            }
            break;
        case 4:
            switch(DataType)
            {
                case 1:
                    Double_t BinsEdges[10]={0.,0.3,0.6,1.};
                    int NPoints = 3;
                    break;
                case 0:
                    Double_t BinsEdges[10]={0,1};
                    int NPoints = 1;
                    break;
            }
            break;
    }
    
    
    char varName [100];
    switch(VarType)
    {
        case 0:
            sprintf(varName,"Jpsi_PT");
            break;
        case 1:
            sprintf(varName,"Phi1_CosTheta_Mix");
            break;
        case 2:
            sprintf(varName,"nTracks");
            break;
        case 3:
            sprintf(varName,"Jpsi_ETA");
            break;
        case 4:
            sprintf(varName,"Kaon_CosTheta");
            break;
    }
    
    
    Float_t BREtacphiphi = 1.76*0.001;
    Float_t BRChi0phiphi = 0.77*0.001;
    Float_t BRChi1phiphi = 0.42*0.001;
    Float_t BRChi2phiphi = 1.12*0.001;
    Float_t BRKK = 0.489;
    
//    Float_t BREtacphiphi = 1;
//    Float_t BRChi0phiphi = 1;
//    Float_t BRChi1phiphi = 1;
//    Float_t BRChi2phiphi = 1;
    
    Float_t erBREtacphiphi = 0.2*0.001;
    Float_t erBRChi0phiphi = 0.07*0.001;
    Float_t erBRChi1phiphi = 0.05*0.001;
    Float_t erBRChi2phiphi = 0.01*0.001;
    Float_t erBRKK = 0.005;
    
    
    Float_t NChi0Generated = 1105362.;
    Float_t NChi1Generated = 1101384.;
    Float_t NChi2Generated = 1135884.;
    Float_t NEtacGenerated = 2054064.;
    Float_t Luminosity = 1;
    
    FILE *fp = fopen("paramsPT.txt","r");
    Float_t x [10], ex [10];
    Float_t x0 [10], x1[10], x2[10], exDr[10];
    Float_t DataChi0 [10], eDataChi0Lo [10], eDataChi0Hi [10];
    Float_t DataChi1 [10], eDataChi1Lo [10], eDataChi1Hi [10];
    Float_t DataChi2 [10], eDataChi2Lo [10], eDataChi2Hi [10];
    Float_t DataEtac[10], eDataEtacLo[10],eDataEtacHi[10];
    Float_t a;
    int nPoint;
    
    for(int ii=0; ii<NPoints; ii++)
    {
        fscanf(fp, "%i %e %e %e",&nPoint,&DataEtac[ii],&eDataEtacLo[ii],&eDataEtacHi[ii]);
        fscanf(fp, "%e %e %e", &DataChi0[ii],&eDataChi0Lo[ii],&eDataChi0Hi[ii]);
        fscanf(fp, "%e %e %e", &DataChi1[ii],&eDataChi1Lo[ii],&eDataChi1Hi[ii]);
        fscanf(fp, "%e %e %e", &DataChi2[ii],&eDataChi2Lo[ii],&eDataChi2Hi[ii]);
        x[ii]=(BinsEdges[ii+1]+BinsEdges[ii])/2;
        x0[ii]=x[ii]-(BinsEdges[ii+1]-BinsEdges[ii])/20;
        x2[ii]=x[ii]+(BinsEdges[ii+1]-BinsEdges[ii])/20;
        x1[ii]=x[ii];
        exDr[ii]=x[ii]-BinsEdges[ii];
        ex[ii]=0;
    }
    fclose(fp);
    
    //     DataChi0GrDraw->Draw("AP");
    //     DataChi1GrDraw->Draw("P");
    //     DataChi2GrDraw->Draw("P");
    
    
    
    
    TChain * DecayTreeEtacMCAll=new TChain("DecayTree");
    if(VarType==4)  DecayTreeEtacMCAll->Add("../../MC/CAL_0/Etac/AllMCReducedAloneKaonCosTheta.root");
    else            DecayTreeEtacMCAll->Add("../../MC/CAL_0/Etac/AllMC_Reduced.root");
    TTree* DecayTreeEtacMC = DecayTreeEtacMCAll->CopyTree("");
    Int_t NEnMC=DecayTreeEtacMC->GetEntries();
    
    if(VarType==2) Int_t Branch_Value2;
    else Double_t Branch_Value2;
    DecayTreeEtacMC->SetBranchAddress(varName,&Branch_Value2);
    TH1D *histMCEtac = new TH1D("etac MC","etac MC", NPoints, BinsEdges);
    for (Int_t i=0; i<NEnMC; i++)
    {
        DecayTreeEtacMC->GetEntry(i);
        histMCEtac->Fill(TMath::Abs(Branch_Value2));
    }
    
    if(VarType==2)
    {
        TChain * DecayTreeEtacGenAll=new TChain("DecayTree");
        DecayTreeEtacGenAll->Add("../../MC/CAL_0/Etac/AllMC_Dirty.root");
    }
    else
    {
        TChain * DecayTreeEtacGenAll=new TChain("MCDecayTree");
        if(VarType==4)  DecayTreeEtacGenAll->Add("../../MC/GenSpectr/Etac/resAloneKaonCosTheta.root");
        else            DecayTreeEtacGenAll->Add("../../MC/GenSpectr/Etac/resEta.root");
    }
    TTree* DecayTreeEtacGen = DecayTreeEtacGenAll->CopyTree("");
    Int_t NEnGen=DecayTreeEtacGen->GetEntries();
    switch(VarType)
    {
        case 0 :
            Double_t Branch_Value3;
            DecayTreeEtacGen->SetBranchAddress("eta_c_1S_TRUEPT",&Branch_Value3);
            break;
        case 1 :
            Double_t Branch_Value3;
            DecayTreeEtacGen->SetBranchAddress("Phi1_TRUECosTheta",&Branch_Value3);
            break;
        case 2 :
            Int_t Branch_Value3;
            DecayTreeEtacGen->SetBranchAddress("nTracks",&Branch_Value3);
            break;
        case 3 :
            Double_t Branch_Value3;
            DecayTreeEtacGen->SetBranchAddress("Jpsi_ETA",&Branch_Value3);
            break;
        case 4 :
            Double_t Branch_Value3;
            DecayTreeEtacGen->SetBranchAddress("Kaon_CosTheta",&Branch_Value3);
            break;
    }
    TH1D *histEtacGen = new TH1D("etac Gen","etac Gen", NPoints, BinsEdges);
    for (Int_t i=0; i<NEnGen; i++)
    {
        DecayTreeEtacGen->GetEntry(i);
        if(VarType!=3)Branch_Value3=TMath::Abs(Branch_Value3);
        histEtacGen->Fill(Branch_Value3);
    }
    Float_t NEtacMCSelected = histMCEtac->Integral();
    Float_t NEtacMCGen = histEtacGen->Integral();
    
    
    
    
    
    
    TChain * DecayTreeChi0MCAll=new TChain("DecayTree");
    if(VarType==4)  DecayTreeChi0MCAll->Add("../../MC/CAL_0/Chic0/AllMCReducedAloneKaonCosTheta.root");
    else            DecayTreeChi0MCAll->Add("../../MC/CAL_0/Chic0/AllMC_Reduced.root");
    TTree* DecayTreeChi0MC = DecayTreeChi0MCAll->CopyTree("");
    Int_t NEnMC=DecayTreeChi0MC->GetEntries();
    if(VarType==2) Int_t Branch_Value2;
    else Double_t Branch_Value2;
    DecayTreeChi0MC->SetBranchAddress(varName,&Branch_Value2);
    TH1D *histMCChi0 = new TH1D("chic0 MC","chic0 MC", NPoints, BinsEdges);
    for (Int_t i=0; i<NEnMC; i++)
    {
        DecayTreeChi0MC->GetEntry(i);
        histMCChi0->Fill(TMath::Abs(Branch_Value2));
    }
    
    if(VarType==2)
    {
        TChain * DecayTreeChi0GenAll=new TChain("DecayTree");
        DecayTreeChi0GenAll->Add("../../MC/CAL_0/Chic0/AllMC_Dirty.root");
    }
    else
    {
        TChain * DecayTreeChi0GenAll=new TChain("MCDecayTree");
        if(VarType==4)  DecayTreeChi0GenAll->Add("../../MC/GenSpectr/Chic0/resAloneKaonCosTheta.root");
        else            DecayTreeChi0GenAll->Add("../../MC/GenSpectr/Chic0/resEta.root");
    }
    TTree* DecayTreeChi0Gen = DecayTreeChi0GenAll->CopyTree("");
    Int_t NEnGen=DecayTreeChi0Gen->GetEntries();
    switch(VarType)
    {
        case 0 :
            Double_t Branch_Value3;
            DecayTreeChi0Gen->SetBranchAddress("chi_c0_1P_TRUEPT",&Branch_Value3);
            break;
        case 1 :
            Double_t Branch_Value3;
            DecayTreeChi0Gen->SetBranchAddress("Phi1_TRUECosTheta",&Branch_Value3);
            break;
        case 2 :
            Int_t Branch_Value3;
            DecayTreeChi0Gen->SetBranchAddress("nTracks",&Branch_Value3);
            break;
        case 3 :
            Double_t Branch_Value3;
            DecayTreeChi0Gen->SetBranchAddress("Jpsi_ETA",&Branch_Value3);
            break;
        case 4 :
            Double_t Branch_Value3;
            DecayTreeChi0Gen->SetBranchAddress("Kaon_CosTheta",&Branch_Value3);
            break;
    }
    TH1D *histChic0Gen = new TH1D("Gen0","Gen0", NPoints, BinsEdges);
    for (Int_t i=0; i<NEnGen; i++)
    {
        DecayTreeChi0Gen->GetEntry(i);
        //if(VarType!=3)Branch_Value3=TMath::Abs(Branch_Value3);
        histChic0Gen->Fill(Branch_Value3);
    }
    Float_t NChic0MCSelected = histMCChi0->Integral();
    Float_t NChic0MCGen = histChic0Gen->Integral();
    
    
    
    TChain * DecayTreeChi1MCAll=new TChain("DecayTree");
    if(VarType==4)  DecayTreeChi1MCAll->Add("../../MC/CAL_0/Chic1/AllMCReducedAloneKaonCosTheta.root");
    else            DecayTreeChi1MCAll->Add("../../MC/CAL_0/Chic1/AllMC_Reduced.root");
    TTree* DecayTreeChi1MC = DecayTreeChi1MCAll->CopyTree("");
    Int_t NEnMC=DecayTreeChi1MC->GetEntries();
    if(VarType==2) Int_t Branch_Value2;
    else Double_t Branch_Value2;
    DecayTreeChi1MC->SetBranchAddress(varName,&Branch_Value2);
    TH1D *histMCChi1 = new TH1D("chic1 MC","chic1 MC", NPoints, BinsEdges);
    for (Int_t i=0; i<NEnMC; i++)
    {
        DecayTreeChi1MC->GetEntry(i);
        histMCChi1->Fill(TMath::Abs(Branch_Value2));
    }
    
    
    switch(VarType)//CORRUPTED MC UP TO NOW !!!!!
    {
        case 0:
            TChain * DecayTreeChi1GenAll=new TChain("MCDecayTree");
            DecayTreeChi1GenAll->Add("../../MC/GenSpectr/Chic1/resEta.root");
            break;
        case 1:
            TChain * DecayTreeChi1GenAll=new TChain("MCDecayTree");
            DecayTreeChi1GenAll->Add("../../MC/GenSpectr/Chic2/resEta.root");
            //DecayTreeChi1GenAll->Add("../../MC/GenSpectr/Chic0/res.root");
            break;
        case 2:
            TChain * DecayTreeChi1GenAll=new TChain("DecayTree");
            DecayTreeChi1GenAll->Add("../../MC/CAL_0/Chic1/AllMC_Dirty.root");
            break;
        case 3:
            TChain * DecayTreeChi1GenAll=new TChain("MCDecayTree");
            DecayTreeChi1GenAll->Add("../../MC/GenSpectr/Chic1/resEta.root");
            break;
        case 4:
            TChain * DecayTreeChi1GenAll=new TChain("MCDecayTree");
            DecayTreeChi1GenAll->Add("../../MC/GenSpectr/Chic2/resAloneKaonCosTheta.root");
            break;
    }
    TTree* DecayTreeChi1Gen = DecayTreeChi1GenAll->CopyTree("");
    Int_t NEnGen=DecayTreeChi1Gen->GetEntries();
    switch(VarType)
    {
        case 0 :
            Double_t Branch_Value3;
            DecayTreeChi1Gen->SetBranchAddress("chi_c1_1P_TRUEPT",&Branch_Value3);
            break;
        case 1 :
            Double_t Branch_Value3;
            DecayTreeChi1Gen->SetBranchAddress("Phi1_TRUECosTheta",&Branch_Value3);
            break;
        case 2 :
            Int_t Branch_Value3;
            DecayTreeChi1Gen->SetBranchAddress("nTracks",&Branch_Value3);
            break;
        case 3 :
            Double_t Branch_Value3;
            DecayTreeChi1Gen->SetBranchAddress("Jpsi_ETA",&Branch_Value3);
            break;
        case 4 :
            Double_t Branch_Value3;
            DecayTreeChi1Gen->SetBranchAddress("Kaon_CosTheta",&Branch_Value3);
            break;
    }
    TH1D *histChic1Gen = new TH1D("Gen1","Gen1", NPoints, BinsEdges);
    for (Int_t i=0; i<NEnGen; i++)
    {
        DecayTreeChi1Gen->GetEntry(i);
        if(VarType!=3)Branch_Value3=TMath::Abs(Branch_Value3);
        histChic1Gen->Fill(Branch_Value3);
    }
    Float_t NChic1MCSelected = histMCChi1->Integral();
    Float_t NChic1MCGen = histChic1Gen->Integral();
    
    
    TChain * DecayTreeChi2MCAll=new TChain("DecayTree");
    if(VarType==4)  DecayTreeChi2MCAll->Add("../../MC/CAL_0/Chic2/AllMCReducedAloneKaonCosTheta.root");
    else            DecayTreeChi2MCAll->Add("../../MC/CAL_0/Chic2/AllMC_Reduced.root");
    TTree* DecayTreeChi2MC = DecayTreeChi2MCAll->CopyTree("");
    Int_t NEnMC=DecayTreeChi2MC->GetEntries();
    if(VarType==2) Int_t Branch_Value2;
    else Double_t Branch_Value2;
    DecayTreeChi2MC->SetBranchAddress(varName,&Branch_Value2);
    TH1D *histMCChi2 = new TH1D("chic2 MC","chic2 MC", NPoints, BinsEdges);
    for (Int_t i=0; i<NEnMC; i++)
    {
        DecayTreeChi2MC->GetEntry(i);
        histMCChi2->Fill(TMath::Abs(Branch_Value2));
    }
    
    if(VarType==2)
    {
        TChain * DecayTreeChi2GenAll=new TChain("DecayTree");
        DecayTreeChi2GenAll->Add("../../MC/CAL_0/Chic2/AllMC_Dirty.root");
    }
    else
    {
        TChain * DecayTreeChi2GenAll=new TChain("MCDecayTree");
        if(VarType==4)  DecayTreeChi2GenAll->Add("../../MC/GenSpectr/Chic2/resAloneKaonCosTheta.root");
        else            DecayTreeChi2GenAll->Add("../../MC/GenSpectr/Chic2/resEta.root");
    }
    TTree* DecayTreeChi2Gen = DecayTreeChi2GenAll->CopyTree("");
    Int_t NEnGen=DecayTreeChi2Gen->GetEntries();
    switch(VarType)
    {
        case 0 :
            Double_t Branch_Value3;
            DecayTreeChi2Gen->SetBranchAddress("chi_c2_1P_TRUEPT",&Branch_Value3);
            break;
        case 1 :
            Double_t Branch_Value3;
            DecayTreeChi2Gen->SetBranchAddress("Phi1_TRUECosTheta",&Branch_Value3);
            break;
        case 2 :
            Int_t Branch_Value3;
            DecayTreeChi2Gen->SetBranchAddress("nTracks",&Branch_Value3);
            break;
        case 3 :
            Double_t Branch_Value3;
            DecayTreeChi2Gen->SetBranchAddress("Jpsi_ETA",&Branch_Value3);
            break;
        case 4 :
            Double_t Branch_Value3;
            DecayTreeChi2Gen->SetBranchAddress("Kaon_CosTheta",&Branch_Value3);
            break;
    }
    TH1D *histChic2Gen = new TH1D("Gen2","Gen2", NPoints, BinsEdges);
    for (Int_t i=0; i<NEnGen; i++)
    {
        DecayTreeChi2Gen->GetEntry(i);
        if(VarType!=3)Branch_Value3=TMath::Abs(Branch_Value3);
        histChic2Gen->Fill(Branch_Value3);
    }
    Float_t NChic2MCSelected = histMCChi2->Integral();
    Float_t NChic2MCGen = histChic2Gen->Integral();
    
    
    
    
    
    
    
    Float_t EtacRes[10], eEtacResLo [10], eEtacResHi [10], epsEtac[10];
    TH1D *histEtacEps = new TH1D("histEtacEps","histEtacEps", NPoints, BinsEdges);
    for(int ii=0; ii<NPoints; ii++)
    {
        epsEtac[ii]=Float_t(Float_t(histMCEtac->GetBinContent(ii+1))/Float_t(histEtacGen->GetBinContent(ii+1))/NEtacGenerated*NEtacMCGen);
        histEtacEps->SetBinContent(ii+1,epsEtac[ii]);
        
        Float_t ErrorRelHi = (Float_t)sqrt(1./(histMCEtac->GetBinContent(ii+1)) + (eDataEtacHi[ii]/DataEtac[ii])**2 );
        Float_t ErrorRelLo = (Float_t)sqrt(1./(histMCEtac->GetBinContent(ii+1)) + (eDataEtacLo[ii]/DataEtac[ii])**2 );
        
        if(resType==1)
            EtacRes[ii]=DataEtac[ii]/epsEtac[ii];
        else
            EtacRes[ii]=DataEtac[ii]/epsEtac[ii]/BREtacphiphi/BRKK/BRKK*1e-6;
        
        eEtacResHi[ii] = EtacRes[ii]*ErrorRelHi;
        eEtacResLo[ii] = EtacRes[ii]*ErrorRelLo;
        
    }
    
    
    
    
    Float_t Chic0Res[10], eChic0ResLo [10], eChic0ResHi [10], epsChic0[10];
    Float_t Chic0RelRes[10], eChic0RelResLo [10], eChic0RelResHi [10], epsChic0Rel[10];
    TH1D *histChic0Eps = new TH1D("histChic0Eps","histChic0Eps", NPoints, BinsEdges);
    for(int ii=0; ii<NPoints; ii++)
    {
        epsChic0[ii]=Float_t(Float_t(histMCChi0->GetBinContent(ii+1))/Float_t(histChic0Gen->GetBinContent(ii+1))/NChi0Generated*NChic0MCGen);
        histChic0Eps->SetBinContent(ii+1,epsChic0[ii]);
        
        Float_t ErrorRelHi = (Float_t)sqrt(1./(histMCChi0->GetBinContent(ii+1)) + (eDataChi0Hi[ii]/DataChi0[ii])**2);
        Float_t ErrorRelLo = (Float_t)sqrt(1./(histMCChi0->GetBinContent(ii+1)) + (eDataChi0Lo[ii]/DataChi0[ii])**2);
        
        if(resType==1)
            Chic0Res[ii]=DataChi0[ii]/epsChic0[ii];
        else
            Chic0Res[ii]=DataChi0[ii]/epsChic0[ii]/BRChi0phiphi/BRKK/BRKK*1e-6;
        
        eChic0ResHi[ii] = Chic0Res[ii]*ErrorRelHi;
        eChic0ResLo[ii] = Chic0Res[ii]*ErrorRelLo;
        
        Chic0RelRes[ii] = Chic0Res[ii]/EtacRes[ii];
        eChic0RelResHi[ii] = Chic0RelRes[ii]*TMath::Sqrt(ErrorRelHi*ErrorRelHi+eEtacResHi[ii]*eEtacResHi[ii]/EtacRes[ii]/EtacRes[ii]);
        eChic0RelResLo[ii] = Chic0RelRes[ii]*TMath::Sqrt(ErrorRelLo*ErrorRelLo+eEtacResLo[ii]*eEtacResLo[ii]/EtacRes[ii]/EtacRes[ii]);
    }
    
    
    Float_t Chic1Res[10], eChic1ResLo [10], eChic1ResHi [10], epsChic1[10];
    Float_t Chic1RelRes[10], eChic1RelResLo [10], eChic1RelResHi [10], epsChic1Rel[10];
    TH1D *histChic1Eps = new TH1D("histChic1Eps","histChic1Eps", NPoints, BinsEdges);
    for(int ii=0; ii<NPoints; ii++)
    {
        epsChic1[ii]=Float_t(Float_t(histMCChi1->GetBinContent(ii+1))/Float_t(histChic1Gen->GetBinContent(ii+1))/NChi1Generated*NChic1MCGen);
        histChic1Eps->SetBinContent(ii+1,epsChic1[ii]);
        
        Float_t ErrorRelHi = (Float_t)sqrt(1./(histMCChi1->GetBinContent(ii+1)) + (eDataChi1Hi[ii]/DataChi1[ii])**2);
        Float_t ErrorRelLo = (Float_t)sqrt(1./(histMCChi1->GetBinContent(ii+1)) + (eDataChi1Lo[ii]/DataChi1[ii])**2);
        
        if(resType==1)
            Chic1Res[ii]=DataChi1[ii]/epsChic1[ii];
        else
            Chic1Res[ii]=DataChi1[ii]/epsChic1[ii]/BRChi1phiphi/BRKK/BRKK*1e-6;
  
        eChic1ResHi[ii] = Chic1Res[ii]*ErrorRelHi;
        eChic1ResLo[ii] = Chic1Res[ii]*ErrorRelLo;
        
        Chic1RelRes[ii] = Chic1Res[ii]/EtacRes[ii];
        eChic1RelResHi[ii] = Chic1RelRes[ii]*TMath::Sqrt(ErrorRelHi*ErrorRelHi+eEtacResHi[ii]*eEtacResHi[ii]/EtacRes[ii]/EtacRes[ii]);
        eChic1RelResLo[ii] = Chic1RelRes[ii]*TMath::Sqrt(ErrorRelLo*ErrorRelLo+eEtacResLo[ii]*eEtacResLo[ii]/EtacRes[ii]/EtacRes[ii]);
    }
    
    
    Float_t Chic2Res[10], eChic2ResLo [10], eChic2ResHi [10], epsChic2[10];
    Float_t Chic2RelRes[10], eChic2RelResLo [10], eChic2RelResHi [10], epsChic2Rel[10];
    TH1D *histChic2Eps = new TH1D("histChic2Eps","histChic2Eps", NPoints, BinsEdges);
    for(int ii=0; ii<NPoints; ii++)
    {
        epsChic2[ii]=Float_t(Float_t(histMCChi2->GetBinContent(ii+1))/Float_t(histChic2Gen->GetBinContent(ii+1))/NChi2Generated*NChic2MCGen);
        histChic2Eps->SetBinContent(ii+1,epsChic2[ii]);
        
        Float_t ErrorRelHi = (Float_t)sqrt(1./(histMCChi2->GetBinContent(ii+1)) +  (eDataChi2Hi[ii]/DataChi2[ii])**2);
        Float_t ErrorRelLo = (Float_t)sqrt(1./(histMCChi2->GetBinContent(ii+1)) +  (eDataChi2Lo[ii]/DataChi2[ii])**2);
        
        if(resType==1)
            Chic2Res[ii]=DataChi2[ii]/epsChic2[ii];
        else
            Chic2Res[ii]=DataChi2[ii]/epsChic2[ii]/BRChi2phiphi/BRKK/BRKK*1e-6;
        
        eChic2ResHi[ii] = Chic2Res[ii]*ErrorRelHi;
        eChic2ResLo[ii] = Chic2Res[ii]*ErrorRelLo;
        
        Chic2RelRes[ii] = Chic2Res[ii]/EtacRes[ii];
        

        
        eChic2RelResHi[ii] = Chic2RelRes[ii]*TMath::Sqrt(ErrorRelHi*ErrorRelHi + eEtacResHi[ii]*eEtacResHi[ii]/EtacRes[ii]/EtacRes[ii]);
        eChic2RelResLo[ii] = Chic2RelRes[ii]*TMath::Sqrt(ErrorRelLo*ErrorRelLo + eEtacResLo[ii]*eEtacResLo[ii]/EtacRes[ii]/EtacRes[ii]);

    }
    
    
    
    Float_t ChicResAv[10], eChicResAvLo [10], ChicResAvHi [10], epsChicResAv[10];
    TH1D *histChicAvEps = new TH1D("histChicAvEps","histChicAvEps", NPoints, BinsEdges);
    for(int ii=0; ii<NPoints; ii++)
    {
        epsChicResAv[ii]=Float_t(Float_t(histMCChi2->GetBinContent(ii+1)+histMCChi1->GetBinContent(ii+1)+histMCChi0->GetBinContent(ii+1))
                                 /Float_t(histChic2Gen->GetBinContent(ii+1)+histChic1Gen->GetBinContent(ii+1)+histChic0Gen->GetBinContent(ii+1))
                                 /(NChi2Generated+NChi1Generated+NChi0Generated)*(NChic2MCGen+NChic1MCGen+NChic0MCGen));
        histChicAvEps->SetBinContent(ii+1,epsChicResAv[ii]);
    }
    
    
    
    TCanvas* canvEps = new TCanvas("canvEps", "canvEps", 800, 700);
    histChic1Eps->SetLineColor(kRed);
    histChic1Eps->Draw(/*"same"*/);
    histChic0Eps->SetLineColor(kBlue);
    histChic0Eps->Draw("same");
    histChic2Eps->SetLineColor(kBlack);
    histChic2Eps->Draw("same");
    leg = new TLegend(0.5,0.65,0.9,0.9);
    leg->AddEntry(histChic0Eps,"#chi_{c0}","l");
    leg->AddEntry(histChic1Eps,"#chi_{c1}","l");
    leg->AddEntry(histChic2Eps,"#chi_{c2}","l");
    leg->Draw();
    //histChicAvEps->Draw();
    
    
    
    TCanvas* canvRel = new TCanvas("canvRel", "canvRel", 800, 700);
    TGraphAsymmErrors* Chic0RelGr = new TGraphAsymmErrors(NPoints,x0,Chic0RelRes,ex,ex,eChic0RelResLo,eChic0RelResHi);
    TGraphAsymmErrors* Chic1RelGr = new TGraphAsymmErrors(NPoints,x1,Chic1RelRes,ex,ex,eChic1RelResLo,eChic1RelResHi);
    TGraphAsymmErrors* Chic2RelGr = new TGraphAsymmErrors(NPoints,x2,Chic2RelRes,ex,ex,eChic2RelResLo,eChic2RelResHi);
    Chic0RelGr->SetMarkerSize(0.4);
    Chic1RelGr->SetMarkerSize(0.4);
    Chic2RelGr->SetMarkerSize(0.4);
    Chic0RelGr->SetMarkerStyle(20);
    Chic1RelGr->SetMarkerStyle(20);
    Chic2RelGr->SetMarkerStyle(20);
    Chic2RelGr->SetMinimum(0);
    Chic2RelGr->SetMaximum(0.5);
    TF1* fit0 = new TF1("fit0","pol0");
    TF1* fit1 = new TF1("fit1","pol0");
    TF1* fit2 = new TF1("fit2","pol0");
    fit0->SetLineColor(1);
    fit1->SetLineColor(2);
    fit2->SetLineColor(3);
    Chic0RelGr->Fit(fit0,"ME");
    Chic1RelGr->Fit(fit1,"ME");
    Chic2RelGr->Fit(fit2,"ME");
    Chic0RelGr->SetLineColor(1);
    Chic1RelGr->SetLineColor(2);
    Chic2RelGr->SetLineColor(3);
    Chic2RelGr->GetXaxis()->SetRangeUser(x0[0]-(BinsEdges[1]-BinsEdges[0])/2,x2[NPoints]+(BinsEdges[1]-BinsEdges[0])/5);
    
    if(resType==1)
        Chic2RelGr->GetYaxis()->SetTitle("\\frac{BR(b->\\chi_{c}X)*BR(\\chi_{c}->\\phi\\phi)}{BR(b->\\eta_{c}X)*BR(\\eta_{c}->\\phi\\phi)}");
    else
        Chic2RelGr->GetYaxis()->SetTitle("\\frac{BR(b->\\chi_{c}X)}{BR(b->\\eta_{c}X)}");
        
    Chic2RelGr->Draw("AP");
    Chic1RelGr->Draw("P");
    Chic0RelGr->Draw("P");
    leg2 = new TLegend(0.7,0.7,0.9,0.9);
    leg2->AddEntry(Chic0RelGr,"#chi_{c0}","l");
    leg2->AddEntry(Chic1RelGr,"#chi_{c1}","l");
    leg2->AddEntry(Chic2RelGr,"#chi_{c2}","l");
    leg2->Draw();
    
    
    Float_t SumEtac =0,SumChic0=0,SumChic1=0,SumChic2=0;
    Float_t SumEtacErr =0,SumChic0Err=0,SumChic1Err=0,SumChic2Err=0;
    for(int ii=0;ii<NPoints;ii++)
    {
        SumEtac += EtacRes[ii];SumEtacErr+=eEtacResHi[ii]**2;
        SumChic0 += Chic0Res[ii];SumChic0Err+=eChic0ResHi[ii]**2;
        SumChic1 += Chic1Res[ii];SumChic1Err+=eChic1ResHi[ii]**2;
        SumChic2 += Chic2Res[ii];SumChic2Err+=eChic2ResHi[ii]**2;
    }
    
    
    SumEtacErr = TMath::Sqrt(SumEtacErr);
    SumChic0Err = TMath::Sqrt(SumChic0Err);
    SumChic1Err = TMath::Sqrt(SumChic1Err);
    SumChic2Err = TMath::Sqrt(SumChic2Err);
    
    cout<<"Etac "<<SumEtac<<" +- "<<SumEtacErr<<endl;
    cout<<"Chic0 "<<SumChic0<<" +- "<<SumChic0Err<<endl;
    cout<<"Chic1 "<<SumChic1<<" +- "<<SumChic1Err<<endl;
    cout<<"Chic2 "<<SumChic2<<" +- "<<SumChic2Err<<endl;
    
    

    cout<<"Chic0 chi2 = "<<fit0->GetChisquare()/fit0->GetNDF()<<endl;
    cout<<"Chic1 chi2 = "<<fit1->GetChisquare()/fit1->GetNDF()<<endl;
    cout<<"Chic2 chi2 = "<<fit2->GetChisquare()/fit2->GetNDF()<<endl;
    
    
    
    FILE *fp = fopen("Chic0Out.txt", "w");
    fprintf(fp,"%i\n",NPoints);
    for(int i=0;i<NPoints;i++)
        fprintf(fp,"%f %f %f %f %f %f \n",x[i],Chic0Res[i],exDr[i],exDr[i],eChic0ResLo[i],eChic0ResHi[i]);
    fclose(fp);
    
    
    
    FILE *fp = fopen("Chic1Out.txt", "w");
    fprintf(fp,"%i\n",NPoints);
    for(int i=0;i<NPoints;i++)
        fprintf(fp,"%f %f %f %f %f %f \n",x[i],Chic1Res[i],exDr[i],exDr[i],eChic1ResLo[i],eChic1ResHi[i]);
    fclose(fp);
    
    
    FILE *fp = fopen("Chic2Out.txt", "w");
    fprintf(fp,"%i\n",NPoints);
    for(int i=0;i<NPoints;i++)
        fprintf(fp,"%f %f %f %f %f %f \n",x[i],Chic2Res[i],exDr[i],exDr[i],eChic2ResLo[i],eChic2ResHi[i]);
    fclose(fp);
}

