
using namespace RooFit;

void gamma_etac(){
    int Type =0;
	
	
	gROOT->Reset();
	gROOT->SetStyle("Plain");
	TProof::Open("");
    
    
    gROOT->ProcessLine(".L RooRelBreitWigner.cxx+");
    gROOT->ProcessLine(".x lhcbStyle.C");

	Float_t etacMass =	2983.6; 
	Float_t chi0Mass =	3414.75; 
	Float_t chi1Mass =	3510.66; 
	Float_t hcMass   = 	3525.38; 
	Float_t chi2Mass =	3556.2; 
	Float_t etac2Mass =	3639.4; 
	Float_t x3872Mass = 	3871.69; 
	Float_t x3915Mass = 	3918.4; 
	Float_t x3927Mass = 	3927.2; 
	
	Float_t etacMassError =	0.7; 
	Float_t chi0MassError =	0.31; 
	Float_t chi1MassError =	0.07; 
	Float_t hcMassError   = 0.11; 
	Float_t chi2MassError =	0.09; 
	Float_t etac2MassError =1.3; 
	Float_t x3872MassError = 0.17; 
	Float_t x3915MassError = 1.9; 
	Float_t x3927MassError = 2.6; 
	
	Float_t etacGamma =	32.2; 
	Float_t chi0Gamma =	10.5; 
	Float_t chi1Gamma =	0.84; 
	Float_t hcGamma   = 	0.7; 
	Float_t chi2Gamma =	1.93; 
	Float_t etac2Gamma =	11.3;

	Float_t x3915Gamma = 	20; 
	Float_t x3927Gamma = 	24; 	
	
	Float_t etacGammaError =	0.9; 
	Float_t chi0GammaError =	0.6; 
	Float_t chi1GammaError =	0.04; 
	Float_t hcGammaError   = 	0.4; 
	Float_t chi2GammaError =	0.11; 
	Float_t etac2GammaError =	3.1; 
	Float_t x3915GammaError = 	5; 
	Float_t x3927GammaError = 	6; 
	
	Float_t EtacSigmaN_MC = 6.311;



	Float_t SigmaPar = 0.224;
	
	Float_t minMass = 2800;
	Float_t maxMass = 3950;
	Float_t binWidth = 10.;
	Int_t binN = int((maxMass-minMass)/binWidth);
	RooRealVar Jpsi_m_scaled("M(#phi#phi)", "M(#phi#phi)", minMass, maxMass, "MeV");
	Jpsi_m_scaled.setBins(10000);
	
	RooRealVar Counts("Counts", "Counts", 0, 2000);
	Counts.setBins(10000);
	
	Float_t Centre = (minMass+maxMass)/2;



// 	histogramms:
	switch(Type)
	{
	  case 4:
	    TFile* file = new TFile("diPhiPureAll_f0.root");
	    break;
	  case 7:
	    TFile* file = new TFile("splotAll.root");
	    break;
	  case 6:
	    TFile* file = new TFile("diPhiPureAll_MC.root");
	    break;
	  case 12:
	    TFile* file = new TFile("diPhiPureAll_f0_970_70.root");
	    break;
	  case 13:
	    TFile* file = new TFile("diPhiPureAll_f0_1010_70.root");
	    break;
	  case 14:
	    TFile* file = new TFile("diPhiPureAll_f0_990_40.root");
	    break;
	  case 15:
	    TFile* file = new TFile("diPhiPureAll_f0_990_100.root");
	    break;
	  case 16:
	    TFile* file = new TFile("diPhiPureAllExp.root");
	  case 17:
	    TFile* file = new TFile("diPhiPureConst.root");
	    break;
	  default:
	    TFile* file = new TFile("diPhiPureAll.root");
	}


	
	
if(Type==7)
{
        TTree* chain;
	file->GetObject("DecayTree", chain);
        RooDataSet* DataSet = new RooDataSet("dset", "dset", chain, RooArgList(Jpsi_m_scaled, varNDiPhi_sw), "", "varNDiPhi_sw");
}
else
{
	TH1D *histJpsiDiPhi  = (TH1D*) file->Get("histJpsiDiPhi");
	RooDataHist* DataSet = new RooDataHist("DataSet", "DataSet", Jpsi_m_scaled, histJpsiDiPhi);
}


	RooArgList listComp, listNorm, listComp2;
	RooArgSet showParams;

	
	
	RooRealVar FourKMass("FourKMass", "FourKMass", 4*493.677);
	RooRealVar varSigmaFactor("varSigmaFactor","varSigmaFactor",2.16);
	RooRealVar FitMin("FitMin","FitMin",minMass);
	
	RooRealVar varNarrowFraction("varNarrowFraction","varNarrowFraction",0.87);
//  	RooRealVar ASig("ASig", "ASig", SigmaPar, 1e-1, 1);
	RooRealVar ASig("ASig", "ASig", SigmaPar,0.1,0.3);

if(Type==8)
  RooRealVar radius("radius", "radius", 0.5);
else
  if(Type==9)
    RooRealVar radius("radius", "radius", 3.0);
  else
	RooRealVar radius("radius", "radius", 1.5);
  
	RooRealVar massa("massa", "massa", 1019.46, "MeV");
	RooRealVar massb("massb", "massb", 1019.46, "MeV");
	RooRealVar ConvCentre("ConvCentre","ConvCentre",0) ;	
	
	showParams.add(ASig);
	showParams.add(varNarrowFraction);

//========== fitting model components:
	RooRealVar varEtacNumber("N(#eta_{c}(1S))", "varEtacNumber", 6800, 1e3, 1e4);
	RooRealVar varEtacMass("M(#eta_{c})", "varEtacMass", 2984, 2975, 2989);
	RooRealVar varEtacGamma("varEtacGamma", "varEtacGamma", 32, 10, 60);
if(Type==2) 	
	RooRealVar varEtacSigmaN("varEtacSigmaN", "varEtacSigmaN", EtacSigmaN_MC); 
else
	RooFormulaVar varEtacSigmaN("varEtacSigmaN", "varEtacSigmaN", "sqrt(@1-@2)*(@0)", RooArgList(ASig, varEtacMass, FourKMass));

	RooFormulaVar varEtacSigmaW("varEtacSigmaW", "varEtacSigmaW", "@0*@1", RooArgList(varSigmaFactor, varEtacSigmaN));

	RooRelBreitWigner pdfEtacBWrel = RooRelBreitWigner("pdfEtacBWrel", "pdfEtacBWrel", Jpsi_m_scaled, varEtacMass, varEtacGamma,
			RooConst(0), radius, massa, massb);

	RooGaussian pdfEtacGaussN("pdfEtacGaussN","pdfEtacGaussN",Jpsi_m_scaled,ConvCentre,varEtacSigmaN);
	RooGaussian pdfEtacGaussW("pdfEtacGaussW","pdfEtacGaussW",Jpsi_m_scaled,ConvCentre,varEtacSigmaN);
	
	RooFFTConvPdf pdfEtacN("pdfEtacN","pdfEtacN",Jpsi_m_scaled,pdfEtacBWrel,pdfEtacGaussN) ;
	RooFFTConvPdf pdfEtacW("pdfEtacW","pdfEtacW",Jpsi_m_scaled,pdfEtacBWrel,pdfEtacGaussW) ;
	
	RooAddPdf pdfEtac("pdfEtac","pdfEtac",RooArgList(pdfEtacN,pdfEtacW),varNarrowFraction);	

	listComp.add(pdfEtac);
	listComp2.add(pdfEtac);
	listNorm.add(varEtacNumber);
	showParams.add(varEtacNumber);
	showParams.add(varEtacMass);
	showParams.add(varEtacGamma);

	
	

	
	RooRealVar varChi0toEtacRatio("N(#chi_{c0})/N(#eta_{c})","varChi0toEtacRatio",0.139, 0 ,0.3);
	RooFormulaVar varChi0Number("N(#chi_{c0})", "varChi0Number", "@0*@1", RooArgList(varEtacNumber, varChi0toEtacRatio));	
//	RooRealVar varChi0Number("N(#chi_{c0})", "varChi0Number", 0, 1e4);

if(Type==1)
	RooRealVar varChi0Mass("M(#chi_{c0})", "varChi0Mass", chi0Mass);
else
 	RooRealVar varChi0Mass("M(#chi_{c0})", "varChi0Mass", chi0Mass, chi0Mass-5, chi0Mass+5);


//        RooFormulaVar varChi0Mass("M(#chi_{c0})", "varChi0Mass", "@0+@1", RooArgList(varEtacMass, vardMEtacChi0));
	RooRealVar varChi0Gamma("varChi0Gamma", "varChi0Gamma", chi0Gamma);
	RooFormulaVar varChi0SigmaN("varChi0SigmaN", "varChi0SigmaN", "sqrt(@1-@2)*(@0)", RooArgList(ASig, varChi0Mass, FourKMass));
	RooFormulaVar varChi0SigmaW("varChi0SigmaW", "varChi0SigmaW", "@0*@1", RooArgList(varSigmaFactor, varChi0SigmaN));

	RooRelBreitWigner pdfChi0BWrel = RooRelBreitWigner("pdfChi0BWrel", "pdfChi0BWrel", Jpsi_m_scaled, varChi0Mass, varChi0Gamma,
			RooConst(0), radius, massa, massb);


	RooGaussian pdfChi0GaussN("pdfChi0GaussN","pdfChi0GaussN",Jpsi_m_scaled,ConvCentre,varChi0SigmaN);
	RooGaussian pdfChi0GaussW("pdfChi0GaussW","pdfChi0GaussW",Jpsi_m_scaled,ConvCentre,varChi0SigmaN);
	
	RooFFTConvPdf pdfChi0N("pdfChi0N","pdfChi0N",Jpsi_m_scaled,pdfChi0BWrel,pdfChi0GaussN) ;
	RooFFTConvPdf pdfChi0W("pdfChi0W","pdfChi0W",Jpsi_m_scaled,pdfChi0BWrel,pdfChi0GaussW) ;
	
	RooAddPdf pdfChi0("pdfChi0","pdfChi0",RooArgList(pdfChi0N,pdfChi0W),varNarrowFraction);	
	listComp.add(pdfChi0);
	listComp2.add(pdfChi0);
	listNorm.add(varChi0Number);
	showParams.add(varChi0toEtacRatio);

	
	RooRealVar varChi1toEtacRatio("N(#chi_{c1})/N(#eta_{c})","varChi1toEtacRatio",0.070, 0 ,0.3);
	RooFormulaVar varChi1Number("N(#chi_{c1})", "varChi1Number", "@0*@1", RooArgList(varEtacNumber, varChi1toEtacRatio));
	RooRealVar varChi1MassPDG("varChi1MassPDG", "varChi1MassPDG", chi1Mass);
	//RooFormulaVar varChi1Mass("varChi1Mass", "varChi1Mass", "@0-(@1-@2)", RooArgList(varChi0Mass, varChi0MassPDG, varChi1MassPDG));
if(Type==1)
	RooRealVar varChi1Mass("M(#chi_{c1})", "varChi1Mass", chi1Mass);
else
        RooRealVar varChi1Mass("M(#chi_{c1})", "varChi1Mass", chi1Mass-5, chi1Mass+5);


	RooRealVar varChi1Gamma("varChi1Gamma", "varChi1Gamma", chi1Gamma);
	RooFormulaVar varChi1SigmaN("varChi1SigmaN", "varChi1SigmaN", "sqrt(@1-@2)*(@0)", RooArgList(ASig, varChi1Mass, FourKMass));
	RooFormulaVar varChi1SigmaW("varChi1SigmaW", "varChi1SigmaW", "@0*@1", RooArgList(varSigmaFactor, varChi1SigmaN));
	
	
	RooRelBreitWigner pdfChi1BWrel = RooRelBreitWigner("pdfChi1BWrel", "pdfChi1BWrel", Jpsi_m_scaled, varChi1Mass, varChi1Gamma,
	RooConst(1), radius, massa, massb);


	
	RooGaussian pdfChi1GaussN("pdfChi1GaussN","pdfChi1GaussN",Jpsi_m_scaled,ConvCentre,varChi1SigmaN);
	RooGaussian pdfChi1GaussW("pdfChi1GaussW","pdfChi1GaussW",Jpsi_m_scaled,ConvCentre,varChi1SigmaN);
	
	RooFFTConvPdf pdfChi1N("pdfChi1N","pdfChi1N",Jpsi_m_scaled,pdfChi1BWrel,pdfChi1GaussN) ;
	RooFFTConvPdf pdfChi1W("pdfChi1W","pdfChi1W",Jpsi_m_scaled,pdfChi1BWrel,pdfChi1GaussW) ;
	
	RooAddPdf pdfChi1("pdfChi1","pdfChi1",RooArgList(pdfChi1N,pdfChi1W),varNarrowFraction);	
	listComp.add(pdfChi1);
	listComp2.add(pdfChi1);
	listNorm.add(varChi1Number);
	showParams.add(varChi1toEtacRatio);
	


	RooRealVar varChi2toEtacRatio("N(#chi_{c2})/N(#eta_{c})","varChi2toEtacRatio",0.090, 0 ,0.3);
	RooFormulaVar varChi2Number("N(#chi_{c2})", "varChi2Number", "@0*@1", RooArgList(varEtacNumber, varChi2toEtacRatio));
	
if(Type==1)
	RooRealVar varChi2Mass("M(#chi_{c2})", "varChi2Mass", chi2Mass);
else
        RooRealVar varChi2Mass("M(#chi_{c2})", "varChi2Mass", chi2Mass-5, chi2Mass+5);

	RooRealVar varChi2Gamma("varChi2Gamma", "varChi2Gamma", chi2Gamma);
	RooFormulaVar varChi2SigmaN("varChi2SigmaN", "varChi2SigmaN", "sqrt(@1-@2)*(@0)", RooArgList(ASig, varChi2Mass, FourKMass));
	RooFormulaVar varChi2SigmaW("varChi2SigmaW", "varChi2SigmaW", "@0*@1", RooArgList(varSigmaFactor, varChi2SigmaN));

	
	RooRelBreitWigner pdfChi2BWrel = RooRelBreitWigner("pdfChi2BWrel", "pdfChi2BWrel", Jpsi_m_scaled, varChi2Mass, varChi2Gamma,
			RooConst(2), radius, massa, massb);



	RooGaussian pdfChi2GaussN("pdfChi2GaussN","pdfChi2GaussN",Jpsi_m_scaled,ConvCentre,varChi2SigmaN);
	RooGaussian pdfChi2GaussW("pdfChi2GaussW","pdfChi2GaussW",Jpsi_m_scaled,ConvCentre,varChi2SigmaN);
	
	RooFFTConvPdf pdfChi2N("pdfChi2N","pdfChi2N",Jpsi_m_scaled,pdfChi2BWrel,pdfChi2GaussN) ;
	RooFFTConvPdf pdfChi2W("pdfChi2W","pdfChi2W",Jpsi_m_scaled,pdfChi2BWrel,pdfChi2GaussW) ;
	
	RooAddPdf pdfChi2("pdfChi2","pdfChi2",RooArgList(pdfChi2N,pdfChi2W),varNarrowFraction);	
	listComp.add(pdfChi2);
	listComp2.add(pdfChi2);
	listNorm.add(varChi2Number);
	showParams.add(varChi2toEtacRatio);
	//showParams.add(varChi1Mass);
        //showParams.add(varChi0Mass);
	//showParams.add(varChi2Mass);

	RooRealVar varEtac2toEtac1Ratio("N(#eta_{c}(2S)/N(#eta_{c}(1S)","varEtac2toEtac1Ratio",0.059, 0,0.1);
	RooFormulaVar varEtac2Number("N(#eta_{c}(2S))", "varEtac2Number", "@0*@1", RooArgList(varEtac2toEtac1Ratio, varEtacNumber));
//	RooRealVar varEtac2MassPDG("m(#eta_{c}(2S))", "varEtac2MassPDG", 3638.9, 3638.9-10, 3638.9+5);
if(Type==10)
  RooRealVar varEtac2Gamma("#Gamma(#eta_{c}(2S))", "varEtac2Gamma", etac2Gamma-etac2GammaError);
else
    if(Type==11)  
	RooRealVar varEtac2Gamma("#Gamma(#eta_{c}(2S))", "varEtac2Gamma", etac2Gamma+etac2GammaError);
    else
	RooRealVar varEtac2Gamma("#Gamma(#eta_{c}(2S))", "varEtac2Gamma", etac2Gamma,0,50);
    
	RooRealVar varEtac2Mass("M(#eta_{c}(2S))", "varEtac2Mass", etac2Mass, etac2Mass-15, etac2Mass+5);
 
	RooGaussian GammaConstraint("GammaConstraint","GammaConstraint",varEtac2Gamma,RooConst(etac2Gamma),RooConst(etac2GammaError)) ;
// 	RooGaussian MassConstraint("MassConstraint","MassConstraint", varEtac2Mass, RooConst(3639.4), RooConst(1.3)) ;
	RooGaussian MassConstraint("MassConstraint","MassConstraint", varEtac2Mass, RooConst(etac2Mass), RooConst(etac2MassError)) ;
	RooProdPdf GMConstraint("GMConstraint", "GMConstraint", RooArgSet(GammaConstraint, MassConstraint));
	
	RooFormulaVar varEtac2SigmaN("varEtac2SigmaN", "varEtac2SigmaN", "sqrt(@1-@2)*(@0)", RooArgList(ASig, varEtac2Mass, FourKMass));
	RooFormulaVar varEtac2SigmaW("varEtac2SigmaW", "varEtac2SigmaW", "@0*@1", RooArgList(varSigmaFactor, varEtac2SigmaN));
	RooRelBreitWigner pdfEtac2BWrel = RooRelBreitWigner("pdfEtac2BWrel", "pdfEtac2BWrel", Jpsi_m_scaled, varEtac2Mass, varEtac2Gamma,
			RooConst(0), radius, massa, massb);

	RooGaussian pdfEtac2GaussN("pdfEtac2GaussN","pdfEtac2GaussN",Jpsi_m_scaled,ConvCentre,varEtac2SigmaN);
	RooGaussian pdfEtac2GaussW("pdfEtac2GaussW","pdfEtac2GaussW",Jpsi_m_scaled,ConvCentre,varEtac2SigmaN);
	
	RooFFTConvPdf pdfEtac2N("pdfEtac2N","pdfEtac2N",Jpsi_m_scaled,pdfEtac2BWrel,pdfEtac2GaussN) ;
// 	pdfEtac2N.setShift(0,Centre);
	RooFFTConvPdf pdfEtac2W("pdfEtac2W","pdfEtac2W",Jpsi_m_scaled,pdfEtac2BWrel,pdfEtac2GaussW) ;
// 	pdfEtac2W.setShift(0,Centre);
	
	
	
	RooAddPdf pdfEtac2("pdfEtac2","pdfEtac2",RooArgList(pdfEtac2N,pdfEtac2W),varNarrowFraction);	
	
	RooAddPdf pdfEtac2G("pdfEtac2G","pdfEtac2G",RooArgList(pdfEtac2GaussN,pdfEtac2GaussW),varNarrowFraction);
	
	listComp.add(pdfEtac2);
	listComp2.add(pdfEtac2);
	
	listNorm.add(varEtac2Number);
	
	showParams.add(varEtac2toEtac1Ratio);
	//showParams.add(varEtac2Mass);
	showParams.add(varEtac2Gamma);
	
	
if(Type==5)
{
	RooRealVar AASig("AASig", "AASig", SigmaPar);


	

	RooRealVar varX3872toChi1Ratio("N(X(3872))/N(#chi_{c1})","varX3872toChi1Ratio",0.05,0,0.3);
	RooFormulaVar varX3872Number("N(X(3872))", "varX3872Number", "@0*@1", RooArgList(varChi1Number,varX3872toChi1Ratio));
	RooRealVar varX3872Mass("varX3872Mass", "varX3872Mass", x3872Mass);
	RooFormulaVar varX3872SigmaN("varX3872SigmaN", "varX3872SigmaN", "sqrt(@1-@2)*(@0)", RooArgList(AASig, varX3872Mass, FourKMass));
	RooFormulaVar varX3872SigmaW("varX3872SigmaW", "varX3872SigmaW", "@0*@1", RooArgList(varSigmaFactor, varX3872SigmaN));
	RooGaussian pdfX3872N("pdfX3872N", "pdfX3872N", Jpsi_m_scaled, varX3872Mass, varX3872SigmaN);
	RooGaussian pdfX3872W("pdfX3872W", "pdfX3872W", Jpsi_m_scaled, varX3872Mass, varX3872SigmaW);
	RooAddPdf pdfX3872("pdfX3872","pdfX3872",RooArgList(pdfX3872N,pdfX3872W),varNarrowFraction);
	listComp.add(pdfX3872);
	listNorm.add(varX3872Number);
	showParams.add(varX3872toChi1Ratio);

	
	
	
	RooRealVar varX3915toChi0Ratio("N(X(3915))/N(#chi_{c0})","varX3915toChi0Ratio",0.005,0,0.3);
	RooFormulaVar varX3915Number("N(X(3915))", "varX3915Number", "@0*@1", RooArgList(varChi0Number,varX3915toChi0Ratio));
	RooRealVar varX3915Mass("varX3915Mass", "varX3915Mass", x3915Mass);
	RooRealVar varX3915Gamma("var3915Gamma", "var3915Gamma", x3915Gamma);
	RooFormulaVar varX3915SigmaN("varX3915SigmaN", "varX3915SigmaN", "sqrt(@1-@2)*(@0)", RooArgList(AASig, varX3915Mass, FourKMass));
	RooFormulaVar varX3915SigmaW("varX3915SigmaW", "varX3915SigmaW", "@0*@1", RooArgList(varSigmaFactor, varX3915SigmaN));
	RooRelBreitWigner pdfX3915BWrel = RooRelBreitWigner("pdfX3915BWrel", "pdfX3915BWrel", Jpsi_m_scaled, varX3915Mass, varX3915Gamma,
			RooConst(0), radius, massa, massb);

	RooGaussian pdfX3915GaussN("pdfX3915GaussN","pdfX3915GaussN",Jpsi_m_scaled,ConvCentre,varX3915SigmaN);
	RooGaussian pdfX3915GaussW("pdfX3915GaussW","pdfX3915GaussW",Jpsi_m_scaled,ConvCentre,varX3915SigmaN);
	
	RooFFTConvPdf pdfX3915N("pdfX3915N","pdfX3915N",Jpsi_m_scaled,pdfX3915BWrel,pdfX3915GaussN) ;
	RooFFTConvPdf pdfX3915W("pdfX3915W","pdfX3915W",Jpsi_m_scaled,pdfX3915BWrel,pdfX3915GaussW) ;
	
	RooAddPdf pdfX3915("pdfX3915","pdfX3915",RooArgList(pdfX3915N,pdfX3915W),varNarrowFraction);
	listComp.add(pdfX3915);
	listNorm.add(varX3915Number);
	showParams.add(varX3915toChi0Ratio);
	
	
	RooRealVar varX3927toChi2Ratio("N(X(3927))/N(#chi_{c2})","varX3927toChi2Ratio",0.005,0,0.3);
	RooFormulaVar varX3927Number("N(X(3927))", "varX3927Number", "@0*@1", RooArgList(varChi2Number,varX3927toChi2Ratio));
	RooRealVar varX3927Mass("varX3927Mass", "varX3927Mass", x3927Mass);
	RooRealVar varX3927Gamma("var3927Gamma", "var3927Gamma", x3927Gamma);
	RooFormulaVar varX3927SigmaN("varX3927SigmaN", "varX3927SigmaN", "sqrt(@1-@2)*(@0)", RooArgList(AASig, varX3927Mass, FourKMass));
	RooFormulaVar varX3927SigmaW("varX3927SigmaW", "varX3927SigmaW", "@0*@1", RooArgList(varSigmaFactor, varX3927SigmaN));
	RooRelBreitWigner pdfX3927BWrel = RooRelBreitWigner("pdfX3927BWrel", "pdfX3927BWrel", Jpsi_m_scaled, varX3927Mass, varX3927Gamma,
			RooConst(2), radius, massa, massb);

	RooGaussian pdfX3927GaussN("pdfX3927GaussN","pdfX3927GaussN",Jpsi_m_scaled,ConvCentre,varX3927SigmaN);
	RooGaussian pdfX3927GaussW("pdfX3927GaussW","pdfX3927GaussW",Jpsi_m_scaled,ConvCentre,varX3927SigmaN);
	
	RooFFTConvPdf pdfX3927N("pdfX3927N","pdfX3927N",Jpsi_m_scaled,pdfX3927BWrel,pdfX3927GaussN) ;
	RooFFTConvPdf pdfX3927W("pdfX3927W","pdfX3927W",Jpsi_m_scaled,pdfX3927BWrel,pdfX3927GaussW) ;
	
	RooAddPdf pdfX3927("pdfX3927","pdfX3927",RooArgList(pdfX3927N,pdfX3927W),varNarrowFraction);
	listComp.add(pdfX3927);
	listNorm.add(varX3927Number);
	showParams.add(varX3927toChi2Ratio);

	
	//RooGaussian HcMCons("HcMCons","HcMCons",varHcMass,RooConst(3525.38),RooConst(0.11)) ;
// 	RooGaussian X3872MCons("X3872MCons","X3872MCons",varX3872Mass,RooConst(x3872Mass),RooConst(x3872MassE)) ;
// 	RooGaussian X3915MCons("X3915MCons","X3915MCons",varX3915Mass,RooConst(3918.4),RooConst(1.9)) ;
// 	RooGaussian X3927MCons("X3927MCons","X3927MCons",varX3927Mass,RooConst(3927.2),RooConst(2.6)) ;
	
// 	RooGaussian X3915GCons("X3915GCons","X3915GCons",varX3915Gamma,RooConst(x3915Gamma),RooConst(x3915GammaError)) ;
// 	RooGaussian X3927GCons("X3927GCons","X3927GCons",varX3927Gamma,RooConst(x3927Gamma),RooConst(x3927GammaError)) ;
// 	RooProdPdf XConstraint("XConstraint", "XConstraint", 
// 			       RooArgSet(X3915GCons,X3927GCons));
	
}

	RooRealVar varBgrNumber("N_{bgr}", "number of background events", 6.5e4, 1e4, 1e5);
	char label[200];
	RooRealVar varE0("varE0", "varE0", -3e-3, -1e-1, 0);
	RooRealVar varA0("varA0", "varA0", 1e-3,-1e-1, +1);
	RooRealVar varA1("varA1", "varA1", 5e-6,-1e-3, +1e-3);
	
	RooRealVar varB0("varB0", "varB0", -1e-3, -1e-1, 1e-1);
	RooRealVar varB1("varB1", "varB1", 5e-7, -1, 1);
	

	
	RooRealVar var2PhiMass("var2PhiMass", "var2PhiMass", 2038.9);


if(Type==3)
	RooGenericPdf pdfBgr("pdfBgr", "pdfBgr", "sqrt(@0-@1)*(1+(@0-@2)*@3+(@0-@2)*(@0-@2)*@4)",    //O2 without exp
			RooArgSet(Jpsi_m_scaled, var2PhiMass, FitMin, varB0, varB1));
else 
 	RooGenericPdf pdfBgr("pdfBgr", "pdfBgr", "sqrt(@0-@1)*exp(@0*@3)*(1+(@0-@2)*@4)",
    		        RooArgSet(Jpsi_m_scaled, var2PhiMass, FitMin, varE0, varA0));	
//  	RooGenericPdf pdfBgr("pdfBgr", "pdfBgr", "sqrt(@0-@1)*exp(@0*@3)*(1+(@0-@2)*@4+(@0-@2)*(@0-@2)*@5)",
//     		        RooArgSet(Jpsi_m_scaled, var2PhiMass, FitMin, varE0, varA0, varA1));	
	
//  	RooGenericPdf pdfBgr("pdfBgr", "pdfBgr", "sqrt(@0-@1)*exp(@0*@2)*(1+(@0-@1)*@3+(@0-@1)*(@0-@1)*@3)",
//     		        RooArgSet(Jpsi_m_scaled, var2PhiMass, varE0, varA0, varA1/*, varD0*/));
	
	
	
	
	
	
//     	RooGenericPdf pdfBgr("pdfBgr", "pdfBgr", "exp(@0*@1)*(1+(@0-@3)*@2+(@0-@3)*(@0-@3)*@4)*sqrt(@0-@3)",
//     			RooArgSet(Jpsi_m_scaled, varC0, varA0, var2PhiMass, varD0));
// 	RooGenericPdf pdfBgr("pdfBgr", "pdfBgr", "exp(@0*@1)*(1+(@0-@3)*@2+(@0-@3)*(@0-@3)*@4+(@0-@3)*(@0-@3)*(@0-@3)*@5)*sqrt(@0-@3)", //exp with O
// 			RooArgSet(Jpsi_m_scaled, varC0, varA0, var2PhiMass, varD0, varF0));

	listComp.add(pdfBgr);
	listComp2.add(pdfBgr);
	listNorm.add(varBgrNumber);

	RooAddPdf pdfModel("pdfModel", "pdfModel", listComp, listNorm);
	RooAddPdf pdfModel2("pdfModel2", "pdfModel2", listComp2, listNorm);
	
        varEtac2Gamma.setConstant(kTRUE);
	RooFitResult* results = pdfModel.fitTo(*DataSet/*, ExternalConstraints(GMConstraint)*//*, Minos(true)*/, Save(true), PrintLevel(0), NumCPU(4));
	
// 	varEtacNumber.setVal(10*varEtacNumber.getVal());
// 	varBgrNumber.setVal(10*varBgrNumber.getVal());
if(Type==7)
{
        varEtac2Gamma.setConstant(kTRUE);
  	results = pdfModel.fitTo(*DataSet, Save(true), PrintLevel(0), NumCPU(4));	
}
else
{
        varEtac2Gamma.setConstant(kTRUE);
	results = pdfModel.chi2FitTo(*DataSet,Extended(varEtacNumber.getVal()+varBgrNumber.getVal()), PrintLevel(2));
}

  RooAbsReal* nll = pdfModel.createChi2(*DataSet/*,ExternalConstraints(GMConstraint)*/) ;

  // I n t e r a c t i v e   m i n i m i z a t i o n ,   e r r o r   a n a l y s i s
  // -------------------------------------------------------------------------------

  // Create MINUIT interface object
  RooMinuit m(*nll) ;

  
//  ASig.setConstant(kTRUE);

  m.setVerbose(kTRUE) ;
  m.optimizeConst(kTRUE) ;
  m.setProfile(kTRUE) ;
  m.setMaxEvalMultiplier(200);


  m.migrad() ;
  m.setVerbose(kFALSE) ;
  m.hesse() ;
  varEtacMass.Print() ;
  m.minos(varEtacMass) ;

  RooFitResult* r = m.save() ;

  RooPlot* frame = m.contour(varEtacMass,varEtacGamma,1/*.516575/2*/,2/*TMath::Sqrt(6.18)/2*/) ;





  frame->SetTitle("RooMinuit contour plot") ;

  // Print the fit result snapshot
  r->Print("v") ;

  m.migrad() ;
  m.hesse() ;
  varEtacMass.Print() ;

  // Now fix Etac2Gamma
  varEtacMass.setConstant(kTRUE) ;

  // Rerun MIGRAD,HESSE
  m.migrad() ;
  m.hesse() ;
  varEtacGamma.Print() ;



  new TCanvas("rf601_intminuit","rf601_intminuit",800,600) ;
  gPad->SetLeftMargin(0.15) ; frame->GetYaxis()->SetTitleOffset(1.4) ;
    
  Float_t jpsiMass = 3096.900;
  Float_t ppbarDM =  114.7,  err_ppbarDM =  TMath::Sqrt(1.5**2+0.1**2);
  Float_t ppbarM = jpsiMass-ppbarDM;
  Float_t ppbarGamma = 25.8,   err_ppbarGamma = TMath::Sqrt(5.2**2+1.9**2);
  TLine* ppbarHor = new TLine(jpsiMass-ppbarDM-err_ppbarDM,ppbarGamma,jpsiMass-ppbarDM+err_ppbarDM,ppbarGamma);
  TLine* ppbarVer = new TLine(jpsiMass-ppbarDM,ppbarGamma-err_ppbarGamma,jpsiMass-ppbarDM,ppbarGamma+err_ppbarGamma);
  TMarker* ppbarPoint = new TMarker(jpsiMass-ppbarDM,ppbarGamma,21);
   ppbarHor->SetLineColor(kBlack);
   ppbarVer->SetLineColor(kBlack);
   ppbarPoint->SetMarkerColor(kBlack);
    
    TGraphErrors* ppbarGraph = new TGraphErrors(1,&ppbarM,&ppbarGamma,&err_ppbarDM,&err_ppbarGamma);
    ppbarGraph->SetLineColor(kBlack);
    ppbarGraph->SetMarkerColor(kBlack);
    ppbarGraph->SetMarkerStyle(21);
    ppbarGraph->SetMarkerSize(0.9);


    
    Float_t PDG_M =  2983.4,  err_PDG_M =  0.5;
    Float_t PDG_G =  31.8,  err_PDG_G =  0.8;
    TLine* PDGHor = new TLine(PDG_M-err_PDG_M,PDG_G,PDG_M+err_PDG_M,PDG_G);
    TLine* PDGVer = new TLine(PDG_M,PDG_G-err_PDG_G,PDG_M,PDG_G+err_PDG_G);
    TMarker* PDGPoint = new TMarker(PDG_M,PDG_G,21);
    
    PDGHor->SetLineColor(kRed);
    PDGVer->SetLineColor(kRed);
    PDGPoint->SetMarkerColor(kRed);
    
    TGraphErrors* PDGGraph = new TGraphErrors(1,&PDG_M,&PDG_G,&err_PDG_M,&err_PDG_G);
    PDGGraph->SetLineColor(kRed);
    PDGGraph->SetMarkerColor(kRed);
    PDGGraph->SetMarkerStyle(33);
    PDGGraph->SetMarkerSize(0.9);



    
    Float_t ppKDM =  110.2,  err_ppKDM =  TMath::Sqrt(0.5**2+0.9**2);
    Float_t ppKGamma = 34.0,   err_ppKGamma = TMath::Sqrt(1.9**2+1.3**2);
    Float_t ppKM = jpsiMass-ppKDM;
    TLine* ppKHor = new TLine(jpsiMass-ppKDM-err_ppKDM,ppKGamma,jpsiMass-ppKDM+err_ppKDM,ppKGamma);
    TLine* ppKVer = new TLine(jpsiMass-ppKDM,ppKGamma-err_ppKGamma,jpsiMass-ppKDM,ppKGamma+err_ppKGamma);
    TMarker* ppKPoint = new TMarker(jpsiMass-ppKDM,ppKGamma,21);
    
    ppKHor->SetLineColor(kBlue);
    ppKVer->SetLineColor(kBlue);
    ppKPoint->SetMarkerColor(kBlue);

    
    TGraphErrors* ppKGraph = new TGraphErrors(1,&ppKM,&ppKGamma,&err_ppKDM,&err_ppKGamma);
    ppKGraph->SetLineColor(kBlue);
    ppKGraph->SetMarkerColor(kBlue);
    ppKGraph->SetMarkerStyle(29);
    ppKGraph->SetMarkerSize(0.9);
    
    
  leg = new TLegend(0.65,0.7,0.9,0.9);
//  leg->AddEntry(PDGHor,"","le");
//  leg->AddEntry(ppbarHor,"","le");
//  leg->AddEntry(ppKHor,"","le");
    leg->AddEntry(PDGGraph," ","lep");
    leg->AddEntry(ppbarGraph," ","lep");
    leg->AddEntry(ppKGraph," ","lep");


    
    
  frame->GetXaxis()->SetLimits(2977.9,2989.);
  frame->GetYaxis()->SetLimits(19.,42.);
//  frame->SetMaximum(42.);
//  frame->SetMinimum(ppbarGamma-err_ppbarGamma);

  frame->Draw() ;

    
  frame->GetXaxis()->SetLimits(2979,2989.5);
  frame->GetYaxis()->SetLimits(18.5,42.);
    
//    ppbarHor->Draw();
//    ppbarVer->Draw();
//    ppbarPoint->Draw();
//    
//    PDGHor->Draw();
//    PDGVer->Draw();
//    PDGPoint->Draw();
//    
//    ppKHor->Draw();
//    ppKVer->Draw();
//    ppKPoint->Draw();
    PDGGraph->Draw("P");
    ppKGraph->Draw("P");
    ppbarGraph->Draw("P");
    
    
    leg->Draw();
}




