

using namespace RooFit;

void diPhiPure(int Type=6,int RangeNumber=0, char* infilename = "2phi_afterCut.root")
{
	gROOT->Reset();
	gROOT->SetStyle("Plain");
	gStyle->SetOptStat(000);
	TProof::Open("");

 
	Float_t minMassJpsi = 2800;
	Float_t maxMassJpsi = 3900;



	
	Float_t binWidthJpsi = 10.;
	Int_t binNJpsi = int((maxMassJpsi-minMassJpsi)/binWidthJpsi);
	Float_t minMassPhi = 1009;
	Float_t maxMassPhi = 1031;
	Float_t binWidthPhi = 1.;
	Int_t binNPhi = int((maxMassPhi-minMassPhi)/binWidthPhi);
	Float_t PhiMass = 1019.46;
	
 	RooRealVar Jpsi_m_scaled("Jpsi_m_scaled", "Jpsi_m_scaled", minMassJpsi, maxMassJpsi, "MeV");
	RooRealVar Phi1_m_scaled_Mix("Phi1_m_scaled_Mix", "Phi1_m_scaled_Mix", minMassPhi, maxMassPhi, "MeV");
	RooRealVar Phi2_m_scaled_Mix("Phi2_m_scaled_Mix", "Phi2_m_scaled_Mix", minMassPhi, maxMassPhi, "MeV");
	TH1F* histJpsiDiPhi = new TH1F("histJpsiDiPhi", "histJpsiDiPhi", binNJpsi, minMassJpsi, maxMassJpsi);
	TH1F* histJpsiDiK = new TH1F("histJpsiDiK", "histJpsiDiK", binNJpsi, minMassJpsi, maxMassJpsi);
	TH1F* histJpsiPhiK = new TH1F("histJpsiPhiK", "histJpsiPhiK", binNJpsi, minMassJpsi, maxMassJpsi);
	TH1F* histA1 = new TH1F("histA1", "histA1", binNJpsi, minMassJpsi, maxMassJpsi);
	TH1F* histA2 = new TH1F("histA2", "histA2", binNJpsi, minMassJpsi, maxMassJpsi);
	TH1F* histSigma = new TH1F("histSigma", "histSigma", binNJpsi, minMassJpsi-4*493.677, maxMassJpsi-4*493.677);

	TChain* chain = new TChain("DecayTree");
	chain->Add(infilename);

	RooDataSet* dsetFull = new RooDataSet("dsetFull", "dsetFull", chain, RooArgSet(Jpsi_m_scaled, Phi1_m_scaled_Mix, Phi2_m_scaled_Mix), "");
	RooRealVar varPhiMass("varPhiMass", "varPhiMass", PhiMass, PhiMass-1, PhiMass+1);
//	RooRealVar varPhiMass("varPhiMass", "varPhiMass", 1019.46);

	    RooRealVar varSigma("varSigma", "varSigma", 1.20);

	  
	RooRealVar varPhiGamma("varPhiGamma", "varPhiGamma", 4.26);
	RooRealVar var2KMass("var2KMass", "var2KMass", 493.67*2);
	
    
    
    RooRealVar massa("massa", "massa", 497.46, "MeV");
    RooRealVar massb("massb", "massb", 497.46, "MeV");
    RooRealVar ConvCentre("ConvCentre","ConvCentre",0) ;
    RooRealVar radius("radius", "radius", 0.5);
    
    RooRelBreitWigner pdfPhi1BWrel = RooRelBreitWigner("pdfPhi1BWrel", "pdfPhi1BWrel", Phi1_m_scaled_Mix, varPhiMass, varPhiGamma,
                                                       RooConst(1.), radius, massa, massb);
    RooGaussian pdfPhi1GaussN("pdfPhi1GaussN","pdfPhi1GaussN",Phi1_m_scaled_Mix,ConvCentre,varSigma);
    RooFFTConvPdf pdfPhi1("pdfPhi1","pdfPhi1",Phi1_m_scaled_Mix,pdfPhi1BWrel,pdfPhi1GaussN) ;

    
    RooRelBreitWigner pdfPhi2BWrel = RooRelBreitWigner("pdfPhi2BWrel", "pdfPhi2BWrel", Phi2_m_scaled_Mix, varPhiMass, varPhiGamma,
                                                       RooConst(1.), radius, massa, massb);
    RooGaussian pdfPhi2GaussN("pdfPhi2GaussN","pdfPhi2GaussN",Phi2_m_scaled_Mix,ConvCentre,varSigma);
    RooFFTConvPdf pdfPhi2("pdfPhi2","pdfPhi2",Phi2_m_scaled_Mix,pdfPhi2BWrel,pdfPhi2GaussN) ;
    
    
//	RooGenericPdf pdfPhi1("pdfPhi1", "pdfPhi1", "sqrt(@0-@1)*TMath::Voigt(@0-@2,@3,@4)",
//			      RooArgList(Phi1_m_scaled_Mix, var2KMass, varPhiMass,varSigma,varPhiGamma));
//	RooGenericPdf pdfPhi2("pdfPhi2", "pdfPhi2", "sqrt(@0-@1)*TMath::Voigt(@0-@2,@3,@4)",
//			      RooArgList(Phi2_m_scaled_Mix, var2KMass, varPhiMass,varSigma,varPhiGamma));
	

	RooRealVar varA1("varA1", "varA1", 0,-1,5);
	RooRealVar varA2("varA2", "varA2", 0,-1,5);

	RooGenericPdf pdfRoot1A1("pdfRoot1A1", "pdfRoot1A1", "sqrt(@0-@1)*TMath::Exp(-(@0-@1)*@2)", RooArgList(Phi1_m_scaled_Mix, var2KMass,varA1));
	RooGenericPdf pdfRoot1A2("pdfRoot1A2", "pdfRoot1A2", "sqrt(@0-@1)*TMath::Exp(-(@0-@1)*@2)", RooArgList(Phi1_m_scaled_Mix, var2KMass,varA2));
	RooGenericPdf pdfRoot2A1("pdfRoot2A1", "pdfRoot2A1", "sqrt(@0-@1)*TMath::Exp(-(@0-@1)*@2)", RooArgList(Phi2_m_scaled_Mix, var2KMass,varA1));
	RooGenericPdf pdfRoot2A2("pdfRoot2A2", "pdfRoot2A2", "sqrt(@0-@1)*TMath::Exp(-(@0-@1)*@2)", RooArgList(Phi2_m_scaled_Mix, var2KMass,varA2));
	
	
	RooProdPdf pdfSS("pdfSS", "pdfSS", RooArgSet(pdfPhi1, pdfPhi2));
	RooProdPdf pdfSB("pdfSB", "pdfSB", RooArgSet(pdfPhi1, pdfRoot2A1));
	RooProdPdf pdfBS("pdfBS", "pdfBS", RooArgSet(pdfRoot1A1, pdfPhi2));
	RooProdPdf pdfBB("pdfBB", "pdfBB", RooArgSet(pdfRoot1A2, pdfRoot2A2));
	
	RooRealVar varNDiPhi("varNDiPhi", "varNDiPhi", 0, 1e7);	
	RooRealVar varNPhiK("varNPhiK", "varNPhiK", 0, 1e7);
	RooRealVar varNDiK("varNDiK", "varNDiK", 0, 1e7);
	
	RooAddPdf pdfModel1("pdfModel1", "pdfModel1",  RooArgList(pdfPhi1,pdfPhi1,pdfRoot1A1,pdfRoot1A2), 
							RooArgList(varNDiPhi,varNPhiK,varNPhiK,varNDiK));
	RooAddPdf pdfModel2("pdfModel2", "pdfModel2",  RooArgList(pdfPhi2,pdfRoot2A1,pdfPhi2,pdfRoot2A2), 
							RooArgList(varNDiPhi,varNPhiK,varNPhiK,varNDiK));
	RooAddPdf pdfModel("pdfModel", "pdfModel",     RooArgList(pdfSS,pdfSB,pdfBS,pdfBB), 
							RooArgList(varNDiPhi,varNPhiK,varNPhiK,varNDiK));


	char label[200];
	Float_t massJpsiLo = minMassJpsi;
	Float_t massJpsiHi = maxMassJpsi;

	sprintf(label, "Jpsi_m_scaled>%i&&Jpsi_m_scaled<%i", massJpsiLo, massJpsiHi);
	RooDataSet* dset = dsetFull->reduce(Cut(label), Name("dset"), Title("dset"));
	varNDiPhi.setVal(dset->numEntries()/1.5);
	varNDiK.setVal(dset->numEntries()/3);
	varNPhiK.setVal(dset->numEntries()/4);
	
	
	    varA1.setVal(0);
	    varA1.setError(0);
	    varA1.setConstant(kTRUE);
	    varA2.setVal(0);
	    varA2.setError(0);
	    varA2.setConstant(kTRUE);


	
	RooFitResult* res = pdfModel.fitTo(*dset, Save(true),Minos(true),Strategy(2), Extended(dset->numEntries()), PrintLevel(0));
	
	varSigma.setError(0);
	float phiKKfr = varNPhiK.getVal()/varNDiPhi.getVal();
	float KKKKfr = varNDiK.getVal()/varNDiPhi.getVal();
	
	varPhiMass.setConstant(kTRUE);
    varPhiMass.setVal(PhiMass);
//	varSigma.setConstant(kTRUE);

	delete dset;
	delete res;
	
	


	
	
	TCanvas* canvTest = new TCanvas("canvTest", "canvTest", 1200, 400);
 	canvTest->Divide(2,1);

	Double_t minMassJpsiTest = minMassJpsi;
    Double_t maxMassJpsiTest = maxMassJpsi;
		

	
		massJpsiLo = minMassJpsiTest;
		massJpsiHi = maxMassJpsiTest;
			
 
		sprintf(label, "Jpsi_m_scaled>%i&&Jpsi_m_scaled<%i", massJpsiLo, massJpsiHi);
		RooDataSet* dset2 = dsetFull->reduce(Cut(label), Name("dset2"), Title("dset2"));
		
		varNDiPhi.setVal(dset2->numEntries()/1.5);
		varNDiK.setVal(dset2->numEntries()/4);
		varNPhiK.setVal(dset2->numEntries()/3);

		varNDiPhi.setMax(Double_t(dset2->numEntries()));	
		varNDiK.setMax(Double_t(dset2->numEntries()));	
		varNPhiK.setMax(Double_t(dset2->numEntries()));			
		
		RooFitResult* res = pdfModel.fitTo(*dset2, Save(true),Minos(true),Strategy(2), Extended(dset2->numEntries()), PrintLevel(0));
    
 		RooFitResult* res = pdfModel.fitTo(*dset2, Save(true),Minos(true),Strategy(1), Extended(dset2->numEntries()), PrintLevel(0));

		EDM = res->edm();		
						



		

		TH2* hPdf = pdfModel.createHistogram("Phi2_m_scaled_Mix,Phi1_m_scaled_Mix");

		RooPlot* frame1 = Phi1_m_scaled_Mix.frame(Title("#phi_{1} mass"));
		dset2->plotOn(frame1, Binning(binNPhi, minMassPhi, maxMassPhi));
		pdfModel1.plotOn(frame1);
//		pdfModel1.plotOn(frame1, Components(pdfRoot1), LineStyle(kDashed));

		RooPlot* frame2 = Phi2_m_scaled_Mix.frame(Title("#phi_{2} mass"));
		dset2->plotOn(frame2, Binning(binNPhi, minMassPhi, maxMassPhi));
		pdfModel2.plotOn(frame2);
//		pdfModel2.plotOn(frame2, Components(pdfRoot2), LineStyle(kDashed));

		canvTest->cd(1);
		frame1->Draw();
		canvTest->cd(2);
		frame2->Draw();



}

void runALL()
{
  diPhiPure(0,0);
  diPhiPure(2,0);
  diPhiPure(3,0);
  diPhiPure(4,0);
  
  diPhiPure(0,1);
  diPhiPure(2,1);
  diPhiPure(3,1);
  diPhiPure(4,1);
}

