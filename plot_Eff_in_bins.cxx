void plot_Eff_in_bins()
{

    const char* etacCut = "Jpsi_m_scaled>2890 && Jpsi_m_scaled<3100";
    const char* chic0Cut = "Jpsi_m_scaled>3380 && Jpsi_m_scaled<3420";
    const char* chic1Cut = "Jpsi_m_scaled>3490 && Jpsi_m_scaled<3535";
    const char* chic2Cut = "Jpsi_m_scaled>3535 && Jpsi_m_scaled<3585";
    const char* etac2Cut = "Jpsi_m_scaled>3600 && Jpsi_m_scaled<3685";
    
    

    
    
    
    TChain *etacMC = new TChain("DecayTree");
    etacMC->Add("../MC/CAL_0/Etac/AllMC_Reduced.root");
    TChain *chic0MC = new TChain("DecayTree");
    chic0MC->Add("../MC/CAL_0/Chic0/AllMC_Reduced.root");
    TChain *chic1MC = new TChain("DecayTree");
    chic1MC->Add("../MC/CAL_0/Chic1/AllMC_Reduced.root");
    TChain *chic2MC = new TChain("DecayTree");
    chic2MC->Add("../MC/CAL_0/Chic2/AllMC_Reduced.root");
    TChain *etac2MC = new TChain("DecayTree");
    etac2MC->Add("../MC/CAL_0/Etac2/AllMC_Reduced.root");

    
   
    
    TChain *etacgen = new TChain("MCDecayTreeTuple/MCDecayTree");
    etacgen->Add("../MC/GenSpectr/Etac/res.root");
    TChain *chic0gen = new TChain("MCDecayTreeTuple/MCDecayTree");
    chic0gen->Add("../MC/GenSpectr/Chic0/res.root");
    TChain *chic1gen = new TChain("MCDecayTreeTuple/MCDecayTree");
    chic1gen->Add("../MC/GenSpectr/Chic0/res.root");
    TChain *chic2gen = new TChain("MCDecayTreeTuple/MCDecayTree");
    chic2gen->Add("../MC/GenSpectr/Chic2/res.root");
    TChain *etac2gen = new TChain("MCDecayTreeTuple/MCDecayTree");
    etac2gen->Add("../MC/GenSpectr/Chic2/res.root");
    
    
    
    
    const char* br_name = "Kaon2_PT";
    float min=500.;
    float max=7000.;
    int nBins=10;
    
    Double_t etac_val,chic0_val,chic1_val,chic2_val,etac2_val;
    Double_t etac_val_MC,chic0_val_MC,chic1_val_MC,chic2_val_MC,etac2_val_MC;
    Double_t etac_val_gen,chic0_val_gen,chic1_val_gen,chic2_val_gen,etac2_val_gen;

    
    etacMC->SetBranchAddress(br_name,&etac_val_MC);
    chic0MC->SetBranchAddress(br_name,&chic0_val_MC);
    chic1MC->SetBranchAddress(br_name,&chic1_val_MC);
    chic2MC->SetBranchAddress(br_name,&chic2_val_MC);
    etac2MC->SetBranchAddress(br_name,&etac2_val_MC);

    
//    etacgen->SetBranchAddress("eta_c_1S_TRUEPT",&etac_val_gen);
//    chic0gen->SetBranchAddress("chi_c0_1P_TRUEPT",&chic0_val_gen);
//    chic1gen->SetBranchAddress("chi_c0_1P_TRUEPT",&chic1_val_gen);
//    chic2gen->SetBranchAddress("chi_c2_1P_TRUEPT",&chic2_val_gen);
//    etac2gen->SetBranchAddress("chi_c2_1P_TRUEPT",&etac2_val_gen);
    

    etacgen->SetBranchAddress("Kaon2_TRUEPT",&etac_val_gen);
    chic0gen->SetBranchAddress("Kaon2_TRUEPT",&chic0_val_gen);
    chic1gen->SetBranchAddress("Kaon2_TRUEPT",&chic1_val_gen);
    chic2gen->SetBranchAddress("Kaon2_TRUEPT",&chic2_val_gen);
    etac2gen->SetBranchAddress("Kaon2_TRUEPT",&etac2_val_gen);

    
    TH1F* etac_hist_MC = new TH1F("etac_MC","etac_MC",nBins,min,max);
    TH1F* chic0_hist_MC = new TH1F("chic0_MC","chic0_MC",nBins,min,max);
    TH1F* chic1_hist_MC = new TH1F("chic1_MC","chic1_MC",nBins,min,max);
    TH1F* chic2_hist_MC = new TH1F("chic2_MC","chic2_MC",nBins,min,max);
    TH1F* etac2_hist_MC = new TH1F("etac2_MC","etac2_MC",nBins,min,max);
    
    
    
    TH1F* etac_hist_gen = new TH1F("etac_gen","etac_gen",nBins,min,max);
    TH1F* chic0_hist_gen = new TH1F("chic0_gen","chic0_gen",nBins,min,max);
    TH1F* chic1_hist_gen = new TH1F("chic1_gen","chic1_gen",nBins,min,max);
    TH1F* chic2_hist_gen = new TH1F("chic2_gen","chic2_gen",nBins,min,max);
    TH1F* etac2_hist_gen = new TH1F("etac2_gen","etac2_gen",nBins,min,max);
    
    
    
    
    for(int i=0;i<etacMC->GetEntries();i++)
    {
        etacMC->GetEntry(i);
        etac_hist_MC->Fill(etac_val_MC);
    }
    etac_hist_MC->SetBinError(nBins,etac_hist_MC->GetBinError(nBins)/etac_hist_MC->Integral());
    etac_hist_MC->Scale(1/etac_hist_MC->Integral());
    
    for(int i=0;i<etac2MC->GetEntries();i++)
    {
        etac2MC->GetEntry(i);
        etac2_hist_MC->Fill(etac2_val_MC);
    }
    etac2_hist_MC->SetBinError(nBins,etac2_hist_MC->GetBinError(nBins)/etac2_hist_MC->Integral());
    etac2_hist_MC->Scale(1/etac2_hist_MC->Integral());
    
    for(int i=0;i<chic0MC->GetEntries();i++)
    {
        chic0MC->GetEntry(i);
        chic0_hist_MC->Fill(chic0_val_MC);
    }
    chic0_hist_MC->SetBinError(nBins,chic0_hist_MC->GetBinError(nBins)/chic0_hist_MC->Integral());
    chic0_hist_MC->Scale(1/chic0_hist_MC->Integral());
    
    for(int i=0;i<chic1MC->GetEntries();i++)
    {
        chic1MC->GetEntry(i);
        chic1_hist_MC->Fill(chic1_val_MC);
    }
    chic1_hist_MC->SetBinError(nBins,chic1_hist_MC->GetBinError(nBins)/chic1_hist_MC->Integral());
    chic1_hist_MC->Scale(1/chic1_hist_MC->Integral());
    
    for(int i=0;i<chic2MC->GetEntries();i++)
    {
        chic2MC->GetEntry(i);
        chic2_hist_MC->Fill(chic2_val_MC);
    }
    chic2_hist_MC->SetBinError(nBins,chic2_hist_MC->GetBinError(nBins)/chic2_hist_MC->Integral());
    chic2_hist_MC->Scale(1/chic2_hist_MC->Integral());
   
    
    

    
    
    for(int i=0;i<etacgen->GetEntries();i++)
    {
        etacgen->GetEntry(i);
        etac_hist_gen->Fill(etac_val_gen);
    }
    etac_hist_gen->SetBinError(nBins+1,etac_hist_gen->GetBinError(nBins+1)/etac_hist_gen->Integral());
    etac_hist_gen->Scale(1/etac_hist_gen->Integral());
    
    for(int i=0;i<etac2gen->GetEntries();i++)
    {
        etac2gen->GetEntry(i);
        etac2_hist_gen->Fill(etac2_val_gen);
    }
    etac2_hist_gen->SetBinError(nBins+1,etac2_hist_gen->GetBinError(nBins+1)/etac2_hist_gen->Integral());
    etac2_hist_gen->Scale(1/etac2_hist_gen->Integral());
    
    for(int i=0;i<chic0gen->GetEntries();i++)
    {
        chic0gen->GetEntry(i);
        chic0_hist_gen->Fill(chic0_val_gen);
    }
    chic0_hist_gen->SetBinError(nBins+1,chic0_hist_gen->GetBinError(nBins+1)/chic0_hist_gen->Integral());
    chic0_hist_gen->Scale(1/chic0_hist_gen->Integral());
    
    for(int i=0;i<chic1gen->GetEntries();i++)
    {
        chic1gen->GetEntry(i);
        chic1_hist_gen->Fill(chic1_val_gen);
    }
    chic1_hist_gen->SetBinError(nBins+1,chic1_hist_gen->GetBinError(nBins+1)/chic1_hist_gen->Integral());
    chic1_hist_gen->Scale(1/chic1_hist_gen->Integral());
    
    for(int i=0;i<chic2gen->GetEntries();i++)
    {
        chic2gen->GetEntry(i);
        chic2_hist_gen->Fill(chic2_val_gen);
    }
    chic2_hist_gen->SetBinError(nBins+1,chic2_hist_gen->GetBinError(nBins+1)/chic2_hist_gen->Integral());
    chic2_hist_gen->Scale(1/chic2_hist_gen->Integral());
    
    
    
    
    TH1F* etac_eff_MC = new TH1F("etac_eff_MC","etac_eff_MC",nBins,min,max);
    TH1F* chic0_eff_MC = new TH1F("chic0_eff_MC","chic0_eff_MC",nBins,min,max);
    TH1F* chic1_eff_MC = new TH1F("chic1_eff_MC","chic1_eff_MC",nBins,min,max);
    TH1F* chic2_eff_MC = new TH1F("chic2_eff_MC","chic2_eff_MC",nBins,min,max);
    TH1F* etac2_eff_MC = new TH1F("etac2_eff_MC","etac2_eff_MC",nBins,min,max);
    
    float eps;
    
    for(int i=0;i<nBins;i++)
    {

        if(etac_hist_gen->GetBinContent(i+1)!=0 && etac_hist_MC->GetBinContent(i+1)!=0)
        {
            etac_eff_MC->SetBinContent(i+1,etac_hist_MC->GetBinContent(i+1)/etac_hist_gen->GetBinContent(i+1));
            eps = TMath::Sqrt(etac_hist_MC->GetBinError(i+1)/etac_hist_MC->GetBinContent(i+1)*etac_hist_MC->GetBinError(i+1)/etac_hist_MC->GetBinContent(i+1)
                              +
                              etac_hist_gen->GetBinError(i+1)/etac_hist_gen->GetBinContent(i+1)*etac_hist_gen->GetBinError(i+1)/etac_hist_gen->GetBinContent(i+1));
            etac_eff_MC->SetBinError(i+1,eps*etac_eff_MC->GetBinContent(i+1));
        }
        
        if(chic0_hist_gen->GetBinContent(i+1)!=0 && chic0_hist_MC->GetBinContent(i+1)!=0)
        {
            chic0_eff_MC->SetBinContent(i+1,chic0_hist_MC->GetBinContent(i+1)/chic0_hist_gen->GetBinContent(i+1));
            eps = TMath::Sqrt(chic0_hist_MC->GetBinError(i+1)/chic0_hist_MC->GetBinContent(i+1)*chic0_hist_MC->GetBinError(i+1)/chic0_hist_MC->GetBinContent(i+1)
                              +
                              chic0_hist_gen->GetBinError(i+1)/chic0_hist_gen->GetBinContent(i+1)*chic0_hist_gen->GetBinError(i+1)/chic0_hist_gen->GetBinContent(i+1));
            chic0_eff_MC->SetBinError(i+1,eps*chic0_eff_MC->GetBinContent(i+1));
        }
        
        
        
        
        
        
        if(chic1_hist_gen->GetBinContent(i+1)!=0 && chic1_hist_MC->GetBinContent(i+1)!=0)
        {
            chic1_eff_MC->SetBinContent(i+1,chic1_hist_MC->GetBinContent(i+1)/chic1_hist_gen->GetBinContent(i+1));
            eps = TMath::Sqrt(chic1_hist_MC->GetBinError(i+1)/chic1_hist_MC->GetBinContent(i+1)*chic1_hist_MC->GetBinError(i+1)/chic1_hist_MC->GetBinContent(i+1)
                              +
                              chic1_hist_gen->GetBinError(i+1)/chic1_hist_gen->GetBinContent(i+1)*chic1_hist_gen->GetBinError(i+1)/chic1_hist_gen->GetBinContent(i+1));
            chic1_eff_MC->SetBinError(i+1,eps*chic1_eff_MC->GetBinContent(i+1));
        }
        
        

        if(chic2_hist_gen->GetBinContent(i+1)!=0 && chic2_hist_MC->GetBinContent(i+1)!=0)
        {
            chic2_eff_MC->SetBinContent(i+1,chic2_hist_MC->GetBinContent(i+1)/chic2_hist_gen->GetBinContent(i+1));
            eps = TMath::Sqrt(chic2_hist_MC->GetBinError(i+1)/chic2_hist_MC->GetBinContent(i+1)*chic2_hist_MC->GetBinError(i+1)/chic2_hist_MC->GetBinContent(i+1)
                              +
                              chic2_hist_gen->GetBinError(i+1)/chic2_hist_gen->GetBinContent(i+1)*chic2_hist_gen->GetBinError(i+1)/chic2_hist_gen->GetBinContent(i+1));
            chic2_eff_MC->SetBinError(i+1,eps*chic2_eff_MC->GetBinContent(i+1));
        }
        
        
        
        if(etac2_hist_gen->GetBinContent(i+1)!=0 && etac2_hist_MC->GetBinContent(i+1)!=0)
        {
            etac2_eff_MC->SetBinContent(i+1,etac2_hist_MC->GetBinContent(i+1)/etac2_hist_gen->GetBinContent(i+1));
            eps = TMath::Sqrt(etac2_hist_MC->GetBinError(i+1)/etac2_hist_MC->GetBinContent(i+1)*etac2_hist_MC->GetBinError(i+1)/etac2_hist_MC->GetBinContent(i+1)
                              +
                              etac2_hist_gen->GetBinError(i+1)/etac2_hist_gen->GetBinContent(i+1)*etac2_hist_gen->GetBinError(i+1)/etac2_hist_gen->GetBinContent(i+1));
            etac2_eff_MC->SetBinError(i+1,eps*etac2_eff_MC->GetBinContent(i+1));
        }
    }
    

    
    gStyle->SetOptStat(000);

    TCanvas* c0 = new TCanvas("c0","c0",800, 700) ;
    etac_eff_MC->SetLineColor(kRed);
    etac_eff_MC->Draw("E");
    chic0_eff_MC->SetLineColor(kGreen);
    chic0_eff_MC->Draw("Esame");
    chic2_eff_MC->SetLineColor(kBlue);
    chic2_eff_MC->Draw("Esame");

    

    
    TLegend *legend=new TLegend(0.7,0.7,0.90,0.90);
    legend->AddEntry(etac_eff_MC,"#eta_{c}");
    legend->AddEntry(chic0_eff_MC,"#chi_{c0}");
    legend->AddEntry(chic2_eff_MC,"#chi_{c2}");
    legend->Draw();
}