#include <TMath.h>


void cosTheta_MC_diff_states()
{
    gROOT->Reset();
    gStyle->SetOptStat(000);
    
    Double_t etac_Cos1, etac_Cos2;
    Double_t chic0_Cos1, chic0_Cos2;
    Double_t chic1_Cos1, chic1_Cos2;
    Double_t chic2_Cos1, chic2_Cos2;
    Double_t etac2_Cos1, etac2_Cos2;
    
    Double_t range0=-1;
    Double_t range1=1;
    Int_t nch=7;
    Double_t binWidth = (range1-range0)/nch;
    
    
    



    
    TChain * etac_DecayTreeMC=new TChain("DecayTree");
    etac_DecayTreeMC->Add("../MC/CAL_0/Etac/AllMC_Reduced.root");

    TChain * chic0_DecayTreeMC=new TChain("DecayTree");
    chic0_DecayTreeMC->Add("../MC/CAL_0/Chic0/AllMC_Reduced.root");
    
    TChain * chic1_DecayTreeMC=new TChain("DecayTree");
    chic1_DecayTreeMC->Add("../MC/CAL_0/Chic1/AllMC_Reduced.root");
    
    TChain * chic2_DecayTreeMC=new TChain("DecayTree");
    chic2_DecayTreeMC->Add("../MC/CAL_0/Chic2/AllMC_Reduced.root");
    
    TChain * etac2_DecayTreeMC=new TChain("DecayTree");
    etac2_DecayTreeMC->Add("../MC/CAL_0/Etac2/AllMC_Reduced.root");
    


    
    
        
    const Int_t etac_NEnMC=etac_DecayTreeMC->GetEntries();
    const Int_t etac2_NEnMC=etac2_DecayTreeMC->GetEntries();
    const Int_t chic0_NEnMC=chic0_DecayTreeMC->GetEntries();
    const Int_t chic1_NEnMC=chic1_DecayTreeMC->GetEntries();
    const Int_t chic2_NEnMC=chic2_DecayTreeMC->GetEntries();
    

    etac_DecayTreeMC->SetBranchAddress("Phi1_CosTheta",&etac_Cos1);
    etac_DecayTreeMC->SetBranchAddress("Phi2_CosTheta",&etac_Cos2);
    
    chic0_DecayTreeMC->SetBranchAddress("Phi1_CosTheta",&chic0_Cos1);
    chic0_DecayTreeMC->SetBranchAddress("Phi2_CosTheta",&chic0_Cos2);
    
    chic1_DecayTreeMC->SetBranchAddress("Phi1_CosTheta",&chic1_Cos1);
    chic1_DecayTreeMC->SetBranchAddress("Phi2_CosTheta",&chic1_Cos2);
    
    chic2_DecayTreeMC->SetBranchAddress("Phi1_CosTheta",&chic2_Cos1);
    chic2_DecayTreeMC->SetBranchAddress("Phi2_CosTheta",&chic2_Cos2);
    
    etac2_DecayTreeMC->SetBranchAddress("Phi1_CosTheta",&etac2_Cos1);
    etac2_DecayTreeMC->SetBranchAddress("Phi2_CosTheta",&etac2_Cos2);
    
    
    
    
    
    TH1D * etac_hist = new TH1D("etac_hist","etac_hist" , nch, range0, range1);
    TH1D * chic0_hist = new TH1D("chic0_hist","chic0_hist" , nch, range0, range1);
    TH1D * chic1_hist = new TH1D("chic1_hist","chic1_hist" , nch, range0, range1);
    TH1D * chic2_hist = new TH1D("chic2_hist","chic2_hist" , nch, range0, range1);
    TH1D * etac2_hist = new TH1D("etac2_hist","etac2_hist" , nch, range0, range1);
    
    
    for (Int_t i=0; i<etac_NEnMC; i++)
    {
        etac_DecayTreeMC->GetEntry(i);
        etac_hist->Fill(etac_Cos1);
        etac_hist->Fill(etac_Cos2);
    }
    
    for (Int_t i=0; i<chic0_NEnMC; i++)
    {
        chic0_DecayTreeMC->GetEntry(i);
        chic0_hist->Fill(chic0_Cos1);
        chic0_hist->Fill(chic0_Cos2);
    }
    
    for (Int_t i=0; i<chic1_NEnMC; i++)
    {
        chic1_DecayTreeMC->GetEntry(i);
        chic1_hist->Fill(chic1_Cos1);
        chic1_hist->Fill(chic1_Cos2);
    }
    
    for (Int_t i=0; i<chic2_NEnMC; i++)
    {
        chic2_DecayTreeMC->GetEntry(i);
        chic2_hist->Fill(chic2_Cos1);
        chic2_hist->Fill(chic2_Cos2);
    }
    
    for (Int_t i=0; i<etac2_NEnMC; i++)
    {
        etac2_DecayTreeMC->GetEntry(i);
        etac2_hist->Fill(etac2_Cos1);
        etac2_hist->Fill(etac2_Cos2);
    }
    
    

    etac_hist->SetBinError(nch+1,etac_hist->GetBinError(nch+1)/etac_NEnMC);
    etac_hist->Scale(1./etac_NEnMC);
    etac_hist->SetLineColor(kRed);
    etac_hist->Draw("E");

    chic0_hist->SetBinError(nch+1,chic0_hist->GetBinError(nch+1)/chic0_NEnMC);
    chic0_hist->Scale(1./chic0_NEnMC);
    chic0_hist->SetLineColor(kBlue);
    chic0_hist->Draw("Esame");
    
    chic1_hist->SetBinError(nch+1,chic1_hist->GetBinError(nch+1)/chic1_NEnMC);
    chic1_hist->Scale(1./chic1_NEnMC);
    chic1_hist->SetLineColor(kGreen);
    chic1_hist->Draw("Esame");
    
    chic2_hist->SetBinError(nch+1,chic2_hist->GetBinError(nch+1)/chic2_NEnMC);
    chic2_hist->Scale(1./chic2_NEnMC);
    chic2_hist->SetLineColor(kBlack);
    chic2_hist->Draw("Esame");
    
    etac2_hist->SetBinError(nch+1,etac2_hist->GetBinError(nch+1)/etac2_NEnMC);
    etac2_hist->Scale(1./etac2_NEnMC);
    etac2_hist->SetLineColor(kOrange);
    etac2_hist->Draw("Esame");
 
    
    TLegend *legend=new TLegend(0.7,0.7,0.90,0.90);
    legend->AddEntry(etac_hist,"#eta_{c}");
    legend->AddEntry(chic0_hist,"#chi_{c0}");
    legend->AddEntry(chic1_hist,"#chi_{c1}");
    legend->AddEntry(chic2_hist,"#chi_{c2}");
    legend->AddEntry(etac2_hist,"#eta_{c}(2S)");
    legend->Draw();
}
