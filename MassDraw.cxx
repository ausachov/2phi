/*
 * bs.cpp
 *
 *  Created on: May 31, 2013
 *      Author: maksym
 */
using namespace RooFit;

void MassDraw(){
	gROOT->Reset();
	gROOT->SetStyle(00);
	TProof::Open("");


	Float_t minMass = 5250;
	Float_t maxMass = 5500;
	Float_t binWidth = 10.;
	Int_t binN = int((maxMass-minMass)/binWidth);
	RooRealVar varMass("Jpsi_m_scaled", "Jpsi_m_scaled", minMass, maxMass, "MeV");

	TChain * DecayTree=new TChain("DecayTree");
	DecayTree->Add("Data_NewTrig/All2phi.root");
	RooDataSet* dset = new RooDataSet("dset", "dset", DecayTree, varMass);
	RooPlot* frame3 =varMass.frame();
	dset->plotOn(frame3, Binning(binN, minMass, maxMass));

	TCanvas* canvB = new TCanvas("canvB", "canvB", 1000, 600);
	frame3->Draw();

}



