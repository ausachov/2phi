/*
 * bs.cpp
 *
 *  Created on: May 31, 2013
 *      Author: maksym
 */
using namespace RooFit;


//0 - Base
//1 - MC in 2D
//2 - Linear Bg
//3 - 1 Gauss
//4 - Shift halfBin
//5 - add Bd

void bs(int Type=0){
	gROOT->Reset();
	gROOT->SetStyle("Plain");
	TProof::Open("");
    
    gROOT->ProcessLine(".x lhcbStyle.C");

	Float_t BsMass = 5366.77; // ±0.24 meV

	Float_t minMass = 5220;
	Float_t maxMass = 5500;
	Float_t binWidth = 10.;	
	Int_t binN = int((maxMass-minMass)/binWidth);

	
	
	
	


// //	unbinned data set:
// 	TChain * DecayTree=new TChain("DecayTree");
// 	DecayTree->Add("Data_phi12/AllStat_mixPhi.root");
// //	chain->Add("Reduced_MagUp2012.root");
// //	chain->Add("Reduced_MagDown2012.root");
	

if(Type==1)
	TFile* file = new TFile("bsPureAll_MC.root"); 
else
	if(Type==4)
	{
		TFile* file = new TFile("bsPureShift.root"); 
		minMass-=5;
		maxMass-=5;
	}
	else
	    TFile* file = new TFile("bsPureAll.root");

	RooRealVar varMass("M(#phi#phi)", "M(#phi#phi)", minMass, maxMass, "MeV");
	
	TH1D *histJpsiDiPhi  = (TH1D*) file->Get("histJpsiDiPhi");
	RooDataHist* dset = new RooDataHist("dset", "dset", varMass, histJpsiDiPhi);
	//RooDataSet* dset = new RooDataSet("dset", "dset", DecayTree, varMass);

	RooPlot* frame = varMass.frame(Title("Bs candidates"));

	RooRealVar varBsNumber("N(B_{s})", "varBsNumber", 2560, 0, 1e6);
	
	RooRealVar varBsMass("M(Bs)", "M(Bs)", 5375, 5375-20, 5375+20);


	RooRealVar varSigmaFactor("varSigmaFactor","varSigmaFactor",2.2);
	
if(Type==3)
        RooRealVar varNarrowFraction("varNarrowFraction","varNarrowFraction",1.0);
else
	RooRealVar varNarrowFraction("varNarrowFraction","varNarrowFraction",0.87);
	
	RooRealVar varBsSigmaN("varBsSigmaN", "varBsSigmaN", 13,5,40);
	RooFormulaVar varBsSigmaW("varBsSigmaW", "varBsSigmaW", "@0*@1", RooArgList(varSigmaFactor, varBsSigmaN));
	RooGaussian pdfBsN("pdfBsN", "pdfBsN", varMass, varBsMass, varBsSigmaN);
	RooGaussian pdfBsW("pdfBsW", "pdfBsW", varMass, varBsMass, varBsSigmaW);
	RooAddPdf pdfBs("pdfBs","pdfBs",RooArgList(pdfBsN,pdfBsW),varNarrowFraction);



    
	
	RooRealVar varBgrNBs("varBgrNBs", "varBgrNBs", 10,0, 1e4);
	RooRealVar varC4("varC4", "varC4", 0, -5, +5);

if(Type==2)
	RooGenericPdf pdfBsBgr("pdfBsBgr", "pdfBsBgr","1+(@0-5200)*@1", RooArgSet(varMass, varC4));
else
	RooExponential pdfBsBgr("pdfBsBgr", "pdfBsBgr", varMass, varC4);

    
if (Type!=5)
	RooAddPdf pdfModelBs("pdfModelBs", "pdfModelBs", RooArgList(pdfBs, pdfBsBgr), RooArgList(varBsNumber, varBgrNBs));
else
{
    RooRealVar varBdMass("M(Bd)", "M(Bd)",5279.58);
    RooRealVar BdBsRatio("BdBsRatio", "BdBsRatio",0.01,0,0.5);
    RooFormulaVar varBdNumber("N(B_{d})", "varBdNumber", "@0*@1",RooArgList(varBsNumber, BdBsRatio));
    
//    RooRealVar varBdNumber("N(B_{d})", "varBdNumber", 0,1000);
    
        
    RooGaussian pdfBdN("pdfBdN", "pdfBdN", varMass, varBdMass, varBsSigmaN);
    RooGaussian pdfBdW("pdfBdW", "pdfBdW", varMass, varBdMass, varBsSigmaW);
    RooAddPdf pdfBd("pdfBd","pdfBd",RooArgList(pdfBdN,pdfBdW),varNarrowFraction);
    
    RooAddPdf pdfModelBs("pdfModelBs", "pdfModelBs", RooArgList(pdfBs, pdfBsBgr, pdfBd), RooArgList(varBsNumber, varBgrNBs, varBdNumber));
}
    
    
	RooFitResult* results2 = pdfModelBs.chi2FitTo(*dset, Extended(), Save(true), Minos(true), PrintLevel(0), NumCPU(4));
	RooPlot* frame3 =varMass.frame();

	dset->plotOn(frame3, Binning(binN, minMass, maxMass));
	pdfModelBs.plotOn(frame3);
    dset->plotOn(frame3, Binning(binN, minMass, maxMass));
	RooArgSet showParams;
	showParams.add(varBsNumber);
	showParams.add(varBsSigmaN);
	showParams.add(varBsMass);
	pdfModelBs.paramOn(frame3, Layout(0.65, 0.95, 0.95), Parameters(showParams), ShowConstants(true));


	TCanvas* canvB = new TCanvas("canvB", "canvB", 1000, 600);
//	canvB->Divide(1, 2);
//	canvB->cd(1);
    frame3->GetYaxis()->SetLimits(0.,frame3->GetYaxis()->GetXmax());
	frame3->Draw();
	
	FILE * fout;
	if(Type==0)
	  fout = fopen ("BSparams.txt", "w");
	else
	  fout = fopen ("BSparams.txt", "a");
	
	

	
	if(Type==0)
	{    
	    fprintf(fout,"%i %3.10f %3.10f %3.10f %3.10f %3.10f %3.10f %3.10f %3.10f %3.10f %3.10f %3.10f \n",
		    Type,
		    varBsNumber.getVal()
		  );
	    fprintf(fout," %i  %f %f %f %f %f %f %f %f %f %f %f \n",
		    Type,
		    varBsNumber.getError()
		   );

	}
	else
	    fprintf(fout,"%i %3.10f %3.10f %3.10f %3.10f %3.10f %3.10f %3.10f %3.10f %3.10f %3.10f %3.10f \n",
		    Type,
		    varBsNumber.getVal()
		  );
	  
	fclose(fout);
}


void SysFramework()
{

  for(int i=0;i<5;i++)
    bs(i);
}


