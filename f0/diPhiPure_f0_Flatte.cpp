/*
 * diKMass.cpp
 *
 *  Created on: Apr 16, 2013
 *      Author: maksym
 */
using namespace RooFit;

//0- FixSigma
//1- VariableSigma
//2- Sigma MC



Double_t diPhiPure_f0_Flatte(Double_t Mass, Double_t g2g1Ratio, int RangeNumber=0, int Type=0)
{

	gROOT->Reset();
	gROOT->SetStyle("Plain");
	gStyle->SetOptStat(000);
	TProof::Open("");

switch(RangeNumber)
{
  case 0: 
	Float_t minMassJpsi = 2920;
	Float_t maxMassJpsi = 3050;
	break;  
  case 1: 
	Float_t minMassJpsi = 3370;
	Float_t maxMassJpsi = 3460;
	break;  
  case 2: 
	Float_t minMassJpsi = 3460;
	Float_t maxMassJpsi = 3530;
	break;
  case 3: 
	Float_t minMassJpsi = 3530;
	Float_t maxMassJpsi = 3600;
	break;  
  case 4: 
	Float_t minMassJpsi = 3600;
	Float_t maxMassJpsi = 3660;
	break;
  case 5:
	Float_t minMassJpsi = 5250;
	Float_t maxMassJpsi = 5500;
	break;
}  		
	
	
	Float_t f0Mass = Mass;	Float_t f0MassError = 20.;
	Float_t f0Gamma = 70.;	Float_t f0GammaError = 30.;	
	
	RooRealVar varg0("varg0","varg0",168);
	RooRealVar varg1("varg1","varg1",168*g2g1Ratio); //+-0.32
	RooRealVar varPiMass("varPiMass","varPiMass",139.57);
	RooRealVar varKMass("varKMass","varKMass",493.67);
	

	Float_t binWidthJpsi = 10.;
	Int_t binNJpsi = int((maxMassJpsi-minMassJpsi)/binWidthJpsi);
	Float_t minMassPhi = 1009;
	Float_t maxMassPhi = 1031;
	Float_t binWidthPhi = 1.;
	Int_t binNPhi = int((maxMassPhi-minMassPhi)/binWidthPhi);
	
	
	Float_t PhiMass = 1019.39;
	
 	RooRealVar Jpsi_m_scaled("Jpsi_m_scaled", "Jpsi_m_scaled", minMassJpsi, maxMassJpsi, "MeV");
	RooRealVar Phi1_m_scaled_Mix("Phi1_m_scaled_Mix", "Phi1_m_scaled_Mix", minMassPhi, maxMassPhi, "MeV");
	RooRealVar Phi2_m_scaled_Mix("Phi2_m_scaled_Mix", "Phi2_m_scaled_Mix", minMassPhi, maxMassPhi, "MeV");
	TH1F* histJpsiDiPhi = new TH1F("histJpsiDiPhi", "histJpsiDiPhi", binNJpsi, minMassJpsi, maxMassJpsi);
	TH1F* histJpsiDiK = new TH1F("histJpsiDiK", "histJpsiDiK", binNJpsi, minMassJpsi, maxMassJpsi);
	TH1F* histJpsiPhiK = new TH1F("histJpsiPhiK", "histJpsiPhiK", binNJpsi, minMassJpsi, maxMassJpsi);

	TH1F* histJpsiDif0 = new TH1F("histJpsiDif0", "histJpsiDif0", binNJpsi, minMassJpsi, maxMassJpsi);
	TH1F* histJpsif0K = new TH1F("histJpsif0K", "histJpsif0K", binNJpsi, minMassJpsi, maxMassJpsi);
	TH1F* histJpsiPhif0 = new TH1F("histJpsiPhif0", "histJpsiPhif0", binNJpsi, minMassJpsi, maxMassJpsi);	
	
	
	Phi1_m_scaled_Mix.setRange("range1",minMassPhi,maxMassPhi);
	Phi2_m_scaled_Mix.setRange("range2",minMassPhi,maxMassPhi);
	
	TChain* chain = new TChain("DecayTree");
	chain->Add("2phi_afterCut.root");

	RooDataSet* dsetFull = new RooDataSet("dsetFull", "dsetFull", chain, RooArgSet(Jpsi_m_scaled, Phi1_m_scaled_Mix, Phi2_m_scaled_Mix), "");

//	RooRealVar varPhiMass("varPhiMass", "varPhiMass", 1019.46, 1019.46-1,1019.46+1);
	RooRealVar varPhiMass("varPhiMass", "varPhiMass", PhiMass);
	
	
	switch(Type)
	{
	  case 2:
	      RooRealVar varSigma("varSigma", "varSigma", 1.15);
	      break;
	  case 1:
	      RooRealVar varSigma("varSigma", "varSigma", 1, 0.1, 4);
	      break;
	  default:
	      RooRealVar varSigma("varSigma", "varSigma", 1.20);
	      break;
	}
	
	RooRealVar varPhiGamma("varPhiGamma", "varPhiGamma", 4.26);
	RooRealVar var2KMass("var2KMass", "var2KMass", 493.67*2);
	
	

	RooRealVar varf0Mass("varf0Mass", "varf0Mass", Mass);
//	RooRealVar varf0Gamma("varf0Gamma", "varf0Gamma", Gamma);
	
	

	RooRealVar varA1("varA1", "varA1", 0,-0.1,1);
	RooRealVar varA2("varA2", "varA2", 0,-0.1,1);
	RooRealVar varA3("varA3", "varA3", 0);
	

	RooGenericPdf pdfPhi1("pdfPhi1", "pdfPhi1", "sqrt(@0-@1)*TMath::Voigt(@0-@2,@3,@4)",
			      RooArgList(Phi1_m_scaled_Mix, var2KMass, varPhiMass,varSigma,varPhiGamma));	
	RooGenericPdf pdfPhi2("pdfPhi2", "pdfPhi2", "sqrt(@0-@1)*TMath::Voigt(@0-@2,@3,@4)",
			      RooArgList(Phi2_m_scaled_Mix, var2KMass, varPhiMass,varSigma,varPhiGamma));
	pdfPhi1.setNormRange("range1");
	pdfPhi2.setNormRange("range2");

	RooGenericPdf pdfRoot1A1("pdfRoot1A1", "pdfRoot1A1", "sqrt(@0-@1)*(1+(@0-@1)*@2)", RooArgList(Phi1_m_scaled_Mix, var2KMass,varA1));
	RooGenericPdf pdfRoot1A2("pdfRoot1A2", "pdfRoot1A2", "sqrt(@0-@1)*(1+(@0-@1)*@2)", RooArgList(Phi1_m_scaled_Mix, var2KMass,varA2));
	RooGenericPdf pdfRoot2A1("pdfRoot2A1", "pdfRoot2A1", "sqrt(@0-@1)*(1+(@0-@1)*@2)", RooArgList(Phi2_m_scaled_Mix, var2KMass,varA1));
	RooGenericPdf pdfRoot2A2("pdfRoot2A2", "pdfRoot2A2", "sqrt(@0-@1)*(1+(@0-@1)*@2)", RooArgList(Phi2_m_scaled_Mix, var2KMass,varA2));	
	RooGenericPdf pdfRoot1A3("pdfRoot1A3", "pdfRoot1A3", "sqrt(@0-@1)*(1+(@0-@1)*@2)", RooArgList(Phi1_m_scaled_Mix, var2KMass,varA3));
	RooGenericPdf pdfRoot2A3("pdfRoot2A3", "pdfRoot2A3", "sqrt(@0-@1)*(1+(@0-@1)*@2)", RooArgList(Phi2_m_scaled_Mix, var2KMass,varA3));
	
	pdfRoot1A1.setNormRange("range1");
	pdfRoot1A2.setNormRange("range1");
	pdfRoot1A3.setNormRange("range1");

	pdfRoot2A1.setNormRange("range2");
	pdfRoot2A2.setNormRange("range2");
	pdfRoot2A3.setNormRange("range2");
		


	
	RooFlatte pdfFlatte1("pdfFlatte1","pdfFlatte1",Phi1_m_scaled_Mix, varf0Mass, 
			   				  varg1, varKMass,  varKMass,
			   				  varg0, varPiMass, varPiMass);
	RooGenericPdf Thresh1("Thresh1","Thresh1","(@0>@1)*TMath::Sqrt(TMath::Abs(@0-@1))",RooArgList(Phi1_m_scaled_Mix,var2KMass));
	RooProdPdf pdff01("f0BW1","f0BW1",RooArgSet(pdfFlatte1,Thresh1));		
	
	RooFlatte pdfFlatte2("pdfFlatte2","pdfFlatte2",Phi2_m_scaled_Mix, varf0Mass, 
			   				  varg1, varKMass,  varKMass,
			   				  varg0, varPiMass, varPiMass);
	RooGenericPdf Thresh2("Thresh2","Thresh2","(@0>@1)*TMath::Sqrt(TMath::Abs(@0-@1))",RooArgList(Phi2_m_scaled_Mix,var2KMass));
	RooProdPdf pdff02("f0BW2","f0BW2",RooArgSet(pdfFlatte2,Thresh2));	
	
	

	pdff01.setNormRange("range1");
	pdff02.setNormRange("range2");
	
	RooProdPdf pdfSS("pdfSS", "pdfSS", RooArgSet(pdfPhi1, pdfPhi2));
	RooProdPdf pdfSB("pdfSB", "pdfSB", RooArgSet(pdfPhi1, pdfRoot2A1));
	RooProdPdf pdfBS("pdfBS", "pdfBS", RooArgSet(pdfRoot1A1, pdfPhi2));
	RooProdPdf pdfBB("pdfBB", "pdfBB", RooArgSet(pdfRoot1A2, pdfRoot2A2));
	
	
	RooRealVar varNDiPhi("varNDiPhi", "varNDiPhi", 0, 1e5);	
	RooRealVar varNPhiK("varNPhiK", "varNPhiK", 0,1e5);
	RooRealVar varNDiK("varNDiK", "varNDiK", 0,1e5);
	
	
	RooProdPdf pdff0S("pdff0S", "pdff0S", RooArgSet(pdff01, pdfPhi2));
	RooProdPdf pdfSf0("pdfSf0", "pdfSf0", RooArgSet(pdfPhi1, pdff02));
	RooProdPdf pdff0B("pdff0B", "pdff0B", RooArgSet(pdff01, pdfRoot2A3));
	RooProdPdf pdfBf0("pdfBf0", "pdfBf0", RooArgSet(pdfRoot1A3, pdff02));
	RooProdPdf pdff0f0("pdff0f0", "pdff0f0", RooArgSet(pdff01, pdff02));
	

	
	RooRealVar varNDif0("varNDif0","varNDif0",0,1e5);	
	RooRealVar varNf0Phi("varNf0Phi","varNf0Phi",0,1e5);
	RooRealVar varNf0K("varNf0K","varNf0K",0,1e5);
	
	

	
	RooAddPdf pdfModel1("pdfModel1", "pdfModel1",  
			    RooArgList(pdfPhi1,  pdfPhi1, pdfRoot1A1,pdfRoot1A2,pdfRoot1A3,pdfPhi1,  pdff01,   pdff01,  pdff01), 
			    RooArgList(varNDiPhi,varNPhiK,varNPhiK,  varNDiK,   varNf0K,   varNf0Phi,varNf0Phi,varNDif0,varNf0K));
	RooAddPdf pdfModel2("pdfModel2", "pdfModel2",  
			    RooArgList(pdfPhi2,  pdfPhi2, pdfRoot2A1,pdfRoot2A2,pdfRoot2A3,pdfPhi2,  pdff02,   pdff02,  pdff02), 
			    RooArgList(varNDiPhi,varNPhiK,varNPhiK,  varNDiK,   varNf0K,   varNf0Phi,varNf0Phi,varNDif0,varNf0K));
	

	RooAddPdf pdfModelf01("pdfModelf01", "pdfModelf01",  
			    RooArgList(pdfRoot1A3,pdfPhi1,  pdff01,   pdff01,  pdff01), 
			    RooArgList(varNf0K,   varNf0Phi,varNf0Phi,varNDif0,varNf0K));
	RooAddPdf pdfModelf02("pdfModelf02", "pdfModelf02",  
			    RooArgList(pdfRoot2A3,pdfPhi2,  pdff02,   pdff02,  pdff02), 
			    RooArgList(varNf0K,   varNf0Phi,varNf0Phi,varNDif0,varNf0K));
		
	

	
	RooAddPdf pdfModel("pdfModel", "pdfModel",  
			   RooArgList(pdfSS,    pdfSB,   pdfBS,   pdfBB,   pdff0S,    pdfSf0,    pdff0B,  pdfBf0,  pdff0f0), 
			   RooArgList(varNDiPhi,varNPhiK,varNPhiK,varNDiK, varNf0Phi, varNf0Phi, varNf0K, varNf0K, varNDif0));


	char label[200];
	Float_t massJpsiLo, massJpsiHi;
	
	
	TCanvas* canvTest = new TCanvas("canvTest", "canvTest", 1200, 400);
 	canvTest->Divide(3,1);

	Double_t minMassJpsiTest = minMassJpsi;
	Double_t maxMassJpsiTest = maxMassJpsi;
	
		massJpsiLo = minMassJpsiTest;
		massJpsiHi = maxMassJpsiTest;
 

		sprintf(label, "Jpsi_m_scaled>%i&&Jpsi_m_scaled<%i", massJpsiLo, massJpsiHi);
		RooDataSet* dset2 = dsetFull->reduce(Cut(label), Name("dset2"), Title("dset2"));
		
	
		varNDiPhi.setVal(Double_t(dset2->numEntries())/2);	
		varNDiK.setVal(Double_t(dset2->numEntries())/5);	
		varNPhiK.setVal(Double_t(dset2->numEntries())/5);

		varNDiPhi.setMax(Double_t(dset2->numEntries()));	
		varNDiK.setMax(Double_t(dset2->numEntries()));	
		varNPhiK.setMax(Double_t(dset2->numEntries()));
		
		varNDif0.setVal(0);		
		varNf0Phi.setVal(0);
		varNf0K.setVal(0);
		varNDif0.setConstant(kTRUE);		
		varNf0Phi.setConstant(kTRUE);
		varNf0K.setConstant(kTRUE);		
		
		varA1.setConstant(kFALSE);
		varA2.setConstant(kFALSE);
		
		RooFitResult* res = pdfModel.fitTo(*dset2, Save(true), PrintLevel(0));
		res = pdfModel.fitTo(*dset2, Save(true), PrintLevel(0));
		
		Double_t EDM = res->edm();		
		
		if(EDM>2e-3)
		{
		    if(TMath::Abs(varA2.getVal(0))<(varA2.getError()/2))
		    {
		      varA2.setConstant(kTRUE);
		      varA2.setVal(0);
		      res = pdfModel.fitTo(*dset2, Save(true), PrintLevel(0));
		      EDM = res->edm();
		    }
				    
	    
		    if(TMath::Abs(varA1.getVal(0))<(varA1.getError()/2))
		    {
		      varA1.setConstant(kTRUE);
		      varA1.setVal(0);
		      res = pdfModel.fitTo(*dset2, Save(true), PrintLevel(0));
		      EDM = res->edm();
		    }
		    
		    if(EDM>2e-3)
		    {
		      varPhiMass.setConstant(kTRUE);
		      varPhiMass.setVal(PhiMass);
		      res = pdfModel.fitTo(*dset2, Save(true), PrintLevel(0));
		      EDM = res->edm();  
		    }
		    
		    if(EDM>2e-3)
		    {
		      varNDiK.setConstant(kTRUE);
		      varNDiK.setVal(0);
		      varA2.setConstant(kTRUE);
		      varA2.setVal(0);
		      res = pdfModel.fitTo(*dset2, Save(true), PrintLevel(0));
		      EDM = res->edm();  
		    }
		     
		}
		
		res = pdfModel.fitTo(*dset2, Save(true), PrintLevel(0));
		EDM = res->edm(); 
		
		if(TMath::Abs(varNDiK.getVal())<(varNDiK.getError()/2.))
		{
		  varNDiK.setConstant(kTRUE);
		  res = pdfModel.fitTo(*dset2, Save(true), PrintLevel(0));
		  EDM = res->edm(); 
		}		
		
	
		
		
		RooFitResult* res = pdfModel.fitTo(*dset2, Save(true), Extended(true), PrintLevel(0));
		res = pdfModel.fitTo(*dset2, Save(true), Extended(true), PrintLevel(0));
		
		Double_t zeroNDiPhi = varNDiPhi.getVal();
		
		varA1.setConstant(kTRUE);
		varA2.setConstant(kTRUE);
		
	
		varNDif0.setConstant(kFALSE);		
		varNf0Phi.setConstant(kFALSE);
		varNf0K.setConstant(kFALSE);
		
		varNDif0.setVal(Double_t(dset2->numEntries())/5);		
		varNf0Phi.setVal(Double_t(dset2->numEntries())/5);
		varNf0K.setVal(Double_t(dset2->numEntries())/10);		
		
		varNDif0.setMax(Double_t(dset2->numEntries()));		
		varNf0Phi.setMax(Double_t(dset2->numEntries()));	
		varNf0K.setMax(Double_t(dset2->numEntries()));
	

		
		
		RooFitResult* res = pdfModel.fitTo(*dset2, Save(true), Extended(true), PrintLevel(0));
		res = pdfModel.fitTo(*dset2, Save(true), Extended(true), PrintLevel(0));

		TH2* hPdf = pdfModel.createHistogram("Phi2_m_scaled_Mix,Phi1_m_scaled_Mix");

		RooPlot* frame1 = Phi1_m_scaled_Mix.frame(Title("#phi_{1} mass"));
		dset2->plotOn(frame1, Binning(binNPhi, minMassPhi, maxMassPhi));
		pdfModel1.plotOn(frame1);
		pdfModel1.plotOn(frame1, Components(pdff01),LineStyle(kDashed));

		RooPlot* frame2 = Phi2_m_scaled_Mix.frame(Title("#phi_{2} mass"));
		dset2->plotOn(frame2, Binning(binNPhi, minMassPhi, maxMassPhi));
		pdfModel2.plotOn(frame2);
		pdfModel2.plotOn(frame2, Components(pdff02),LineStyle(kDashed));

		canvTest->cd(1);
		frame1->DrawClone();
		canvTest->cd(2);
		frame2->DrawClone();

		canvTest->cd(3);
		hPdf->DrawClone("surf");
		
		
		delete frame1;
		delete frame2;
		delete hPdf;

		delete dset2;
		delete res;

	
	
/*	TCanvas* canvA = new TCanvas("canvA", "canvA", 800, 700);
	canvA->Divide(1, 3);
	canvA->cd(1);
	histJpsiDiPhi->DrawClone();
	canvA->cd(2);
	histJpsiPhiK->DrawClone();
	canvA->cd(3);
	histJpsiDiK->DrawClone();	
	
	TCanvas* canvB = new TCanvas("canvB", "canvB", 800, 700);
	canvB->Divide(1, 3);
	canvB->cd(1);
	histJpsiDif0->DrawClone();
	canvB->cd(2);
	histJpsif0K->DrawClone();
	canvB->cd(3);
	histJpsiPhif0->DrawClone();*/	
	

	
	cout<<varf0Mass.getVal()<<endl;
	cout<<varg1.getVal()<<endl;
	
	cout<<zeroNDiPhi<<endl<<endl;
	
	return varNDiPhi.getVal();
}

void Framework()
{
  Double_t results[5];
  int RangeNumber = 5;
  int Type = 0;
  
  results[0]=diPhiPure_f0_Flatte(990,4.12,RangeNumber);
  results[1]=diPhiPure_f0_Flatte(990,4.12-0.32,RangeNumber);
  results[2]=diPhiPure_f0_Flatte(990,4.12+0.32,RangeNumber);
  results[3]=diPhiPure_f0_Flatte(970,4.12,RangeNumber);
  results[4]=diPhiPure_f0_Flatte(1010,4.12,RangeNumber);
  
  cout<<"990 4.12 NDiPhi = "<<results[0]<<endl;
  cout<<"990 4.12-0.32 NDiPhi = "<<results[1]<<endl;
  cout<<"990 4.12+0.32 NDiPhi = "<<results[2]<<endl;
  cout<<"970 4.12 NDiPhi = "<<results[3]<<endl;
  cout<<"1010 4.12 NDiPhi = "<<results[4]<<endl;
}
