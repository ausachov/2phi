/*
 * diKMass.cpp
 *
 *  Created on: Apr 16, 2013
 *      Author: maksym
 */
using namespace RooFit;

//0- FixSigma
//1- VariableSigma
//2- Sigma MC

//3- 970, 70
//4- 1010, 70
//5- 990, 40
//6- 990, 100


void diPhiPure(int Type=0,int RangeNumber=2){
  

  
	gROOT->Reset();
	gROOT->SetStyle("Plain");
	gStyle->SetOptStat(000);
	TProof::Open("");

switch(RangeNumber)
{
  case 0: 
	Float_t minMassJpsi = 2700;
	Float_t maxMassJpsi = 4000;
	break;  
  case 1: 
	Float_t minMassJpsi = 5200;
	Float_t maxMassJpsi = 5550;
	break;  
  case 2: 
	Float_t minMassJpsi = 2700;
	Float_t maxMassJpsi = 6000;
	break;
}  	
	

	Float_t binWidthJpsi = 10.;
	Int_t binNJpsi = int((maxMassJpsi-minMassJpsi)/binWidthJpsi);
	Float_t minMassPhi = 1009;
	Float_t maxMassPhi = 1031;
	Float_t binWidthPhi = 1.;
	Int_t binNPhi = int((maxMassPhi-minMassPhi)/binWidthPhi);
	
	
	Float_t PhiMass = 1019.46;
	Float_t f0Mass = 990.;	Float_t f0MassError = 20.;
	Float_t f0Gamma = 70.;	Float_t f0GammaError = 30.;
	
 	RooRealVar Jpsi_m_scaled("Jpsi_m_scaled", "Jpsi_m_scaled", minMassJpsi, maxMassJpsi, "MeV");
	RooRealVar Phi1_m_scaled_Mix("Phi1_m_scaled_Mix", "Phi1_m_scaled_Mix", minMassPhi, maxMassPhi, "MeV");
	RooRealVar Phi2_m_scaled_Mix("Phi2_m_scaled_Mix", "Phi2_m_scaled_Mix", minMassPhi, maxMassPhi, "MeV");
	TH1F* histJpsiDiPhi = new TH1F("histJpsiDiPhi", "histJpsiDiPhi", binNJpsi, minMassJpsi, maxMassJpsi);
	TH1F* histJpsiDiK = new TH1F("histJpsiDiK", "histJpsiDiK", binNJpsi, minMassJpsi, maxMassJpsi);
	TH1F* histJpsiPhiK = new TH1F("histJpsiPhiK", "histJpsiPhiK", binNJpsi, minMassJpsi, maxMassJpsi);

	TChain* chain = new TChain("DecayTree");
	chain->Add("2phi_afterCut.root");

	RooDataSet* dsetFull = new RooDataSet("dsetFull", "dsetFull", chain, RooArgSet(Jpsi_m_scaled, Phi1_m_scaled_Mix, Phi2_m_scaled_Mix), "");

	RooRealVar varPhiMass("varPhiMass", "varPhiMass", 1019.46);
//	RooRealVar varPhiMass("varPhiMass", "varPhiMass", 1019.46);
	switch(Type)
	{
	  case 2:
	      RooRealVar varSigma("varSigma", "varSigma", 1.15);
	  case 1:
	      RooRealVar varSigma("varSigma", "varSigma", 1, 0.1, 4);
	  default:
	      RooRealVar varSigma("varSigma", "varSigma", 1.20);
	}
	RooRealVar varPhiGamma("varPhiGamma", "varPhiGamma", 4.26);
	RooRealVar var2KMass("var2KMass", "var2KMass", 493.67*2);
	
	
	switch(Type)
	{
	  case 3:
	    RooRealVar varf0Mass("varf0Mass", "varf0Mass", 970);
	    RooRealVar varf0Gamma("varf0Gamma", "varf0Gamma", 70);
	    break;
	  case 4:
	    RooRealVar varf0Mass("varf0Mass", "varf0Mass", 1010);
	    RooRealVar varf0Gamma("varf0Gamma", "varf0Gamma", 70);
	    break;
	  case 5:
	    RooRealVar varf0Mass("varf0Mass", "varf0Mass", 990);
	    RooRealVar varf0Gamma("varf0Gamma", "varf0Gamma", 40);
	    break;
	  case 6:
	    RooRealVar varf0Mass("varf0Mass", "varf0Mass", 990);
	    RooRealVar varf0Gamma("varf0Gamma", "varf0Gamma", 100);
	    break;
	  default:
	    RooRealVar varf0Mass("varf0Mass", "varf0Mass", 990);
	    RooRealVar varf0Gamma("varf0Gamma", "varf0Gamma", 70);
	    break;
	}
	
	

	RooRealVar varA1("varA1", "varA1", 0,-1,1);
	RooRealVar varA2("varA2", "varA2", 0,-1,1);
	

	RooGenericPdf pdfPhi1("pdfPhi1", "pdfPhi1", "sqrt(@0-@1)*TMath::Voigt(@0-@2,@3,@4)",
			      RooArgList(Phi1_m_scaled_Mix, var2KMass, varPhiMass,varSigma,varPhiGamma));	
	RooGenericPdf pdfPhi2("pdfPhi2", "pdfPhi2", "sqrt(@0-@1)*TMath::Voigt(@0-@2,@3,@4)",
			      RooArgList(Phi2_m_scaled_Mix, var2KMass, varPhiMass,varSigma,varPhiGamma));

	RooGenericPdf pdfRoot1A1("pdfRoot1A1", "pdfRoot1A1", "sqrt(@0-@1)*(1+(@0-@1)*@2)", RooArgList(Phi1_m_scaled_Mix, var2KMass,varA1));
	RooGenericPdf pdfRoot1A2("pdfRoot1A2", "pdfRoot1A2", "sqrt(@0-@1)*(1+(@0-@1)*@2)", RooArgList(Phi1_m_scaled_Mix, var2KMass,varA2));
	RooGenericPdf pdfRoot2A1("pdfRoot2A1", "pdfRoot2A1", "sqrt(@0-@1)*(1+(@0-@1)*@2)", RooArgList(Phi2_m_scaled_Mix, var2KMass,varA1));
	RooGenericPdf pdfRoot2A2("pdfRoot2A2", "pdfRoot2A2", "sqrt(@0-@1)*(1+(@0-@1)*@2)", RooArgList(Phi2_m_scaled_Mix, var2KMass,varA2));
	
	
		

	
	RooGenericPdf pdff01("f0BW1","f0BW1","sqrt(@0-@1)*TMath::BreitWigner(@0,@2,@3)", RooArgSet(Phi1_m_scaled_Mix, var2KMass, varf0Mass, varf0Gamma));
	RooGenericPdf pdff02("f0BW2","f0BW2","sqrt(@0-@1)*TMath::BreitWigner(@0,@2,@3)", RooArgSet(Phi2_m_scaled_Mix, var2KMass, varf0Mass, varf0Gamma));

	
	
	RooProdPdf pdfSS("pdfSS", "pdfSS", RooArgSet(pdfPhi1, pdfPhi2));
	RooProdPdf pdfSB("pdfSB", "pdfSB", RooArgSet(pdfPhi1, pdfRoot2A1));
	RooProdPdf pdfBS("pdfBS", "pdfBS", RooArgSet(pdfRoot1A1, pdfPhi2));
	RooProdPdf pdfBB("pdfBB", "pdfBB", RooArgSet(pdfRoot1A2, pdfRoot2A2));
	
	
	
	
	RooRealVar varNDiPhi("varNDiPhi", "varNDiPhi", 1e2, 0, 1e7);	
	RooRealVar varNPhiK("varNPhiK", "varNPhiK", 0, 1e7);
	RooRealVar varNDiK("varNDiK", "varNDiK", 0, 1e7);
	
	
	RooProdPdf pdff0S("pdff0S", "pdff0S", RooArgSet(pdff01, pdfPhi2));
	RooProdPdf pdfSf0("pdfSf0", "pdfSf0", RooArgSet(pdfPhi1, pdff02));
	RooProdPdf pdff0B("pdff0B", "pdff0B", RooArgSet(pdff01, pdfRoot2A1));
	RooProdPdf pdfBf0("pdfBf0", "pdfBf0", RooArgSet(pdfRoot1A1, pdff02));
	RooProdPdf pdff0f0("pdff0f0", "pdff0f0", RooArgSet(pdff01, pdff02));
	
	RooRealVar f0Fraction("f0Fraction","f0Fraction",0.1,0,0.3);
	RooFormulaVar varNDif0("varNDif0","varNDif0","@0*@1*@1",RooArgList(varNDiPhi,f0Fraction));	
	RooFormulaVar varNf0K("varNf0K","varNf0K","@0*@1",RooArgList(varNPhiK,f0Fraction));
	RooFormulaVar varNf0Phi("varNf0Phi","varNf0Phi","@0*@1",RooArgList(varNDiPhi,f0Fraction));
	
	RooAddPdf pdfModel1("pdfModel1", "pdfModel1",  
			    RooArgList(pdfPhi1,  pdfPhi1, pdfRoot1A1,pdfRoot1A2,pdfRoot1A1,pdfPhi1,  pdff01,   pdff01,  pdff01), 
			    RooArgList(varNDiPhi,varNPhiK,varNPhiK,  varNDiK,   varNf0K,   varNf0Phi,varNf0Phi,varNDif0,varNf0K));
	RooAddPdf pdfModel2("pdfModel2", "pdfModel2",  
			    RooArgList(pdfPhi2,  pdfPhi2, pdfRoot2A1,pdfRoot2A2,pdfRoot2A1,pdfPhi2,  pdff02,   pdff02,  pdff02), 
			    RooArgList(varNDiPhi,varNPhiK,varNPhiK,  varNDiK,   varNf0K,   varNf0Phi,varNf0Phi,varNDif0,varNf0K));
	


		
	

	
	RooAddPdf pdfModel("pdfModel", "pdfModel",  
			   RooArgList(pdfSS,pdfSB,pdfBS,pdfBB, pdff0S, pdfSf0, pdff0B, pdfBf0, pdff0f0), 
			   RooArgList(varNDiPhi,varNPhiK,varNPhiK,varNDiK, varNf0Phi, varNf0Phi, varNf0K, varNf0K, varNDif0));


	char label[200];
	Float_t massJpsiLo, massJpsiHi;

	for (Int_t i=0; i<binNJpsi; i++){
		massJpsiLo = minMassJpsi + i*binWidthJpsi;
		massJpsiHi = minMassJpsi + (i+1)*binWidthJpsi;

		varA1.setConstant(kFALSE);
		varA2.setConstant(kFALSE);
		varNDiK.setConstant(kFALSE);

		
		sprintf(label, "Jpsi_m_scaled>%i&&Jpsi_m_scaled<%i", massJpsiLo, massJpsiHi);
		RooDataSet* dset = dsetFull->reduce(Cut(label), Name("dset"), Title("dset"));
		
		varNDiPhi.setVal(dset.numEntries()/1.5);
		varNDiK.setVal(dset.numEntries()/3);
		varNPhiK.setVal(dset.numEntries()/4);
		
		RooFitResult* res = pdfModel.fitTo(*dset, Save(true), PrintLevel(0));
		res = pdfModel.fitTo(*dset, Save(true), PrintLevel(0));
		
		Double_t EDM = res->edm();
		
		
		
		if(EDM>2e-3)
		{
		    if(TMath::Abs(varA2.getVal(0))<(varA2.getError()/2))
		    {
		      varA2.setConstant(kTRUE);
		      varA2.setVal(0);
		      res = pdfModel.fitTo(*dset, Save(true), PrintLevel(0));
		      EDM = res->edm();
		    }
				    
	    
		    if(TMath::Abs(varA1.getVal(0))<(varA1.getError()/2))
		    {
		      varA1.setConstant(kTRUE);
		      varA1.setVal(0);
		      res = pdfModel.fitTo(*dset, Save(true), PrintLevel(0));
		      EDM = res->edm();
		    }
		    
		    
		    if(EDM>2e-3)
		    {
		      varNDiK.setConstant(kTRUE);
		      varNDiK.setVal(0);
		      varA2.setConstant(kTRUE);
		      varA2.setVal(0);
		      res = pdfModel.fitTo(*dset, Save(true), PrintLevel(0));
		      EDM = res->edm();  
		    }

		    
// 		    if(EDM>2e-3)
// 		    {
// 		      varNPhiK.setVal(0);
// 		      res = pdfModel.fitTo(*dset, Save(true), PrintLevel(0));
// 		      EDM = res->edm(); 
// 		    }
		    res = pdfModel.fitTo(*dset, Save(true), PrintLevel(0));
		    EDM = res->edm();  
		}
		
		
		histJpsiDiPhi->SetBinContent(i+1, varNDiPhi.getVal());
		histJpsiDiPhi->SetBinError(i+1, varNDiPhi.getError());

		histJpsiDiK->SetBinContent(i+1, varNDiK.getVal());
		histJpsiDiK->SetBinError(i+1, varNDiK.getError());

		histJpsiPhiK->SetBinContent(i+1, varNPhiK.getVal());
		histJpsiPhiK->SetBinError(i+1, varNPhiK.getError());

		

		if(EDM>2e-3)return;
		


		delete dset;
		delete res;
	}

	
	
// 	TCanvas* canvTest = new TCanvas("canvTest", "canvTest", 1200, 400);
//  	canvTest->Divide(3,1);
// 
// 	Double_t minMassJpsiTest = minMassJpsi;
// 	Double_t maxMassJpsiTest = maxMassJpsi;
// 		
// // 	Double_t minMassJpsiTest = 3510;
// // 	Double_t maxMassJpsiTest = 3520;
// 	
// 		massJpsiLo = minMassJpsiTest;
// 		massJpsiHi = maxMassJpsiTest;
//  
// 		varA1.setConstant(kFALSE);
// 		varA2.setConstant(kFALSE);
// 		varPhiMass.setConstant(kFALSE);
// 		varNDiK.setConstant(kFALSE);
// 		f0Fraction.setConstant(kFALSE);
// 		
// 		sprintf(label, "Jpsi_m_scaled>%i&&Jpsi_m_scaled<%i", massJpsiLo, massJpsiHi);
// 		RooDataSet* dset2 = dsetFull->reduce(Cut(label), Name("dset2"), Title("dset2"));
// 		RooFitResult* res = pdfModel.fitTo(*dset2, Save(true), PrintLevel(0));
// 		
// 		if(varA1.getError()>TMath::Abs(varA1.getVal()))
// 		{
// 		  varA1.setConstant(kTRUE);
// 		  varA1.setVal(0);
// 		}
// 		
// 		res = pdfModel.fitTo(*dset2, Save(true), PrintLevel(0));
// 		
// 		if(varA2.getError()>TMath::Abs(varA2.getVal()))
// 		{
// 		  varA2.setConstant(kTRUE);
// 		  varA2.setVal(0);
// 		}
// 		res = pdfModel.fitTo(*dset2, Save(true), PrintLevel(0));
// 		Double_t EDM = res->edm();
// 		
// 		
// 		if(EDM>2e-3)
// 		{
// 		  varPhiMass.setConstant(kTRUE);
// 		  varPhiMass.setVal(PhiMass);
// 		  res = pdfModel.fitTo(*dset2, Save(true), PrintLevel(0));
// 		  EDM = res->edm();  
// 		}
// 		
// 		if(EDM>2e-3)
// 		{
// 		  varNDiK.setConstant(kTRUE);
// 		  varNDiK.setVal(0);
// 		  varA2.setConstant(kTRUE);
// 		  varA2.setVal(0);
// 		  res = pdfModel.fitTo(*dset2, Save(true), PrintLevel(0));
// 		  EDM = res->edm();  
// 		}
// 		
// 		
// 		if(EDM>2e-3)
// 		{
// 		  f0Fraction.setConstant(kTRUE);
// 		  f0Fraction.setVal(0);
// 		  res = pdfModel.fitTo(*dset2, Save(true), PrintLevel(0));
// 		  EDM = res->edm();  
// 		}
// 
// 		TH2* hPdf = pdfModel.createHistogram("Phi2_m_scaled_Mix,Phi1_m_scaled_Mix");
// 
// 		RooPlot* frame1 = Phi1_m_scaled_Mix.frame(Title("#phi_{1} mass"));
// 		dset2->plotOn(frame1, Binning(binNPhi, minMassPhi, maxMassPhi));
// 		pdfModel1.plotOn(frame1);
// //		pdfModel1.plotOn(frame1, Components(pdfRoot1), LineStyle(kDashed));
// 
// 		RooPlot* frame2 = Phi2_m_scaled_Mix.frame(Title("#phi_{2} mass"));
// 		dset2->plotOn(frame2, Binning(binNPhi, minMassPhi, maxMassPhi));
// 		pdfModel2.plotOn(frame2);
// //		pdfModel2.plotOn(frame2, Components(pdfRoot2), LineStyle(kDashed));
// 
// 		canvTest->cd(1);
// 		frame1->DrawClone();
// 		canvTest->cd(2);
// 		frame2->DrawClone();
// 
// 		canvTest->cd(3);
// 		hPdf->DrawClone("surf");
// 		delete frame1;
// 		delete frame2;
// 		delete hPdf;
// 
// 		delete dset2;
// 		delete res;

	
	
	
	
	
	

	TCanvas* canvA = new TCanvas("canvA", "canvA", 800, 700);
	canvA->Divide(1, 1);
	canvA->cd(1);
	histJpsiDiPhi->Draw();
// 	canvA->cd(2);
// 	histJpsiDiK->Draw();
// 	canvA->cd(3);
// 	histJpsiPhiK->Draw();

	
	switch(Type)
	{
	  case 3:
	    TFile file("diPhiPureAll_f0_970_70.root","recreate");
	    break;
	  case 4:
	    TFile file("diPhiPureAll_f0_1010_70.root","recreate");
	    break;
	  case 5:
	    TFile file("diPhiPureAll_f0_990_40.root","recreate");
	    break;
	  case 6:
	    TFile file("diPhiPureAll_f0_990_100.root","recreate");
	    break;
	  default:
	    TFile file("diPhiPureAll_f0.root","recreate");
	}
	

	histJpsiDiPhi->Write();
	histJpsiDiK->Write();
	histJpsiPhiK->Write();
	file.Close();
	
	cout<<varf0Mass.getVal()<<endl;
	cout<<varf0Gamma.getVal()<<endl;
}

void runAll()
{
  diPhiPure(0,0);diPhiPure(0,1);
  diPhiPure(2,0);diPhiPure(2,1);
  diPhiPure(3,0);diPhiPure(3,1);
  diPhiPure(4,0);diPhiPure(4,1);
  diPhiPure(5,0);diPhiPure(5,1);
  diPhiPure(6,0);diPhiPure(6,1);
}

void runDiPhi()
{
  diPhiPure(0,0);
  diPhiPure(2,0);
  diPhiPure(3,0);
  diPhiPure(4,0);
  diPhiPure(5,0);
  diPhiPure(6,0);
}
