/*
 * diKMass.cpp
 *
 *  Created on: Apr 16, 2013
 *      Author: maksym
 */
using namespace RooFit;

//0- FixSigma
//1- VariableSigma
//2- Sigma MC

//1- 970, 70
//2- 1010, 70
//3- 990, 40
//4- 990, 100
//0- 990, 70


//0 - Flatte
//1 - BW

void f0_Draw(int Type=0,int f0Type=0, int PdfType = 0){
  

  
	gROOT->Reset();
	gROOT->SetStyle("Plain");
	gStyle->SetOptStat(000);
	TProof::Open("");


	Float_t minMassPhi = 600;
	Float_t maxMassPhi = 1400;
	Float_t binWidthPhi = 1.;
	Int_t binNPhi = int((maxMassPhi-minMassPhi)/binWidthPhi);
	
	
	Float_t PhiMass = 1019.46;
	Float_t f0Mass = 990.;	Float_t f0MassError = 20.;
	Float_t f0Gamma = 70.;	Float_t f0GammaError = 30.;
	
	RooRealVar Phi1_m_scaled_Mix("Phi1_m_scaled_Mix", "Phi1_m_scaled_Mix", minMassPhi, maxMassPhi, "MeV");
	Phi1_m_scaled_Mix.setBins(10000);



	RooRealVar varPhiMass("varPhiMass", "varPhiMass", 1019.46);
//	RooRealVar varPhiMass("varPhiMass", "varPhiMass", 1019.46);
	switch(Type)
	{
	  case 2:
	      RooRealVar varSigma("varSigma", "varSigma", 1.15);
	  case 1:
	      RooRealVar varSigma("varSigma", "varSigma", 1, 0.1, 4);
	  default:
	      RooRealVar varSigma("varSigma", "varSigma", 1.20);
	}
	RooRealVar varPhiGamma("varPhiGamma", "varPhiGamma", 4.26);
	RooRealVar var2KMass("var2KMass", "var2KMass", 493.67*2);
	
	
	switch(f0Type)
	{
	  case 1:
	    RooRealVar varf0Mass("varf0Mass", "varf0Mass", 970);
	    RooRealVar varf0Gamma("varf0Gamma", "varf0Gamma", 70);
	    break;
	  case 2:
	    RooRealVar varf0Mass("varf0Mass", "varf0Mass", 1010);
	    RooRealVar varf0Gamma("varf0Gamma", "varf0Gamma", 70);
	    break;
	  case 3:
	    RooRealVar varf0Mass("varf0Mass", "varf0Mass", 990);
	    RooRealVar varf0Gamma("varf0Gamma", "varf0Gamma", 40);
	    break;
	  case 4:
	    RooRealVar varf0Mass("varf0Mass", "varf0Mass", 990);
	    RooRealVar varf0Gamma("varf0Gamma", "varf0Gamma", 100);
	    break;
	  default:
	    RooRealVar varf0Mass("varf0Mass", "varf0Mass", 990);
	    RooRealVar varf0Gamma("varf0Gamma", "varf0Gamma", 70);
	    break;
	}
	
	
	RooRealVar varA1("varA1", "varA1", 0,-1,1);

	RooGenericPdf pdfPhi1("pdfPhi1", "pdfPhi1", "sqrt(@0-@1)*TMath::Voigt(@0-@2,@3,@4)",
			      RooArgList(Phi1_m_scaled_Mix, var2KMass, varPhiMass,varSigma,varPhiGamma));	
	RooGenericPdf pdfRoot1A1("pdfRoot1A1", "pdfRoot1A1", "sqrt(@0-@1)*(1+(@0-@1)*@2)", RooArgList(Phi1_m_scaled_Mix, var2KMass,varA1));
	
	
	if(PdfType==0)
	{
	  RooRealVar varg0("varg0","varg0",168);
	  RooRealVar varg1("varg1","varg1",168*4.12); //+-0.32
	  RooRealVar varPiMass("varPiMass","varPiMass",139.57);
	  RooRealVar varKMass("varKMass","varKMass",493.67);
	  RooFlatte pdfFlatte("pdfFlatte","pdfFlatte",Phi1_m_scaled_Mix, varf0Mass, 
			   				  varg1, varKMass,  varKMass,
			   				  varg0, varPiMass, varPiMass);
	  RooGenericPdf Thresh("Thresh","Thresh","(@0>@1)*TMath::Sqrt(TMath::Abs(@0-@1))",RooArgList(Phi1_m_scaled_Mix,var2KMass));
	  RooProdPdf pdff01("f0BW1","f0BW1",RooArgSet(pdfFlatte,Thresh));	  
	}
	if(PdfType==1)
	  RooGenericPdf pdff01("f0BW1","f0BW1","sqrt(@0-@1)*TMath::BreitWigner(@0,@2,@3)", RooArgSet(Phi1_m_scaled_Mix, var2KMass, varf0Mass, varf0Gamma));

	
		
	RooRealVar varNPhi("varNPhi", "varNPhi", 0, 0, 1e7);	
	RooRealVar varNK("varNK", "varNK", 0, 0, 1e7);	
	RooRealVar varNf0("varNf0","varNf0",1e5, 0,1e6);	

	
	RooAddPdf pdfModel1("pdfModel1", "pdfModel1",  
			    RooArgList(pdfPhi1,  pdff01, pdfRoot1A1), 
			    RooArgList(varNPhi,varNf0,varNK));
	
	TCanvas* canvTest = new TCanvas("canvTest", "canvTest", 1200, 400);
	RooPlot* frame1 = Phi1_m_scaled_Mix.frame(Title("#phi_{1} mass"));
	pdfModel1.plotOn(frame1);
	frame1->Draw();
}


