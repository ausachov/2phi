/*
 * diKMass.cpp
 *
 *  Created on: Apr 16, 2013
 *      Author: maksym
 */

using namespace RooFit;


//0- FixSigma
//1- VariableSigma
//2- Sigma MC
void RelBW_Test(){
	gROOT->Reset();
	gROOT->SetStyle("Plain");
	gStyle->SetOptStat(000);
	TProof::Open("");

	Float_t minMassJpsi = 2800;
	Float_t maxMassJpsi = 4050;
	Float_t binWidthJpsi = 10.;
	Int_t binNJpsi = int((maxMassJpsi-minMassJpsi)/binWidthJpsi);
	Float_t minMassPhi = 1009;
	Float_t maxMassPhi = 1031;
	Float_t binWidthPhi = 1.;
	Int_t binNPhi = int((maxMassPhi-minMassPhi)/binWidthPhi);
	
	
	Float_t PhiMass = 1019.46;
	
 	RooRealVar Jpsi_m_scaled("Jpsi_m_scaled", "Jpsi_m_scaled", minMassJpsi, maxMassJpsi, "MeV");
	RooRealVar Phi1_MM_Mix("Phi1_MM_Mix", "Phi1_MM_Mix", minMassPhi, maxMassPhi, "MeV");
	RooRealVar Phi2_MM_Mix("Phi2_MM_Mix", "Phi2_MM_Mix", minMassPhi, maxMassPhi, "MeV");
	Phi1_MM_Mix.setBins(10000);
	Phi2_MM_Mix.setBins(10000);
	
	
	TH1F* histJpsiDiPhi = new TH1F("histJpsiDiPhi", "histJpsiDiPhi", binNJpsi, minMassJpsi, maxMassJpsi);
	TH1F* histJpsiDiK = new TH1F("histJpsiDiK", "histJpsiDiK", binNJpsi, minMassJpsi, maxMassJpsi);
	TH1F* histJpsiPhiK = new TH1F("histJpsiPhiK", "histJpsiPhiK", binNJpsi, minMassJpsi, maxMassJpsi);

	TChain* chain = new TChain("DecayTree");
	chain->Add("2phi_afterCut.root");

	RooDataSet* dsetFull = new RooDataSet("dsetFull", "dsetFull", chain, RooArgSet(Jpsi_m_scaled, Phi1_MM_Mix, Phi2_MM_Mix), "");

	RooRealVar varPhiMass("varPhiMass", "varPhiMass", 1019.46, 1019.46-1, 1019.46+1);

	RooRealVar varSigma("varSigma", "varSigma", 1.20);

	RooRealVar varPhiGamma("varPhiGamma", "varPhiGamma", 4.26);
	RooRealVar var2KMass("var2KMass", "var2KMass", 493.67*2);
	

	RooRealVar varA1("varA1", "varA1", 0);
	RooRealVar varA2("varA2", "varA2", 0);
	


	RooRealVar PhiSpin("PhiSpin", "PhiSpin", 1);
	RooRealVar ConvCentre("ConvCentre", "ConvCentre", 0);
	RooRealVar radius("radius", "radius", 1);
	RooRealVar massa("massa", "massa", 493.67, "MeV");
	RooRealVar massb("massb", "massb", 493.67, "MeV");
	
	RooRelBreitWigner pdfPhi1BWrel = RooRelBreitWigner("pdfPhi1BWrel", "pdfPhi1BWrel", Phi1_MM_Mix, varPhiMass, varPhiGamma,
			PhiSpin, radius, massa, massb);
	RooGaussian pdfPhi1Gauss("pdfPhi1Gauss","pdfPhi1Gauss",Phi1_MM_Mix,ConvCentre,varSigma);
	RooNumConvPdf pdfPhi1Clean("pdfPhi1Clean","pdfPhi1Clean",Phi1_MM_Mix,pdfPhi1BWrel,pdfPhi1Gauss);
	RooVoigtian pdfPhi1CleanNR("pdfPhi1CleanNR", "pdfPhi1CleanNR", Phi1_MM_Mix, varPhiMass, varPhiGamma, varSigma);	
	
	RooGenericPdf Root1("Root1","Root1","sqrt(@0-@1)",RooArgList(Phi1_MM_Mix, var2KMass));
	RooProdPdf pdfPhi1("pdfPhi1","pdfPhi1",RooArgSet(Root1, pdfPhi1Clean));
	RooProdPdf pdfPhi1NR("pdfPhi1NR","pdfPhi1NR",RooArgSet(Root1, pdfPhi1CleanNR));

	RooRelBreitWigner pdfPhi2BWrel = RooRelBreitWigner("pdfPhi2BWrel", "pdfPhi2BWrel", Phi2_MM_Mix, varPhiMass, varPhiGamma,
			PhiSpin, radius, massa, massb);
	RooGaussian pdfPhi2Gauss("pdfPhi2Gauss","pdfPhi2Gauss",Phi2_MM_Mix,ConvCentre,varSigma);
	RooNumConvPdf pdfPhi2Clean("pdfPhi2Clean","pdfPhi2Clean",Phi2_MM_Mix,pdfPhi2BWrel,pdfPhi2Gauss);
	RooVoigtian pdfPhi2CleanNR("pdfPhi2CleanNR", "pdfPhi2CleanNR", Phi2_MM_Mix, varPhiMass, varPhiGamma, varSigma);	
	RooGenericPdf Root2("Root2","Root2","sqrt(@0-@1)",RooArgList(Phi2_MM_Mix, var2KMass));
	RooProdPdf pdfPhi2("pdfPhi2","pdfPhi2",RooArgSet(Root2, pdfPhi2Clean));
	RooProdPdf pdfPhi2NR("pdfPhi2NR","pdfPhi2NR",RooArgSet(Root2, pdfPhi2CleanNR));
	
	RooProdPdf pdfSS("pdfSS", "pdfSS", RooArgSet(pdfPhi1, pdfPhi2));
	RooProdPdf pdfSSNR("pdfSSNR", "pdfSSNR", RooArgSet(pdfPhi1NR, pdfPhi2NR));

	TH2* hPdf = pdfSS.createHistogram("Phi2_MM_Mix,Phi1_MM_Mix");
	TH2* hPdfNR = pdfSSNR.createHistogram("Phi2_MM_Mix,Phi1_MM_Mix");
	

	TCanvas* canvA = new TCanvas("canvA", "canvA", 800, 700);
// 	RooPlot* frame = Phi1_MM_Mix.frame(Title("M(#phi)"));
// 	pdfPhi1.plotOn(frame);
// 	pdfPhi1NR.plotOn(frame,LineColor(kRed));
// 	frame->Draw();
	hPdf->DrawClone();



}


