/*
 * diKMass.cpp
 *
 *  Created on: Apr 16, 2013
 *      Author: maksym
 */

//Type


//default  - without both slopes


//0- VariableSigma
//2- Sigma MC
//3 - Exp bg
//4 - Shift bg
//5 - A2 = 0, A1-floating
//7 - fix A2 at BaseFit
//9 - A1=0,  A2 - floating
//10 - const sigma for PT
// 11 - const A1
// 12 - const 12


//RangeNumber
// 0 - ccbar
// 1 - bs

using namespace RooFit;

void diPhiPure(int Type=6,int RangeNumber=0, char* infilename = "2phi_afterCut.root")
{
	gROOT->Reset();
	gROOT->SetStyle("Plain");
	gStyle->SetOptStat(000);
	TProof::Open("");
    gROOT->ProcessLine(".x lhcbStyle.C");

switch(RangeNumber)
{
  case 0: 
	Float_t minMassJpsi = 2700;
	Float_t maxMassJpsi = 4000;
	break;  
  case 1: 
	Float_t minMassJpsi = 5200;
	Float_t maxMassJpsi = 5550;
	break;  
  case 2: 
	Float_t minMassJpsi = 2700;
	Float_t maxMassJpsi = 6000;
	break;
}

    if(Type==4)
	{
	  minMassJpsi-=5;
	  maxMassJpsi-=5;
	}
	
	Float_t binWidthJpsi = 10.;
	Int_t binNJpsi = int((maxMassJpsi-minMassJpsi)/binWidthJpsi);
	Float_t minMassPhi = 1009;
	Float_t maxMassPhi = 1031;
	Float_t binWidthPhi = 1.;
	Int_t binNPhi = int((maxMassPhi-minMassPhi)/binWidthPhi);
	Float_t PhiMass = 1019.46;
	
 	RooRealVar Jpsi_m_scaled("Jpsi_m_scaled", "Jpsi_m_scaled", minMassJpsi, maxMassJpsi, "MeV");
    //RooRealVar weight("weight", "weight", 0,5);
	RooRealVar Phi1_m_scaled_Mix("Phi1_m_scaled_Mix", "Phi1_m_scaled_Mix", minMassPhi, maxMassPhi, "MeV");
	RooRealVar Phi2_m_scaled_Mix("Phi2_m_scaled_Mix", "Phi2_m_scaled_Mix", minMassPhi, maxMassPhi, "MeV");
	TH1F* histJpsiDiPhi = new TH1F("histJpsiDiPhi", "histJpsiDiPhi", binNJpsi, minMassJpsi, maxMassJpsi);
	TH1F* histJpsiDiK = new TH1F("histJpsiDiK", "histJpsiDiK", binNJpsi, minMassJpsi, maxMassJpsi);
	TH1F* histJpsiPhiK = new TH1F("histJpsiPhiK", "histJpsiPhiK", binNJpsi, minMassJpsi, maxMassJpsi);
	TH1F* histA1 = new TH1F("histA1", "histA1", binNJpsi, minMassJpsi, maxMassJpsi);
	TH1F* histA2 = new TH1F("histA2", "histA2", binNJpsi, minMassJpsi, maxMassJpsi);
	TH1F* histSigma = new TH1F("histSigma", "histSigma", binNJpsi, minMassJpsi-4*493.677, maxMassJpsi-4*493.677);

	TChain* chain = new TChain("DecayTree");
	chain->Add(infilename);
    TTree* tree = chain->CopyTree("");

//	RooDataSet* dsetFull = new RooDataSet("dsetFull", "dsetFull", tree, RooArgSet(Jpsi_m_scaled, Phi1_m_scaled_Mix, Phi2_m_scaled_Mix,weight), "");
    RooDataSet* dsetFull = new RooDataSet("dsetFull", "dsetFull", tree, RooArgSet(Jpsi_m_scaled, Phi1_m_scaled_Mix, Phi2_m_scaled_Mix), "");

    
	RooRealVar varPhiMass("varPhiMass", "varPhiMass", PhiMass, PhiMass-1, PhiMass+1);
//	RooRealVar varPhiMass("varPhiMass", "varPhiMass", 1019.46);
	
	switch(Type)
	{
	  case 1:
	    RooRealVar varSigma("varSigma", "varSigma", 1.18);
	    break;
	  case 2:
	    RooRealVar varSigma("varSigma", "varSigma", 1.15);
	    break;
      case 10:
        RooRealVar varSigma("varSigma", "varSigma", 1.18);
        varPhiMass.setConstant(kTRUE);
        break;
	  default:
        RooRealVar varSigma("varSigma", "varSigma", 1.18);
        varPhiMass.setConstant(kTRUE);
	    break;
	}
	  
	RooRealVar varPhiGamma("varPhiGamma", "varPhiGamma", 4.26);
	RooRealVar var2KMass("var2KMass", "var2KMass", 493.67*2);
	
	RooGenericPdf pdfPhi1("pdfPhi1", "pdfPhi1", "sqrt(@0-@1)*TMath::Voigt(@0-@2,@3,@4)",
			      RooArgList(Phi1_m_scaled_Mix, var2KMass, varPhiMass,varSigma,varPhiGamma));	
	RooGenericPdf pdfPhi2("pdfPhi2", "pdfPhi2", "sqrt(@0-@1)*TMath::Voigt(@0-@2,@3,@4)",
			      RooArgList(Phi2_m_scaled_Mix, var2KMass, varPhiMass,varSigma,varPhiGamma));
	
if(Type==3)
{
	RooRealVar varA1("varA1", "varA1", 0,-1,5);
	RooRealVar varA2("varA2", "varA2", 0,-1,5);

	RooGenericPdf pdfRoot1A1("pdfRoot1A1", "pdfRoot1A1", "sqrt(@0-@1)*TMath::Exp(-(@0-@1)*@2)", RooArgList(Phi1_m_scaled_Mix, var2KMass,varA1));
	RooGenericPdf pdfRoot1A2("pdfRoot1A2", "pdfRoot1A2", "sqrt(@0-@1)*TMath::Exp(-(@0-@1)*@2)", RooArgList(Phi1_m_scaled_Mix, var2KMass,varA2));
	RooGenericPdf pdfRoot2A1("pdfRoot2A1", "pdfRoot2A1", "sqrt(@0-@1)*TMath::Exp(-(@0-@1)*@2)", RooArgList(Phi2_m_scaled_Mix, var2KMass,varA1));
	RooGenericPdf pdfRoot2A2("pdfRoot2A2", "pdfRoot2A2", "sqrt(@0-@1)*TMath::Exp(-(@0-@1)*@2)", RooArgList(Phi2_m_scaled_Mix, var2KMass,varA2));  
}
else
{
	Double_t maxA = 0.7;
	RooRealVar varA1("varA1", "varA1", 0,0,maxA);//-0.1
	RooRealVar varA2("varA2", "varA2", 0,0,maxA);//-0.1

	RooGenericPdf pdfRoot1A1("pdfRoot1A1", "pdfRoot1A1", "sqrt(@0-@1)*(1+(@0-@1)*@2)", RooArgList(Phi1_m_scaled_Mix, var2KMass,varA1));
	RooGenericPdf pdfRoot1A2("pdfRoot1A2", "pdfRoot1A2", "sqrt(@0-@1)*(1+(@0-@1)*@2)", RooArgList(Phi1_m_scaled_Mix, var2KMass,varA2));
	RooGenericPdf pdfRoot2A1("pdfRoot2A1", "pdfRoot2A1", "sqrt(@0-@1)*(1+(@0-@1)*@2)", RooArgList(Phi2_m_scaled_Mix, var2KMass,varA1));
	RooGenericPdf pdfRoot2A2("pdfRoot2A2", "pdfRoot2A2", "sqrt(@0-@1)*(1+(@0-@1)*@2)", RooArgList(Phi2_m_scaled_Mix, var2KMass,varA2));
}
	
	
	RooProdPdf pdfSS("pdfSS", "pdfSS", RooArgSet(pdfPhi1, pdfPhi2));
	RooProdPdf pdfSB("pdfSB", "pdfSB", RooArgSet(pdfPhi1, pdfRoot2A1));
	RooProdPdf pdfBS("pdfBS", "pdfBS", RooArgSet(pdfRoot1A1, pdfPhi2));
	RooProdPdf pdfBB("pdfBB", "pdfBB", RooArgSet(pdfRoot1A2, pdfRoot2A2));
	
    
    RooRealVar varTotEvents("varTotEvents","varTotEvents",0,1e7);
	RooRealVar varNDiPhi("varNDiPhi", "varNDiPhi", 0, 1e7);	
	RooRealVar varNPhiK("varNPhiK", "varNPhiK", 0, 1e7);
//	RooRealVar varNDiK("varNDiK", "varNDiK", 0, 1e7);
    
    RooFormulaVar varNDiK("varNDiK", "varNDiK", "(@0-@1-@2)",RooArgList(varTotEvents,varNDiPhi,varNPhiK));
	
	RooAddPdf pdfModel1("pdfModel1", "pdfModel1",  RooArgList(pdfPhi1,pdfPhi1,pdfRoot1A1,pdfRoot1A2), 
							RooArgList(varNDiPhi,varNPhiK,varNPhiK,varNDiK));
	RooAddPdf pdfModel2("pdfModel2", "pdfModel2",  RooArgList(pdfPhi2,pdfRoot2A1,pdfPhi2,pdfRoot2A2), 
							RooArgList(varNDiPhi,varNPhiK,varNPhiK,varNDiK));
	RooAddPdf pdfModel("pdfModel", "pdfModel",     RooArgList(pdfSS,pdfSB,pdfBS,pdfBB), 
							RooArgList(varNDiPhi,varNPhiK,varNPhiK,varNDiK));


	char label[200];
	Float_t massJpsiLo = minMassJpsi;
	Float_t massJpsiHi = maxMassJpsi;

	sprintf(label, "Jpsi_m_scaled>%i&&Jpsi_m_scaled<%i", massJpsiLo, massJpsiHi);
	RooDataSet* dset = dsetFull->reduce(Cut(label), Name("dset"), Title("dset"));
	varNDiPhi.setVal(dset->numEntries()/1.5);
	//varNDiK.setVal(dset->numEntries()/3);
	varNPhiK.setVal(dset->numEntries()/4);
	
	
	switch(Type)
	{
	  case 5:
	    varA2.setVal(0);
	    varA2.setError(0);
	    varA2.setConstant(kTRUE);	
	    break;
	  case 9:
	    varA1.setVal(0);
	    varA1.setError(0);
	    varA1.setConstant(kTRUE);
	    break;
      case 11:
        varA2.setVal(0);
        varA2.setError(0);
        varA2.setConstant(kTRUE);
        break;
      case 12:
        varA1.setVal(0);
        varA1.setError(0);
        varA1.setConstant(kTRUE);
        break;
	  default:
	    varA1.setVal(0);
	    varA1.setError(0);
	    varA1.setConstant(kTRUE);
	    varA2.setVal(0);
	    varA2.setError(0);
	    varA2.setConstant(kTRUE);
	    break;
	}

    varTotEvents.setVal(dset->numEntries());
    varTotEvents.setConstant(kTRUE);
	RooFitResult* res = pdfModel.fitTo(*dset, Save(true),SumW2Error(true),Minos(true),Strategy(2), PrintLevel(0));
	
	varSigma.setError(0);
    varPhiMass.setError(0);
	float phiKKfr = varNPhiK.getVal()/varNDiPhi.getVal();
	float KKKKfr = varNDiK.getVal()/varNDiPhi.getVal();
	
	varPhiMass.setConstant(kTRUE);
	varSigma.setConstant(kTRUE);
    varA1.setConstant(kTRUE);
    varA2.setConstant(kTRUE);


	delete dset;
	delete res;
	
	
	for (Int_t i=0; i<binNJpsi; i++)
	{
		massJpsiLo = minMassJpsi + i*binWidthJpsi;
		massJpsiHi = minMassJpsi + (i+1)*binWidthJpsi;

		Bool_t flagA2 = kTRUE;
		Bool_t flagA1 = kTRUE;		
		
		if(Type==5)
		{
		  varA1.setConstant(kFALSE);
		  Bool_t flagA1 = kFALSE;
		}
		
		if(Type==9)
		{
		  varA2.setConstant(kFALSE);
		  Bool_t flagA2 = kFALSE;
		}

		//varNDiK.setConstant(kFALSE);

		sprintf(label, "Jpsi_m_scaled>%i&&Jpsi_m_scaled<%i", massJpsiLo, massJpsiHi);
		RooDataSet* dset = (RooDataSet*)dsetFull->reduce(Cut(label), Name("dset"), Title("dset"));
		
		varNDiPhi.setVal(dset->numEntries()/(1+phiKKfr+KKKKfr));
		//varNDiK.setVal(phiKKfr*dset->numEntries()/(1+phiKKfr+KKKKfr));
		varNPhiK.setVal(KKKKfr*dset->numEntries()/(1+phiKKfr+KKKKfr));

		varNDiPhi.setMax(Double_t(dset->numEntries()));
		//varNDiK.setMax(Double_t(4*dset->numEntries()));
		varNPhiK.setMax(Double_t(dset->numEntries()));
		

        
        varTotEvents.setVal(dset->numEntries());
        //varTotEvents.setConstant(kTRUE);
        
		if(Type==5 || Type==9)
		  RooFitResult* res = pdfModel.fitTo(*dset, Save(true),SumW2Error(true),Strategy(2), PrintLevel(0));
		else
		  res = pdfModel.fitTo(*dset, Save(true),Minos(true),Strategy(2), PrintLevel(0));
        
 		Double_t EDM = res->edm();
        
		

        if(Type==5)
		if((TMath::Abs(varA1.getVal())<varA1.getErrorHi()/2) || (TMath::Abs(maxA - varA1.getVal())<3*varA1.getErrorHi()) 
		  || TMath::Abs(varNDiK.getVal())<varNDiK.getPropagatedError(*res)/3 || EDM>1e-2)
		{
		    varA1.setVal(0);
		    varA1.setError(0);
		    varA1.setConstant(kTRUE);
		    varNDiPhi.setVal(dset->numEntries()/(1+phiKKfr+KKKKfr));
		    //varNDiK.setVal(phiKKfr*dset->numEntries()/(1+phiKKfr+KKKKfr));
		    varNPhiK.setVal(KKKKfr*dset->numEntries()/(1+phiKKfr+KKKKfr));
		    res = pdfModel.fitTo(*dset, Save(true),Strategy(2), Extended(dset->numEntries()), PrintLevel(0));
		    res = pdfModel.fitTo(*dset, Save(true),Minos(true),Strategy(2), Extended(dset->numEntries()), PrintLevel(0));
		    EDM = res->edm();
		    flagA1 = kTRUE;
		}
		
        if(Type==9)
		if((TMath::Abs(varA2.getVal())<varA2.getErrorHi()/2) || (TMath::Abs(maxA - varA2.getVal())<3*varA2.getErrorHi()) 
		  || TMath::Abs(varNDiK.getVal())<varNDiK.getPropagatedError(*res)/3 || EDM>1e-2)
		{
		    varA2.setVal(0);
		    varA2.setError(0);
		    varA2.setConstant(kTRUE);
		    varNDiPhi.setVal(dset->numEntries()/(1+phiKKfr+KKKKfr));
		    //varNDiK.setVal(phiKKfr*dset->numEntries()/(1+phiKKfr+KKKKfr));
		    varNPhiK.setVal(KKKKfr*dset->numEntries()/(1+phiKKfr+KKKKfr));
		    res = pdfModel.fitTo(*dset, Save(true),Strategy(2), Extended(dset->numEntries()), PrintLevel(0));
		    res = pdfModel.fitTo(*dset, Save(true),Minos(true),Strategy(2), Extended(dset->numEntries()), PrintLevel(0));
		    EDM = res->edm();
		    flagA2 = kTRUE;
		}


		
//      if(TMath::Abs(varNDiK.getVal())<varNDiK.getPropagatedError(*res)/4.)
//      {
//
//        varNDiK.setConstant(kTRUE);
//          varNDiK.setError(0);
//        //varNPhiK.setConstant(kTRUE);
//// 		  res = pdfModel.fitTo(*dset2, Save(true),SumW2Error(true),Minos(true),Strategy(2), Extended(dset2->numEntries()), PrintLevel(0));
//// 		  varNDiK.setConstant(kFALSE);
//// 		  varNPhiK=dset2->numEntries()/3;
//        //varNDiK.setVal(dset2->numEntries()-varNDiPhi.getVal()-varNPhiK.getVal());
//        res = pdfModel.fitTo(*dset, Save(true),Strategy(2), Extended(dset->numEntries()), PrintLevel(0));
//        res = pdfModel.fitTo(*dset, Save(true),Minos(true),Strategy(2), Extended(dset->numEntries()), PrintLevel(0));
//        varNDiK.setConstant(kFALSE);
//        //varNPhiK.setConstant(kFALSE);
//        //res = pdfModel.fitTo(*dset2, Save(true),SumW2Error(true),Strategy(2), Extended(dset2->numEntries()), PrintLevel(0));
//        EDM = res->edm();
//      }
        
    
    if(TMath::Abs(varNPhiK.getVal())<varNPhiK.getErrorHi()/4.)
    {
        
        varNPhiK.setConstant(kTRUE);
        varNPhiK.setError(0);
        //varNPhiK.setConstant(kTRUE);
        // 		  res = pdfModel.fitTo(*dset2, Save(true),SumW2Error(true),Minos(true),Strategy(2), Extended(dset2->numEntries()), PrintLevel(0));
        // 		  varNPhiK.setConstant(kFALSE);
        // 		  varNPhiK=dset2->numEntries()/3;
        //varNPhiK.setVal(dset2->numEntries()-varNDiPhi.getVal()-varNPhiK.getVal());
        res = pdfModel.fitTo(*dset, Save(true),Strategy(2), Extended(dset->numEntries()), PrintLevel(0));
        res = pdfModel.fitTo(*dset, Save(true),Minos(true),Strategy(2), Extended(dset->numEntries()), PrintLevel(0));
        varNPhiK.setConstant(kFALSE);
        //varNPhiK.setConstant(kFALSE);
        //res = pdfModel.fitTo(*dset2, Save(true),SumW2Error(true),Strategy(2), Extended(dset2->numEntries()), PrintLevel(0));
        EDM = res->edm();
    }
        //res = pdfModel.fitTo(*dset, Save(true),Strategy(2), Extended(dset->numEntries()), PrintLevel(0));
		
        if(Type==5 || Type==9)
		{
		  histJpsiDiPhi->SetBinContent(i+1, varNDiPhi.getVal());
		  histJpsiDiPhi->SetBinError(i+1, varNDiPhi.getError());

		  histJpsiDiK->SetBinContent(i+1, varNDiK.getVal());
		  histJpsiDiK->SetBinError(i+1, varNDiK.getPropagatedError(*res));

		  histJpsiPhiK->SetBinContent(i+1, varNPhiK.getVal());
		  histJpsiPhiK->SetBinError(i+1, varNPhiK.getError());
		  
		  histA1->SetBinContent(i+1, varA1.getVal());
		  histA1->SetBinError(i+1, varA1.getError());
		  
		  histA2->SetBinContent(i+1, varA2.getVal());
		  histA2->SetBinError(i+1, varA2.getError());
		  
		  histSigma->SetBinContent(i+1, varSigma.getVal());
		  histSigma->SetBinError(i+1, varSigma.getError());
		}
		else
		{	
		  histJpsiDiPhi->SetBinContent(i+1, varNDiPhi.getVal());
		  histJpsiDiPhi->SetBinError(i+1, varNDiPhi.getErrorHi());

		  histJpsiDiK->SetBinContent(i+1, varNDiK.getVal());
		  histJpsiDiK->SetBinError(i+1, varNDiK.getPropagatedError(*res));

		  histJpsiPhiK->SetBinContent(i+1, varNPhiK.getVal());
		  histJpsiPhiK->SetBinError(i+1, varNPhiK.getErrorHi());
		  
		  histA1->SetBinContent(i+1, varA1.getVal());
		  histA1->SetBinError(i+1, varA1.getErrorHi());
		  
		  histA2->SetBinContent(i+1, varA2.getVal());
		  histA2->SetBinError(i+1, varA2.getErrorHi());
		  
		  histSigma->SetBinContent(i+1, varSigma.getVal());
		  histSigma->SetBinError(i+1, varSigma.getErrorHi());
		}


		

		if(EDM>3e-3)return;
		delete dset;
		delete res;
	}

	
	
	TCanvas* canvTest = new TCanvas("canvTest", "canvTest", 1200, 400);
 	canvTest->Divide(3,1);

	Double_t minMassJpsiTest = minMassJpsi;
	Double_t maxMassJpsiTest = maxMassJpsi;
		

	
		massJpsiLo = minMassJpsiTest;
		massJpsiHi = maxMassJpsiTest;
			
 
		sprintf(label, "Jpsi_m_scaled>%i&&Jpsi_m_scaled<%i", massJpsiLo, massJpsiHi);
		RooDataSet* dset2 = dsetFull->reduce(Cut(label), Name("dset2"), Title("dset2"));
		
		varNDiPhi.setVal(dset2->numEntries()/1.5);
		//varNDiK.setVal(dset2->numEntries()/4);
		varNPhiK.setVal(dset2->numEntries()/3);

		varNDiPhi.setMax(Double_t(dset2->numEntries()));	
		//varNDiK.setMax(Double_t(dset2->numEntries()));
		varNPhiK.setMax(Double_t(dset2->numEntries()));			
		
    
        varTotEvents.setVal(dset2->numEntries());
        varTotEvents.setConstant(kTRUE);
    
    
		RooFitResult* res = pdfModel.fitTo(*dset2, Save(true),SumW2Error(true),Minos(true),Strategy(2), Extended(dset2->numEntries()), PrintLevel(0));
        res = pdfModel.fitTo(*dset2, Save(true),SumW2Error(true),Minos(true), Extended(dset2->numEntries()), PrintLevel(0));
		EDM = res->edm();		
						
		if(Type==5)
		  if((TMath::Abs(varA1.getVal())<varA1.getErrorHi()/2) || (TMath::Abs(maxA - varA1.getVal())<3*varA1.getErrorHi()) 
		    || TMath::Abs(varNDiK.getVal())<varNDiK.getPropagatedError(*res)/3 || EDM>1e-2)
		  {
		      varA1.setVal(0);
		      varA1.setError(0);
		      varA1.setConstant(kTRUE);
		      varNDiPhi.setVal(dset2->numEntries()/(1+phiKKfr+KKKKfr));
		      //varNDiK.setVal(phiKKfr*dset2->numEntries()/(1+phiKKfr+KKKKfr));
		      varNPhiK.setVal(KKKKfr*dset2->numEntries()/(1+phiKKfr+KKKKfr));
		      res = pdfModel.fitTo(*dset2, Save(true),SumW2Error(true),Strategy(2), Extended(dset2->numEntries()), PrintLevel(0));
		      //res = pdfModel.fitTo(*dset2, Save(true),SumW2Error(true),Strategy(2), Extended(dset2->numEntries()), PrintLevel(0));
		      EDM = res->edm();
		      flagA1 = kTRUE;
		  }
		  
		if(Type==9)
		  if((TMath::Abs(varA2.getVal())<varA2.getErrorHi()/2) || (TMath::Abs(maxA - varA2.getVal())<3*varA2.getErrorHi()) 
		    || TMath::Abs(varNDiK.getVal())<varNDiK.getPropagatedError(*res)/3 || EDM>1e-2)
		  {
		      varA2.setVal(0);
		      varA2.setError(0);
		      varA2.setConstant(kTRUE);
		      varNDiPhi.setVal(dset2->numEntries()/(1+phiKKfr+KKKKfr));
		      //varNDiK.setVal(phiKKfr*dset2->numEntries()/(1+phiKKfr+KKKKfr));
		      varNPhiK.setVal(KKKKfr*dset2->numEntries()/(1+phiKKfr+KKKKfr));
		      res = pdfModel.fitTo(*dset2, Save(true),SumW2Error(true),Strategy(2), Extended(dset2->numEntries()), PrintLevel(0));
		      //res = pdfModel.fitTo(*dset2, Save(true),SumW2Error(true),Strategy(2), Extended(dset2->numEntries()), PrintLevel(0));
		      EDM = res->edm();
		      flagA2 = kTRUE;
		  }


		  
// 		  if(TMath::Abs(varNDiK.getVal())<varNDiK.getPropagatedError(*res)/3)
// 		  {
// 
// 		    varNDiPhi.setConstant(kTRUE);
// 		    varNPhiK.setConstant(kTRUE);
//   // 		  res = pdfModel.fitTo(*dset2, Save(true),SumW2Error(true),Minos(true),Strategy(2), Extended(dset2->numEntries()), PrintLevel(0));
//   // 		  varNDiK.setConstant(kFALSE);
//   // 		  varNPhiK=dset2->numEntries()/3;
// 		    //varNDiK.setVal(dset2->numEntries()-varNDiPhi.getVal()-varNPhiK.getVal());
// 		    res = pdfModel.fitTo(*dset2, Save(true),SumW2Error(true),Strategy(1), Extended(dset2->numEntries()), PrintLevel(0));
// 		    varNDiPhi.setConstant(kFALSE);
// 		    varNPhiK.setConstant(kFALSE);
// 		    //res = pdfModel.fitTo(*dset2, Save(true),SumW2Error(true),Strategy(2), Extended(dset2->numEntries()), PrintLevel(0));
// 		    EDM = res->edm(); 
// 		    flagA2 = kTRUE;
// 		  }
		

		TH2* hPdf = pdfModel.createHistogram("Phi2_m_scaled_Mix,Phi1_m_scaled_Mix");

		RooPlot* frame1 = Phi1_m_scaled_Mix.frame(Title("#phi_{1} mass"));
		dset2->plotOn(frame1, Binning(binNPhi, minMassPhi, maxMassPhi));
		pdfModel1.plotOn(frame1);
		//pdfModel1.plotOn(frame1, Components(pdfRoot1), LineStyle(kDashed));

		RooPlot* frame2 = Phi2_m_scaled_Mix.frame(Title("#phi_{2} mass"));
		dset2->plotOn(frame2, Binning(binNPhi, minMassPhi, maxMassPhi));
		pdfModel2.plotOn(frame2);
		//pdfModel2.plotOn(frame2, Components(pdfRoot2), LineStyle(kDashed));

    
        canvTest->cd(1);
        hPdf->Draw("surf");
    
		canvTest->cd(2);
		frame1->Draw();
		canvTest->cd(3);
		frame2->Draw();


		delete frame1;
		delete frame2;
		delete hPdf;

		delete dset2;
		delete res;

	
	
	TCanvas* canvB = new TCanvas("canvB", "canvB", 800, 700);
	histSigma->Draw();

	if(RangeNumber==0)
	  switch(Type)
	  {
	    case 2:
	      TFile file("diPhiPureAll_MC.root","recreate");
	      break;
	    case 3:
	      TFile file("diPhiPureAllExp.root","recreate");
	      break;
	    case 4:
	      TFile file("diPhiPureShift.root","recreate");
	      break;
	    case 5:
	      TFile file("diPhiPureA1.root","recreate");
	      break;
	    case 9:
	      TFile file("diPhiPureA2.root","recreate");
	      break;
        case 11:
          TFile file("diPhiPureA1.root","recreate");
          break;
        case 12:
          TFile file("diPhiPureA2.root","recreate");
          break;
	    default:
	      TFile file("diPhiPureAll.root","recreate");
	      break;
	  }
	  
	if(RangeNumber==1)
	  switch(Type)
	  {
	    case 2:
	      TFile file("bsPureAll_MC.root","recreate");
	      break;
	    case 3:
	      TFile file("bsPureAllExp.root","recreate");
	      break;
	    case 4:
	      TFile file("bsPureShift.root","recreate");
	      break;
	    default:
	      TFile file("bsPureAll.root","recreate");
	      break;
	  }

	TCanvas* canvSlopes = new TCanvas("canvSlopes", "canvSlopes", 800, 700);
	canvSlopes->Divide(1, 2);
	canvSlopes->cd(1);
	histA1->Draw();
	canvSlopes->cd(2);
	histA2->Draw();
	
	    
	TCanvas* canvA = new TCanvas("canvA", "canvA", 800, 700);
	canvA->Divide(1, 3);
	canvA->cd(1);
	histJpsiDiPhi->Draw();
	canvA->cd(2);
	histJpsiPhiK->Draw();
	canvA->cd(3);
	histJpsiDiK->Draw();	
	

	histJpsiDiPhi->Write();
	histJpsiDiK->Write();
	histJpsiPhiK->Write();
	histSigma->Write();

}

void runALL()
{
  diPhiPure(0,0);
  diPhiPure(2,0);
  diPhiPure(3,0);
  diPhiPure(4,0);
  
  diPhiPure(0,1);
  diPhiPure(2,1);
  diPhiPure(3,1);
  diPhiPure(4,1);
}

