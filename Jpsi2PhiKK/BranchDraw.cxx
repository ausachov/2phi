#include <TMath.h>




void main()
{
  gROOT->Reset();
   gStyle->SetOptStat(kFALSE);
  
  Double_t range0=1000;
  Double_t range1=1250;
  Int_t nch=125;
  
  TChain * DecayTree=new TChain("DecayTree");
  DecayTree->Add("Data/PhiKK_All.root");
//  DecayTree->Add("2phi_afterCut.root");
  const Int_t NEn=DecayTree->GetEntries();
  Double_t Branch_Value;
  DecayTree->SetBranchAddress("Phi2_MM",&Branch_Value);

  TH1D * m_phiphi_hist = new TH1D("#phiKK candidates","3#phi candidates" , nch, range0, range1);
  for (Int_t i=0; i<NEn; i++) 
  {
	  DecayTree->GetEntry(i);
	  m_phiphi_hist->Fill(Branch_Value);
  }
  
  TCanvas *canv1 = new TCanvas("canv1","B_MM",5,85,800,600);
//   m_phiphi_hist->SetAxisRange(5,50);
  m_phiphi_hist->SetXTitle("Jpsi_MM");
//  m_phiphi_hist->SetMaximum(1500);
  m_phiphi_hist->DrawCopy();
  
//   TCanvas *canv2 = new TCanvas("canv2","B_MM",5,85,1200,800);
//   m_phiphi_hist->SetAxisRange(6000,8000);
//   m_phiphi_hist->DrawCopy();
//   
//   TCanvas *canv3 = new TCanvas("canv3","B_MM",5,85,1200,800);
//   m_phiphi_hist->SetAxisRange(8000,10000);
//   m_phiphi_hist->DrawCopy();
//   
//   TCanvas *canv4 = new TCanvas("canv4","B_MM",5,85,1200,800);
//   m_phiphi_hist->SetMaximum(50);
//   m_phiphi_hist->SetAxisRange(10000,12000);
//   m_phiphi_hist->DrawCopy();
}
