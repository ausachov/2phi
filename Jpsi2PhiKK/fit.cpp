
using namespace RooFit;

void fit(){
	gROOT->Reset();
	gROOT->SetStyle("Plain");
	TProof::Open("");

	Float_t etacMass =	2981.0; // ±1.1 MeV
	Float_t jpsiMass =	3096.92;
	Float_t chi0Mass =	3414.75; // ±0.31 MeV
	Float_t chi1Mass =	3510.66; // ±0.07 MeV <-- refer point
	Float_t hcMass   = 	3525.41; // ±0.16 MeV
	Float_t chi2Mass =	3556.20; // ±0.09 MeV
	Float_t etac2Mass =	3638.9; // ±1.3 MeV
	Float_t psi2sMass =	3686.11;
	Float_t psi3770Mass =	3773.15; // ±0.33 MeV
	Float_t x3872Mass = 	3871.69; // ±0.17 MeV
	Float_t x3915Mass = 	3918.4; // ±2.7 MeV
	
	Float_t SigmaPar = 0.224;

	Float_t minMass = 2700;
	Float_t maxMass = 3300;
	Float_t binWidth = 10.;
	Int_t binN = int((maxMass-minMass)/binWidth);
	RooRealVar varMass("M(#phi#phi)", "M(#phi#phi)", minMass, maxMass, "MeV");



// 	histogramms:
	TFile* file = new TFile("PhiKKPureAll.root");
	TH1D *histJpsiDiPhi  = (TH1D*) file->Get("histJpsiPhiK");
	
	RooDataHist* DataSet = new RooDataHist("DataSet", "DataSet", varMass, histJpsiDiPhi);


	RooPlot* frame = varMass.frame(Title("M(#phi#phi)"));

	RooArgList listComp, listNorm;
	RooArgSet showParams;
	
	RooRealVar ASig("ASig", "ASig", 0.224, 1e-1,0.5);
	RooRealVar FourKMass("FourKMass", "FourKMass", 4*493.677);
	RooRealVar varSigmaFactor("varSigmaFactor","varSigmaFactor",1.85);
	RooRealVar varNarrowFraction("varNarrowFraction","varNarrowFraction",0.76);
	
//========== fitting model components:
	RooRealVar varEtacNumber("N(#eta_{c}(1S))", "varEtacNumber", 10000, 0, 2e4);
	RooRealVar varEtacMass("varEtacMass", "varEtacMass", /*2983.58*/2985, 2981, 2989);
	RooRealVar varEtacGamma("varEtacGamma", "varEtacGamma", 28, 20, 45);
	RooFormulaVar varEtacSigmaN("varEtacSigmaN", "varEtacSigmaN", "sqrt(@1-@2)*(@0)", RooArgList(ASig, varEtacMass, FourKMass));
	RooFormulaVar varEtacSigmaW("varEtacSigmaW", "varEtacSigmaW", "@0*@1", RooArgList(varSigmaFactor, varEtacSigmaN));

	RooVoigtian pdfEtacN("pdfEtacN", "pdfEtacN", varMass, varEtacMass, varEtacGamma, varEtacSigmaN);
	RooVoigtian pdfEtacW("pdfEtacW", "pdfEtacW", varMass, varEtacMass, varEtacGamma, varEtacSigmaW);
	RooAddPdf pdfEtac("pdfEtac","pdfEtac",RooArgList(pdfEtacN,pdfEtacW),varNarrowFraction);
	listComp.add(pdfEtac);
	listNorm.add(varEtacNumber);
	showParams.add(varEtacNumber);
//	showParams.add(varEtacMass);
// 	showParams.add(varEtacGamma);
	showParams.add(varEtacSigmaN);
	
	
	RooRealVar MassDif("MassDif","MassDif",jpsiMass-etacMass);
	
	RooRealVar varJpsiNumber("N(J/#psi)", "varJpsiNumber", 200, 50, 1e3);
	RooFormulaVar varJpsiMass("varJpsiMass", "varJpsiMass","@0+@1",RooArgList(varEtacMass,MassDif));
	
	RooFormulaVar varJpsiSigmaN("varJpsiSigmaN", "varJpsiSigmaN", "sqrt(@1-@2)*(@0)", RooArgList(ASig, varJpsiMass, FourKMass));
	RooFormulaVar varJpsiSigmaW("varJpsiSigmaW", "varJpsiSigmaW", "@0*@1", RooArgList(varSigmaFactor, varJpsiSigmaN));

	RooGaussian pdfJpsiN("pdfJpsiN", "pdfJpsiN", varMass, varJpsiMass, varJpsiSigmaN);
	RooGaussian pdfJpsiW("pdfJpsiW", "pdfJpsiW", varMass, varJpsiMass, varJpsiSigmaW);
	RooAddPdf pdfJpsi("pdfJpsi","pdfJpsi",RooArgList(pdfJpsiN,pdfJpsiW),varNarrowFraction);
	
	listComp.add(pdfJpsi);
	listNorm.add(varJpsiNumber);
	showParams.add(varJpsiNumber);



	RooRealVar varBgrNumber("N_{bgr}", "number of background events", 8e4, 1e3, 2e5);
	char label[200];
	RooRealVar varA0("varA0", "varA0", -1e-1, +1e-1);
	RooRealVar varC0("varC0", "varC0", -2e-3,-1e-2, 0);
	RooRealVar var2PhiMass("var2PhiMass", "var2PhiMass", 2038.9);
	RooExponential pdfExp("pdfExp", "pdfExp", varMass, varC0);
	RooGenericPdf pdfBgr("pdfBgr", "pdfBgr", "exp(@0*@1)*sqrt(@0-@2)*(@0>@2)",
			RooArgSet(varMass, varC0, var2PhiMass));
// 	RooGenericPdf pdfBgr("pdfBgr", "pdfBgr", "exp(@0*@1)*(1+@0*@2)*sqrt(@0-@3)*(@0>@3)",
// 			RooArgSet(varMass, varC0, varA0, var2PhiMass));
	sprintf(label, "e^{-c_{0} M}#times(1+c_{1} M)#times #sqrt{M-2M_{#phi}}");
//	RooGenericPdf pdfBgr("pdfBgr", "pdfBgr", "exp(@0*@3)+@2",
//			RooArgSet(varMass, var2PhiMass, varA0, varA1));
//	RooGenericPdf pdfBgr("pdfBgr", "pdfBgr", "(exp(@0*@3)+@2)*sqrt(@0-@1)*(@0>@1)",
//			RooArgSet(varMass, var2PhiMass, varA0, varA1));
	listComp.add(pdfBgr);
	listNorm.add(varBgrNumber);

	RooAddPdf pdfModel("pdfModel", "pdfModel", listComp, listNorm);

	
	results = pdfModel.fitTo(*DataSet,Save(true), PrintLevel(0), NumCPU(4));
	//results = pdfModel.fitTo(*DataSet/*, ExternalConstraints(G1Constraint)*/, Strategy(2), Save(true), PrintLevel(0), NumCPU(4));

	DataSet->plotOn(frame, Binning(binN, minMass, maxMass));


	pdfModel.plotOn(frame);

	pdfModel.paramOn(frame, Layout(0.75, 0.95, 0.95), Parameters(showParams), ShowConstants(true), Format("NELU",AutoPrecision(1)));
	frame->getAttText()->SetTextSize(0.02) ;



	TCanvas* canvA = new TCanvas("canvA", "canvA", 1000, 600);
	frame->Draw();
	
        showParams.writeToFile("params.txt");

}


