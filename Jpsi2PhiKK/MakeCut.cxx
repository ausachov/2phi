// $Id: $
{
  TChain *chain = new TChain("DecayTree");
  chain->Add("Data/PhiKK_All.root");
  
  
  TCut Cut("Jpsi_ENDVERTEX_CHI2<45 && Phi1_ENDVERTEX_CHI2<16 && abs(Phi1_MM-1020)<12 && Kaon1_ProbNNk>0.1 && Kaon2_ProbNNk>0.1 && Kaon3_ProbNNk>0.2 && Kaon4_ProbNNk>0.2"); 
  TCut Phi2Cut("Phi2_MM>1025 || Phi2_MM<1015");
  TFile *newfile = new TFile("2phi_afterCut.root","recreate");
  TTree *newtree = chain->CopyTree(Cut/* && Phi2Cut*/);
 
  newtree->Print();
  newfile->Write();
  
  delete chain;
  delete newfile;
  
}

