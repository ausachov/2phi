void MakeDown()
{
  TChain *chain = new TChain("DecayTree");
  chain->Add("Reduced_MagDown2011.root");
  chain->Add("Reduced_MagDown2012.root");
  
  TFile *newfile = new TFile("2phiAllDown.root","recreate");
  TTree *newtree = chain->CopyTree("");
  
    Double_t Phi1_MM_Mix;
  Double_t Phi2_MM_Mix;
  Double_t Phi1_MM;
  Double_t Phi2_MM;

   
  chain->SetBranchAddress("Phi1_MM",&Phi1_MM);
  chain->SetBranchAddress("Phi2_MM",&Phi2_MM);
   
  TBranch *Phi1_MM_Mix_Branch = newtree->Branch("Phi1_MM_Mix", &Phi1_MM_Mix, "Phi1_MM_Mix/D");
  TBranch *Phi2_MM_Mix_Branch = newtree->Branch("Phi2_MM_Mix", &Phi2_MM_Mix, "Phi2_MM_Mix/D");

  Long64_t NEntries = newtree->GetEntries();

  for (Long64_t i = 0; i < NEntries; i++)
  {
     chain->GetEntry(i);
     if(i%2 == 0)
     {
       Phi1_MM_Mix = Phi1_MM;
       Phi2_MM_Mix = Phi2_MM;
     }
     else
     {
       Phi1_MM_Mix = Phi2_MM;
       Phi2_MM_Mix = Phi1_MM;
     }
     Phi1_MM_Mix_Branch->Fill();
     Phi2_MM_Mix_Branch->Fill();
  }
  
  newtree->Print();
  newfile->Write();
  
  delete chain;
  delete newfile;
}
