void Make2011()
{
  TChain *chain = new TChain("DecayTree");
  chain->Add("Reduced_MagDown2011.root");
  chain->Add("Reduced_MagUp2011.root");
  
  TFile *newfile = new TFile("PhiKK_2011.root","recreate");
  TTree *newtree = chain->CopyTree("");
  
  Double_t Jpsi_PE,Jpsi_PX,Jpsi_PY,Jpsi_PZ;
  Double_t Phi1_PE,Phi1_PX,Phi1_PY,Phi1_PZ;
  
  chain->SetBranchAddress("Jpsi_PE",&Jpsi_PE);
  chain->SetBranchAddress("Jpsi_PX",&Jpsi_PX);
  chain->SetBranchAddress("Jpsi_PY",&Jpsi_PY);
  chain->SetBranchAddress("Jpsi_PZ",&Jpsi_PZ);
  
  chain->SetBranchAddress("Phi1_PE",&Phi1_PE);
  chain->SetBranchAddress("Phi1_PX",&Phi1_PX);
  chain->SetBranchAddress("Phi1_PY",&Phi1_PY);
  chain->SetBranchAddress("Phi1_PZ",&Phi1_PZ);
  
  Double_t PX, PY, PZ, PE, Phi2_MM;
  
  TBranch *Phi2_MM_Branch = newtree->Branch("Phi2_MM", &Phi2_MM, "Phi2_MM/D");

  Long64_t NEntries = newtree->GetEntries();

  for (Long64_t i = 0; i < NEntries; i++)
  {
     chain->GetEntry(i);
     PE = Jpsi_PE - Phi1_PE;
     PX = Jpsi_PX - Phi1_PX;
     PY = Jpsi_PY - Phi1_PY;
     PZ = Jpsi_PZ - Phi1_PZ;
     Phi2_MM = TMath::Sqrt(PE*PE-PX*PX-PY*PY-PZ*PZ);
     Phi2_MM_Branch->Fill();
  }
  newtree->Print();
  newfile->Write();
  
  delete chain;
  delete newfile;
}
