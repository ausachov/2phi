// $Id: $
{
  TChain *chain = new TChain("DecayTree");
  chain->Add("Data/PhiKK_2012.root");
  
  
  TCut Cut("Jpsi_ENDVERTEX_CHI2<20 && Phi1_ENDVERTEX_CHI2<25 && abs(Phi1_MM-1020)<12 && Kaon1_ProbNNk>0.1 && Kaon2_ProbNNk>0.1 && Kaon3_ProbNNk>0.2 && Kaon4_ProbNNk>0.2"); 
  TCut Phi2Cut("Phi2_MM>1030 || Phi2_MM<1010");
  TFile *newfile = new TFile("2phi_afterCut.root","recreate");
  TTree *newtree = chain->CopyTree(Cut && Phi2Cut);
 
  newtree->Print();
  newfile->Write();
  
  delete chain;
  delete newfile;
  
}

