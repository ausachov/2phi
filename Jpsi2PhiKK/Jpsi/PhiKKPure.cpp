
using namespace RooFit;

void PhiKKPure(){
	gROOT->Reset();
	gROOT->SetStyle("Plain");
	TProof::Open("");

	Float_t minMassJpsi = 2700;
	Float_t maxMassJpsi = 4000;
	Float_t binWidthJpsi = 10.;
	Int_t binNJpsi = int((maxMassJpsi-minMassJpsi)/binWidthJpsi);
	Float_t minMassPhi = 1009;
	Float_t maxMassPhi = 1031;
	Float_t binWidthPhi = 1.;
	Int_t binNPhi = int((maxMassPhi-minMassPhi)/binWidthPhi);
	RooRealVar Jpsi_m_scaled("Jpsi_m_scaled", "Jpsi_m_scaled", minMassJpsi, maxMassJpsi, "MeV");
	RooRealVar Phi1_MM("Phi1_MM", "Phi1_MM", minMassPhi, maxMassPhi, "MeV");
	//RooRealVar Phi2_MM("Phi2_MM", "Phi2_MM", 500, 1500, "MeV");
	TH1F* histJpsiPhiK = new TH1F("histJpsiPhiK", "histJpsiPhiK", binNJpsi, minMassJpsi, maxMassJpsi);
	TH1F* histJpsiDiK = new TH1F("histJpsiDiK", "histJpsiDiK", binNJpsi, minMassJpsi, maxMassJpsi);
	TH1F* histJpsiPhiK = new TH1F("histJpsiPhiK", "histJpsiPhiK", binNJpsi, minMassJpsi, maxMassJpsi);

	TChain* chain = new TChain("DecayTree");
	chain->Add("2phi_afterCut.root");

	RooDataSet* dsetFull = new RooDataSet("dsetFull", "dsetFull", chain, RooArgSet(Jpsi_m_scaled, Phi1_MM), "");

	RooRealVar varPhiMass("varPhiMass", "varPhiMass", 1019.46/*, 1019.46-1, 1019.46+1*/);
	RooRealVar varSigma("varSigma", "varSigma", 1, 0.1, 3);
	RooRealVar varPhiGamma("varPhiGamma", "varPhiGamma", 4.26);
	RooRealVar var2KMass("var2KMass", "var2KMass", 493.67*2);
	

	RooGenericPdf pdfPhi1("pdfPhi1", "pdfPhi1", "sqrt(@0-@1)*TMath::Voigt(@0-@2,@3,@4)",
			      RooArgList(Phi1_MM, var2KMass, varPhiMass,varSigma,varPhiGamma));
	RooRealVar varA1("varA1", "varA1", 0,10);
	RooGenericPdf pdfRoot1("pdfRoot1", "pdfRoot1", "sqrt(@0-@1)*(1+(@0-@1)*@2)", RooArgList(Phi1_MM, var2KMass,varA1));	
	RooRealVar varK1("varK1", "varK1", 0, 1);
	RooAddPdf pdfModel1("pdfModel1", "pdfModel1", RooArgList(pdfPhi1, pdfRoot1), varK1);
	
// 	RooGenericPdf pdfPhi2("pdfPhi2", "pdfPhi2", "sqrt(@0-@1)*TMath::Voigt(@0-@2,@3,@4)",
// 			      RooArgList(Phi2_MM, var2KMass, varPhiMass,varSigma,varPhiGamma));
// 	RooRealVar varA2("varA2", "varA2", 0,10);
// 	RooGenericPdf pdfRoot2("pdfRoot2", "pdfRoot2", "sqrt(@0-@1)*(1+(@0-@1)*@2)", RooArgList(Phi2_MM, var2KMass,varA2));
// 	RooRealVar varK2("varK2", "varK2", 0, 1);
// 	RooAddPdf pdfModel2("pdfModel2", "pdfModel2", RooArgList(pdfPhi2, pdfRoot2), varK1);
	
//	RooProdPdf pdfModelNE("pdfModelNE", "pdfModelNE", RooArgSet(pdfModel1, pdfModel2));
	RooRealVar varEvN("varEvN", "varEvN", 0, 1e5);
	RooExtendPdf pdfModel("pdfModel", "pdfModel",  pdfModel1, varEvN);

	RooFormulaVar varNDiPhi("varNDiPhi", "varNDiPhi", "@0*@1", RooArgList(varEvN, varK1));
	RooFormulaVar varNDiK("varNDiK", "varNDiK", "@0*(1-@1)", RooArgList(varEvN, varK1));
	//RooFormulaVar varNPhiK("varNPhiK", "varNPhiK", "2*@0*(@1*(1-@1))", RooArgList(varEvN, varK1));

	char label[200];
	Float_t massJpsiLo, massJpsiHi;

	for (Int_t i=0; i<binNJpsi; i++){
		massJpsiLo = minMassJpsi + i*binWidthJpsi;
		massJpsiHi = minMassJpsi + (i+1)*binWidthJpsi;

		sprintf(label, "Jpsi_m_scaled>%i&&Jpsi_m_scaled<%i", massJpsiLo, massJpsiHi);
		RooDataSet* dset = dsetFull->reduce(Cut(label), Name("dset"), Title("dset"));
		RooFitResult* res = pdfModel.fitTo(*dset, Save(true), PrintLevel(0));

		histJpsiPhiK->SetBinContent(i+1, varNDiPhi.getVal());
		histJpsiPhiK->SetBinError(i+1, varNDiPhi.getPropagatedError(*res));

		histJpsiDiK->SetBinContent(i+1, varNDiK.getVal());
		histJpsiDiK->SetBinError(i+1, varNDiK.getPropagatedError(*res));


		delete dset;
		delete res;
	}

	
	
	TCanvas* canvTest = new TCanvas("canvTest", "canvTest", 1200, 400);
//	canvTest->Divide(3, 1);

	Double_t minMassJpsiTest = minMassJpsi;
	Double_t maxMassJpsiTest = maxMassJpsi;
	
	TPostScript ps("test.ps", 111);
	for (Int_t i=0; i<1; i++){
		massJpsiLo = minMassJpsiTest;
		massJpsiHi = maxMassJpsiTest;
 
		sprintf(label, "Jpsi_m_scaled>%i&&Jpsi_m_scaled<%i", massJpsiLo, massJpsiHi);
		RooDataSet* dset2 = dsetFull->reduce(Cut(label), Name("dset2"), Title("dset2"));
		RooFitResult* res = pdfModel.fitTo(*dset2, Save(true), PrintLevel(0));

//		TH2* hPdf = pdfModel.createHistogram("Phi1_MM,Phi2_MM");

		RooPlot* frame1 = Phi1_MM.frame(Title("#phi_{1} mass"));
		dset2->plotOn(frame1, Binning(binNPhi, minMassPhi, maxMassPhi));
		pdfModel1.plotOn(frame1);
		pdfModel1.plotOn(frame1, Components(pdfRoot1), LineStyle(kDashed));

// 		RooPlot* frame2 = Phi2_MM.frame(Title("#phi_{2} mass"));
// 		dset2->plotOn(frame2, Binning(binNPhi, minMassPhi, maxMassPhi));
// 		pdfModel2.plotOn(frame2);
// 		pdfModel2.plotOn(frame2, Components(pdfRoot2), LineStyle(kDashed));

// 		canvTest->cd(1);
// 		hPdf->DrawClone("surf");
//		canvTest->cd(2);
		frame1->DrawClone();
// 		canvTest->cd(3);
// 		frame2->DrawClone();

		delete frame1;
//		delete frame2;
//		delete hPdf;

		delete dset2;
		delete res;
	}
	
		ps.Close();
	
	
	
	
	
	

	TCanvas* canvA = new TCanvas("canvA", "canvA", 800, 700);
	canvA->Divide(1, 2);
	canvA->cd(1);
	histJpsiPhiK->Draw();
	canvA->cd(2);
	histJpsiDiK->Draw();
// 	canvA->cd(3);
// 	histJpsiPhiK->Draw();

	TFile file("PhiKKPureAll.root","recreate");
	histJpsiPhiK->Write();
	histJpsiDiK->Write();
	histJpsiPhiK->Write();
	file.Close();
}


