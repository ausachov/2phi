
//PT_Type
// 0 - Etac
// 1 - Chic
void ProcessData(int PT_Type=0, int DataType=12)
{
    gROOT->Reset();
    switch(PT_Type)
    {
        case 0:
            Float_t BinsEdges[10]={3,5,7,9,11,13,15,19};
            int NPoints = 7;
            break;
        case 1:
            Float_t BinsEdges[10]={2,7.5,11,19};
            int NPoints = 3;
            break;
    }
    
    TChain *chain = new TChain("DecayTree");
    if(DataType==12)
        chain->Add("../Data_07_15/2phi2012.root");
    if(DataType==11)
        chain->Add("../Data_07_15/2phi2011.root");
    char filename[100], label[100];
    
    gROOT->ProcessLine(".L ../diPhiPure.cp");
    gROOT->ProcessLine(".L ../diPhi_fit_framework.cp");
    gROOT->ProcessLine(".L ../RooRelBreitWigner.cxx+");
 
//    for(int ii=2; ii<=2; ii++)
    for(int ii=1; ii<=NPoints; ii++)
    {
        sprintf(label, "Jpsi_PT>%i&&Jpsi_PT<%i", 1000*BinsEdges[ii-1], 1000*BinsEdges[ii]);
        TCut CutPT(label);
        sprintf(label, "Jpsi_PT>%3.1f&&Jpsi_PT<%3.1f", BinsEdges[ii-1], BinsEdges[ii]);
        sprintf(filename,"%s.root",label);
        TFile *newfile = new TFile(filename,"recreate");
        TTree *newtree = chain->CopyTree(CutPT);
        
        newtree->Print();
        newfile->Write();
        newfile->Close();
        
        char commandPure[100];
        sprintf(commandPure, "diPhiPure(10,0,\"%s\")",filename);
        gROOT->ProcessLine(commandPure);
        
        
        sprintf(commandPure, "diPhiPure(2,0,\"%s\")",filename);
        gROOT->ProcessLine(commandPure);
        sprintf(commandPure, "diPhiPure(11,0,\"%s\")",filename);
        gROOT->ProcessLine(commandPure);
        sprintf(commandPure, "diPhiPure(12,0,\"%s\")",filename);
        gROOT->ProcessLine(commandPure);

        char commandFit[100];

        if(PT_Type==1)
        {
            sprintf(commandFit, "PTFramework(3,15,%i)",ii);
            gROOT->ProcessLine(commandFit);
        }

        
        sprintf(commandFit, "PTFramework(3,0,%i)",ii);
        gROOT->ProcessLine(commandFit);
        sprintf(commandFit, "PTFramework(3,1,%i)",ii);
        gROOT->ProcessLine(commandFit);
        sprintf(commandFit, "PTFramework(3,2,%i)",ii);
        gROOT->ProcessLine(commandFit);
        sprintf(commandFit, "PTFramework(3,3,%i)",ii);
        gROOT->ProcessLine(commandFit);
        sprintf(commandFit, "PTFramework(3,4,%i)",ii);
        gROOT->ProcessLine(commandFit);
        sprintf(commandFit, "PTFramework(3,5,%i)",ii);
        gROOT->ProcessLine(commandFit);
        sprintf(commandFit, "PTFramework(3,6,%i)",ii);
        gROOT->ProcessLine(commandFit);
//        sprintf(commandFit, "PTFramework(7,0,%i)",ii);
//        gROOT->ProcessLine(commandFit);
//        sprintf(commandFit, "PTFramework(8,0,%i)",ii);
//        gROOT->ProcessLine(commandFit);
        sprintf(commandFit, "PTFramework(3,9,%i)",ii);
        gROOT->ProcessLine(commandFit);
//        sprintf(commandFit, "PTFramework(10,0,%i)",ii);
//        gROOT->ProcessLine(commandFit);
        sprintf(commandFit, "PTFramework(3,11,%i)",ii);
        gROOT->ProcessLine(commandFit);
//        sprintf(commandFit, "PTFramework(12,0,%i)",ii);
//        gROOT->ProcessLine(commandFit);
        sprintf(commandFit, "PTFramework(3,13,%i)",ii);
        gROOT->ProcessLine(commandFit);
//        sprintf(commandFit, "PTFramework(14,0,%i)",ii);
//        gROOT->ProcessLine(commandFit);

        sprintf(commandFit, "PTFramework(3,16,%i)",ii);
        gROOT->ProcessLine(commandFit);
        sprintf(commandFit, "PTFramework(3,17,%i)",ii);
        gROOT->ProcessLine(commandFit);
//        sprintf(commandFit, "PTFramework(18,0,%i)",ii);
//        gROOT->ProcessLine(commandFit);
    
        
        
        cout<<"FILE  "<<filename<<endl;
        cout<<"COMMAND  "<<commandFit<<endl;
        delete newfile;
    }
    delete chain;
}




//errType!=0 - all uncorrelated syst
void ProcessDataSIMULChic(int errType=0, int DataType=12)
{
    gROOT->Reset();

    
    Float_t BinsEdges[10]={2,8,12,19};
    
    int NPoints = 3;

    
    TChain *chain = new TChain("DecayTree");
    if(DataType==12)
        chain->Add("../Data_07_15/2phi2012.root");
    if(DataType==11)
        chain->Add("../Data_07_15/2phi2011.root");
    char filename[100], label[100];
    char commandPure[100];
    char commandFit[100];
    
    
    gROOT->ProcessLine(".L ../diPhiPure.cp");
    gROOT->ProcessLine(".L ../diPhi_fit_framework.cp");
    gROOT->ProcessLine(".L ../RooRelBreitWigner.cxx+");
    
    
    

    gROOT->ProcessLine(".L RooRelBreitWigner.cxx+");
    TProof::Open("");
        
    
    
    int NSysts = 12;
    if(errType==0) NSysts=1;
    int SysTypes [20]= {0,1,3,4,5,2,9,11,15,     /* when 2D fit has to be changed*/  6,16,17/*,13*/};
    int SysType = 0;
    
    
    
   
    
    for(int isyst=0;isyst<NSysts;isyst++)
    {
        
        RooWorkspace* wspaceSim = new RooWorkspace("SIM_WS");
    
        SysType = SysTypes[isyst];
        Add_SIMULTANEOUS_Model(3, SysType,wspaceSim);
        
        for(int ii=1; ii<=NPoints; ii++)
        {
            sprintf(label, "Jpsi_PT>%i&&Jpsi_PT<%i", 1000*BinsEdges[ii-1], 1000*BinsEdges[ii]);
            TCut CutPT(label);
            sprintf(label, "Jpsi_PT>%3.1f&&Jpsi_PT<%3.1f", BinsEdges[ii-1], BinsEdges[ii]);
            sprintf(filename,"%s.root",label);
            TFile *newfile = new TFile(filename,"recreate");
            TTree *newtree = chain->CopyTree(CutPT);
            
            newtree->Print();
            newfile->Write();
            newfile->Close();

            sprintf(label, "DataSet_%i",ii);
            switch(SysType)
            {
                case 0:
                    sprintf(commandPure, "diPhiPure(10,0,\"%s\",\"%s\")",filename,label);
                    gROOT->ProcessLine(commandPure);
                    break;
                case 6:
                    sprintf(commandPure, "diPhiPure(2,0,\"%s\",\"%s\")",filename,label);
                    gROOT->ProcessLine(commandPure);
                    break;
                case 16:
                    sprintf(commandPure, "diPhiPure(11,0,\"%s\",\"%s\")",filename,label);
                    gROOT->ProcessLine(commandPure);
                    break;
                case 17:
                    sprintf(commandPure, "diPhiPure(12,0,\"%s\",\"%s\")",filename,label);
                    gROOT->ProcessLine(commandPure);
                    break;
                default:
                    break;
            }
            
            
            AddData(SysType,wspaceSim,label);
        }
        
        
        sprintf(label, "Jpsi_PT>%i&&Jpsi_PT<%i", 1000*BinsEdges[0], 1000*BinsEdges[NPoints]);
        TCut CutPT(label);
        sprintf(label, "Jpsi_PT>%3.1f&&Jpsi_PT<%3.1f", BinsEdges[0], BinsEdges[NPoints]);
        sprintf(filename,"%s.root",label);
        TFile *newfile = new TFile(filename,"recreate");
        TTree *newtree = chain->CopyTree(CutPT);
        
        newtree->Print();
        newfile->Write();
        newfile->Close();
        
        
        
        sprintf(label, "DataSet_Tot");
        switch(SysType)
        {
            case 0:
                sprintf(commandPure, "diPhiPure(10,0,\"%s\",\"%s\")",filename,label);
                gROOT->ProcessLine(commandPure);
                break;
            case 6:
                sprintf(commandPure, "diPhiPure(2,0,\"%s\",\"%s\")",filename,label);
                gROOT->ProcessLine(commandPure);
                break;
            case 16:
                sprintf(commandPure, "diPhiPure(11,0,\"%s\",\"%s\")",filename,label);
                gROOT->ProcessLine(commandPure);
                break;
            case 17:
                sprintf(commandPure, "diPhiPure(12,0,\"%s\",\"%s\")",filename,label);
                gROOT->ProcessLine(commandPure);
                break;
            default:
                break;
        }
        AddData(SysType,wspaceSim,label);
        
        
        RooAbsPdf* pdfModel  = wspaceSim->pdf("pdfModel");
        RooAbsPdf* pdfModel1  = wspaceSim->pdf("pdfModel_1");
        RooAbsPdf* pdfModel2  = wspaceSim->pdf("pdfModel_2");
        RooAbsPdf* pdfModelTot  = wspaceSim->pdf("pdfModelTot");
        

        
        
        RooDataHist* DataSet1 = (RooDataHist*)wspaceSim->data("DataSet_1");
        RooDataHist* DataSet2 = (RooDataHist*)wspaceSim->data("DataSet_2");
        RooDataHist* DataSet3 = (RooDataHist*)wspaceSim->data("DataSet_3");
        RooDataHist* DataSetTot = (RooDataHist*)wspaceSim->data("DataSet_Tot");

        
        RooRealVar* Jpsi_m_scaled = wspaceSim->var("Jpsi_m_scaled");
        RooRealVar* varEtac2Gamma = wspaceSim->var("#Gamma(#eta_{c}(2S))");
        varEtac2Gamma->setConstant(kTRUE);
        
      

        
        RooAbsReal* chi2_1 = pdfModel->createChi2(*DataSet1,Extended(true)) ;
        RooAbsReal* chi2_2 = pdfModel1->createChi2(*DataSet2,Extended(true)) ;
        RooAbsReal* chi2_3 = pdfModel2->createChi2(*DataSet3,Extended(true)) ;
        RooAbsReal* chi2_Tot = pdfModelTot->createChi2(*DataSetTot,Extended(true)) ;
        
        
        
        RooAddition sumchi2("sumchi2","sumchi2",RooArgSet(*chi2_1,*chi2_2,*chi2_3,*chi2_Tot)) ;
        
        

        RooMinuit m2(sumchi2);
        m2.migrad();
        m2.hesse();
        m2.migrad();
        m2.hesse();
        m2.minos();

        
        
        
        
        if(SysType!=15)
            RooRealVar* varEtacNumber = wspaceSim->var("NEta_");
        RooRealVar* varChi0Number = wspaceSim->var("NChi0");
        RooRealVar* varChi1Number = wspaceSim->var("NChi1");
        RooRealVar* varChi2Number = wspaceSim->var("NChi2");
        RooRealVar* varEtac2Number= wspaceSim->var("NEta2");
        
        
        if(SysType!=15)
            RooRealVar* varEtac_1Number = wspaceSim->var("NEta_1");
        RooRealVar* varChi01Number  = wspaceSim->var("NChi01");
        RooRealVar* varChi11Number  = wspaceSim->var("NChi11");
        RooRealVar* varChi21Number  = wspaceSim->var("NChi21");
        RooRealVar* varEtac21Number = wspaceSim->var("NEta21");
        
        
        if(SysType!=15)
            RooRealVar* varEtac_2Number = wspaceSim->var("NEta_2");
        
        RooRealVar* varChi02Number  = wspaceSim->var("NChi02");
        RooRealVar* varChi12Number  = wspaceSim->var("NChi12");
        RooRealVar* varChi22Number  = wspaceSim->var("NChi22");
        RooRealVar* varEtac22Number = wspaceSim->var("NEta22");
        
        
        
        


        char txtName [200];
        sprintf(txtName,"paramsPT_%i.txt",SysType);
        
        FILE * fout = fopen (txtName, "w");

        if(SysType!=15)
        {
            fprintf(fout,"%i %f %f %f %f %f %f %f %f %f %f %f %f\n",
                    1,
                    varEtacNumber->getVal(),  -varEtacNumber->getErrorLo(), varEtacNumber->getErrorHi(),
                    varChi0Number->getVal(), -varChi0Number->getErrorLo(), varChi0Number->getErrorHi(),
                    varChi1Number->getVal(), -varChi1Number->getErrorLo(), varChi1Number->getErrorHi(),
                    varChi2Number->getVal(), -varChi2Number->getErrorLo(), varChi2Number->getErrorHi()
                    );
            fprintf(fout,"%i %f %f %f %f %f %f %f %f %f %f %f %f\n",
                    2,
                    varEtac_1Number->getVal(),  -varEtac_1Number->getErrorLo(), varEtac_1Number->getErrorHi(),
                    varChi01Number->getVal(), -varChi01Number->getErrorLo(), varChi01Number->getErrorHi(),
                    varChi11Number->getVal(), -varChi11Number->getErrorLo(), varChi11Number->getErrorHi(),
                    varChi21Number->getVal(), -varChi21Number->getErrorLo(), varChi21Number->getErrorHi()
                    );
            fprintf(fout,"%i %f %f %f %f %f %f %f %f %f %f %f %f\n",
                    3,
                    varEtac_2Number->getVal(),  -varEtac_2Number->getErrorLo(), varEtac_2Number->getErrorHi(),
                    varChi02Number->getVal(), -varChi02Number->getErrorLo(), varChi02Number->getErrorHi(),
                    varChi12Number->getVal(), -varChi12Number->getErrorLo(), varChi12Number->getErrorHi(),
                    varChi22Number->getVal(), -varChi22Number->getErrorLo(), varChi22Number->getErrorHi()
                    );
        }
        else
        {
            fprintf(fout,"%i %f %f %f %f %f %f %f %f %f %f %f %f\n",
                    1,
                    0,0,0,
                    varChi0Number->getVal(), -varChi0Number->getErrorLo(), varChi0Number->getErrorHi(),
                    varChi1Number->getVal(), -varChi1Number->getErrorLo(), varChi1Number->getErrorHi(),
                    varChi2Number->getVal(), -varChi2Number->getErrorLo(), varChi2Number->getErrorHi()
                    );
            fprintf(fout,"%i %f %f %f %f %f %f %f %f %f %f %f %f\n",
                    2,
                    0,0,0,
                    varChi01Number->getVal(), -varChi01Number->getErrorLo(), varChi01Number->getErrorHi(),
                    varChi11Number->getVal(), -varChi11Number->getErrorLo(), varChi11Number->getErrorHi(),
                    varChi21Number->getVal(), -varChi21Number->getErrorLo(), varChi21Number->getErrorHi()
                    );
            fprintf(fout,"%i %f %f %f %f %f %f %f %f %f %f %f %f\n",
                    3,
                    0,0,0,
                    varChi02Number->getVal(), -varChi02Number->getErrorLo(), varChi02Number->getErrorHi(),
                    varChi12Number->getVal(), -varChi12Number->getErrorLo(), varChi12Number->getErrorHi(),
                    varChi22Number->getVal(), -varChi22Number->getErrorLo(), varChi22Number->getErrorHi()
                    );
        }

        fclose(fout);
        
        
        TCanvas* canv1 = new TCanvas("canv1", "canv1", 1000, 600);
        RooPlot* frame1 = Jpsi_m_scaled->frame(Title("Jpsi_m_scaled_1"));
        DataSet1->plotOn(frame1);
        pdfModel->plotOn(frame1);
        frame1->Draw();
        canv1->SaveAs("1.pdf");
        
        
        TCanvas* canv2 = new TCanvas("canv2", "canv2", 1000, 600);
        RooPlot* frame2 = Jpsi_m_scaled->frame(Title("Jpsi_m_scaled_2"));
        DataSet2->plotOn(frame2);
        pdfModel1->plotOn(frame2);
        frame2->Draw();
        canv2->SaveAs("2.pdf");
        
        
        
        TCanvas* canv3 = new TCanvas("canv3", "canv3", 1000, 600);
        RooPlot* frame3 = Jpsi_m_scaled->frame(Title("Jpsi_m_scaled_3"));
        DataSet3->plotOn(frame3);
        pdfModel2->plotOn(frame3);
        frame3->Draw();
        canv3->SaveAs("3.pdf");
        
        
        delete wspaceSim;
    }

   



    
    delete chain;
   
}




//errType
// 1 - with MC and BR error
// 0 - stat only
// 2 - with syst error
void plotSpectraEtac(int errType=2, int Year = 12)
{
    gROOT->Reset();
    gROOT->ProcessLine(".x ../lhcbStyle.C");
    
    Float_t BinsEdges[10]={3,5,7,9,11,13,15,19};
    int NPoints = 7;
    
    Float_t BRphiphi = 3.21*0.001;
    Float_t BRKK = 0.489;
    
    Float_t erBRphiphi = 0.72*0.001;
    Float_t erBRKK = 0.005;
    
    //     Float_t erBRphiphi = 0;
    //     Float_t erBRKK = 0;
    
    Float_t NGenerated = 2054064.;
    Float_t Luminosity;
    switch(Year)
    {
        case 11:
            Luminosity=1.;
            break;
        case 12:
            Luminosity=2.;
            break;
    }
    
    

    
    FILE* fp = fopen("paramsPT_0.txt","r");
    Float_t x [10], ex1 [10], ex2 [10];
    Float_t Data [10], eDataLo [10], eDataHi [10];
    Float_t a;
    int nPoint;
    for(int ii=0; ii<NPoints; ii++)
    {
        fscanf(fp, "%i %e %e %e", &nPoint,&Data[ii],&eDataLo[ii],&eDataHi[ii]);
        fscanf(fp,"%e %e %e %e %e %e %e %e %e", &a,&a,&a,&a,&a,&a,&a,&a,&a);
        x[ii]=(BinsEdges[ii+1]+BinsEdges[ii])/2;
        ex2[ii]=x[ii]-BinsEdges[ii];
        ex1[ii]=0;
    }
    fclose(fp);
    

    int nSysts = 11;
    int Systs[20] = {1,2,3,4,5,6,9,11,13,16,17};
    char txtfname [200];
    
    Float_t DataSys [20][10], eDataSysLo [20][10], eDataSysHi [20][10];
    Float_t Sys;
    Float_t eSysRel[20];


    for(int i=0;i<nSysts;i++)
    {
        sprintf(txtfname,"paramsPT_%i.txt",Systs[i]);
        fp = fopen(txtfname,"r");
        for(int ii=0; ii<NPoints; ii++)
        {
            fscanf(fp, "%i %e %e %e", &nPoint,&DataSys[i][ii],&eDataSysLo[i][ii],&eDataSysHi[i][ii]);
            fscanf(fp,"%e %e %e %e %e %e %e %e %e", &a,&a,&a,&a,&a,&a,&a,&a,&a);
        }
        fclose(fp);
    }
    
    
    FILE* fpp = fopen("paramsSysEtac.txt","w");
    Float_t TotalSys = 0;
    Float_t SysFract=0;
    if(errType==2)SysFract=1.;
    
    
    
    for(int ii=0; ii<NPoints; ii++)
    {
        TotalSys = 0;
        fprintf(fpp,"%i         %f  %f  %f\n",ii, Data[ii],eDataLo[ii],eDataHi[ii]);
        for(int i=0;i<nSysts;i++)
        {
            Sys = DataSys[i][ii]-Data[ii];
            TotalSys+=Sys*Sys;
            fprintf(fpp,"       %i  %f  %f  %f\n",Systs[i], Sys,eDataSysLo[i][ii],eDataSysHi[i][ii]);
        }
        fprintf(fpp,"     Tot:  %f  \n",TMath::Sqrt(TotalSys));
        eDataLo[ii]=TMath::Sqrt(eDataLo[ii]*eDataLo[ii]+SysFract*TotalSys);
        eDataHi[ii]=TMath::Sqrt(eDataHi[ii]*eDataHi[ii]+SysFract*TotalSys);
        fprintf(fpp,"\n");
        
        eSysRel[ii]=TMath::Sqrt(TotalSys)/Data[ii];
    }
    fclose(fpp);
    

    
    TGraphAsymmErrors* DataGr = new TGraphAsymmErrors(NPoints,x,Data,ex1,ex1,eDataLo,eDataHi);
    TGraphAsymmErrors* DataGrDraw = new TGraphAsymmErrors(NPoints,x,Data,ex2,ex2,eDataLo,eDataHi);
    DataGrDraw->Draw("AP");
    
    TCut KaonIPCut("Kaon1_IPCHI2_OWNPV>4 && Kaon2_IPCHI2_OWNPV>4 && Kaon3_IPCHI2_OWNPV>4 && Kaon4_IPCHI2_OWNPV>4");
    TCut KaonTRCut("Kaon1_TRACK_CHI2NDOF<3 && Kaon2_TRACK_CHI2NDOF<3 && Kaon3_TRACK_CHI2NDOF<3 && Kaon4_TRACK_CHI2NDOF<3");
    TCut PIDCut("Kaon1_ProbNNk>0.1 && Kaon2_ProbNNk>0.1 && Kaon3_ProbNNk>0.1 && Kaon4_ProbNNk>0.1");
    TCut PhiMCut("abs(Phi1_MM-1020)<12 && abs(Phi2_MM-1020)<12");
    TCut PhiVerCut("Phi1_ENDVERTEX_CHI2<25 && Phi2_ENDVERTEX_CHI2<25");
    TCut JpsiCut("Jpsi_ENDVERTEX_CHI2<45 && Jpsi_MM>2000 && Jpsi_MM<6000 && Jpsi_FDCHI2_OWNPV>100");// >49");
    TCut PTCut("Kaon1_PT>500 && Kaon2_PT>500 && Kaon3_PT>500 && Kaon4_PT>500");
    
    TCut Trigger0Cut("JpsiL0HadronDecision_TOS || JpsiL0Global_TIS");
    TCut Trigger1Cut("JpsiHlt1TrackAllL0Decision_TOS");
    TCut Trigger2Cut("JpsiHlt2Topo2BodyBBDTDecision_TOS || JpsiHlt2Topo3BodyBBDTDecision_TOS || JpsiHlt2Topo4BodyBBDTDecision_TOS || JpsiHlt2IncPhiDecision_TOS");
    
    
    TCut totCut = KaonIPCut && KaonTRCut && PIDCut && PhiMCut && PhiVerCut && JpsiCut && PTCut && Trigger0Cut && Trigger1Cut && Trigger2Cut;
    
    TCut AllPTCut("Jpsi_PT>0 && Jpsi_PT<19000");
    
    TChain * DecayTreeMCAll=new TChain("DecayTree");
    DecayTreeMCAll->Add("../../MC/CAL_0/Etac/AllMC_Dirty.root");
    TTree* DecayTreeMC = DecayTreeMCAll->CopyTree(AllPTCut&&totCut);
    
    Int_t NEnMC=DecayTreeMC->GetEntries();
    Double_t Branch_Value2;
    DecayTreeMC->SetBranchAddress("Jpsi_PT",&Branch_Value2);
    TH1D *histMC = new TH1D("etac MC","etac MC", NPoints, BinsEdges);
    
    for (Int_t i=0; i<NEnMC; i++)
    {
        DecayTreeMC->GetEntry(i);
        histMC->Fill(Branch_Value2/1000);
    }
    
    
    TChain * DecayTreeGenAll=new TChain("MCDecayTreeTuple/MCDecayTree");
    DecayTreeGenAll->Add("../../MC/GenSpectr/Etac/res.root");
    
    TCut AllPTetacCut("eta_c_1S_TRUEPT>3000 && eta_c_1S_TRUEPT<19000");
    TTree* DecayTreeGen = DecayTreeGenAll->CopyTree(AllPTetacCut);
    Int_t NEnGen=DecayTreeGen->GetEntries();
    Double_t Branch_Value3;
    DecayTreeGen->SetBranchAddress("eta_c_1S_TRUEPT",&Branch_Value3);
    TH1D *histGen = new TH1D("Gen","Gen", NPoints, BinsEdges);
    for (Int_t i=0; i<NEnGen; i++)
    {
        DecayTreeGen->GetEntry(i);
        histGen->Fill(Branch_Value3/1000);
    }
    
    Float_t NMCSelected = histMC->Integral();
    Float_t NMCGen = histGen->Integral();
    
    //    histMC->Scale(1./histMC->Integral());
    //    histGen->Scale(1./histGen->Integral());
    
    TCanvas* canvC = new TCanvas("canvC", "canv", 800, 700);
    histGen->SetLineColor(kRed);
    histGen->Draw();
    histMC->Draw("same");

    Float_t Res[20], eResLo [20], eResHi [20], eps[20],eBRResLo[20];
    TH1D *histEps = new TH1D("histEps","histEps", NPoints, BinsEdges);
    
    
    
    for(int ii=0; ii<NPoints; ii++)
    {
        eps[ii]=Float_t(Float_t(histMC->GetBinContent(ii+1))/Float_t(histGen->GetBinContent(ii+1))/NGenerated*NMCGen);
        histEps->SetBinContent(ii+1,eps[ii]);
        
        Res[ii]=Data[ii]/eps[ii]/Luminosity/(BinsEdges[ii+1]-BinsEdges[ii])/BRphiphi/BRKK/BRKK*1e-6;


            Float_t BRErrorRelLo = TMath::Sqrt(1./histMC->GetBinContent(ii+1) + (erBRphiphi/BRphiphi)**2 + (2*erBRKK/BRKK)**2 +  + 1/histGen->GetBinContent(ii+1));


            Float_t ErrorRelHi = eDataHi[ii]/Data[ii];
            Float_t ErrorRelLo = eDataLo[ii]/Data[ii];
        
        eResHi[ii] = Res[ii]*ErrorRelHi;
        eResLo[ii] = Res[ii]*ErrorRelLo;
        
        eBRResLo[ii] = Res[ii]*BRErrorRelLo;
    }
    TCanvas* canvEps = new TCanvas("canvEps", "canvEps", 800, 700);
    histEps->Draw();
    
    
    
    char directory [200];
    char fname [200];
    sprintf(directory, "etac%i_upto19", Year);
    sprintf(fname,"%s/EtacOut.txt",directory);
    FILE *fp = fopen(fname, "w");
    
    fprintf(fp,"%i\n",NPoints);
    for(int i=0;i<NPoints;i++)
        fprintf(fp,"%f %f %f %f %f %f \n",x[i],Res[i],ex2[i],ex2[i],eResLo[i],eResHi[i]);
    fclose(fp);
    
    
    sprintf(fname,"%s/EtacOut_Expl.txt",directory);
    FILE *fp = fopen(fname, "w");
    
    fprintf(fp,"%i\n",NPoints);
    for(int i=0;i<NPoints;i++)
        fprintf(fp,"%f %f %f %f %f %f %f %f\n",x[i],Res[i],ex2[i],ex2[i],eResLo[i],eResHi[i],eSysRel[i]*Res[i],eBRResLo[i]);
    fclose(fp);
    
}


//2 - total w/o correlated
//0 - stat

void plotSpectraChic(int errType=2, int Year = 12)
{
    
    Float_t BinsEdges[10]={2,8,12,19};

    
    int NPoints = 3;
    
    Float_t BRChi0phiphi = 0.77*0.001;
    Float_t BRChi1phiphi = 0.42*0.001;
    Float_t BRChi2phiphi = 1.12*0.001;
    Float_t BRKK = 0.489;
    
    Float_t erBRChi0phiphi = 0.07*0.001;
    Float_t erBRChi1phiphi = 0.05*0.001;
    Float_t erBRChi2phiphi = 0.1*0.001;
    Float_t erBRKK = 0.005;
    
    //     Float_t erBRChi0phiphi = 0;
    //     Float_t erBRChi1phiphi = 0;
    //     Float_t erBRChi2phiphi = 0;
    //     Float_t erBRKK = 0;
    
    Float_t NChi0Generated = 1105362.;
    Float_t NChi1Generated = 1101384.;
    Float_t NChi2Generated = 1135884.;
    
    
    char DirectoryName [200];
    
    Float_t Luminosity;
    switch(Year)
    {
        case 11:
            Luminosity=1.;
            break;
        case 12:
            Luminosity=2.;
            break;
    }
    
    
    char directory [200];
    char fname [200];
    sprintf(directory, "chic%iSIM", Year);
    
   
    char basename [200];
    sprintf(basename,"%s/paramsPT_0.txt",directory);

    
    
    FILE *fp = fopen(basename,"r");
    Float_t x [10], ex [10];
    Float_t x0 [10], x1[10], x2[10], exDr[10];
    Float_t DataChi0 [10], eDataChi0Lo [10], eDataChi0Hi [10];
    Float_t DataChi1 [10], eDataChi1Lo [10], eDataChi1Hi [10];
    Float_t DataChi2 [10], eDataChi2Lo [10], eDataChi2Hi [10];
    Float_t a;
    int nPoint;
    
    for(int ii=0; ii<NPoints; ii++)
    {
        fscanf(fp, "%i %e %e %e",&nPoint,&a,&a,&a);
        fscanf(fp, "%e %e %e", &DataChi0[ii],&eDataChi0Lo[ii],&eDataChi0Hi[ii]);
        fscanf(fp, "%e %e %e", &DataChi1[ii],&eDataChi1Lo[ii],&eDataChi1Hi[ii]);
        fscanf(fp, "%e %e %e", &DataChi2[ii],&eDataChi2Lo[ii],&eDataChi2Hi[ii]);
        x[ii]=(BinsEdges[ii+1]+BinsEdges[ii])/2;
        x0[ii]=x[ii]-0.2;
        x2[ii]=x[ii]+0.2;
        x1[ii]=x[ii];
        exDr[ii]=x[ii]-BinsEdges[ii];
        ex[ii]=0;
    }
    fclose(fp);

    
    int nSysts = 11;
    int Systs[20] = {1,2,3,4,5,6,9,11,15,16,17};
    char txtfname [200];
    
    Float_t DataSysChi0 [20][10], eDataSysChi0Lo [20][10], eDataSysChi0Hi [20][10];
    Float_t DataSysChi1 [20][10], eDataSysChi1Lo [20][10], eDataSysChi1Hi [20][10];
    Float_t DataSysChi2 [20][10], eDataSysChi2Lo [20][10], eDataSysChi2Hi [20][10];
    
    for(int i=0;i<nSysts;i++)
    {
        sprintf(txtfname,"%s/paramsPT_%i.txt",directory,Systs[i]);
        cout<<"A  "<<Systs[i]<<endl;
        fp = fopen(txtfname,"r");
        for(int ii=0; ii<NPoints; ii++)
        {
            fscanf(fp, "%i %e %e %e",&nPoint,&a,&a,&a);
            fscanf(fp, "%e %e %e", &DataSysChi0[i][ii],&eDataSysChi0Lo[i][ii],&eDataSysChi0Hi[i][ii]);
            fscanf(fp, "%e %e %e", &DataSysChi1[i][ii],&eDataSysChi1Lo[i][ii],&eDataSysChi1Hi[i][ii]);
            fscanf(fp, "%e %e %e", &DataSysChi2[i][ii],&eDataSysChi2Lo[i][ii],&eDataSysChi2Hi[i][ii]);
        }
        fclose(fp);
    }
    
    FILE* fpp = fopen("paramsSysChic.txt","w");
    Float_t Sys0, Sys1, Sys2;
    Float_t TotalSys0 = 0,TotalSys1 = 0,TotalSys2 = 0;
    
    Float_t SysFract=0;
    if(errType==2)SysFract=1.;
    
    
    
    
    Float_t eSysRelChi0[20], eSysRelChi1[20], eSysRelChi2[20];
    for(int ii=0; ii<NPoints; ii++)
    {
        
        TotalSys0 = 0;
        TotalSys1 = 0;
        TotalSys2 = 0;
        fprintf(fpp,"%i         %f  %f  %f %f  %f  %f  %f  %f  %f\n",ii,
                                                  DataChi0[ii],eDataChi0Lo[ii],eDataChi0Hi[ii],
                                                  DataChi1[ii],eDataChi1Lo[ii],eDataChi1Hi[ii],
                                                  DataChi2[ii],eDataChi2Lo[ii],eDataChi2Hi[ii]);
        for(int i=0;i<nSysts;i++)
        {
            Sys0 = DataSysChi0[i][ii]-DataChi0[ii];
            Sys1 = DataSysChi1[i][ii]-DataChi1[ii];
            Sys2 = DataSysChi2[i][ii]-DataChi2[ii];
            
            
            TotalSys0+=Sys0*Sys0;
            TotalSys1+=Sys1*Sys1;
            TotalSys2+=Sys2*Sys2;
            
            fprintf(fpp,"       %i  %f  %f  %f  %f  %f  %f  %f  %f  %f\n",Systs[i],
                            Sys0,eDataSysChi0Lo[i][ii],eDataSysChi0Hi[i][ii],
                            Sys1,eDataSysChi1Lo[i][ii],eDataSysChi1Hi[i][ii],
                            Sys2,eDataSysChi2Lo[i][ii],eDataSysChi2Hi[i][ii]);
        }
        eDataChi0Lo[ii]=TMath::Sqrt(eDataChi0Lo[ii]*eDataChi0Lo[ii]+SysFract*TotalSys0);
        eDataChi0Hi[ii]=TMath::Sqrt(eDataChi0Hi[ii]*eDataChi0Hi[ii]+SysFract*TotalSys0);
        
        eDataChi1Lo[ii]=TMath::Sqrt(eDataChi1Lo[ii]*eDataChi1Lo[ii]+SysFract*TotalSys1);
        eDataChi1Hi[ii]=TMath::Sqrt(eDataChi1Hi[ii]*eDataChi1Hi[ii]+SysFract*TotalSys1);
        
        eDataChi2Lo[ii]=TMath::Sqrt(eDataChi2Lo[ii]*eDataChi2Lo[ii]+SysFract*TotalSys2);
        eDataChi2Hi[ii]=TMath::Sqrt(eDataChi2Hi[ii]*eDataChi2Hi[ii]+SysFract*TotalSys2);
        fprintf(fpp,"       Tot:  %f            %f              %f \n",
                TMath::Sqrt(TotalSys0),
                TMath::Sqrt(TotalSys1),
                TMath::Sqrt(TotalSys2));
        
        
        
        eSysRelChi0[ii]=TMath::Sqrt(TotalSys0)/DataChi0[ii];
        eSysRelChi1[ii]=TMath::Sqrt(TotalSys1)/DataChi1[ii];
        eSysRelChi2[ii]=TMath::Sqrt(TotalSys2)/DataChi2[ii];
    }
    fclose(fpp);
    
    
    

    
    TChain * DecayTreeChi0MCAll=new TChain("DecayTree");
    DecayTreeChi0MCAll->Add("../../MC/CAL_0/Chic0/AllMC_Reduced.root");
    TTree* DecayTreeChi0MC = DecayTreeChi0MCAll->CopyTree("Jpsi_PT>0 && Jpsi_PT<25000");
    Int_t NEnMC=DecayTreeChi0MC->GetEntries();
    Double_t Branch_Value2;
    DecayTreeChi0MC->SetBranchAddress("Jpsi_PT",&Branch_Value2);
    TH1D *histMCChi0 = new TH1D("chic0 MC","chic0 MC", NPoints, BinsEdges);
    for (Int_t i=0; i<NEnMC; i++)
    {
        DecayTreeChi0MC->GetEntry(i);
        histMCChi0->Fill(Branch_Value2/1000);
    }
    TChain * DecayTreeChi0GenAll=new TChain("MCDecayTreeTuple/MCDecayTree");
    DecayTreeChi0GenAll->Add("../../MC/GenSpectr/Chic0/res.root");
    TTree* DecayTreeChi0Gen = DecayTreeChi0GenAll->CopyTree("chi_c0_1P_TRUEPT>3000 && chi_c0_1P_TRUEPT<25000");
    Int_t NEnGen=DecayTreeChi0Gen->GetEntries();
    Double_t Branch_Value3;
    DecayTreeChi0Gen->SetBranchAddress("chi_c0_1P_TRUEPT",&Branch_Value3);
    TH1D *histChic0Gen = new TH1D("Gen0","Gen0", NPoints, BinsEdges);
    for (Int_t i=0; i<NEnGen; i++)
    {
        DecayTreeChi0Gen->GetEntry(i);
        histChic0Gen->Fill(Branch_Value3/1000);
    }
    Float_t NChic0MCSelected = histMCChi0->Integral();
    Float_t NChic0MCGen = histChic0Gen->Integral();
    
    
    TChain * DecayTreeChi1MCAll=new TChain("DecayTree");
    DecayTreeChi1MCAll->Add("../../MC/CAL_0/Chic1/AllMC_Reduced.root");
    TTree* DecayTreeChi1MC = DecayTreeChi1MCAll->CopyTree("Jpsi_PT>0 && Jpsi_PT<25000");
    Int_t NEnMC=DecayTreeChi1MC->GetEntries();
    Double_t Branch_Value2;
    DecayTreeChi1MC->SetBranchAddress("Jpsi_PT",&Branch_Value2);
    TH1D *histMCChi1 = new TH1D("chic1 MC","chic1 MC", NPoints, BinsEdges);
    for (Int_t i=0; i<NEnMC; i++)
    {
        DecayTreeChi1MC->GetEntry(i);
        histMCChi1->Fill(Branch_Value2/1000.);
    }
    TChain * DecayTreeChi1GenAll=new TChain("MCDecayTreeTuple/MCDecayTree");
    DecayTreeChi1GenAll->Add("../../MC/GenSpectr/Chic1/res.root");
    TTree* DecayTreeChi1Gen = DecayTreeChi1GenAll->CopyTree("chi_c1_1P_TRUEPT>3000 && chi_c1_1P_TRUEPT<25000");
    Int_t NEnGen=DecayTreeChi1Gen->GetEntries();
    Double_t Branch_Value3;
    DecayTreeChi1Gen->SetBranchAddress("chi_c1_1P_TRUEPT",&Branch_Value3);
    TH1D *histChic1Gen = new TH1D("Gen1","Gen1", NPoints, BinsEdges);
    for (Int_t i=0; i<NEnGen; i++)
    {
        DecayTreeChi1Gen->GetEntry(i);
        histChic1Gen->Fill(Branch_Value3/1000.);
    }
    Float_t NChic1MCSelected = histMCChi1->Integral();
    Float_t NChic1MCGen = histChic1Gen->Integral();
    
    
    TChain * DecayTreeChi2MCAll=new TChain("DecayTree");
    DecayTreeChi2MCAll->Add("../../MC/CAL_0/Chic2/AllMC_Reduced.root");
    TTree* DecayTreeChi2MC = DecayTreeChi2MCAll->CopyTree("Jpsi_PT>0 && Jpsi_PT<25000");
    Int_t NEnMC=DecayTreeChi2MC->GetEntries();
    Double_t Branch_Value2;
    DecayTreeChi2MC->SetBranchAddress("Jpsi_PT",&Branch_Value2);
    TH1D *histMCChi2 = new TH1D("chic2 MC","chic2 MC", NPoints, BinsEdges);
    for (Int_t i=0; i<NEnMC; i++)
    {
        DecayTreeChi2MC->GetEntry(i);
        histMCChi2->Fill(Branch_Value2/1000);
    }
    TChain * DecayTreeChi2GenAll=new TChain("MCDecayTreeTuple/MCDecayTree");
    DecayTreeChi2GenAll->Add("../../MC/GenSpectr/Chic2/res.root");
    TTree* DecayTreeChi2Gen = DecayTreeChi2GenAll->CopyTree("chi_c2_1P_TRUEPT>3000 && chi_c2_1P_TRUEPT<25000");
    Int_t NEnGen=DecayTreeChi2Gen->GetEntries();
    Double_t Branch_Value3;
    DecayTreeChi2Gen->SetBranchAddress("chi_c2_1P_TRUEPT",&Branch_Value3);
    TH1D *histChic2Gen = new TH1D("Gen2","Gen2", NPoints, BinsEdges);
    for (Int_t i=0; i<NEnGen; i++)
    {
        DecayTreeChi2Gen->GetEntry(i);
        histChic2Gen->Fill(Branch_Value3/1000);
    }
    Float_t NChic2MCSelected = histMCChi2->Integral();
    Float_t NChic2MCGen = histChic2Gen->Integral();
    
    
    
    //    histMCChi0->Scale(1./histMCChi0->Integral());
    //    histChic0Gen->Scale(1./histChic0Gen->Integral());
    
    TCanvas* canvC = new TCanvas("canvC", "canvC", 800, 700);
    histChic0Gen->SetLineColor(kRed);
    histChic0Gen->Draw();
    histMCChi0->Draw("same");
    
    //    float DataChi0 [10], eDataChi0Lo [10], eDataChi0Hi [10];
    Float_t Chic0Res[10], eChic0ResLo [10], eChic0ResHi [10], epsChic0[10],eBRChic0ResLo [10], eBRChic0ResHi [10],eMCChic0Res[10];
    TH1D *histChic0Eps = new TH1D("histChic0Eps","histChic0Eps", NPoints, BinsEdges);
    
    Float_t BRErrorRelLo = 0;
    Float_t MCErrorRel = 0;
    for(int ii=0; ii<NPoints; ii++)
    {
        epsChic0[ii]=Float_t(Float_t(histMCChi0->GetBinContent(ii+1))/Float_t(histChic0Gen->GetBinContent(ii+1))/NChi0Generated*NChic0MCGen);
        histChic0Eps->SetBinContent(ii+1,epsChic0[ii]);
        Chic0Res[ii]=DataChi0[ii]/epsChic0[ii]/Luminosity/(BinsEdges[ii+1]-BinsEdges[ii])/BRChi0phiphi/BRKK/BRKK*1e-6;


        BRErrorRelLo = TMath::Sqrt(1./(histMCChi0->GetBinContent(ii+1)) + (erBRChi0phiphi/BRChi0phiphi)**2 + (2*erBRKK/BRKK)**2 + 1./histChic0Gen->GetBinContent(ii+1));
        MCErrorRel = TMath::Sqrt(1./(histMCChi0->GetBinContent(ii+1)) + 1./histChic0Gen->GetBinContent(ii+1));
        

        Float_t ErrorRelHi = eDataChi0Hi[ii]/DataChi0[ii];
        Float_t ErrorRelLo = eDataChi0Lo[ii]/DataChi0[ii];
        
        eChic0ResHi[ii] = Chic0Res[ii]*ErrorRelHi;
        eChic0ResLo[ii] = Chic0Res[ii]*ErrorRelLo;
        
        eBRChic0ResLo[ii] = Chic0Res[ii]*BRErrorRelLo;
        eMCChic0Res[ii] = Chic0Res[ii]*MCErrorRel;
        
    }
    
    
    Float_t Chic1Res[10], eChic1ResLo [10], eChic1ResHi [10], epsChic1[10],eBRChic1ResLo [10], eBRChic1ResHi [10], eMCChic1Res[10];

    TH1D *histChic1Eps = new TH1D("histChic1Eps","histChic1Eps", NPoints, BinsEdges);
    for(int ii=0; ii<NPoints; ii++)
    {
        epsChic1[ii]=Float_t(Float_t(histMCChi1->GetBinContent(ii+1))/Float_t(histChic1Gen->GetBinContent(ii+1))/NChi1Generated*NChic1MCGen);
        histChic1Eps->SetBinContent(ii+1,epsChic1[ii]);
        
        Chic1Res[ii]=DataChi1[ii]/epsChic1[ii]/Luminosity/(BinsEdges[ii+1]-BinsEdges[ii])/BRChi1phiphi/BRKK/BRKK*1e-6;
        


        BRErrorRelLo = TMath::Sqrt(1./(histMCChi1->GetBinContent(ii+1)) + (erBRChi1phiphi/BRChi1phiphi)**2 + (2*erBRKK/BRKK)**2 + 1/histChic1Gen->GetBinContent(ii+1));
        
        MCErrorRel = TMath::Sqrt(1./(histMCChi1->GetBinContent(ii+1)) + 1./histChic1Gen->GetBinContent(ii+1));

        Float_t ErrorRelHi = eDataChi1Hi[ii]/DataChi1[ii];
        Float_t ErrorRelLo = eDataChi1Lo[ii]/DataChi1[ii];
        
        eChic1ResHi[ii] = Chic1Res[ii]*ErrorRelHi;
        eChic1ResLo[ii] = Chic1Res[ii]*ErrorRelLo;
        
        eBRChic1ResLo[ii] = Chic1Res[ii]*BRErrorRelLo;
        eMCChic1Res[ii] = Chic1Res[ii]*MCErrorRel;
        
    }
    
    
    Float_t Chic2Res[10], eChic2ResLo [10], eChic2ResHi [10], epsChic2[10], eBRChic2ResLo [10], eBRChic2ResHi [10],  eMCChic2Res [10];
    TH1D *histChic2Eps = new TH1D("histChic2Eps","histChic2Eps", NPoints, BinsEdges);
    for(int ii=0; ii<NPoints; ii++)
    {
        epsChic2[ii]=Float_t(Float_t(histMCChi2->GetBinContent(ii+1))/Float_t(histChic2Gen->GetBinContent(ii+1))/NChi2Generated*NChic2MCGen);
        histChic2Eps->SetBinContent(ii+1,epsChic2[ii]);
        
        Chic2Res[ii]=DataChi2[ii]/epsChic2[ii]/Luminosity/(BinsEdges[ii+1]-BinsEdges[ii])/BRChi2phiphi/BRKK/BRKK*1e-6;
        

        BRErrorRelLo = TMath::Sqrt(1./(histMCChi2->GetBinContent(ii+1)) + (erBRChi2phiphi/BRChi2phiphi)**2 + (2*erBRKK/BRKK)**2 +  1/histChic2Gen->GetBinContent(ii+1));

        MCErrorRel = TMath::Sqrt(1./(histMCChi2->GetBinContent(ii+1)) + 1./histChic2Gen->GetBinContent(ii+1));
        
        Float_t ErrorRelHi = eDataChi2Hi[ii]/DataChi2[ii];
        Float_t ErrorRelLo = eDataChi2Lo[ii]/DataChi2[ii];
        
        eChic2ResHi[ii] = Chic2Res[ii]*ErrorRelHi;
        eChic2ResLo[ii] = Chic2Res[ii]*ErrorRelLo;
        

        eBRChic2ResLo[ii] = Chic2Res[ii]*BRErrorRelLo;
        eMCChic2Res[ii] = Chic2Res[ii]*MCErrorRel;
        
    }
    Float_t ChicResAv[10], eChicResAvLo [10], ChicResAvHi [10], epsChicResAv[10];
    TH1D *histChicAvEps = new TH1D("histChicAvEps","histChicAvEps", NPoints, BinsEdges);
    for(int ii=0; ii<NPoints; ii++)
    {
        epsChicResAv[ii]=Float_t(Float_t(histMCChi2->GetBinContent(ii+1)+histMCChi1->GetBinContent(ii+1)+histMCChi0->GetBinContent(ii+1))
                                 /Float_t(histChic2Gen->GetBinContent(ii+1)+histChic1Gen->GetBinContent(ii+1)+histChic0Gen->GetBinContent(ii+1))
                                 /(NChi2Generated+NChi1Generated+NChi0Generated)*(NChic2MCGen+NChic1MCGen+NChic0MCGen));
        histChicAvEps->SetBinContent(ii+1,epsChicResAv[ii]);
    }
    
    
    TCanvas* canvEps = new TCanvas("canvEps", "canvEps", 800, 700);
    
    histChic1Eps->SetLineColor(kRed);
    histChic1Eps->Draw(/*"same"*/);
    histChic0Eps->SetLineColor(kBlue);
    histChic0Eps->Draw("same");
    histChic2Eps->SetLineColor(kBlack);
    histChic2Eps->Draw("same");
    leg = new TLegend(0.5,0.65,0.9,0.9);
    leg->AddEntry(histChic0Eps,"#chi_{c0}","l");
    leg->AddEntry(histChic1Eps,"#chi_{c1}","l");
    leg->AddEntry(histChic2Eps,"#chi_{c2}","l");
    leg->Draw();
    histChicAvEps->Draw();
    
    //     TCanvas* canvD = new TCanvas("canvD", "canvD", 800, 700);
    //     canvD->SetLogy();
    
    
    


    sprintf(fname,"%s/Chic0Out.txt",directory);
    FILE *fp = fopen(fname, "w");
    fprintf(fp,"%i\n",NPoints);
    for(int i=0;i<NPoints;i++)
        fprintf(fp,"%f %f %f %f %f %f \n",x[i],Chic0Res[i],exDr[i],exDr[i],eChic0ResLo[i],eChic0ResHi[i]);
    fclose(fp);
    
    sprintf(fname,"%s/Chic1Out.txt",directory);
    FILE *fp = fopen(fname, "w");
    fprintf(fp,"%i\n",NPoints);
    for(int i=0;i<NPoints;i++)
        fprintf(fp,"%f %f %f %f %f %f \n",x[i],Chic1Res[i],exDr[i],exDr[i],eChic1ResLo[i],eChic1ResHi[i]);
    fclose(fp);
    
    sprintf(fname,"%s/Chic2Out.txt",directory);
    FILE *fp = fopen(fname, "w");
    fprintf(fp,"%i\n",NPoints);
    for(int i=0;i<NPoints;i++)
        fprintf(fp,"%f %f %f %f %f %f \n",x[i],Chic2Res[i],exDr[i],exDr[i],eChic2ResLo[i],eChic2ResHi[i]);
    fclose(fp);
    
    
    
    
    
    
    
    
    sprintf(fname,"%s/Chic0Out_Expl.txt",directory);
    FILE *fp = fopen(fname, "w");
    fprintf(fp,"%i\n",NPoints);
    for(int i=0;i<NPoints;i++)
        fprintf(fp,"%f %f %f %f %f %f %f %f %f\n",x[i],Chic0Res[i],exDr[i],exDr[i],eChic0ResLo[i],eChic0ResHi[i],eSysRelChi0[i]*Chic0Res[i],eBRChic0ResLo[i],eMCChic0Res[i]);
    fclose(fp);
    
    sprintf(fname,"%s/Chic1Out_Expl.txt",directory);
    FILE *fp = fopen(fname, "w");
    fprintf(fp,"%i\n",NPoints);
    for(int i=0;i<NPoints;i++)
        fprintf(fp,"%f %f %f %f %f %f %f %f %f\n",x[i],Chic1Res[i],exDr[i],exDr[i],eChic1ResLo[i],eChic1ResHi[i],eSysRelChi1[i]*Chic1Res[i],eBRChic1ResLo[i],eMCChic1Res[i]);
    fclose(fp);
    
    sprintf(fname,"%s/Chic2Out_Expl.txt",directory);
    FILE *fp = fopen(fname, "w");
    fprintf(fp,"%i\n",NPoints);
    for(int i=0;i<NPoints;i++)
        fprintf(fp,"%f %f %f %f %f %f %f %f %f\n",x[i],Chic2Res[i],exDr[i],exDr[i],eChic2ResLo[i],eChic2ResHi[i],eSysRelChi2[i]*Chic2Res[i],eBRChic2ResLo[i],eMCChic2Res[i]);
    fclose(fp);
}


using namespace RooFit;
void fitSpectra(int PT_Type=0)
{
    gROOT->Reset();
    gROOT->ProcessLine(".x ../lhcbStyle.C");

    Float_t minPT = 1.8;
    Float_t maxPT = 19.2;
    Float_t binWidth = 10.;
    
    
    float chi2s [4];
    float slopes [4];
    float slopesErr [4];
    float integrals[4];
    float integralERRS[4];
    
    
    switch(PT_Type)
    {
        case 0:
            Float_t BinsEdgesEtac[10]={3,5,7,9,11,13,15,19};
            int istart = 0;
            int iend = 1;
            break;
        case 1:
            Float_t BinsEdgesChic[10]={2,8,12,19};
            int istart = 1;
            int iend = 4;
            break;
    }
    
    
    
    RooRealVar varPT("varPT", "varPT", minPT, maxPT, "MeV");
    RooRealVar varPTDr("varPTDr", "varPTDr", minPT-1, maxPT+1, "MeV");
    RooRealVar varPT2("varPT2", "varPT2", minPT, maxPT, "MeV");
    varPT.setRange("fitRange",minPT,maxPT);

    
    RooRealVar varData("varData","varData",0,5e5);
    RooRealVar varData2("varData2","varData2",0,5e5);
    
    
    RooRealVar varSlope("varSlope", "varSlope", 0.5,-1,1);
    RooRealVar varYield("varYield", "varYield",1e3,1e2,1e5);
    
    RooGenericPdf pdfExp("pdfExp","pdfExp","TMath::Exp(-@1*@0)",RooArgList(varSlope,varPT));
    RooExtendPdf pdfModel("pdfModel", "pdfModel", pdfExp, varYield);
    
    RooGenericPdf pdfExp2("pdfExp2","pdfExp2","TMath::Exp(-@1*@0)",RooArgList(varSlope,varPT2));
    RooExtendPdf pdfModel2("pdfModel2", "pdfModel2", pdfExp2, varYield);
    

    TCanvas* canvA = new TCanvas("canvA", "canvA", 1400, 560);
    gStyle->SetPadBorderMode(0);
    
    
    canvA->Divide(2,1);
    
    gStyle->SetPadBorderMode(0);
    gStyle->SetFrameBorderMode(0);
    
    string FileNames [4];

    
    if(PT_Type==1)leg = new TLegend(0.65,0.7,0.9,0.9);
    if(PT_Type==0)leg = new TLegend(0.65,0.86,0.9,0.9);
    
    
    char objname [200];
    for(int iyear=11;iyear<13;iyear++)
    {
        
        RooPlot* frame = varPT.frame(Title("PT"));
        RooPlot* frame2 = varPT2.frame(Title("PT2"));
        
        switch(iyear)
        {
            case 11:
                FileNames[0] = "etac11/EtacOut.txt";
//                FileNames[0] = "etac11_upto19/EtacOut.txt";
                FileNames[1] = "chic11SIM/Chic0Out.txt";
                FileNames[2] = "chic11SIM/Chic1Out.txt";
                FileNames[3] = "chic11SIM/Chic2Out.txt";
                canvA->cd(1);
                gPad->SetLogy();
                break;
            case 12:
                FileNames[0] = "etac12/EtacOut.txt";
//                FileNames[0] = "etac12_upto19/EtacOut.txt";
                FileNames[1] = "chic12SIM/Chic0Out.txt";
                FileNames[2] = "chic12SIM/Chic1Out.txt";
                FileNames[3] = "chic12SIM/Chic2Out.txt";
                canvA->cd(2);
                gPad->SetLogy();
                break;
                
        }
        
        
        int LineColor = 0;
        int LineStyle = 0;
        int LineWidth = 0;
        TBox* boxes [5][5];
        
        gStyle->SetLineStyleString(11,"15 10");
        gStyle->SetLineStyleString(12,"40 15");
        
        TLine* lines1 [5][5];
        TLine* lines2 [5][5];
        TLine* lines3 [5][5];
        TLine* lines4 [5][5];
        
        for(int ichic=istart;ichic<iend;ichic++)
        {
            
            sprintf(objname, "DataSetXYErrDR%i", ichic);
            
            RooDataSet DataSetXYErr("DataSetXYErr","DataSetXYErr",RooArgSet(varPT,varData),
                                    StoreAsymError(RooArgSet(varPT,varData)));
            RooDataSet DataSetXYErrDR(objname,objname,RooArgSet(varPT,varData),
                                      StoreAsymError(RooArgSet(varPT,varData)));
            RooDataSet DataSetYErr("DataSetYErr","DataSetYErr",RooArgSet(varPT2,varData2),
                                   StoreAsymError(RooArgSet(varData2)));
            
            Float_t xx[1000], yy[1000], ey1[1000], ey2[1000], ex1[1000], ex2[1000], ex[1000];
            FILE *fp = fopen(FileNames[ichic].c_str(), "r");
            
            int NPoints;
            fscanf(fp,"%i",&NPoints);

            
            
            for(int i=0;i<NPoints;i++)
            {
                fscanf(fp,"%f",&xx[i]);
                fscanf(fp,"%f",&yy[i]);
                fscanf(fp,"%f",&ex1[i]);
                fscanf(fp,"%f",&ex2[i]);
                fscanf(fp,"%f",&ey1[i]);
                fscanf(fp,"%f",&ey2[i]);
                varPT = xx[i];
                ex[i]=0;
                varPT.setAsymError(-ex1[i],ex2[i]);
                //		varPT.setAsymError(-ex[i],ex[i]);
                
                varPT2 =  xx[i];
                varPTDr = xx[i]+0.2*(ichic-1);
                printf("%f %f %f %f %f %f\n",xx[i],yy[i],ex1[i],ex2[i],ey1[i],ey2[i]);
                
                varData = yy[i];
                varData.setAsymError(-ey1[i],ey2[i]);
                
                varData2 = yy[i];
                varData2.setAsymError(-ey1[i],ey2[i]);
                
                DataSetXYErr.add(RooArgSet(varPT,varData));
                
                
                Double_t xPoint = 0;
                
                
                
                switch (ichic) {
                    case 0:
                        xPoint = xx[i]-0.2;
                        LineColor = kBlack;
                        LineStyle = kSolid;
                        LineWidth = 2;
                        break;
                    case 1:
                        xPoint = xx[i]-0.2;
                        LineColor = kRed;
                        LineStyle = 12;
                        LineWidth = 2.;
                        break;
                    case 2:
                        xPoint = xx[i]+0.2;
                        LineColor = kBlue;
                        LineStyle = 11;
                        LineWidth = 2.;
                        break;
                    case 3:
                        xPoint = xx[i];
                        LineColor = kMagenta;
                        LineStyle = kSolid;
//                        LineStyle = kDashed;
                        LineWidth = 2.;
                        break;
                    default:
                        break;
                }
                varPT = xPoint;
                
                lines1[ichic][i] = new TLine(xPoint-ex1[i],yy[i]-ey1[i],xPoint+ex1[i],yy[i]-ey1[i]);
                lines2[ichic][i] = new TLine(xPoint-ex1[i],yy[i]-ey1[i],xPoint-ex1[i],yy[i]+ey1[i]);
                lines3[ichic][i] = new TLine(xPoint-ex1[i],yy[i]+ey2[i],xPoint+ex1[i],yy[i]+ey2[i]);
                lines4[ichic][i] = new TLine(xPoint+ex1[i],yy[i]-ey1[i],xPoint+ex1[i],yy[i]+ey1[i]);
                
                lines1[ichic][i]->SetLineColor(LineColor);
                lines1[ichic][i]->SetLineStyle(LineStyle);
                lines2[ichic][i]->SetLineColor(LineColor);
                lines2[ichic][i]->SetLineStyle(LineStyle);
                lines3[ichic][i]->SetLineColor(LineColor);
                lines3[ichic][i]->SetLineStyle(LineStyle);
                lines4[ichic][i]->SetLineColor(LineColor);
                lines4[ichic][i]->SetLineStyle(LineStyle);
                
                lines1[ichic][i]->SetLineWidth(LineWidth);
                lines2[ichic][i]->SetLineWidth(LineWidth);
                lines3[ichic][i]->SetLineWidth(LineWidth);
                lines4[ichic][i]->SetLineWidth(LineWidth);

                
                
                boxes[ichic][i] = new TBox(xPoint-ex1[i],yy[i]-ey1[i],xPoint+ex1[i],yy[i]+ey2[i]);
                boxes[ichic][i]->SetLineColor(0);
                boxes[ichic][i]->SetLineStyle(0);
                boxes[ichic][i]->SetFillStyle(0);
                
                if(LineColor==kRed)
                {
                    boxes[ichic][i]->SetFillStyle(1001);
                    boxes[ichic][i]->SetFillColor(kRed-7);
                }
                
                DataSetXYErrDR.add(RooArgSet(varPT,varData));
                DataSetYErr.add(RooArgSet(varPT2,varData2));
            }
            fclose(fp);
            
            RooBinning abins(2.,19);
            for(int i=0;i<(NPoints-1);i++)
                abins.addBoundary(BinsEdges[i+1]);
            
            
            TH1D *cGrHist = new TH1D("cGrHist","cGrHist", NPoints, BinsEdges);
            
            for(int ii=0;ii<NPoints;ii++)
            {
                cGrHist->SetBinContent(ii+1,yy[ii]);
                cGrHist->SetBinError(ii+1,0.5*(ey1[ii]+ey2[ii]));
            }
            TF1 *fitFunc = new TF1("fitFunc","[0]*exp(-[1]*x)", BinsEdges[0],BinsEdges[NPoints]);

            cGrHist->Fit(fitFunc,"RME");
            cGrHist->Fit(fitFunc,"RME");
            cGrHist->Fit(fitFunc,"IRME");
            
            varSlope.setVal(fitFunc->GetParameter(1));
            varYield.setVal(1.);

            float Slope = fitFunc->GetParameter(1);
            float SlopeErr = fitFunc->GetParError(1);
            float chi2  = fitFunc->GetChisquare()/fitFunc->GetNDF();
            float integral = fitFunc->Integral(BinsEdges[0],BinsEdges[NPoints]);
            float integralERR = fitFunc->IntegralError(BinsEdges[0],BinsEdges[NPoints]);

            varSlope.setConstant(kTRUE);
            varYield.setConstant(kTRUE);
            
            varPT.setBinning(abins);
            
            if(PT_Type==1)
            {
                DataSetXYErrDR.plotOnXY(frame,YVar(varData),LineColor(kWhite),FillColor(0),FillStyle(0),MarkerColor(kWhite),DrawOption("E5"),Name(objname));
                
                    for(int j=0;j<NPoints;j++)
                    {
                        if(LineColor==kRed)frame->addObject(boxes[ichic][j]);
                        frame->addObject(lines1[ichic][j],"l");
                        frame->addObject(lines2[ichic][j],"l");
                        frame->addObject(lines3[ichic][j],"l");
                        frame->addObject(lines4[ichic][j],"l");
                        
                    }
            }
            else
                DataSetXYErrDR.plotOnXY(frame,YVar(varData),LineColor(kBlack),FillColor(0),MarkerColor(kBlack),DrawOption("E5"),Name(objname));
            DataSetYErr.plotOnXY(frame2,YVar(varData2),LineColor(LineColor),MarkerColor(LineColor));
            
            
            
            //pdfModel.chi2FitTo(DataSetXYErr,YVar(varData),Strategy(2),PrintLevel(0),Save(true),Minos(true));
            TGraphAsymmErrors* cGr = new TGraphAsymmErrors(NPoints,xx,yy,ex,ex,ey1,ey2);
            cGr->SetMinimum(1e-1);


            //if(LineColor==kRed)LineWidth=2;
            pdfModel.plotOn(frame,//Normalization(pdfModel.expectedEvents(pdfModel.getVariables())),
                            Normalization(fitFunc->Integral(minPT,maxPT,fitFunc->GetParameters())),
                            LineColor(LineColor),LineWidth(LineWidth),LineStyle(LineStyle));
            pdfModel2.plotOn(frame2,//Normalization(pdfModel2.expectedEvents(pdfModel2.getVariables())),//Binning(abins),
                             Normalization(fitFunc->Integral(minPT,maxPT,fitFunc->GetParameters())),
                             LineColor(LineColor),LineWidth(LineWidth),LineStyle(LineStyle));
            
            if(iyear==12)
                leg->AddEntry(lines1[ichic][1]," ","F");
           
            
            
            chi2s[ichic] = chi2;
            slopes[ichic] = Slope;
            slopesErr[ichic] = SlopeErr;
            integrals[ichic] = integral;
            integralERRS[ichic] = integralERR;
            
            DataSetXYErr.Clear();
            DataSetYErr.Clear();
            
            delete cGrHist;
            delete cGr;
            delete fitFunc;
        }

        
        frame->SetMinimum(0.1);
        frame->SetMaximum(6e2);
        frame->GetXaxis()->SetLimits(minPT,maxPT);
        frame->Draw();
        
        
        if(iyear==12)leg->Draw();
        
        cout<<endl<<endl;
        for(int ich=istart;ich<iend;ich++)
            cout<<iyear<<" chic"<<ich-1<<": slope="<<slopes[ich]<<" +- "<<slopesErr[ich]<<"  chi2="<<chi2s[ich]<<"  "<<integrals[ich]<<"  "<<integralERRS[ich]<<endl;
        cout<<endl<<endl;
    }

//    TCanvas* canvB = new TCanvas("canvB", "canvB", 800, 700);
//    canvB->SetLogy();
//    frame2->SetMinimum(0.1);
//    // 	pdfModel2.plotOn(frame2,Normalization(pdfModel2.expectedEvents(pdfModel2.getVariables())),Binning(abins));
//    frame2->Draw();
}




void fitSpectraATLAS(int PT_Type=0, int Year=11)
{
    Float_t ATLASx [5] = {13.,15.,17.,20.,26.};
    Float_t errATLASx [5] = {1.,1.,1.,2.,4.};
    
    
    Float_t ATLASChic1 [5] = {41.6,23.0,9.62,5.22,2.08};
    Float_t errStatATLASChic1 [5] = {5.7,2.3,1.63,0.77,0.26};
    Float_t errSysATLASChic1 [5] = {5.1,2.8,1.18,0.64,0.26};
    Float_t BRChic1Jpsi =0.339;
    Float_t errBRChic1Jpsi =0.012;
    Float_t errATLASChic1 [5];
    
    Float_t ATLASChic2 [5] = {9.33,2.71,1.76,0.86,0.27};
    Float_t errStatATLASChic2 [5] = {4.23,1.77,0.91,0.46,0.17};
    Float_t errSysATLASChic2 [5] = {1.36,0.39,0.25,0.12,0.04};
    Float_t BRChic2Jpsi =0.192;
    Float_t errBRChic2Jpsi =0.007;
    Float_t errATLASChic2 [5];
    
    Float_t BRJpsiMuMu = 0.05961;
    
    Float_t stat = 0;
    Float_t syst = 0;
    Float_t data = 0;
    Float_t relErr = 0;
    
    Float_t rapCorr = 3.56;
    for(int i=0;i<5;i++)
    {
        stat = errStatATLASChic1[i];
        syst = errSysATLASChic1[i];
        data = ATLASChic1[i];
        relErr = TMath::Sqrt((stat/data)**2+(syst/data)**2+(errBRChic1Jpsi/BRChic1Jpsi)**2);
        ATLASChic1[i] = rapCorr*ATLASChic1[i]/BRChic1Jpsi/1000./BRJpsiMuMu;
        errATLASChic1[i] = ATLASChic1[i]*relErr;
        
        
        stat = errStatATLASChic2[i];
        syst = errSysATLASChic2[i];
        data = ATLASChic2[i];
        relErr = TMath::Sqrt((stat/data)**2+(syst/data)**2+(errBRChic2Jpsi/BRChic2Jpsi)**2);
        ATLASChic2[i] = rapCorr*ATLASChic2[i]/BRChic2Jpsi/1000./BRJpsiMuMu;
        errATLASChic2[i] = ATLASChic2[i]*relErr;
        
    }
    
    
    Float_t minPT = 2;
    Float_t maxPT = 19;
    Float_t binWidth = 10.;
    
    
    string FileNames [3] = {"Chic0Out.txt","Chic1Out.txt","Chic2Out.txt"};
    
    
    TLegend *leg = new TLegend(0.65,0.73,0.75,0.87);
    leg->SetFillColor(kWhite);
    leg->SetLineColor(kWhite);
    
    switch(Year)
    {
            
        case 12:
            Float_t BinsEdges[10]={2,7.5,11,19};
            break;
        case 11:
            Float_t BinsEdges[10]={2,7.5,11,19};
            break;
    }
    
    
    int ichic=PT_Type;
    
    
    Float_t xx[1000], yy[1000], ey1[1000], ey2[1000], xx[1000], yy[1000], ex1[1000], ex2[1000], ex[1000];
    FILE *fp = fopen(FileNames[ichic].c_str(), "r");
    
    int NPoints;
    fscanf(fp,"%i",&NPoints);
    
    for(int i=0;i<NPoints;i++)
    {
        fscanf(fp,"%f",&xx[i]);
        fscanf(fp,"%f",&yy[i]);
        fscanf(fp,"%f",&ex1[i]);
        fscanf(fp,"%f",&ex2[i]);
        fscanf(fp,"%f",&ey1[i]);
        fscanf(fp,"%f",&ey2[i]);
        
        ey1[i]=ey1[i];
        ey2[i]=ey2[i];
        
        ex[i]=0;
        
        printf("%f %f %f %f %f %f\n",xx[i],yy[i],ex1[i],ex2[i],ey1[i],ey2[i]);
    }
    fclose(fp);
    
    
    TGraphAsymmErrors* cGr = new TGraphAsymmErrors(NPoints,xx,yy,ex1,ex2,ey1,ey2);
    switch(PT_Type)
    {
        case 1:
            TGraphErrors* ATLASGr = new TGraphErrors(5,ATLASx,ATLASChic1,errATLASx,errATLASChic1);
            break;
        case 2:
            TGraphErrors* ATLASGr = new TGraphErrors(5,ATLASx,ATLASChic2,errATLASx,errATLASChic2);
            break;
    }
    
    cGr->SetMinimum(1e-2);
    cGr->GetXaxis()->SetLimits(0.,30.);
    ATLASGr->SetLineColor(kRed);
    ATLASGr->GetXaxis()->SetLimits(0.,30.);
    
    TCanvas* canvC = new TCanvas("canvC", "canvC", 800, 700);
    canvC->SetLogy();
    cGr->DrawClone("AP");
    ATLASGr->DrawClone("P");
    //canvC->Update();
}