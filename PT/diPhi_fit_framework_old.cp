

using namespace RooFit;
using namespace RooStats;

//fitType:
//0 - Absolute
//1 - ratioToEtac
//2 - DifRatio
//3 - PT
//4 - relativePT


//SysType:
//0 - Base
//1 - Fix M(Chic) to PDG
//2 - MC Sigma For Etac
//3 - Parabolic BG
//4 - 1 Gaussian
//5 - add X's to fit and
//6 - MC Sigma in 2D
//7 - splot
//8 - R=0.5
//9 - R=3.0
//10 - GammaEtac - dGamma
//11 - GammaEtac + dGamma
//12 - Exp in 2D
//13 - Narrow region
//14 - Shifted
//15 - chic region
//16 - A1
//17 - A2
//18 - add Jpsi peak
//19 - fit w/o Jpsi region
	

void AddModel(int fitType, int SysType, RooWorkspace* ws)
{
	Float_t etacMass =	2983.6; 
	Float_t chi0Mass =	3414.75; 
	Float_t chi1Mass =	3510.66; 
	Float_t hcMass   = 	3525.38; 
	Float_t chi2Mass =	3556.2; 
	Float_t etac2Mass =	3639.4; 
	Float_t x3872Mass = 	3871.69; 
	Float_t x3915Mass = 	3918.4; 
	Float_t x3927Mass = 	3927.2;
    
    Float_t jpsi_mass =3096.97;
	
	Float_t etacMassError =	0.7; 
	Float_t chi0MassError =	0.31; 
	Float_t chi1MassError =	0.07; 
	Float_t hcMassError   = 0.11; 
	Float_t chi2MassError =	0.09; 
	Float_t etac2MassError =1.3; 
	Float_t x3872MassError = 0.17; 
	Float_t x3915MassError = 1.9; 
	Float_t x3927MassError = 2.6; 
	
	Float_t etacGamma =	32.2; 
	Float_t chi0Gamma =	10.5; 
	Float_t chi1Gamma =	0.84; 
	Float_t hcGamma   = 	0.7; 
	Float_t chi2Gamma =	1.93; 
	Float_t etac2Gamma =	11.3;

	Float_t x3915Gamma = 	20; 
	Float_t x3927Gamma = 	24; 	
	
	Float_t etacGammaError =	0.9; 
	Float_t chi0GammaError =	0.6; 
	Float_t chi1GammaError =	0.04; 
	Float_t hcGammaError   = 	0.4; 
	Float_t chi2GammaError =	0.11; 
	Float_t etac2GammaError =	3.1; 
	Float_t x3915GammaError = 	5; 
	Float_t x3927GammaError = 	6; 
	
	Float_t EtacSigmaN_MC = 6.311;

	Float_t SigmaPar = 0.214;
	
	Float_t etacMassBase = 2982.81;
	Float_t chi0MassBase = 3412.99;
	Float_t chi1MassBase = 3508.38;
	Float_t chi2MassBase = 3557.29;
	Float_t etac2MassBase = 3636.35;
	Float_t etacGammaBase = 31.35;
	
	switch(SysType)
	{
	  case 13:
	    Float_t minMass = 2800;
	    Float_t maxMass = 3700;
	    break;
	  case 14:
	    Float_t minMass = 2800-5;
	    Float_t maxMass = 3950-5;
	    break;
	  case 15:
	    Float_t minMass = 3150;
	    Float_t maxMass = 3950;
	    break;
      case 7:
        Float_t minMass = 2800;
        Float_t maxMass = 4000;
        break;
	  default:
	    Float_t minMass = 2800;
	    Float_t maxMass = 3950;
	    break;
	}

	Float_t binWidth = 10.;
	Int_t binN = int((maxMass-minMass)/binWidth);
	RooRealVar Jpsi_m_scaled("Jpsi_m_scaled", "Jpsi_m_scaled", minMass, maxMass, "MeV");
    //if(SysType==7)RooRealVar varNDiPhi_sw("varNDiPhi_sw", "varNDiPhi_sw", -50, 50);
    
	Jpsi_m_scaled.setBins(10000);
	
	Float_t Centre = (minMass+maxMass)/2;


	RooArgList listComp, listNorm;
	RooArgSet showParams;

	
	RooRealVar FourKMass("FourKMass", "FourKMass", 4*493.677);
	RooRealVar var2PhiMass("var2PhiMass", "var2PhiMass", 2038.9);
	RooRealVar varSigmaFactor("varSigmaFactor","varSigmaFactor",2.16);
	RooRealVar FitMin("FitMin","FitMin",minMass);
	
	
if(SysType==4)
	RooRealVar varNarrowFraction("varNarrowFraction","varNarrowFraction",1.);
else
	RooRealVar varNarrowFraction("varNarrowFraction","varNarrowFraction",0.87);

if(SysType!=15)
    RooRealVar ASig("ASig", "ASig", SigmaPar,0.1,0.3);
else
	RooRealVar ASig("ASig", "ASig", SigmaPar);

  
	switch(SysType)
	{
	  case 8:
	    RooRealVar radius("radius", "radius", 0.5);
	    break;
	  case 9:
	    RooRealVar radius("radius", "radius", 3.0);
	    break;
	  default:
	    RooRealVar radius("radius", "radius", 1.5);
	    break;
	}
  
	RooRealVar massa("massa", "massa", 1019.46, "MeV");
	RooRealVar massb("massb", "massb", 1019.46, "MeV");
	RooRealVar ConvCentre("ConvCentre","ConvCentre",0) ;	
	
	showParams.add(ASig);
	showParams.add(varNarrowFraction);


	
	
	//========== fitting model components:

if(fitType==3 || fitType==4)
{
   RooRealVar varEtacMass("M(#eta_{c})", "varEtacMass", etacMassBase);
   RooRealVar varEtacGamma("varEtacGamma", "varEtacGamma", etacGammaBase);
   RooRealVar varEtac2Mass("M(#eta_{c}(2S))", "varEtac2Mass", etac2MassBase);
   ASig.setVal(SigmaPar);
   ASig.setConstant(kTRUE);
   switch(SysType)
    {
      case 1:
	    RooRealVar varChi0Mass("M(#chi_{c0})", "varChi0Mass", chi0Mass);
	    RooRealVar varChi1Mass("M(#chi_{c1})", "varChi1Mass", chi1Mass);
	    RooRealVar varChi2Mass("M(#chi_{c2})", "varChi2Mass", chi2Mass);
	    break;
      default:
	    RooRealVar varChi0Mass("M(#chi_{c0})", "varChi0Mass", chi0MassBase);
	    RooRealVar varChi1Mass("M(#chi_{c1})", "varChi1Mass", chi1MassBase);
	    RooRealVar varChi2Mass("M(#chi_{c2})", "varChi2Mass", chi2MassBase);
	    break;
    }  
}
else
{
    RooRealVar varEtacMass("M(#eta_{c})", "varEtacMass", 2984.00, 2975, 2989);
    RooRealVar varEtacGamma("varEtacGamma", "varEtacGamma", 32.00, 10, 60);
    switch(SysType)
    {
      case 1:
	    RooRealVar varChi0Mass("M(#chi_{c0})", "varChi0Mass", chi0Mass);
	    RooRealVar varChi1Mass("M(#chi_{c1})", "varChi1Mass", chi1Mass);
	    RooRealVar varChi2Mass("M(#chi_{c2})", "varChi2Mass", chi2Mass);
	    RooRealVar varEtac2Mass("M(#eta_{c}(2S))", "varEtac2Mass", etac2Mass, etac2Mass-15.00, etac2Mass+5.00);
	    break;
      default:
	    if(fitType==2)
	    {
		  RooRealVar varChi0Mass("M(#chi_{c0})", "varChi0Mass", chi0Mass, chi0Mass-5.00, chi0Mass+5.00);
		  
		  RooRealVar vardMChi01 ("M(#chi_{1})-M(#chi_{0})","vardMChi01", chi1Mass-chi0Mass, chi1Mass-chi0Mass-7.00, chi1Mass-chi0Mass +7.00);
		  RooRealVar vardMChi02 ("M(#chi_{2})-M(#chi_{0})","vardMChi02", chi2Mass-chi0Mass, chi2Mass-chi0Mass-7.00, chi2Mass-chi0Mass +7.00);
		  RooRealVar vardMEtac12 ("M[#eta_{c}(2S)]-M[#eta_{c}(1S)]","vardMEtac12", etac2Mass-etacMass, etac2Mass-etacMass-15.00, etac2Mass-etacMass +10.00);	      
			
		  RooFormulaVar varChi1Mass("M(#chi_{c1})", "varChi1Mass", "@0+@1", RooArgList(varChi0Mass, vardMChi01));
		  RooFormulaVar varChi2Mass("M(#chi_{c2})", "varChi2Mass", "@0+@1", RooArgList(varChi0Mass, vardMChi02));
		  RooFormulaVar varEtac2Mass("M(#eta_{c}(2S))", "varEtac2Mass", "@0+@1", RooArgList(varEtacMass, vardMEtac12));	
	    }	
	    else
	    {
		  RooRealVar varChi0Mass("M(#chi_{c0})", "varChi0Mass", chi0Mass, chi0Mass-5.00, chi0Mass+5.00);
		  RooRealVar varChi1Mass("M(#chi_{c1})", "varChi1Mass", chi1Mass-5.00, chi1Mass+5.00);
		  RooRealVar varChi2Mass("M(#chi_{c2})", "varChi2Mass", chi2Mass-5.00, chi2Mass+5.00);
          RooRealVar varEtac2Mass("M(#eta_{c}(2S))", "varEtac2Mass", etac2Mass, etac2Mass-15.00, etac2Mass+5.00);
	    }
	    break;
    }
}





	
	RooRealVar varChi0Gamma("varChi0Gamma", "varChi0Gamma", chi0Gamma);	
	RooRealVar varChi1Gamma("varChi1Gamma", "varChi1Gamma", chi1Gamma);
	RooRealVar varChi2Gamma("varChi2Gamma", "varChi2Gamma", chi2Gamma);	
if(SysType==10)
  RooRealVar varEtac2Gamma("#Gamma(#eta_{c}(2S))", "varEtac2Gamma", etac2Gamma-etac2GammaError);
else
    if(SysType==11)  
	RooRealVar varEtac2Gamma("#Gamma(#eta_{c}(2S))", "varEtac2Gamma", etac2Gamma+etac2GammaError);
    else
	RooRealVar varEtac2Gamma("#Gamma(#eta_{c}(2S))", "varEtac2Gamma", etac2Gamma,0,50);
    
    
    
    
	
if(SysType==2) 	
	RooRealVar varEtacSigmaN("varEtacSigmaN", "varEtacSigmaN", EtacSigmaN_MC); 
else
	RooFormulaVar varEtacSigmaN("varEtacSigmaN", "varEtacSigmaN", "sqrt(@1-@2)*(@0)", RooArgList(ASig, varEtacMass, FourKMass));

	RooFormulaVar varChi0SigmaN("varChi0SigmaN", "varChi0SigmaN", "sqrt(@1-@2)*(@0)", RooArgList(ASig, varChi0Mass, FourKMass));
	RooFormulaVar varChi1SigmaN("varChi1SigmaN", "varChi1SigmaN", "sqrt(@1-@2)*(@0)", RooArgList(ASig, varChi1Mass, FourKMass));
	RooFormulaVar varChi2SigmaN("varChi2SigmaN", "varChi2SigmaN", "sqrt(@1-@2)*(@0)", RooArgList(ASig, varChi2Mass, FourKMass));
	RooFormulaVar varEtac2SigmaN("varEtac2SigmaN", "varEtac2SigmaN", "sqrt(@1-@2)*(@0)", RooArgList(ASig, varEtac2Mass, FourKMass));

	RooFormulaVar varEtacSigmaW("varEtacSigmaW", "varEtacSigmaW", "@0*@1", RooArgList(varSigmaFactor, varEtacSigmaN));
	RooFormulaVar varChi0SigmaW("varChi0SigmaW", "varChi0SigmaW", "@0*@1", RooArgList(varSigmaFactor, varChi0SigmaN));
	RooFormulaVar varChi1SigmaW("varChi1SigmaW", "varChi1SigmaW", "@0*@1", RooArgList(varSigmaFactor, varChi1SigmaN));
	RooFormulaVar varChi2SigmaW("varChi2SigmaW", "varChi2SigmaW", "@0*@1", RooArgList(varSigmaFactor, varChi2SigmaN));
	RooFormulaVar varEtac2SigmaW("varEtac2SigmaW", "varEtac2SigmaW", "@0*@1", RooArgList(varSigmaFactor, varEtac2SigmaN));

	
	
	RooRealVar varEtacNumber("NEta", "varEtacNumber", 6800, 1e1, 1e5);
    
    

	
switch(fitType)
{
   case 1:
	RooRealVar varChi0toEtacRatio("NChi0/N(#eta_{c})","varChi0toEtacRatio",0.139, 0 ,0.3);
	    RooFormulaVar varChi0Number("NChi0", "varChi0Number", "@0*@1", RooArgList(varEtacNumber, varChi0toEtacRatio));
	RooRealVar varChi1toEtacRatio("NChi1/N(#eta_{c})","varChi1toEtacRatio",0.070, 0 ,0.3);
	    RooFormulaVar varChi1Number("NChi1", "varChi1Number", "@0*@1", RooArgList(varEtacNumber, varChi1toEtacRatio));	
	RooRealVar varChi2toEtacRatio("NChi2/N(#eta_{c})","varChi2toEtacRatio",0.090, 0 ,0.3);
	    RooFormulaVar varChi2Number("NChi2", "varChi2Number", "@0*@1", RooArgList(varEtacNumber, varChi2toEtacRatio));	
	RooRealVar varEtac2toEtac1Ratio("N(#eta_{c}(2S)/N(#eta_{c}(1S)","varEtac2toEtac1Ratio",0.059, 0,0.2);
	    RooFormulaVar varEtac2Number("NEta2", "varEtac2Number", "@0*@1", RooArgList(varEtac2toEtac1Ratio, varEtacNumber));
	break;
   case 2:
	RooRealVar varChi0toEtacRatio("NChi0/N(#eta_{c})","varChi0toEtacRatio",0.139, 0 ,0.3);
	    RooFormulaVar varChi0Number("NChi0", "varChi0Number", "@0*@1", RooArgList(varEtacNumber, varChi0toEtacRatio));
	RooRealVar varChi1toChi0Ratio("NChi1/NChi0","varChi1toChi0Ratio",0.576, 0.1 ,1.0);
	    RooFormulaVar varChi1Number("NChi1", "varChi1Number", "@0*@1", RooArgList(varChi0Number, varChi1toChi0Ratio));
	RooRealVar varChi2toChi0Ratio("NChi2/NChi0","varChi2toChi0Ratio",0.696, 0.1 ,1.0);
	    RooFormulaVar varChi2Number("NChi2", "varChi2Number", "@0*@1", RooArgList(varChi0Number, varChi2toChi0Ratio));
	RooRealVar varEtac2toEtac1Ratio("N(#eta_{c}(2S)/N(#eta_{c}(1S)","varEtac2toEtac1Ratio",0.059, 0,0.2);
	    RooFormulaVar varEtac2Number("NEta2", "varEtac2Number", "@0*@1", RooArgList(varEtac2toEtac1Ratio, varEtacNumber));
	break;
   case 4:
    RooRealVar varChi0toEtacRatio("NChi0/N(#eta_{c})","varChi0toEtacRatio",0.139, 0 ,0.3);
    RooFormulaVar varChi0Number("NChi0", "varChi0Number", "@0*@1", RooArgList(varEtacNumber, varChi0toEtacRatio));
    RooRealVar varChi1toEtacRatio("NChi1/N(#eta_{c})","varChi1toEtacRatio",0.070, 0 ,0.3);
    RooFormulaVar varChi1Number("NChi1", "varChi1Number", "@0*@1", RooArgList(varEtacNumber, varChi1toEtacRatio));
    RooRealVar varChi2toEtacRatio("NChi2/N(#eta_{c})","varChi2toEtacRatio",0.090, 0 ,0.3);
    RooFormulaVar varChi2Number("NChi2", "varChi2Number", "@0*@1", RooArgList(varEtacNumber, varChi2toEtacRatio));
    RooRealVar varEtac2toEtac1Ratio("N(#eta_{c}(2S)/N(#eta_{c}(1S)","varEtac2toEtac1Ratio",0.059, 0,0.2);
    RooFormulaVar varEtac2Number("NEta2", "varEtac2Number", "@0*@1", RooArgList(varEtac2toEtac1Ratio, varEtacNumber));
    break;
   default:
    RooRealVar varChi0Number("NChi0", "varChi0Number", 1000,0,1e5);
	RooRealVar varChi1Number("NChi1", "varChi1Number", 1000,0,1e5);
	RooRealVar varChi2Number("NChi2", "varChi2Number",1000,0,1e5);
	RooRealVar varEtac2Number("NEta2", "varEtac2Number", 400,0,1e5);
	break;
}
	

    if(SysType==18)
    {
        RooRealVar varJpsiNumber("N(J/#psi)", "varJpsiNumber", 10, 0, 1e4);
        RooRealVar varJpsiMass("M(J/#psi)", "varJpsiMass",jpsi_mass);
        RooFormulaVar varJpsiSigmaN("varJpsiSigmaN", "varJpsiSigmaN", "sqrt(@1-@2)*(@0)", RooArgList(ASig, varJpsiMass, FourKMass));
        RooGaussian pdfJpsi("pdfJpsi","pdfJpsi",Jpsi_m_scaled,varJpsiMass,varJpsiSigmaN);
        listComp.add(pdfJpsi);
        listNorm.add(varJpsiNumber);
    }
    

	RooRelBreitWigner pdfEtacBWrel = RooRelBreitWigner("pdfEtacBWrel", "pdfEtacBWrel", Jpsi_m_scaled, varEtacMass, varEtacGamma,
			RooConst(0), radius, massa, massb);
	RooGaussian pdfEtacGaussN("pdfEtacGaussN","pdfEtacGaussN",Jpsi_m_scaled,ConvCentre,varEtacSigmaN);
	RooGaussian pdfEtacGaussW("pdfEtacGaussW","pdfEtacGaussW",Jpsi_m_scaled,ConvCentre,varEtacSigmaN);
	RooFFTConvPdf pdfEtacN("pdfEtacN","pdfEtacN",Jpsi_m_scaled,pdfEtacBWrel,pdfEtacGaussN) ;
	RooFFTConvPdf pdfEtacW("pdfEtacW","pdfEtacW",Jpsi_m_scaled,pdfEtacBWrel,pdfEtacGaussW) ;
	RooAddPdf pdfEtac("pdfEtac","pdfEtac",RooArgList(pdfEtacN,pdfEtacW),varNarrowFraction);	
	
    
	
	RooRelBreitWigner pdfChi0BWrel = RooRelBreitWigner("pdfChi0BWrel", "pdfChi0BWrel", Jpsi_m_scaled, varChi0Mass, varChi0Gamma,
			RooConst(0), radius, massa, massb);
	RooGaussian pdfChi0GaussN("pdfChi0GaussN","pdfChi0GaussN",Jpsi_m_scaled,ConvCentre,varChi0SigmaN);
	RooGaussian pdfChi0GaussW("pdfChi0GaussW","pdfChi0GaussW",Jpsi_m_scaled,ConvCentre,varChi0SigmaN);
	RooFFTConvPdf pdfChi0N("pdfChi0N","pdfChi0N",Jpsi_m_scaled,pdfChi0BWrel,pdfChi0GaussN) ;
	RooFFTConvPdf pdfChi0W("pdfChi0W","pdfChi0W",Jpsi_m_scaled,pdfChi0BWrel,pdfChi0GaussW) ;
	RooAddPdf pdfChi0("pdfChi0","pdfChi0",RooArgList(pdfChi0N,pdfChi0W),varNarrowFraction);	
	

	RooRelBreitWigner pdfChi1BWrel = RooRelBreitWigner("pdfChi1BWrel", "pdfChi1BWrel", Jpsi_m_scaled, varChi1Mass, varChi1Gamma,
			RooConst(1), radius, massa, massb);
	RooGaussian pdfChi1GaussN("pdfChi1GaussN","pdfChi1GaussN",Jpsi_m_scaled,ConvCentre,varChi1SigmaN);
	RooGaussian pdfChi1GaussW("pdfChi1GaussW","pdfChi1GaussW",Jpsi_m_scaled,ConvCentre,varChi1SigmaN);
	RooFFTConvPdf pdfChi1N("pdfChi1N","pdfChi1N",Jpsi_m_scaled,pdfChi1BWrel,pdfChi1GaussN) ;
	RooFFTConvPdf pdfChi1W("pdfChi1W","pdfChi1W",Jpsi_m_scaled,pdfChi1BWrel,pdfChi1GaussW) ;
	RooAddPdf pdfChi1("pdfChi1","pdfChi1",RooArgList(pdfChi1N,pdfChi1W),varNarrowFraction);	
	
	
	RooRelBreitWigner pdfChi2BWrel = RooRelBreitWigner("pdfChi2BWrel", "pdfChi2BWrel", Jpsi_m_scaled, varChi2Mass, varChi2Gamma,
			RooConst(2), radius, massa, massb);
	RooGaussian pdfChi2GaussN("pdfChi2GaussN","pdfChi2GaussN",Jpsi_m_scaled,ConvCentre,varChi2SigmaN);
	RooGaussian pdfChi2GaussW("pdfChi2GaussW","pdfChi2GaussW",Jpsi_m_scaled,ConvCentre,varChi2SigmaN);
	RooFFTConvPdf pdfChi2N("pdfChi2N","pdfChi2N",Jpsi_m_scaled,pdfChi2BWrel,pdfChi2GaussN) ;
	RooFFTConvPdf pdfChi2W("pdfChi2W","pdfChi2W",Jpsi_m_scaled,pdfChi2BWrel,pdfChi2GaussW) ;
	RooAddPdf pdfChi2("pdfChi2","pdfChi2",RooArgList(pdfChi2N,pdfChi2W),varNarrowFraction);	
	
	
	RooRelBreitWigner pdfEtac2BWrel = RooRelBreitWigner("pdfEtac2BWrel", "pdfEtac2BWrel", Jpsi_m_scaled, varEtac2Mass, varEtac2Gamma,
			RooConst(0), radius, massa, massb);
	RooGaussian pdfEtac2GaussN("pdfEtac2GaussN","pdfEtac2GaussN",Jpsi_m_scaled,ConvCentre,varEtac2SigmaN);
	RooGaussian pdfEtac2GaussW("pdfEtac2GaussW","pdfEtac2GaussW",Jpsi_m_scaled,ConvCentre,varEtac2SigmaW);
	RooFFTConvPdf pdfEtac2N("pdfEtac2N","pdfEtac2N",Jpsi_m_scaled,pdfEtac2BWrel,pdfEtac2GaussN) ;
// 	pdfEtac2N.setShift(0,Centre);
	RooFFTConvPdf pdfEtac2W("pdfEtac2W","pdfEtac2W",Jpsi_m_scaled,pdfEtac2BWrel,pdfEtac2GaussW) ;
// 	pdfEtac2W.setShift(0,Centre);
	RooAddPdf pdfEtac2("pdfEtac2","pdfEtac2",RooArgList(pdfEtac2N,pdfEtac2W),varNarrowFraction);	
	
	

if(SysType!=15)	
	listComp.add(pdfEtac);

	listComp.add(pdfChi0);	
	listComp.add(pdfChi1);	
	listComp.add(pdfChi2);	
	listComp.add(pdfEtac2);

if(SysType!=15)	
	listNorm.add(varEtacNumber);

	listNorm.add(varChi0Number);	
	listNorm.add(varChi1Number);
	listNorm.add(varChi2Number);
	listNorm.add(varEtac2Number);
	
	
	
// 	showParams.add(varEtacNumber);
// 	showParams.add(varChi0toEtacRatio);
// 	showParams.add(varChi1toEtacRatio);	
// 	showParams.add(varChi2toEtacRatio);
// 	showParams.add(varEtac2toEtac1Ratio);
// 	showParams.add(varEtacMass);
	//showParams.add(varChi1Mass);
        //showParams.add(varChi0Mass);
	//showParams.add(varChi2Mass);
	//showParams.add(varEtac2Mass);
// 	showParams.add(varEtacGamma);
// 	showParams.add(varEtac2Gamma);	
	
if(SysType==5)
{
	RooRealVar AASig("AASig", "AASig", SigmaPar);

	RooRealVar varHctoChi0Ratio("N(#h_{c})/NChi0","varHctoChi0Ratio",0.005,0,0.2);
	RooRealVar varX3872toChi1Ratio("N(X(3872))/NChi1","varX3872toChi1Ratio",0.05,0,3);
	RooRealVar varX3915toChi0Ratio("N(X(3915))/NChi0","varX3915toChi0Ratio",0.005,0,0.3);
	RooRealVar varX3927toChi2Ratio("N(X(3927))/NChi2","varX3927toChi2Ratio",0.005,0,0.3);
	
	RooRealVar varHcMass("varHcMass", "varHcMass", hcMass);	
	RooRealVar varX3872Mass("varX3872Mass", "varX3872Mass", x3872Mass);
	RooRealVar varX3915Mass("varX3915Mass", "varX3915Mass", x3915Mass);	
	RooRealVar varX3927Mass("varX3927Mass", "varX3927Mass", x3927Mass);
	
	RooRealVar varX3915Gamma("var3915Gamma", "var3915Gamma", x3915Gamma);
	RooRealVar varX3927Gamma("var3927Gamma", "var3927Gamma", x3927Gamma);	
	
	RooFormulaVar varHcSigmaN("varHcSigmaN", "varHcSigmaN", "sqrt(@1-@2)*(@0)", RooArgList(AASig, varHcMass, FourKMass));
	RooFormulaVar varX3872SigmaN("varX3872SigmaN", "varX3872SigmaN", "sqrt(@1-@2)*(@0)", RooArgList(AASig, varX3872Mass, FourKMass));	
	RooFormulaVar varX3915SigmaN("varX3915SigmaN", "varX3915SigmaN", "sqrt(@1-@2)*(@0)", RooArgList(AASig, varX3915Mass, FourKMass));
	RooFormulaVar varX3927SigmaN("varX3927SigmaN", "varX3927SigmaN", "sqrt(@1-@2)*(@0)", RooArgList(AASig, varX3927Mass, FourKMass));

	
	RooFormulaVar varHcSigmaW("varHcSigmaW", "varHcSigmaW", "@0*@1", RooArgList(varSigmaFactor, varHcSigmaN));
	RooFormulaVar varX3872SigmaW("varX3872SigmaW", "varX3872SigmaW", "@0*@1", RooArgList(varSigmaFactor, varX3872SigmaN));	
	RooFormulaVar varX3915SigmaW("varX3915SigmaW", "varX3915SigmaW", "@0*@1", RooArgList(varSigmaFactor, varX3915SigmaN));		
	RooFormulaVar varX3927SigmaW("varX3927SigmaW", "varX3927SigmaW", "@0*@1", RooArgList(varSigmaFactor, varX3927SigmaN));
	
	RooFormulaVar varHcNumber("N(h_{c})", "varHcNumber", "@0*@1", RooArgList(varChi0Number,varHctoChi0Ratio));
	RooFormulaVar varX3872Number("N(X(3872))", "varX3872Number", "@0*@1", RooArgList(varChi1Number,varX3872toChi1Ratio));	
	RooFormulaVar varX3915Number("N(X(3915))", "varX3915Number", "@0*@1", RooArgList(varChi0Number,varX3915toChi0Ratio));	
	RooFormulaVar varX3927Number("N(X(3927))", "varX3927Number", "@0*@1", RooArgList(varChi2Number,varX3927toChi2Ratio));

	
	

	RooGaussian pdfHcN("pdfHcN", "pdfHcN", Jpsi_m_scaled, varHcMass, varHcSigmaN);
	RooGaussian pdfHcW("pdfHcW", "pdfHcW", Jpsi_m_scaled, varHcMass, varHcSigmaW);
	RooAddPdf pdfHc("pdfHc","pdfHc",RooArgList(pdfHcN,pdfHcW),varNarrowFraction);
	
	RooGaussian pdfX3872N("pdfX3872N", "pdfX3872N", Jpsi_m_scaled, varX3872Mass, varX3872SigmaN);
	RooGaussian pdfX3872W("pdfX3872W", "pdfX3872W", Jpsi_m_scaled, varX3872Mass, varX3872SigmaW);
	RooAddPdf pdfX3872("pdfX3872","pdfX3872",RooArgList(pdfX3872N,pdfX3872W),varNarrowFraction);	
	
	RooRelBreitWigner pdfX3915BWrel = RooRelBreitWigner("pdfX3915BWrel", "pdfX3915BWrel", Jpsi_m_scaled, varX3915Mass, varX3915Gamma,
			RooConst(0), radius, massa, massb);
	RooGaussian pdfX3915GaussN("pdfX3915GaussN","pdfX3915GaussN",Jpsi_m_scaled,ConvCentre,varX3915SigmaN);
	RooGaussian pdfX3915GaussW("pdfX3915GaussW","pdfX3915GaussW",Jpsi_m_scaled,ConvCentre,varX3915SigmaN);
	RooFFTConvPdf pdfX3915N("pdfX3915N","pdfX3915N",Jpsi_m_scaled,pdfX3915BWrel,pdfX3915GaussN) ;
	RooFFTConvPdf pdfX3915W("pdfX3915W","pdfX3915W",Jpsi_m_scaled,pdfX3915BWrel,pdfX3915GaussW) ;
	RooAddPdf pdfX3915("pdfX3915","pdfX3915",RooArgList(pdfX3915N,pdfX3915W),varNarrowFraction);
	
	RooRelBreitWigner pdfX3927BWrel = RooRelBreitWigner("pdfX3927BWrel", "pdfX3927BWrel", Jpsi_m_scaled, varX3927Mass, varX3927Gamma,
			RooConst(2), radius, massa, massb);
	RooGaussian pdfX3927GaussN("pdfX3927GaussN","pdfX3927GaussN",Jpsi_m_scaled,ConvCentre,varX3927SigmaN);
	RooGaussian pdfX3927GaussW("pdfX3927GaussW","pdfX3927GaussW",Jpsi_m_scaled,ConvCentre,varX3927SigmaN);
	RooFFTConvPdf pdfX3927N("pdfX3927N","pdfX3927N",Jpsi_m_scaled,pdfX3927BWrel,pdfX3927GaussN) ;
	RooFFTConvPdf pdfX3927W("pdfX3927W","pdfX3927W",Jpsi_m_scaled,pdfX3927BWrel,pdfX3927GaussW) ;
	RooAddPdf pdfX3927("pdfX3927","pdfX3927",RooArgList(pdfX3927N,pdfX3927W),varNarrowFraction);	
	
	
	
	//listComp.add(pdfHc);
	listComp.add(pdfX3872);	
	listComp.add(pdfX3915);
	listComp.add(pdfX3927);
	
	//listNorm.add(varHcNumber);
	listNorm.add(varX3872Number);	
	listNorm.add(varX3915Number);
	listNorm.add(varX3927Number);
	
	//showParams.add(varHctoChi0Ratio);
	showParams.add(varX3872toChi1Ratio);
	showParams.add(varX3915toChi0Ratio);
	showParams.add(varX3927toChi2Ratio);

	
	//RooGaussian HcMCons("HcMCons","HcMCons",varHcMass,RooConst(3525.38),RooConst(0.11)) ;
// 	RooGaussian X3872MCons("X3872MCons","X3872MCons",varX3872Mass,RooConst(x3872Mass),RooConst(x3872MassE)) ;
// 	RooGaussian X3915MCons("X3915MCons","X3915MCons",varX3915Mass,RooConst(3918.4),RooConst(1.9)) ;
// 	RooGaussian X3927MCons("X3927MCons","X3927MCons",varX3927Mass,RooConst(3927.2),RooConst(2.6)) ;
	
// 	RooGaussian X3915GCons("X3915GCons","X3915GCons",varX3915Gamma,RooConst(x3915Gamma),RooConst(x3915GammaError)) ;
// 	RooGaussian X3927GCons("X3927GCons","X3927GCons",varX3927Gamma,RooConst(x3927Gamma),RooConst(x3927GammaError)) ;
// 	RooProdPdf XConstraint("XConstraint", "XConstraint", 
// 			       RooArgSet(X3915GCons,X3927GCons));
	
}

	RooRealVar varBgrNumber("N_{bgr}", "number of background events", 6.5e4, 1e2, 1e5);
	RooRealVar varE0("varE0", "varE0", -3e-3, -1e-1, 0);
	RooRealVar varA0("varA0", "varA0", 1e-3,-1e-1, +1e-1);
	RooRealVar varA1("varA1", "varA1", 5e-6,-1e-3, +1e-3);
	
	RooRealVar varB0("varB0", "varB0", -1e-3, -1e-1, 1e-1);
	RooRealVar varB1("varB1", "varB1", 5e-7, -1, 1);
	

if(SysType==3)
	RooGenericPdf pdfBgr("pdfBgr", "pdfBgr", "sqrt(@0-@1)*(1+(@0-@2)*@3+(@0-@2)*(@0-@2)*@4)",    //O2 without exp
			RooArgSet(Jpsi_m_scaled, var2PhiMass, FitMin, varB0, varB1));
else 
 	RooGenericPdf pdfBgr("pdfBgr", "pdfBgr", "sqrt(@0-@1)*exp(@0*@3)*(1+(@0-@2)*@4)",
    		        RooArgSet(Jpsi_m_scaled, var2PhiMass, FitMin, varE0, varA0));	
//  	RooGenericPdf pdfBgr("pdfBgr", "pdfBgr", "sqrt(@0-@1)*exp(@0*@3)*(1+(@0-@2)*@4+(@0-@2)*(@0-@2)*@5)",
//     		        RooArgSet(Jpsi_m_scaled, var2PhiMass, FitMin, varE0, varA0, varA1));	
	
//  	RooGenericPdf pdfBgr("pdfBgr", "pdfBgr", "sqrt(@0-@1)*exp(@0*@2)*(1+(@0-@1)*@3+(@0-@1)*(@0-@1)*@3)",
//     		        RooArgSet(Jpsi_m_scaled, var2PhiMass, varE0, varA0, varA1/*, varD0*/));
	
	
	
	
	
	
//     	RooGenericPdf pdfBgr("pdfBgr", "pdfBgr", "exp(@0*@1)*(1+(@0-@3)*@2+(@0-@3)*(@0-@3)*@4)*sqrt(@0-@3)",
//     			RooArgSet(Jpsi_m_scaled, varC0, varA0, var2PhiMass, varD0));
// 	RooGenericPdf pdfBgr("pdfBgr", "pdfBgr", "exp(@0*@1)*(1+(@0-@3)*@2+(@0-@3)*(@0-@3)*@4+(@0-@3)*(@0-@3)*(@0-@3)*@5)*sqrt(@0-@3)", //exp with O
// 			RooArgSet(Jpsi_m_scaled, varC0, varA0, var2PhiMass, varD0, varF0));

	listComp.add(pdfBgr);
	listNorm.add(varBgrNumber);
	
	RooAddPdf pdfModel("pdfModel", "pdfModel", listComp, listNorm);
	
	ws->import(pdfModel);
	
	 
	RooGaussian GammaConstraint("GammaConstraint","GammaConstraint",varEtac2Gamma,RooConst(etac2Gamma),RooConst(etac2GammaError)) ;
// 	RooGaussian MassConstraint("MassConstraint","MassConstraint", varEtac2Mass, RooConst(3639.4), RooConst(1.3)) ;
	RooGaussian MassConstraint("MassConstraint","MassConstraint", varEtac2Mass, RooConst(etac2Mass), RooConst(etac2MassError)) ;
	RooProdPdf GMConstraint("GMConstraint", "GMConstraint", RooArgSet(GammaConstraint, MassConstraint));
	
	
}


void AddData(int SysType, RooWorkspace* ws)
{
    RooRealVar* Jpsi_m_scaled = ws->var("Jpsi_m_scaled");
    //if(SysType==7)RooRealVar* varNDiPhi_sw  = ws->var("varNDiPhi_sw");

    switch(SysType)
    {
      case 7:
        TFile* file = new TFile("splotAll.root");
        break;
      case 6:
        TFile* file = new TFile("diPhiPureAll_MC.root");
        break;
      case 12:
        TFile* file = new TFile("diPhiPureAllExp.root");
        break;
      case 14:
        TFile* file = new TFile("diPhiPureShift.root");
        break;
      case 16:
        TFile* file = new TFile("diPhiPureA1.root");
        break;
      case 17:
        TFile* file = new TFile("diPhiPureA2.root");
        break;
      default:
        TFile* file = new TFile("diPhiPureAll.root");
        break;
    }	
    if(SysType==7)
    {
        
        TTree* tree = (TTree*)file->Get("DecayTree");
      //file->GetObject("DecayTree", chain);
       // RooRealVar varNDiPhi_sw("varNDiPhi_sw", "varNDiPhi_sw", -5, 5);
        RooDataSet* dataSet = new RooDataSet("DataSet", "DataSet", tree, RooArgList(*Jpsi_m_scaled),"", "varNDiPhi_sw");
    }
    else
    {
      TH1D *histJpsiDiPhi  = (TH1D*) file->Get("histJpsiDiPhi");
      RooDataHist* dataSet = new RooDataHist("DataSet", "DataSet", *Jpsi_m_scaled, histJpsiDiPhi);
    }  
    ws->import(*dataSet);
    
    delete dataSet;
}


void framework(int fitType, int SysType)
{
    gROOT->Reset();
    gROOT->SetStyle("Plain");
    gROOT->ProcessLine(".L RooRelBreitWigner.cxx+");
    gROOT->ProcessLine(".x lhcbStyle.C");
    TProof::Open("");
  
    RooWorkspace* wspace = new RooWorkspace("diPhiWS");
    AddModel(fitType, SysType,wspace);
    AddData(SysType,wspace);
    
    
    RooAbsPdf* pdfModel  = wspace->pdf("pdfModel");

    RooRealVar* Jpsi_m_scaled = wspace->var("Jpsi_m_scaled");
    RooRealVar* varEtac2Gamma = wspace->var("#Gamma(#eta_{c}(2S))");
    
if(SysType!=15)
{
    RooRealVar* varEtac2toEtac1Ratio = wspace->var("N(#eta_{c}(2S)/N(#eta_{c}(1S)");
    RooRealVar* varEtacNumber = wspace->var("NEta");
}
    RooRealVar* varBgrNumber = wspace->var("N_{bgr}");
    
if(SysType==7)
{
    RooRealVar* varChi0Number = wspace->var("NChi0");
    RooRealVar* varChi1Number = wspace->var("NChi1");
    RooRealVar* varChi2Number = wspace->var("NChi2");
    RooRealVar* varEtac2Number = wspace->var("NEta2");
    
    RooRealVar varNDiPhi_sw("varNDiPhi_sw", "varNDiPhi_sw", -20, 20);
    RooRealVar Jpsi_PT("Jpsi_PT","Jpsi_PT",0,50000);
    RooRealVar diPhiWeight("diPhiWeight","diPhiWeight",-20,20);
    
    TFile* file = new TFile("splotAllAdd.root");
    TTree* tree = (TTree*)file->Get("DecayTree");

    RooDataSet* DataSet = new RooDataSet("DataSetFr", "DataSetFr", tree, RooArgList(*Jpsi_m_scaled,varNDiPhi_sw,Jpsi_PT,diPhiWeight),0, "varNDiPhi_sw");
}
else
    RooDataHist* DataSet = wspace->data("DataSet");


///******

    varEtac2Gamma->setConstant(kTRUE);
    RooFitResult* results = pdfModel->fitTo(*DataSet/*, ExternalConstraints(GMConstraint)*//*, Minos(true)*/, Save(true), PrintLevel(0), NumCPU(4));
	
// 	varEtacNumber.setVal(10*varEtacNumber.getVal());
// 	varBgrNumber.setVal(10*varBgrNumber.getVal());
    
    
    Float_t minMass = Jpsi_m_scaled->getMin();
    Float_t maxMass = Jpsi_m_scaled->getMax();
    
    Float_t binWidth = 10.;
    Int_t binN = int((maxMass-minMass)/binWidth);
    
if(SysType==7)
{
    varEtac2Gamma->setConstant(kTRUE);
    results = pdfModel->fitTo(*DataSet, Save(true), PrintLevel(0), NumCPU(4),SumW2Error(kTRUE));
    results = pdfModel->fitTo(*DataSet, Save(true), Minos(true),PrintLevel(0), NumCPU(4),SumW2Error(kTRUE),Extended(true),SumW2Error(kTRUE));
    SPlot* sData2 = new SPlot("sData2", "sData2", *DataSet, pdfModel, RooArgList(*varEtacNumber,*varChi0Number,*varChi1Number,*varChi2Number,*varEtac2Number,*varBgrNumber));
    RooDataSet* dsetNew = new RooDataSet(DataSet->GetName(),DataSet->GetTitle(),DataSet,*DataSet->get(),0,"NEta_sw");
    
    
//    cout<<"AAAA  DataSet"<<endl;
//    DataSet->printArgs();
//    cout<<"AAAA  dsetNew"<<endl;
//    dsetNew->printArgs();
    
    
    
    TFile fileNew("DoubleSplotAll.root","recreate");
    TTree* treeNew = dsetNew->tree();
    treeNew->SetName("DecayTree");
    treeNew->SetTitle("DecayTree");
    treeNew->Write();
    fileNew.Close();
    
    RooPlot* frameSplot = Jpsi_m_scaled->frame(Title("Jpsi_m_scaled_splot"));
    dsetNew->plotOn(frameSplot, Binning(binN, minMass, maxMass),DataError(RooAbsData::SumW2));
    TCanvas* canvSplot = new TCanvas("canvSplot", "canvSplot", 1000, 600);
    frameSplot->Draw();
    
}
else
{
    varEtac2Gamma->setConstant(kTRUE);
    if(Sys!-15)
        results = pdfModel->chi2FitTo(*DataSet,Extended(varEtacNumber->getVal()+varBgrNumber->getVal()), Minos(true),PrintLevel(2));
    else
        results = pdfModel->chi2FitTo(*DataSet,Extended(varBgrNumber->getVal()), Minos(true),PrintLevel(2));
	
    RooChi2Var chi2("chi2","chi2",*pdfModel,*DataSet);
    Double_t central =  chi2.getVal();
}


if(SysType==111)
{
    
    Double_t etac2GammaPDG = 11.3;
    Double_t etac2GammaPDGErrorLo = 2.9;
    Double_t etac2GammaPDGErrorHi = 3.2;

    
    varEtac2Gamma->setConstant(kTRUE);
	double x[100], y[100],ey[100],ex[100],eyStat[100];
	Int_t NPoints =30;
	Float_t minGamma = 0.001;
	Float_t maxGamma = 31;
	Float_t step = (maxGamma-minGamma)/double(NPoints);
	Double_t effFactor = (6368.*1087897.)/(4705.*(1030306.+1023758.));

	for(int i=0;i<NPoints;i++)
	{
	  
	  varEtac2Gamma->setVal(minGamma+step*i);
          results = pdfModel->chi2FitTo(*DataSet,Extended(varEtacNumber->getVal()+varBgrNumber->getVal()), PrintLevel(0));
	  x[i]=minGamma+step*i;
	  y[i]=effFactor*varEtac2toEtac1Ratio->getVal();
 	  ey[i]=effFactor*TMath::Sqrt(varEtac2toEtac1Ratio->getError()*varEtac2toEtac1Ratio->getError()+0.005*0.005);
 	  eyStat[i]=effFactor*TMath::Sqrt(varEtac2toEtac1Ratio->getError()*varEtac2toEtac1Ratio->getError());
	}
	TGraphErrors* SignEtac2 = new TGraphErrors(NPoints, x, y, ex, ey);	
	TGraphErrors* SignEtacStat = new TGraphErrors(NPoints, x, y, ex, eyStat);
	
	TCanvas* canvGr = new TCanvas("canvGr", "canvGr", 600, 400);
    
	SignEtac2->SetLineColor(kRed);
    SignEtac2->GetXaxis()->SetLimits(minGamma-0.4,maxGamma);

    TLine* GammaAv = new TLine(etac2GammaPDG,SignEtac2->GetYaxis()->GetXmin(),etac2GammaPDG,SignEtac2->GetYaxis()->GetXmax());
    TLine* GammaLo = new TLine(etac2GammaPDG - etac2GammaPDGErrorLo,SignEtac2->GetYaxis()->GetXmin(),etac2GammaPDG - etac2GammaPDGErrorLo,SignEtac2->GetYaxis()->GetXmax());
    TLine* GammaHi = new TLine(etac2GammaPDG + etac2GammaPDGErrorHi,SignEtac2->GetYaxis()->GetXmin(),etac2GammaPDG + etac2GammaPDGErrorHi,SignEtac2->GetYaxis()->GetXmax());
    GammaAv->SetLineColor(kBlue);
    GammaLo->SetLineColor(kBlue);
    GammaHi->SetLineColor(kBlue);
    GammaLo->SetLineStyle(kDashed);
    GammaHi->SetLineStyle(kDashed);

	SignEtac2->DrawClone("APC");
	SignEtacStat->DrawClone("P");
    GammaAv->DrawClone();
    GammaHi->DrawClone();
    GammaLo->DrawClone();
   

}

 if(SysType==222)
 {
    varEtac2Gamma.setConstant(kTRUE);
 	Float_t x[100], y[100],ey[100],ex[100];
 	Int_t NPoints =30;
 	Float_t minGamma = 0;
 	Float_t maxGamma = 30;
 	Float_t step = (maxGamma-minGamma)/NPoints;
 	TH1F* SignEtac2 = new TH1F("SignEtac2","SignEtac2",NPoints, minGamma,maxGamma);
 	for(int i=0;i<NPoints;i++)
 	{
 	  varEtac2Gamma.setVal(minGamma+step*i);
 	  results = pdfModel.chi2FitTo(*DataSet,Extended(varEtacNumber.getVal()+varBgrNumber.getVal()), PrintLevel(0));
           
 	  
 	  SignEtac2->SetBinContent(i+1, chi2.getVal()-central);
 	}
 	TCanvas* canvGr = new TCanvas("canvGr", "canvGr", 600, 400);
 	SignEtac2->Draw();
 
 
 }
 
 if(SysType==333)
 {
    varEtac2toEtac1Ratio->setConstant(kTRUE);
 	Float_t x[100], y[100],ey[100],ex[100];
 	int NPoints =40;
 	Float_t minYield = 0.;
 	Float_t maxYield = 0.15;
 	Float_t step = (maxYield-minYield)/double(NPoints);
 
 	for(int i=0;i<NPoints;i++)
 	{
 	  varEtac2toEtac1Ratio->setVal(minYield+step*i);
 	  results = pdfModel->chi2FitTo(*DataSet,Extended(varEtacNumber->getVal()+varBgrNumber->getVal()), PrintLevel(0));
           
 	  x[i]=varEtac2toEtac1Ratio->getVal();
 	  y[i]=chi2.getVal()-central;
 	  
 	}
 
 	TGraph* SignEtac2 = new TGraph(NPoints, x,y);
 	SignEtac2->GetXaxis()->SetRangeUser(minYield,maxYield);
 	SignEtac2->SetLineColor(kRed);
 	//SignEtac2->SetLineWidth(2);
 	TCanvas* canvGr = new TCanvas("canvGr", "canvGr", 600, 400);
 	SignEtac2->Draw("AL");
 
 
 }



	
	RooPlot* frame = Jpsi_m_scaled->frame(Title("Jpsi_m_scaled"));
	RooPlot* frame2 = Jpsi_m_scaled->frame(Title("Jpsi_m_scaled2"));
	

	DataSet->plotOn(frame, Binning(binN, minMass, maxMass),DataError(RooAbsData::SumW2));
	pdfModel->plotOn(frame);

    //	frame->getAttText()->SetTextSize(0.03) ;

	TCanvas* canvA = new TCanvas("canvA", "canvA", 1000, 600);
	frame->Draw();
	    
	RooArgSet* AllVars = pdfModel->getVariables();
	
	char label[200];
	sprintf(label, "%i.txt", SysType);
	AllVars->writeToFile(label); 
	
	FILE* fout = fopen (label, "a");
	fprintf(fout,"\n varChic0Mass = %f",AllVars->getRealValue("M(#chi_{c0})"));
	fprintf(fout,"\n varChic1Mass = %f",AllVars->getRealValue("M(#chi_{c1})"));
	fprintf(fout,"\n varChic2Mass = %f",AllVars->getRealValue("M(#chi_{c2})"));
	fprintf(fout,"\n varEtac2Mass = %f",AllVars->getRealValue("M(#eta_{c}(2S))"));
	fprintf(fout,"\n varEtacMass = %f",AllVars->getRealValue("M(#eta_{c})"));
	
	fprintf(fout,"\n M(#chi_{1})-M(#chi_{0}) = %f",AllVars->getRealValue("M(#chi_{1})-M(#chi_{0})"));
	fprintf(fout,"\n M(#chi_{2})-M(#chi_{0}) = %f",AllVars->getRealValue("M(#chi_{2})-M(#chi_{0})"));
	fprintf(fout,"\n M[#eta_{c}(2S)]-M[#eta_{c}(1S)] = %f",AllVars->getRealValue("M[#eta_{c}(2S)]-M[#eta_{c}(1S)]"));
	
	fclose(fout);
	
	delete wspace;
}




void PTFramework(int fitType = 3, int SysType=0, int PTBinNumber=0)
{
    gROOT->Reset();
    gROOT->SetStyle("Plain");
    gROOT->ProcessLine(".L RooRelBreitWigner.cxx+");
    TProof::Open("");
  
    RooWorkspace* wspace = new RooWorkspace("diPhiWS");
    AddModel(fitType, SysType,wspace);
    AddData(SysType,wspace);
    
    
    RooAbsPdf* pdfModel  = wspace->pdf("pdfModel");

    RooRealVar* Jpsi_m_scaled = wspace->var("Jpsi_m_scaled");
    RooRealVar* varEtac2Gamma = wspace->var("#Gamma(#eta_{c}(2S))");
    
if(SysType!=15)
    RooRealVar* varEtacNumber = wspace->var("NEta");
    
    RooRealVar* varBgrNumber = wspace->var("N_{bgr}");    
    
    if(SysType==7)
        RooDataSet* DataSet = wspace->data("DataSet");
    else
        RooDataHist* DataSet = wspace->data("DataSet");




    varEtac2Gamma->setConstant(kTRUE);
    RooFitResult* results = pdfModel->fitTo(*DataSet/*, ExternalConstraints(GMConstraint)*//*, Minos(true)*/, Save(true), PrintLevel(0), NumCPU(4));
	
// 	varEtacNumber.setVal(10*varEtacNumber.getVal());
// 	varBgrNumber.setVal(10*varBgrNumber.getVal());
    if(SysType==7)
    {
        varEtac2Gamma->setConstant(kTRUE);
        results = pdfModel->fitTo(*DataSet, Save(true), PrintLevel(0), NumCPU(4));	
    }
    else
    {
        varEtac2Gamma->setConstant(kTRUE);
        if(SysType!=15)
            results = pdfModel->chi2FitTo(*DataSet,Extended(varEtacNumber->getVal()+varBgrNumber->getVal()), PrintLevel(2));
        else
            results = pdfModel->chi2FitTo(*DataSet,Extended(varBgrNumber->getVal()), PrintLevel(2));
            
        RooChi2Var chi2("chi2","chi2",*pdfModel,*DataSet);
        Double_t central =  chi2.getVal();
    }


	Float_t minMass = Jpsi_m_scaled->getMin();
	Float_t maxMass = Jpsi_m_scaled->getMax();

	Float_t binWidth = 10.;
	Int_t binN = int((maxMass-minMass)/binWidth);
	
	RooPlot* frame = Jpsi_m_scaled.frame(Title("Jpsi_m_scaled"));


	FILE * fout;
    
    char txtName [200];
    sprintf(txtName,"paramsPT_%i.txt",SysType);
	if(PTBinNumber==1)
	    fout = fopen (txtName, "w");
	else
	    fout = fopen (txtName, "a");
	   
    if(SysType!=15)
        results = pdfModel->chi2FitTo(*DataSet, Minos(true), Extended(varEtacNumber->getVal()+varBgrNumber->getVal()), PrintLevel(2));
    else
        results = pdfModel->chi2FitTo(*DataSet, Minos(true), Extended(varBgrNumber->getVal()), PrintLevel(2));
//	results = pdfModel->chi2FitTo(*DataSet, Minos(true), Extended(varEtacNumber->getVal()+varBgrNumber->getVal()), PrintLevel(2));
	   
	DataSet->plotOn(frame, Binning(binN, minMass, maxMass));
	pdfModel->plotOn(frame);
    //showParams.paramOn(frame,Layout(0.55));
	TCanvas* canvA = new TCanvas("canvA", "canvA", 1000, 600);
	frame->Draw();
    
    cout<<"AAAAAAA   "<<frame->chiSquare()<<endl<<endl;
	    
	char pdfName[100];
	sprintf(pdfName,"%i.pdf",PTBinNumber);
	canvA->SaveAs(pdfName);
	    
    
    switch (fitType)
    {
        case 3:
            RooRealVar* varChic0Number = wspace->var("NChi0");
            RooRealVar* varChic1Number = wspace->var("NChi1");
            RooRealVar* varChic2Number = wspace->var("NChi2");
            
            if(SysType!=15)
                fprintf(fout,"%i %f %f %f %f %f %f %f %f %f %f %f %f \n",
                        PTBinNumber,
                        varEtacNumber->getVal(),  -varEtacNumber->getErrorLo(), varEtacNumber->getErrorHi(),
                        varChic0Number->getVal(), -varChic0Number->getErrorLo(), varChic0Number->getErrorHi(),
                        varChic1Number->getVal(), -varChic1Number->getErrorLo(), varChic1Number->getErrorHi(),
                        varChic2Number->getVal(), -varChic2Number->getErrorLo(), varChic2Number->getErrorHi()
                        );
            else
                fprintf(fout,"%i %f %f %f %f %f %f %f %f %f %f %f %f \n",
                        PTBinNumber,
                        0,  0, 0,
                        varChic0Number->getVal(), -varChic0Number->getErrorLo(), varChic0Number->getErrorHi(),
                        varChic1Number->getVal(), -varChic1Number->getErrorLo(), varChic1Number->getErrorHi(),
                        varChic2Number->getVal(), -varChic2Number->getErrorLo(), varChic2Number->getErrorHi()
                        );
            break;
        case 4:
            RooRealVar* varChic0_to_Etac_Number = wspace->var("NChi0/N(#eta_{c})");
            RooRealVar* varChic1_to_Etac_Number = wspace->var("NChi1/N(#eta_{c})");
            RooRealVar* varChic2_to_Etac_Number = wspace->var("NChi2/N(#eta_{c})");
            
            fprintf(fout,"%i %f %f %f %f %f %f %f %f %f %f %f %f \n",
                    PTBinNumber,
                    varEtacNumber->getVal(),  -varEtacNumber->getErrorLo(), varEtacNumber->getErrorHi(),
                    varChic0_to_Etac_Number->getVal(), -varChic0_to_Etac_Number->getErrorLo(), varChic0_to_Etac_Number->getErrorHi(),
                    varChic1_to_Etac_Number->getVal(), -varChic1_to_Etac_Number->getErrorLo(), varChic1_to_Etac_Number->getErrorHi(),
                    varChic2_to_Etac_Number->getVal(), -varChic2_to_Etac_Number->getErrorLo(), varChic2_to_Etac_Number->getErrorHi()
                    );
            break;
        default:
            break;
    }
    
    fclose(fout);
	delete canvA; 
	delete wspace;	    
}

void SysFramework()
{
    int ii = 1;
    framework(ii,0);
//    framework(ii,1);
//    framework(ii,2);
//    framework(ii,3);
//    framework(ii,4);
    framework(ii,5);
//    framework(ii,6);
//    framework(ii,7);
//    framework(ii,8);
//    framework(ii,9);
//    framework(ii,10);
//    framework(ii,11);
//    framework(ii,12);
//    framework(ii,13);
//    framework(ii,14);
//    framework(ii,15);
}

