// $Id: $



double bgr(double *x, double *par)  {
        double xx = x[0];
 	return par[0]*(TMath::Exp(-par[1]*xx));
}


//PT_Type
// 0 - Etac
// 1 - Chic

void ProcessData(int PT_Type=0, int DataType=12)
{  
    gROOT->Reset();
    
    switch(PT_Type)
    {
      case 0:
	Float_t BinsEdges[10]={3,5,7,9,11,13,15,17,19};
	int NPoints = 7;
	break;
      case 1:
	switch(DataType)
	{
// 	  case 12:
// 	    Float_t BinsEdges[10]={2,7,8,11,19};
// 	    break;
// 	  case 11:    
// 	    Float_t BinsEdges[10]={2,7.25,8.25,11.25,19};
// 	    break;
	  case 12:
	    Float_t BinsEdges[10]={2,7.5,11,19};
	    break;
	  case 11:    
	    Float_t BinsEdges[10]={2,7.5,11,19};
	    break;
	}
	int NPoints = 3;
	break;
    }
    
    TChain *chain = new TChain("DecayTree");
    if(DataType==12)
      chain->Add("../Data_07_15/2phi2012.root");  
    if(DataType==11)
      chain->Add("../Data_07_15/2phi2011.root");  

    char filename[100], label[100];
    
    gROOT->ProcessLine(".L ../diPhiPure.cpp");
    gROOT->ProcessLine(".L ../diPhi_fit_framework.cpp");
    gROOT->ProcessLine(".L ../RooRelBreitWigner.cxx+");
    
    for(int ii=1; ii<=NPoints; ii++)
    {

      sprintf(label, "Jpsi_PT>%i&&Jpsi_PT<%i", 1000*BinsEdges[ii-1], 1000*BinsEdges[ii]);
      TCut CutPT(label);

      sprintf(label, "Jpsi_PT>%i&&Jpsi_PT<%i", BinsEdges[ii-1], BinsEdges[ii]);
      sprintf(filename,"%s.root",label);

      TFile *newfile = new TFile(filename,"recreate");
      TTree *newtree = chain->CopyTree(CutPT);
    
      newtree->Print();
      newfile->Write();
      newfile->Close();
      
      char commandPure[100];
      sprintf(commandPure, "diPhiPure(6,0,\"%s\")",filename);
      gROOT->ProcessLine(commandPure);
      
      char commandFit[100];
      sprintf(commandFit, "PTFramework(0,%i)",ii);
      gROOT->ProcessLine(commandFit);
      delete newfile; 
    }
    delete chain;


    

}

//errType
// 1 - with MC error
// 2 - wothout MC error

void plotSpectraEtac(int errType=1)
{
    Float_t BinsEdges[10]={3,5,7,9,11,13,15,17,19};
    int NPoints = 7;  
    
    Float_t BRphiphi = 1.76*0.001;     
    Float_t BRKK = 0.489;
    
    
    Float_t erBRphiphi = 0.2*0.001; 
    Float_t erBRKK = 0.005;
    
//     Float_t erBRphiphi = 0; 
//     Float_t erBRKK = 0;
    
    Float_t NGenerated = 2054064.;
    Float_t Luminosity = 2;
  
    FILE *fp = fopen("paramsPT.txt","r");	
    Float_t x [10], ex1 [10], ex2 [10];
    Float_t Data [10], eDataLo [10], eDataHi [10];
    Float_t a;

    for(int ii=0; ii<NPoints; ii++)
    {
        int nPoint;
	fscanf(fp, "%i %e %e %e", &nPoint,&Data[ii],&eDataLo[ii],&eDataHi[ii]);
	fscanf(fp,"%e %e %e %e %e %e %e %e %e", &a,&a,&a,&a,&a,&a,&a,&a,&a);
        x[ii]=(BinsEdges[ii+1]+BinsEdges[ii])/2;
        ex2[ii]=x[ii]-BinsEdges[ii];
	ex1[ii]=0;
    }
    fclose(fp);
    phAsymmErrors* DataGr = nTGraew TGraphAsymmErrors(NPoints,x,Data,ex1,ex1,eDataLo,eDataHi);
    TGraphAsymmErrors* DataGrDraw = new TGraphAsymmErrors(NPoints,x,Data,ex2,ex2,eDataLo,eDataHi);  
    DataGrDraw->Draw("AP");
    
    TCut KaonIPCut("Kaon1_IPCHI2_OWNPV>4 && Kaon2_IPCHI2_OWNPV>4 && Kaon3_IPCHI2_OWNPV>4 && Kaon4_IPCHI2_OWNPV>4");
    TCut KaonTRCut("Kaon1_TRACK_CHI2NDOF<4 && Kaon2_TRACK_CHI2NDOF<4 && Kaon3_TRACK_CHI2NDOF<4 && Kaon4_TRACK_CHI2NDOF<4");
    TCut PIDCut("Kaon1_ProbNNk>0.1 && Kaon2_ProbNNk>0.1 && Kaon3_ProbNNk>0.1 && Kaon4_ProbNNk>0.1");
    TCut PhiMCut("abs(Phi1_MM-1020)<12 && abs(Phi2_MM-1020)<12");
    TCut PhiVerCut("Phi1_ENDVERTEX_CHI2<25 && Phi2_ENDVERTEX_CHI2<25");
    TCut JpsiCut("Jpsi_ENDVERTEX_CHI2<45 && Jpsi_MM>2000 && Jpsi_MM<6000 && Jpsi_FDCHI2_OWNPV>100");// >49");
    TCut PTCut("Kaon1_PT>500 && Kaon2_PT>500 && Kaon3_PT>500 && Kaon4_PT>500");
    
    TCut Trigger0Cut("JpsiL0HadronDecision_TOS || JpsiL0Global_TIS");
    TCut Trigger1Cut("JpsiHlt1TrackAllL0Decision_TOS");
    TCut Trigger2Cut("JpsiHlt2Topo2BodyBBDTDecision_TOS || JpsiHlt2Topo3BodyBBDTDecision_TOS || JpsiHlt2Topo4BodyBBDTDecision_TOS || JpsiHlt2IncPhiDecision_TOS");    
      
  

    TCut totCut = KaonIPCut && KaonTRCut && PIDCut && PhiMCut && PhiVerCut && JpsiCut && PTCut && Trigger0Cut && Trigger1Cut && Trigger2Cut;   

  
    TCut AllPTCut("Jpsi_PT>0 && Jpsi_PT<25000");
    
    TChain * DecayTreeMCAll=new TChain("DecayTree");
    DecayTreeMCAll->Add("/home/aus/CERN/Analysis/MC/CAL_0/Etac/AllMC_Dirty.root");
    TTree* DecayTreeMC = DecayTreeMCAll->CopyTree(AllPTCut&&totCut);
	 
    Int_t NEnMC=DecayTreeMC->GetEntries();
    Double_t Branch_Value2;
    DecayTreeMC->SetBranchAddress("Jpsi_PT",&Branch_Value2);
    TH1D *histMC = new TH1D("etac MC","etac MC", NPoints, BinsEdges[0], BinsEdges[NPoints-1]);

	
    for (Int_t i=0; i<NEnMC; i++) 
    {
	DecayTreeMC->GetEntry(i);
	histMC->Fill(Branch_Value2/1000);
    }	
	
	
    TChain * DecayTreeGenAll=new TChain("MCDecayTreeTuple/MCDecayTree");
    DecayTreeGenAll->Add("/home/aus/CERN/Analysis/MC/GenSpectr/EtacGenSpectr/DVntuple*");
    
    TCut AllPTetacCut("eta_c_1S_TRUEPT>0 && eta_c_1S_TRUEPT<25000");
    TTree* DecayTreeGen = DecayTreeGenAll->CopyTree(AllPTetacCut);
    Int_t NEnGen=DecayTreeGen->GetEntries();
    Double_t Branch_Value3;
    DecayTreeGen->SetBranchAddress("eta_c_1S_TRUEPT",&Branch_Value3);
    TH1D *histGen = new TH1D("Gen","Gen", NPoints, BinsEdges[0], BinsEdges[NPoints-1]);
    for (Int_t i=0; i<NEnGen; i++) 
    {
	DecayTreeGen->GetEntry(i);
	histGen->Fill(Branch_Value3/1000);
    }
	
    Float_t NMCSelected = histMC->Integral();
    Float_t NMCGen = histGen->Integral();

	
//    histMC->Scale(1./histMC->Integral());
//    histGen->Scale(1./histGen->Integral());
	
    TCanvas* canvC = new TCanvas("canvC", "canv", 800, 700);
    histGen->SetLineColor(kRed);
    histGen->Draw();	
    histMC->Draw("same");
    

//    float Data [10], eDataLo [10], eDataHi [10];
    Float_t Res[10], eResLo [10], eResHi [10], eps[10];
    TH1D *histEps = new TH1D("histEps","histEps", NPoints, BinsEdges[0], BinsEdges[NPoints-1]);
    
    for(int ii=0; ii<NPoints; ii++)
    {
        eps[ii]=Float_t(Float_t(histMC->GetBinContent(ii+1))/Float_t(histGen->GetBinContent(ii+1))/NGenerated*NMCGen);
	histEps->SetBinContent(ii+1,eps[ii]);
	
 	Res[ii]=Data[ii]/eps[ii]/Luminosity/(BinsEdges[ii+1]-BinsEdges[ii])/BRphiphi/BRKK/BRKK*1e-6;
	
	if(errType==1)
	{
	  Float_t ErrorRelHi = (Float_t)sqrt(1./histMC->GetBinContent(ii+1) + (erBRphiphi/BRphiphi)**2 + (2*erBRKK/BRKK)**2 + (eDataHi[ii]/Data[ii])**2 + 1/histGen->GetBinContent(ii+1));
	  Float_t ErrorRelLo = (Float_t)sqrt(1./histMC->GetBinContent(ii+1) + (erBRphiphi/BRphiphi)**2 + (2*erBRKK/BRKK)**2 + (eDataLo[ii]/Data[ii])**2 + 1/histGen->GetBinContent(ii+1));
	}
	else
	{
	  Float_t ErrorRelHi = (Float_t)eDataHi[ii]/Data[ii];
	  Float_t ErrorRelLo = (Float_t)eDataLo[ii]/Data[ii];
	}

	eResHi[ii] = Res[ii]*ErrorRelHi;
	eResLo[ii] = Res[ii]*ErrorRelLo;	
    }
    TCanvas* canvEps = new TCanvas("canvEps", "canvEps", 800, 700);  
    histEps->Draw();
    
  
    TCanvas* canvD = new TCanvas("canvD", "canvD", 800, 700);    
    canvD->SetLogy();
    TGraphAsymmErrors* ResGr = new TGraphAsymmErrors(NPoints,x,Res,ex1,ex1,eResLo,eResHi);
    TGraphAsymmErrors* ResGrDraw = new TGraphAsymmErrors(NPoints,x,Res,ex2,ex2,eResLo,eResHi); 

    ResGr->SetMarkerStyle(21);
    ResGr->SetMarkerSize(0.4);

    TF1 *fitFunc = new TF1("fitFunc","[0]*exp(-[1]*x)", 0, 20);


    
    fitFunc->SetLineWidth(2);
    ResGr->Fit(fitFunc);
    ResGr->Fit(fitFunc);
    ResGr->Fit(fitFunc);
    ResGrDraw->Draw("AP");
    fitFunc->Draw("same");
		
    cout<<"chi^2  "<<fitFunc->GetChisquare()/fitFunc->GetNDF()<<endl;
    cout<<"Slope  "<<fitFunc->GetParameter(1)<<"  "<<fitFunc->GetParError(1)<<endl;
  
}


void plotSpectraChic(int errType=1, int Year = 12)
{
    
//    Float_t BinsEdges[10]={2,7,8,11,19};
    switch(Year)
    {
//       case 12:
// 	Float_t BinsEdges[10]={2,7,8,11,19};
// 	break;
//       case 11:
// 	Float_t BinsEdges[10]={2,7.25,8.25,11.25,19};
// 	break;
      case 12:
	Float_t BinsEdges[10]={2,7.5,11,19};
	break;
      case 11:
	Float_t BinsEdges[10]={2,7.5,11,19};
	break;
    }
    
    int NPoints = 3;
    
    Float_t BRChi0phiphi = 0.77*0.001;     
    Float_t BRChi1phiphi = 0.42*0.001;  
    Float_t BRChi2phiphi = 1.12*0.001;  
    Float_t BRKK = 0.489;
    
    Float_t erBRChi0phiphi = 0.07*0.001; 
    Float_t erBRChi1phiphi = 0.05*0.001; 
    Float_t erBRChi2phiphi = 0.01*0.001; 
    Float_t erBRKK = 0.005;
    
//     Float_t erBRChi0phiphi = 0; 
//     Float_t erBRChi1phiphi = 0; 
//     Float_t erBRChi2phiphi = 0; 
//     Float_t erBRKK = 0;
    
    Float_t NChi0Generated = 1105362.;
    Float_t NChi1Generated = 1101384.;
    Float_t NChi2Generated = 1135884.;
    Float_t Luminosity = 1;
  
    FILE *fp = fopen("paramsPT.txt","r");	
    Float_t x [10], ex [10];
    Float_t x0 [10], x1[10], x2[10], exDr[10];
    Float_t DataChi0 [10], eDataChi0Lo [10], eDataChi0Hi [10];
    Float_t DataChi1 [10], eDataChi1Lo [10], eDataChi1Hi [10];
    Float_t DataChi2 [10], eDataChi2Lo [10], eDataChi2Hi [10];
    Float_t a;
    int nPoint;

    for(int ii=0; ii<NPoints; ii++)
    {
	fscanf(fp, "%i %e %e %e",&nPoint,&a,&a,&a);
	fscanf(fp, "%e %e %e", &DataChi0[ii],&eDataChi0Lo[ii],&eDataChi0Hi[ii]);
	fscanf(fp, "%e %e %e", &DataChi1[ii],&eDataChi1Lo[ii],&eDataChi1Hi[ii]);
	fscanf(fp, "%e %e %e", &DataChi2[ii],&eDataChi2Lo[ii],&eDataChi2Hi[ii]);
        x[ii]=(BinsEdges[ii+1]+BinsEdges[ii])/2;
	x0[ii]=x[ii]-0.2;
	x2[ii]=x[ii]+0.2;
	x1[ii]=x[ii];
        exDr[ii]=x[ii]-BinsEdges[ii];
        ex[ii]=0;
    }
    fclose(fp);
    
//     DataChi0GrDraw->Draw("AP");
//     DataChi1GrDraw->Draw("P");
//     DataChi2GrDraw->Draw("P");
    
 
    TChain * DecayTreeChi0MCAll=new TChain("DecayTree");
    DecayTreeChi0MCAll->Add("/home/aus/CERN/Analysis/MC/CAL_0/Chic0/AllMC_Reduced.root");
    TTree* DecayTreeChi0MC = DecayTreeChi0MCAll->CopyTree("Jpsi_PT>0 && Jpsi_PT<25000"); 
    Int_t NEnMC=DecayTreeChi0MC->GetEntries();
    Double_t Branch_Value2;
    DecayTreeChi0MC->SetBranchAddress("Jpsi_PT",&Branch_Value2);
    TH1D *histMCChi0 = new TH1D("chic0 MC","chic0 MC", NPoints, BinsEdges[0], BinsEdges[NPoints]);
    for (Int_t i=0; i<NEnMC; i++) 
    {
	DecayTreeChi0MC->GetEntry(i);
	histMCChi0->Fill(Branch_Value2/1000);
    }	
    TChain * DecayTreeChi0GenAll=new TChain("MCDecayTreeTuple/MCDecayTree");
    DecayTreeChi0GenAll->Add("/home/aus/CERN/Analysis/MC/GenSpectr/Chic0GenSpectr/DVntuple*");
    TTree* DecayTreeChi0Gen = DecayTreeChi0GenAll->CopyTree("chi_c0_1P_TRUEPT>0 && chi_c0_1P_TRUEPT<25000");
    Int_t NEnGen=DecayTreeChi0Gen->GetEntries();
    Double_t Branch_Value3;
    DecayTreeChi0Gen->SetBranchAddress("chi_c0_1P_TRUEPT",&Branch_Value3);
    TH1D *histChic0Gen = new TH1D("Gen0","Gen0", NPoints, BinsEdges[0], BinsEdges[NPoints]);
    for (Int_t i=0; i<NEnGen; i++) 
    {
	DecayTreeChi0Gen->GetEntry(i);
	histChic0Gen->Fill(Branch_Value3/1000);
    }
    Float_t NChic0MCSelected = histMCChi0->Integral();
    Float_t NChic0MCGen = histChic0Gen->Integral();
    
    
    
    TChain * DecayTreeChi1MCAll=new TChain("DecayTree");
    DecayTreeChi1MCAll->Add("/home/aus/CERN/Analysis/MC/CAL_0/Chic1/AllMC_Reduced.root");
    TTree* DecayTreeChi1MC = DecayTreeChi1MCAll->CopyTree("Jpsi_PT>0 && Jpsi_PT<25000"); 
    Int_t NEnMC=DecayTreeChi1MC->GetEntries();
    Double_t Branch_Value2;
    DecayTreeChi1MC->SetBranchAddress("Jpsi_PT",&Branch_Value2);
    TH1D *histMCChi1 = new TH1D("chic1 MC","chic1 MC", NPoints, BinsEdges[0], BinsEdges[NPoints]);
    for (Int_t i=0; i<NEnMC; i++) 
    {
	DecayTreeChi1MC->GetEntry(i);
	histMCChi1->Fill(Branch_Value2/1000);
    }	
    TChain * DecayTreeChi1GenAll=new TChain("MCDecayTreeTuple/MCDecayTree");
    DecayTreeChi1GenAll->Add("/home/aus/CERN/Analysis/MC/GenSpectr/Chic1GenSpectr/DVntuple*");
    TTree* DecayTreeChi1Gen = DecayTreeChi1GenAll->CopyTree("chi_c1_1P_TRUEPT>0 && chi_c1_1P_TRUEPT<25000");
    Int_t NEnGen=DecayTreeChi1Gen->GetEntries();
    Double_t Branch_Value3;
    DecayTreeChi1Gen->SetBranchAddress("chi_c1_1P_TRUEPT",&Branch_Value3);
    TH1D *histChic1Gen = new TH1D("Gen1","Gen1", NPoints, BinsEdges[0], BinsEdges[NPoints]);
    for (Int_t i=0; i<NEnGen; i++) 
    {
	DecayTreeChi1Gen->GetEntry(i);
	histChic1Gen->Fill(Branch_Value3/1000);
    }
    Float_t NChic1MCSelected = histMCChi1->Integral();  
    Float_t NChic1MCGen = histChic1Gen->Integral();
    
    
    TChain * DecayTreeChi2MCAll=new TChain("DecayTree");
    DecayTreeChi2MCAll->Add("/home/aus/CERN/Analysis/MC/CAL_0/Chic2/AllMC_Reduced.root");
    TTree* DecayTreeChi2MC = DecayTreeChi2MCAll->CopyTree("Jpsi_PT>0 && Jpsi_PT<25000"); 
    Int_t NEnMC=DecayTreeChi2MC->GetEntries();
    Double_t Branch_Value2;
    DecayTreeChi2MC->SetBranchAddress("Jpsi_PT",&Branch_Value2);
    TH1D *histMCChi2 = new TH1D("chic2 MC","chic2 MC", NPoints, BinsEdges[0], BinsEdges[NPoints]);
    for (Int_t i=0; i<NEnMC; i++) 
    {
	DecayTreeChi2MC->GetEntry(i);
	histMCChi2->Fill(Branch_Value2/1000);
    }	
    TChain * DecayTreeChi2GenAll=new TChain("MCDecayTreeTuple/MCDecayTree");
    DecayTreeChi2GenAll->Add("/home/aus/CERN/Analysis/MC/GenSpectr/Chic2GenSpectr/DVntuple*");
    TTree* DecayTreeChi2Gen = DecayTreeChi2GenAll->CopyTree("chi_c2_1P_TRUEPT>0 && chi_c2_1P_TRUEPT<25000");
    Int_t NEnGen=DecayTreeChi2Gen->GetEntries();
    Double_t Branch_Value3;
    DecayTreeChi2Gen->SetBranchAddress("chi_c2_1P_TRUEPT",&Branch_Value3);
    TH1D *histChic2Gen = new TH1D("Gen2","Gen2", NPoints, BinsEdges[0], BinsEdges[NPoints]);
    for (Int_t i=0; i<NEnGen; i++) 
    {
	DecayTreeChi2Gen->GetEntry(i);
	histChic2Gen->Fill(Branch_Value3/1000);
    }
    Float_t NChic2MCSelected = histMCChi2->Integral();  
    Float_t NChic2MCGen = histChic2Gen->Integral();  
    
    
	
//    histMCChi0->Scale(1./histMCChi0->Integral());
//    histChic0Gen->Scale(1./histChic0Gen->Integral());
	
    TCanvas* canvC = new TCanvas("canvC", "canvC", 800, 700);
    histChic0Gen->SetLineColor(kRed);
    histChic0Gen->Draw();	
    histMCChi0->Draw("same");

    

//    float DataChi0 [10], eDataChi0Lo [10], eDataChi0Hi [10];
    Float_t Chic0Res[10], eChic0ResLo [10], eChic0ResHi [10], epsChic0[10];
    TH1D *histChic0Eps = new TH1D("histChic0Eps","histChic0Eps", NPoints, BinsEdges[0], BinsEdges[NPoints]);
    for(int ii=0; ii<NPoints; ii++)
    {   
        epsChic0[ii]=Float_t(Float_t(histMCChi0->GetBinContent(ii+1))/Float_t(histChic0Gen->GetBinContent(ii+1))/NChi0Generated*NChic0MCGen);
	histChic0Eps->SetBinContent(ii+1,epsChic0[ii]);

 	Chic0Res[ii]=DataChi0[ii]/epsChic0[ii]/Luminosity/(BinsEdges[ii+1]-BinsEdges[ii])/BRChi0phiphi/BRKK/BRKK*1e-6;
	if(errType==1)
	{
	  Float_t ErrorRelHi = (Float_t)sqrt(1./(histMCChi0->GetBinContent(ii+1)) + (erBRChi0phiphi/BRChi0phiphi)**2 + (2*erBRKK/BRKK)**2 + (eDataChi0Hi[ii]/DataChi0[ii])**2 + 1/histChic0Gen->GetBinContent(ii+1));
	  Float_t ErrorRelLo = (Float_t)sqrt(1./(histMCChi0->GetBinContent(ii+1)) + (erBRChi0phiphi/BRChi0phiphi)**2 + (2*erBRKK/BRKK)**2 + (eDataChi0Lo[ii]/DataChi0[ii])**2 + 1/histChic0Gen->GetBinContent(ii+1));
	}
	else
	{
	  Float_t ErrorRelHi = (Float_t)eDataChi0Hi[ii]/DataChi0[ii];
	  Float_t ErrorRelLo = (Float_t)eDataChi0Lo[ii]/DataChi0[ii];
	}

	eChic0ResHi[ii] = Chic0Res[ii]*ErrorRelHi;
	eChic0ResLo[ii] = Chic0Res[ii]*ErrorRelLo;
	
    }
    
    
    Float_t Chic1Res[10], eChic1ResLo [10], eChic1ResHi [10], epsChic1[10];
    TH1D *histChic1Eps = new TH1D("histChic1Eps","histChic1Eps", NPoints, BinsEdges[0], BinsEdges[NPoints]);
    for(int ii=0; ii<NPoints; ii++)
    {
        epsChic1[ii]=Float_t(Float_t(histMCChi1->GetBinContent(ii+1))/Float_t(histChic1Gen->GetBinContent(ii+1))/NChi1Generated*NChic1MCGen);
	histChic1Eps->SetBinContent(ii+1,epsChic1[ii]);
	
 	Chic1Res[ii]=DataChi1[ii]/epsChic1[ii]/Luminosity/(BinsEdges[ii+1]-BinsEdges[ii])/BRChi1phiphi/BRKK/BRKK*1e-6;
	
	if(errType==1)
	{
	  Float_t ErrorRelHi = (Float_t)sqrt(1./(histMCChi1->GetBinContent(ii+1)) + (erBRChi1phiphi/BRChi0phiphi)**2 + (2*erBRKK/BRKK)**2 + (eDataChi1Hi[ii]/DataChi1[ii])**2 + 1/histChic1Gen->GetBinContent(ii+1));
	  Float_t ErrorRelLo = (Float_t)sqrt(1./(histMCChi1->GetBinContent(ii+1)) + (erBRChi1phiphi/BRChi0phiphi)**2 + (2*erBRKK/BRKK)**2 + (eDataChi1Lo[ii]/DataChi1[ii])**2 + 1/histChic1Gen->GetBinContent(ii+1));
	}
	else
	{
	  Float_t ErrorRelHi = (Float_t)eDataChi1Hi[ii]/DataChi1[ii];
	  Float_t ErrorRelLo = (Float_t)eDataChi1Lo[ii]/DataChi1[ii];
	}

	eChic1ResHi[ii] = Chic1Res[ii]*ErrorRelHi;
	eChic1ResLo[ii] = Chic1Res[ii]*ErrorRelLo;
	
    }
    
    
    Float_t Chic2Res[10], eChic2ResLo [10], eChic2ResHi [10], epsChic2[10];
    TH1D *histChic2Eps = new TH1D("histChic2Eps","histChic2Eps", NPoints, BinsEdges[0], BinsEdges[NPoints]);
    for(int ii=0; ii<NPoints; ii++)
    {
        epsChic2[ii]=Float_t(Float_t(histMCChi2->GetBinContent(ii+1))/Float_t(histChic2Gen->GetBinContent(ii+1))/NChi2Generated*NChic2MCGen);
	histChic2Eps->SetBinContent(ii+1,epsChic2[ii]);
	
	
 	Chic2Res[ii]=DataChi2[ii]/epsChic2[ii]/Luminosity/(BinsEdges[ii+1]-BinsEdges[ii])/BRChi2phiphi/BRKK/BRKK*1e-6;
	
	if(errType==1)
	{
	  Float_t ErrorRelHi = (Float_t)sqrt(1./(histMCChi2->GetBinContent(ii+1)) + (erBRChi2phiphi/BRChi2phiphi)**2 + (2*erBRKK/BRKK)**2 + (eDataChi2Hi[ii]/DataChi2[ii])**2 + 1/histChic2Gen->GetBinContent(ii+1));
	  Float_t ErrorRelLo = (Float_t)sqrt(1./(histMCChi2->GetBinContent(ii+1)) + (erBRChi2phiphi/BRChi2phiphi)**2 + (2*erBRKK/BRKK)**2 + (eDataChi2Lo[ii]/DataChi2[ii])**2 + 1/histChic2Gen->GetBinContent(ii+1));
	}
	else
	{
	  Float_t ErrorRelHi = (Float_t)eDataChi2Hi[ii]/DataChi2[ii];
	  Float_t ErrorRelLo = (Float_t)eDataChi2Lo[ii]/DataChi2[ii];
	}
	eChic2ResHi[ii] = Chic2Res[ii]*ErrorRelHi;
	eChic2ResLo[ii] = Chic2Res[ii]*ErrorRelLo;
	
    }



    Float_t ChicResAv[10], eChicResAvLo [10], ChicResAvHi [10], epsChicResAv[10];
    TH1D *histChicAvEps = new TH1D("histChicAvEps","histChicAvEps", NPoints, BinsEdges[0], BinsEdges[NPoints]);
    for(int ii=0; ii<NPoints; ii++)
    {
        epsChicResAv[ii]=Float_t(Float_t(histMCChi2->GetBinContent(ii+1)+histMCChi1->GetBinContent(ii+1)+histMCChi0->GetBinContent(ii+1))
	/Float_t(histChic2Gen->GetBinContent(ii+1)+histChic1Gen->GetBinContent(ii+1)+histChic0Gen->GetBinContent(ii+1))
	/(NChi2Generated+NChi1Generated+NChi0Generated)*(NChic2MCGen+NChic1MCGen+NChic0MCGen));
	histChicAvEps->SetBinContent(ii+1,epsChicResAv[ii]);
    }    
    
    
    TCanvas* canvEps = new TCanvas("canvEps", "canvEps", 800, 700);  
    
    histChic1Eps->SetLineColor(kRed);
    histChic1Eps->Draw(/*"same"*/);    
    histChic0Eps->SetLineColor(kBlue);
    histChic0Eps->Draw("same");
    histChic2Eps->SetLineColor(kBlack);
    histChic2Eps->Draw("same");
    leg = new TLegend(0.5,0.65,0.9,0.9);
    leg->AddEntry(histChic0Eps,"#chi_{c0}","l");
    leg->AddEntry(histChic1Eps,"#chi_{c1}","l");
    leg->AddEntry(histChic2Eps,"#chi_{c2}","l");
    leg->Draw();    
    histChicAvEps->Draw();
    
//     TCanvas* canvD = new TCanvas("canvD", "canvD", 800, 700);    
//     canvD->SetLogy();
  

    FILE *fp = fopen("Chic0Out.txt", "w");
    fprintf(fp,"%i\n",NPoints);
    for(int i=0;i<NPoints;i++)
	fprintf(fp,"%f %f %f %f %f %f \n",x[i],Chic0Res[i],exDr[i],exDr[i],eChic0ResLo[i],eChic0ResHi[i]);
    fclose(fp);    
    


    FILE *fp = fopen("Chic1Out.txt", "w");
    fprintf(fp,"%i\n",NPoints);
    for(int i=0;i<NPoints;i++)
	fprintf(fp,"%f %f %f %f %f %f \n",x[i],Chic1Res[i],exDr[i],exDr[i],eChic1ResLo[i],eChic1ResHi[i]);
    fclose(fp); 

   
    FILE *fp = fopen("Chic2Out.txt", "w");
    fprintf(fp,"%i\n",NPoints);
    for(int i=0;i<NPoints;i++)
	fprintf(fp,"%f %f %f %f %f %f \n",x[i],Chic2Res[i],exDr[i],exDr[i],eChic2ResLo[i],eChic2ResHi[i]);
    fclose(fp); 


}


using namespace RooFit;
void fitSpectra(int PT_Type=0, int Year=12)
{
	Float_t minPT = 2;
	Float_t maxPT = 19;
	Float_t binWidth = 10.;
	
	RooRealVar varPT("varPT", "varPT", minPT, maxPT, "MeV");
	RooRealVar varPTDr("varPTDr", "varPTDr", minPT-1, maxPT+1, "MeV");
	RooRealVar varPT2("varPT2", "varPT2", minPT, maxPT, "MeV");
	varPT.setRange("fitRange",minPT,maxPT);

	RooPlot* frame = varPT.frame(Title("PT"));
	RooPlot* frame2 = varPT2.frame(Title("PT2"));
	
	RooRealVar varData("varData","varData",0,5e5);
	RooRealVar varData2("varData2","varData2",0,5e5);
	
  
	string FileNames [3] = {"Chic0Out.txt","Chic1Out.txt","Chic2Out.txt"};
	float chi2s [3];
	float slopes [3];
	float slopesErr [3];
	
	

	RooRealVar varSlope("varSlope", "varSlope", 0.5,-1,1);
	RooRealVar varYield("varYield", "varYield",1e3,1e2,1e5);
	    	    
	RooGenericPdf pdfExp("pdfExp","pdfExp","TMath::Exp(-@1*@0)",RooArgList(varSlope,varPT));
	RooExtendPdf pdfModel("pdfModel", "pdfModel", pdfExp, varYield); 
	
	RooGenericPdf pdfExp2("pdfExp2","pdfExp2","TMath::Exp(-@1*@0)",RooArgList(varSlope,varPT2));
	RooExtendPdf pdfModel2("pdfModel2", "pdfModel2", pdfExp2, varYield);	
    

	
	TLegend *leg = new TLegend(0.65,0.73,0.86,0.87);
	leg->SetFillColor(kWhite);
	leg->SetLineColor(kWhite);
	
	switch(Year)
	{
// 	  case 12:
// 	    Float_t BinsEdges[10]={2,7,8,11,19};
// 	    break;
// 	  case 11:    
// 	    Float_t BinsEdges[10]={2,7.25,8.25,11.25,19};
// 	    break;
	  case 12:
	    Float_t BinsEdges[10]={2,7.5,11,19};
	    break;
	  case 11:    
	    Float_t BinsEdges[10]={2,7.5,11,19};
	    break;
	}
	
	TCanvas* canvC = new TCanvas("canvC", "canvC", 800, 700);
	canvC->SetLogy();
	
	for(int ichic=0;ichic<3;ichic++)
	{
	  
	    RooDataSet DataSetXYErr("DataSetXYErr","DataSetXYErr",RooArgSet(varPT,varData),
				StoreAsymError(RooArgSet(varPT,varData)));
	    RooDataSet DataSetXYErrDR("DataSetXYErrDR","DataSetXYErrDR",RooArgSet(varPT,varData),
				StoreAsymError(RooArgSet(varPT,varData)));
	    RooDataSet DataSetYErr("DataSetYErr","DataSetYErr",RooArgSet(varPT2,varData2),
				StoreAsymError(RooArgSet(varData2)));
	  
	    Float_t xx[1000], yy[1000], ey1[1000], ey2[1000], xx[1000], yy[1000], ex1[1000], ex2[1000], ex[1000];
	    FILE *fp = fopen(FileNames[ichic].c_str(), "r");
	    
	    int NPoints;
	    fscanf(fp,"%i",&NPoints);
	    
	    for(int i=0;i<NPoints;i++) 
	    {
		fscanf(fp,"%f",&xx[i]);
		fscanf(fp,"%f",&yy[i]);
		fscanf(fp,"%f",&ex1[i]);
		fscanf(fp,"%f",&ex2[i]);
		fscanf(fp,"%f",&ey1[i]);
		fscanf(fp,"%f",&ey2[i]);
		varPT = xx[i];
		ex[i]=0;
 		varPT.setAsymError(-ex1[i],ex2[i]);
//		varPT.setAsymError(-ex[i],ex[i]);
		
		varPT2 =  xx[i];
		varPTDr = xx[i]+0.2*(ichic-1);

		printf("%f %f %f %f %f %f\n",xx[i],yy[i],ex1[i],ex2[i],ey1[i],ey2[i]);
		
		varData = yy[i];
		varData.setAsymError(-ey1[i],ey2[i]);
		
		varData2 = yy[i];
		varData2.setAsymError(-ey1[i],ey2[i]);
		
		DataSetXYErr.add(RooArgSet(varPT,varData));
		
		varPT = xx[i]+0.2*(ichic-1);
		DataSetXYErrDR.add(RooArgSet(varPT,varData));
		DataSetYErr.add(RooArgSet(varPT2,varData2));
	    }
	    
	    fclose(fp);

	    

	    RooBinning abins(2,19);
	    for(int i=0;i<(NPoints-1);i++)
	      abins.addBoundary(BinsEdges[i+1]);
	    
	    
	    
	    varPT.setBinning(abins);
	   
	    DataSetXYErrDR.plotOnXY(frame,YVar(varData),LineColor(ichic+2),MarkerColor(ichic+2));
// 	    DataSetXYErr.plotOnXY(frame,YVar(varData),LineColor(ichic+2),MarkerColor(ichic+2));
	    DataSetYErr.plotOnXY(frame2,YVar(varData2),LineColor(ichic+2),MarkerColor(ichic+2));
	    

	    varSlope.setVal(0.5);
	    varYield.setVal(1e3);
	    pdfModel.chi2FitTo(DataSetXYErr,YVar(varData),Strategy(2),PrintLevel(0),Save(true),Minos(true));
	    
	    
	    varSlope.setConstant(kFALSE);
	    varYield.setConstant(kFALSE);

	    //pdfModel2.chi2FitTo(DataSetYErr,YVar(varData2),Strategy(1),PrintLevel(0),Save(true), Minos(true));

	    
	    TGraphAsymmErrors* cGr = new TGraphAsymmErrors(NPoints,xx,yy,ex,ex,ey1,ey2);
	    TF1 *fitFunc = new TF1("fitFunc","[0]*exp(-[1]*x)", 0, 20);
	    cGr->SetMinimum(1e-1);

	    
	    float nsteps = 100;
	    float slmin = 0.420,slmax=0.520,slst=(slmax-slmin)/nsteps;
	    float ymin = 2.5e3, ymax = 3e3;yst=(ymax-ymin)/nsteps;
	    
	    float optSl, optY,optchi2=999;
	    if(ichic==1 && Year==11)
	    {
// 	      for(int ii=0;ii<nsteps;ii++)
// 		for(int jj=0;jj<nsteps;jj++)
// 		{
// 		  fitFunc->FixParameter(1,slmin+ii*slst);
// 		  fitFunc->FixParameter(0,ymin+jj*yst);
// 		  cGr->Fit(fitFunc);
// 		  if(fitFunc->GetChisquare()< optchi2)
// 		  {
// 		    optchi2=fitFunc->GetChisquare();
// 		    optSl = slmin+ii*slst;
// 		    optY = ymin+jj*yst;
// 		  }
// 		}
// 	      varYield.setVal(optY);
// 	      varSlope.setVal(optSl);
// 	      cout<<"AAAA  "<<optSl<<"    "<<optY<<endl;
	      
	      fitFunc->FixParameter(1,varSlope.getVal());
	      fitFunc->FixParameter(0,varYield.getVal());
	      
	    }
	    else
	    {
	      fitFunc->FixParameter(1,varSlope.getVal());
	      fitFunc->FixParameter(0,varYield.getVal());
	    }

	    pdfModel.plotOn(frame,Normalization(pdfModel.expectedEvents(pdfModel.getVariables())),
			    LineColor(ichic+2),LineWidth(2));
	    pdfModel2.plotOn(frame2,Normalization(pdfModel2.expectedEvents(pdfModel2.getVariables())),Binning(abins),
			    LineColor(ichic+2),LineWidth(2));	

	    cGr->Fit(fitFunc);
	    

	    cGr->DrawClone("AP");
	
	    
// 	    varYield.setVal(fitFunc->GetParameter(0));
 //	    varSlope.setVal(fitFunc->GetParameter(1));
	    
	     //fitFunc->SetParameter(0,varYield.getVal());
	    
	    float chi2  = fitFunc->GetChisquare()/*/fitFunc->GetNDF()*/; 
	    float Slope = varSlope.getVal();
	    float SlopeErr = varSlope.getError();
	    
	    
	    chi2s[ichic] = chi2;
	    slopes[ichic] = Slope;
	    slopesErr[ichic] = SlopeErr;
	
	    DataSetXYErr.Clear();
	    DataSetYErr.Clear();
	    
	    delete cGr;
	    delete fitFunc;		    
	    
	    
	}

	TCanvas* canvA = new TCanvas("canvA", "canvA", 800, 700);
	canvA->SetLogy();

        frame->SetMinimum(0.1);
        frame->Draw();

	TCanvas* canvB = new TCanvas("canvB", "canvB", 800, 700);
	canvB->SetLogy();	

 	frame2->SetMinimum(0.1);
// 	pdfModel2.plotOn(frame2,Normalization(pdfModel2.expectedEvents(pdfModel2.getVariables())),Binning(abins));
	frame2->Draw();

	for(int ichic=0;ichic<3;ichic++)
	  cout<<"chic"<<ichic<<": slope="<<slopes[ichic]<<" +- "<<slopesErr[ichic]<<"  chi2="<<chi2s[ichic]<<endl;
	
}


